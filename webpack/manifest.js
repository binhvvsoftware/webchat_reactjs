const path = require('path');

module.exports = {
    filename: 'manifest.json',
    short_name: 'Kyuun',
    name: 'Kyuun Web',
    inject: true,
    ios: true,
    description: 'Kyuun Web!',
    background_color: '#ec008c',
    theme_color: '#ec008c',
    start_url: '/',
    crossorigin: 'use-credentials', // can be null, use-credentials or anonymous
    icons: [
        {
            src: path.resolve(__dirname, '../public/logo.png'),
            sizes: [96, 128, 192, 256, 384, 512], // multiple sizes
        },
        {
            src: path.resolve(__dirname, '../public/logo.png'),
            size: '1024x1024', // you can also use the specifications pattern
        },
    ],
};
