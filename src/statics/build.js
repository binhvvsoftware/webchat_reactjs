/* eslint-disable */
const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
// you can pass the parameter in the command line. e.g. node static_server.js 3000
const port = process.argv[2] || 9000;
// maps file extention to MIME types
const mimeType = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
    '.eot': 'appliaction/vnd.ms-fontobject',
    '.ttf': 'aplication/font-sfnt'
};
http.createServer(function (req, res) {
    console.log(`${req.method} ${req.url}`);
    // parse URL
    const parsedUrl = url.parse(req.url);
    // extract URL path
    // Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
    // e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
    // by limiting the path to current directory only
    const sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');
    let pathname = path.join(__dirname, sanitizePath);
    console.log(pathname);
    fs.exists(pathname, function (exist) {
        if(!exist) {
            // if the file is not found, return 404
            res.statusCode = 404;
            res.end(`File ${pathname} not found!`);
            return;
        }
        // if is a directory, then look for index.html
        if (!fs.statSync(pathname).isDirectory() && fs.statSync(pathname).isFile()) {
            // read file from file system
            fs.readFile(pathname, function(err, data){
                if(err){
                    res.statusCode = 500;
                    res.end(`Error getting the file: ${err}.`);
                } else {
                    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                    const ext = path.parse(pathname).ext;
                    // if the file is found, set Content-type and send data
                    res.setHeader('Content-type', mimeType[ext] || 'text/plain' );
                    res.end(data);
                }
            });
        } else {
            const pathIcons = path.join(__dirname, './icons');
            const pathImages = path.join(__dirname, './images');
            const icons = fs.readdirSync(pathIcons);
            const images = fs.readdirSync(pathImages);
            console.log(icons);
            console.log(images);
            console.log(2);

            res.setHeader('Content-type', 'text/html' );
            const html = `
<style>
    .container {
        background: rgba(0, 0, 0, 0.35);
        max-width: 1200px;
        margin: 0 auto;
    }
    .title {
        padding: 15px 0;
    }
    
    .images, .icons {
        clear: both;
    }
    
    .images:after, .icons:after {
        content: '';
        clear: both;
        width: 100%;
        display: block;
    }
    
    .icons__item {
        float: left;
        width: 20%;
        margin-bottom: 15px;
        padding: 5px;
    }
    
    .images__item {
        float: left;
        width: 25%;
        margin-bottom: 15px;
        padding: 5px;
    }
    
    .container img {
        max-width: 100%;
        display: block;
        margin: 0 auto;
    }
    .icons__item p, .images__item p {
        font-size: 10px;
        text-align: center;
    }
    
</style>


<div class="container">
    <h3 class="title">Icons</h3>
    <div class="icons">
        ${icons.map(item =>  `<div class="icons__item copy" data-clipboard-text="${item}"><img src="/icons/${item}" alt="${item}"><p>${item}</p></div>`).join('\n')}
    </div>
    <h3 class="title">Images</h3>

    <div class="images">
        ${images.map(item => `<div class="images__item copy" data-clipboard-text="${item}"><img src="/images/${item}" alt="${item}"><p>${item}</p></div>`).join('\n')}        
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script>
var clipboard = new ClipboardJS('.copy');
clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);

    e.clearSelection();
});

</script>
`;
            res.end(html);
        }


    });
}).listen(parseInt(port));
console.log(`Server listening on port ${port}`);













