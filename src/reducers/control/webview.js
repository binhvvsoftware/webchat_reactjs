import ActionTypes from '../../constants/action-webview';

const initialState = {
    isOpenWebView: false,
    webBackButton: false,
    webViewTitle: ' ',
    webViewUrl: '',
};

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.OPEN_WEB_VIEW:
            return Object.assign({}, state, {
                isOpenWebView: true,
                webBackButton: !!action.payload.backButton,
                webViewTitle: action.payload.title || ' ',
                webViewUrl: action.payload.url,
            })

        case ActionTypes.CLOSE_WEB_VIEW:
            return Object.assign({}, state, {
                isOpenWebView: false,
                webBackButton: false,
                webViewTitle: ' ',
                webViewUrl: '',
            });

        default:
            return state;
    }
};

export default pure;
