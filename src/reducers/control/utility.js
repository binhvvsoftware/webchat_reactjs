import ActionTypes from "../../constants/action-types";
import { PrimaryUtilityTypes } from "../../constants/types";

const initialState = {
    primaryChatOpen: false,
    primaryCallOpen: false,
    primaryVideoOpen: false,
    isOpenReportUserDialog: false,

}

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.CLICK_UTILITY_BUTTON:
            return Object.assign({}, state, {
                primaryChatOpen: action.payload.type === PrimaryUtilityTypes.CHAT,
                primaryCallOpen: action.payload.type === PrimaryUtilityTypes.VOICE,
                primaryVideoOpen: action.payload.type === PrimaryUtilityTypes.VIDEO,
            });
        case ActionTypes.CLICK_CHAT_BACK_BUTTON:
            return Object.assign({}, state, {
                primaryChatOpen: false,
            });
        case ActionTypes.TOGGLE_REPORT_USER_DIALOG:
            return {
                ...state,
                isOpenReportUserDialog: !state.isOpenReportUserDialog,
            }
        default:
            return state;
    }
}

export default pure;
