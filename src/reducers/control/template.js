const defaultState = {
    isLoading: false,
}


const ActionTypes = {
    TEMPLATE_TOGGLE_LOADING: "@@template-reducers/TEMPLATE_TOGGLE_LOADING",
}

export const toggleLoading = () => ({ type: ActionTypes.TEMPLATE_TOGGLE_LOADING })



const template = (state = defaultState, action) => {
    switch (action.type) {
        case ActionTypes.TEMPLATE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: !state.isLoading,
            }

        default:
            return state;
    }
}
export default template;