import ActionTypes from '../../constants/action-types';
import { PrimaryUtilityTypes } from '../../constants/types';
import MessageSocket from '../../entities/MessageSocket';

const initialState = {
    socket: null,
    isSocketNewMessageListenersLoadedFully: false,
    isShowButtonBack: false,
    me: { id: null, data: {} },
    you: { id: null, data: {} },
    pendingFileMessage: null,
    standbyMessage: '',
    hasNewMessageToScroll: false,
    templateId: null,
    newestMessage: null,
    arrivedRawMessage: null,
    sticker: null,
    emoji: {},
    currentLastMessage: null,
    isHeaderMenuOpened: false,
    isFooterPlusMenuOpened: false,
    isFooterEmoticonMenuOpened: false,
    isFooterKeyBoardOpened: true,
    isGiftPoolOpened: false,
    isOpenGalleryHistory: false,
    isOpenTemplate: false,
    serverChain: [],
    clientChain: [],
    clientAttentionNumbers: {},
    loadingStatus: { flag: false, message: null },
    isShowPopupBuyPoint: false,
    isTyping: false,
    hasLoadMore: true,
    rotateImage: 0,
    imageBox: {
        isOpened: false,
        message: null,
    },
    listConversation: [],
    filterConversation: 'all',
};

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.INIT_CHAT:
            return Object.assign({}, state, {
                pendingFileMessage: null,
                standbyMessage: '',
                you: { id: null, data: {} },
                imageBox: {
                    isOpened: false,
                    message: null,
                },
                isHeaderMenuOpened: false,
                isFooterPlusMenuOpened: false,
                isFooterEmoticonMenuOpened: false,
                isFooterKeyBoardOpened: true,
                isGiftPoolOpened: false,
                isOpenGalleryHistory: false,
                isOpenTemplate: false,
                serverChain: [],
                clientChain: [],
                loadingStatus: { flag: false, message: null },
                hasLoadMore: true,
                isTyping: false,
            });

        case ActionTypes.UPDATE_SHOW_HIDE_BUTTON_BACK:
            return Object.assign({}, state, {
                isShowButtonBack: action.payload.isShowButtonBack,
            });

        case ActionTypes.CHANGE_WHO_I_AM:
            return Object.assign({}, state, {
                me: { id: action.payload.id, data: action.payload.data },
            });

        case ActionTypes.CHANGE_WHO_YOU_ARE:
            return Object.assign({}, state, {
                you: { id: action.payload.id, data: action.payload.data },
            });

        case ActionTypes.UPDATE_ATTENTION_NUMBERS:
            return Object.assign({}, state, {
                clientAttentionNumbers: Object.assign({}, state.clientAttentionNumbers, action.payload.attentionNumbers),
            });

        case ActionTypes.SCROLL_TO_TOP_CHAT_POOL:
            return Object.assign({}, state, {
                loadingStatus: ((lastMessage) => ({
                    flag: true,
                    message: lastMessage,
                }))(action.payload.message),
            });

        case ActionTypes.OPEN_BUYING_POINT_PROMOTION:
            return Object.assign({}, state, {
                isShowPopupBuyPoint: true,
            });

        case ActionTypes.CLOSE_BUYING_POINT_PROMOTION:
            return Object.assign({}, state, {
                isShowPopupBuyPoint: false,
            });

        case ActionTypes.SCROLL_TO_NEWEST_CHAT_POOL:
            return Object.assign({}, state, {
                hasNewMessageToScroll: false,
            });

        case ActionTypes.UPDATE_CHAT_TYPING: {
            return Object.assign({}, state, {
                isTyping: action.payload.isTyping,
            });
        }

        case ActionTypes.SUBMIT_CHAT_MESSAGE:
            return Object.assign({}, state, {
                pendingFileMessage: action.payload.isFile ? action.payload.message : null,
                newestMessage: action.payload.message,
                hasNewMessageToScroll: true,
                standbyMessage: '',
                templateId: null,
                isShowPopupBuyPoint: false,
            });

        case ActionTypes.FIRE_NEW_MESSAGE_RAW_DATA:
            return Object.assign({}, state, {
                arrivedRawMessage: action.payload.messageDataObject,
            });

        case ActionTypes.CLOSE_GIFT_POOL:
            return Object.assign({}, state, {
                isGiftPoolOpened: false,
            });

        case ActionTypes.OPEN_GIFT_POOL:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isFooterPlusMenuOpened: false,
                isGiftPoolOpened: true,
                isShowPopupBuyPoint: false,
                you: action.payload.you,
            });

        case ActionTypes.TOGGLE_CHAT_HEADER_SUB_MENU:
            return Object.assign({}, state, {
                isHeaderMenuOpened: !state.isHeaderMenuOpened,
                isFooterPlusMenuOpened: false,
                isGiftPoolOpened: false,
            });

        case ActionTypes.CLOSE_CHAT_HEADER_SUB_MENU:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isFooterPlusMenuOpened: false,
                isGiftPoolOpened: false,
            });

        case ActionTypes.TOGGLE_FOOTER_PLUS_MENU:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isFooterPlusMenuOpened: !state.isFooterPlusMenuOpened,
                isGiftPoolOpened: false,
                isShowPopupBuyPoint: false,
            });

        case ActionTypes.COMMIT_RAW_MESSAGE_HANDLING:
            return Object.assign({}, state, {
                arrivedRawMessage: null,
            });

        case ActionTypes.COMMIT_FILE_SENDING:
            return Object.assign({}, state, {
                clientChain: ((message) => {
                    const chain = state.clientChain;
                    const pawn = [];
                    chain[message.id] = message;
                    Object.entries(chain).forEach(([key, value]) => {
                        pawn[key] = value;
                    });
                    return pawn;
                })(action.payload.message),
                pendingFileMessage: null,
                arrivedRawMessage: null,
            });

        case ActionTypes.LOAD_SOCKET_IO:
            if (state.socket) {
                return state;
            }
            return Object.assign({}, state, {
                socket: ((payload) => {
                    const { socket, authMessage } = payload;
                    authMessage.newSend();
                    return socket;
                })(action.payload),
            });

        case ActionTypes.COMPLETE_SOCKET_IO_NEW_MESSAGE_LISTENERS:
            return Object.assign({}, state, {
                isSocketNewMessageListenersLoadedFully: true,
            });

        case ActionTypes.DISCONNECT_SOCKET:
            return Object.assign({}, state, {
                isSocketNewMessageListenersLoadedFully: false,
                socket: null,
            });

        case ActionTypes.CLICK_CHAT_BACK_BUTTON:
            return Object.assign({}, state, {
                clientChain: [],
                isHeaderMenuOpened: false,
                isFooterPlusMenuOpened: false,
            });

        case ActionTypes.CLICK_UTILITY_BUTTON:
            if (!action.payload.socket) {
                return state;
            }
            return Object.assign({}, state, {
                socket: ((payload) => {
                    if (payload.type === PrimaryUtilityTypes.CHAT) {
                        const { socket } = payload;
                        const { authMessage } = payload;
                        authMessage.newSend();
                        return socket;
                    }
                    return null;
                })(action.payload),
            });

        case ActionTypes.CLOSE_CHAT_IMAGE_BOX:
            return Object.assign({}, state, {
                imageBox: {
                    isOpened: false,
                    message: null,
                },
            });

        case ActionTypes.OPEN_CHAT_IMAGE_BOX:
            return Object.assign({}, state, {
                imageBox: ((message) => ({
                    isOpened: true,
                    message,
                }))(action.payload.message),
            });

        case ActionTypes.PUSH_CHAT_POOL:
            if (action.payload.message.sender !== state.me.id && action.payload.message.sender !== state.you.id && !action.payload.history) {
                return state;
            }
            return Object.assign({}, state, {
                clientChain: ((message) => {
                    const chain = state.clientChain;
                    const pawn = [];
                    chain[message.id] = message;
                    Object.entries(chain).forEach(([key, value]) => {
                        pawn[key] = value;
                    });
                    return pawn;
                })(action.payload.message),
                newestMessage: action.payload.message,
                hasNewMessageToScroll: true,
            });

        case ActionTypes.UPDATE_CHAT_POOL:
            return Object.assign({}, state, {
                clientChain: ((message) => {
                    const chain = state.clientChain;
                    const pawn = [];
                    if (!chain[message.id]) {
                        return chain;
                    }
                    chain[message.id] = new MessageSocket(Object.assign({}, chain[message.id], message));
                    Object.entries(chain).forEach(([key, value]) => {
                        pawn[key] = value;
                    });

                    return pawn;
                })(action.payload.message),
            });

        case ActionTypes.CONCAT_CHAT_POOL:
            if (action.payload.data) {
                const pawn = [];
                const chain = state.clientChain;
                const currentLastMessage = action.payload.data.length === 0 ? null : action.payload.data[action.payload.data.length - 1];

                action.payload.data.concat().reverse().forEach(piece => {
                    pawn[piece.id] = piece;
                });

                Object.entries(chain).forEach(([key, value]) => {
                    pawn[key] = value;
                });

                return Object.assign({}, state, {
                    loadingStatus: { flag: false, message: null },
                    clientChain: pawn,
                    hasLoadMore: !!action.payload.data.length,
                    currentLastMessage,
                });
            }

            return state;

        case ActionTypes.LOAD_CHAT_POOL:
            if (action.payload.data) {
                const chain = [];
                action.payload.data.concat().reverse().forEach(piece => {
                    chain[piece.id] = piece;
                });

                const chainEntries = Object.entries(chain);
                const newestMessage = chainEntries.length > 0 ? chainEntries[chainEntries.length - 1][1] : null;
                const currentLastMessage = chainEntries.length > 0 ? chainEntries[0][1] : null;

                return Object.assign({}, state, {
                    serverChain: chain,
                    clientChain: chain,
                    newestMessage,
                    hasLoadMore: true,
                    currentLastMessage,
                });
            }
            return state;

        case ActionTypes.CHANGE_CHAT_MESSAGE_INPUT:
            return Object.assign({}, state, {
                standbyMessage: action.payload.message,
                templateId: state.standbyMessage !== action.payload.message ? null : state.templateId,
            });

        case ActionTypes.OPEN_GALLERY_HISTORY:
            return Object.assign({}, state, {
                isOpenGalleryHistory: true,
            });

        case ActionTypes.CLOSE_GALLERY_HISTORY:
            return Object.assign({}, state, {
                isOpenGalleryHistory: false,
            });

        case ActionTypes.OPEN_TEMPLATE:
            return Object.assign({}, state, {
                isOpenTemplate: true,
            });

        case ActionTypes.CLOSE_TEMPLATE:
            return Object.assign({}, state, {
                isOpenTemplate: false,
            });

        case ActionTypes.SELECTED_TEMPLATE:
            return Object.assign({}, state, {
                standbyMessage: action.payload.template_content,
                templateId: action.payload.template_id,
            });


        case ActionTypes.OPEN_FOOTER_PLUS_MENU:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isGiftPoolOpened: false,
                isFooterPlusMenuOpened: true,
                isFooterEmoticonMenuOpened: false,
                isFooterKeyBoardOpened: false,
                sticker: null,
            });

        case ActionTypes.CLOSE_FOOTER_PLUS_MENU:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isGiftPoolOpened: false,
                isFooterPlusMenuOpened: false,
                isFooterEmoticonMenuOpened: false,
                isFooterKeyBoardOpened: true,
            });

        case ActionTypes.OPEN_FOOTER_KEYBOARD:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isGiftPoolOpened: false,
                isFooterPlusMenuOpened: false,
                isFooterEmoticonMenuOpened: false,
                isFooterKeyBoardOpened: true,
                sticker: null,
            });

        case ActionTypes.OPEN_FOOTER_EMOJI:
            return Object.assign({}, state, {
                isHeaderMenuOpened: false,
                isGiftPoolOpened: false,
                isFooterPlusMenuOpened: false,
                isFooterEmoticonMenuOpened: true,
                isFooterKeyBoardOpened: false,
            });

        case ActionTypes.CLOSE_FOOTER_EMOJI:
            return Object.assign({}, state, {
                isFooterEmoticonMenuOpened: false,
            });

        case ActionTypes.SELECTED_EMOJI:
            return Object.assign({}, state, {
                emoji: action.payload.emoji,
            });

        case ActionTypes.OPEN_SELECTED_STICKER:
            return Object.assign({}, state, {
                sticker: action.payload.sticker,
            });

        case ActionTypes.CLOSE_SELECTED_STICKER:
            return Object.assign({}, state, {
                sticker: null,
            });

        case ActionTypes.UPDATE_CONVERSATION:
            return Object.assign({}, state, {
                listConversation: action.payload.data,
                filterConversation: action.payload.filter || state.filterConversation,
            });
        default:
            return state;
    }
};

export default pure;
