import ActionTypes from '../../constants/action-main';

const initialState = {
    freePage: null,
    isOpenSidebarMenu: false,
    isOpenChatConversation: false,
    notification: [],
    listUserSystem: [],
    isOpenBuyPoint: false,
};

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.OPEN_BUYPOINT:
            return Object.assign({}, state, {
                isOpenBuyPoint: true,
            });

        case ActionTypes.CLOSE_BUYPOINT:
            return Object.assign({}, state, {
                isOpenBuyPoint: false,
            });

        case ActionTypes.UPDATE_FREE_PAGE:
            return Object.assign({}, state, {
                freePage: action.payload.data,
            });

        case ActionTypes.OPEN_CHAT_CONVERSATION:
            return Object.assign({}, state, {
                isOpenChatConversation: true,
            });

        case ActionTypes.CLOSE_CHAT_CONVERSATION:
            return Object.assign({}, state, {
                isOpenChatConversation: false,
            });

        case ActionTypes.OPEN_SIDEBAR_MENU:
            return Object.assign({}, state, {
                isOpenSidebarMenu: true,
            });

        case ActionTypes.CLOSE_SIDEBAR_MENU:
            return Object.assign({}, state, {
                isOpenSidebarMenu: false,
            });

        case ActionTypes.PUSH_NOTIFICATION:
            return Object.assign({}, state, {
                notification: state.notification.concat(action.payload.data),
            });

        case ActionTypes.UPDATE_NOTIFICATION:
            return Object.assign({}, state, {
                notification: action.payload.data,
            });

        case ActionTypes.UPDATE_LIST_ACCOUNT_SYSTEM:
            return Object.assign({}, state, {
                listUserSystem: action.payload.data,
            });
        default:
            return state;
    }
};

export default pure;
