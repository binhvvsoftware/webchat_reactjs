import storage from "../../utils/storage";
import { NAME_SEARCH } from '../../constants/storage_constant';

const defaultState = {
    nameSearch: storage.get(NAME_SEARCH) || "",
}

const ActionTypes = {
    HOME_CHANGE_NAME_SEARCH: "@@home-reducers/HOME_CHANGE_NAME_SEARCH",
}

export const changeNameSearch = (name) => ({ type: ActionTypes.HOME_CHANGE_NAME_SEARCH, name })


const home = (state = defaultState, action) => {
    
    switch (action.type) {
        case ActionTypes.HOME_CHANGE_NAME_SEARCH:
            return {
                ...state,
                nameSearch: action.name,
            }
        default:
            return state
    }
}

export default home;