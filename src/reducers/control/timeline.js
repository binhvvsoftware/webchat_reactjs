const defaultState = {
    activeType: 1,
}


const ActionTypes = {
    TIMELINE_CHANGE_ACTIVE_TYPE: "@@timeline-reducers/TIMELINE_CHANGE_ACTIVE_TYPE",
}

export const changeActiveType = (value) => ({ type: ActionTypes.TIMELINE_CHANGE_ACTIVE_TYPE, value })



const timeline = (state = defaultState, action) => {
    switch (action.type) {
        case ActionTypes.TIMELINE_CHANGE_ACTIVE_TYPE:
            return {
                ...state,
                activeType: action.value,
            }

        default:
            return state;
    }
}
export default timeline;