import ActionTypes from '../../constants/action-favorite';

const initialState = {
    isLoadingFavorite: false,
};

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.OPEN_FAVORITE_LOADING:
            return Object.assign({}, state, {
                isLoadingFavorite: true,
            });

        case ActionTypes.CLOSE_FAVORITE_LOADING:
            return Object.assign({}, state, {
                isLoadingFavorite: false,
            });

        default:
            return state;
    }
};

export default pure;
