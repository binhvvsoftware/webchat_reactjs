const defaultState = {
    isOpenMenuSettingTop: false,
    isOpenDialogMemo: false,
}

const ActionTypes = {
    PROFILE_TOGGLE_MENU_SETTING_TOP: "@@profile-reducers/PROFILE_TOGGLE_MENU_SETTING_TOP",
    PROFILE_TOGGLE_DIALOg_MEMO: "@@profile-reducers/PROFILE_TOGGLE_DIALOg_MEMO",
}

export const toggleMenuSettingTop = (value) => ({ type: ActionTypes.PROFILE_TOGGLE_MENU_SETTING_TOP, value })
export const toggleDialogMemo = () => ({ type: ActionTypes.PROFILE_TOGGLE_DIALOg_MEMO })

const profile = (state = defaultState, action) => {
    switch (action.type) {
        case ActionTypes.PROFILE_TOGGLE_MENU_SETTING_TOP:
            return {
                ...state,
                isOpenMenuSettingTop:action.value,
            }
        case ActionTypes.PROFILE_TOGGLE_DIALOg_MEMO:
            return {
                ...state,
                isOpenDialogMemo: !state.isOpenDialogMemo,
            }
        default:
            return state
    }
}
export default profile;