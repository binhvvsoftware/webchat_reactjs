import ActionTypes from '../../constants/action-download';

const initialState = {
    downloadURL: '',
    fileName: '',
    isOpenDownload: false,
};

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.OPEN_DOWNLOAD:
            return Object.assign({}, state, {
                isOpenDownload: true,
                fileName: action.payload.name,
                downloadURL: action.payload.url,
            });

        case ActionTypes.CLOSE_DOWNLOAD:
            return Object.assign({}, state, {
                isOpenDownload: false,
                downloadURL: '',
                fileName: '',
            });

        default:
            return state;
    }
};

export default pure;
