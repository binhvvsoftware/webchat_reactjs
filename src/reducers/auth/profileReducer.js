import {
    UPDATE_PROFILE_REQUEST,
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAILURE,
} from '../../constants/action_constant';
const initialState = require('./profileInitialState').default;

const reducer = (state = null, action) => {
    if (!state) return initialState;
    switch (action.type) {
        case UPDATE_PROFILE_REQUEST:
            return {...state, loading: true};
        case UPDATE_PROFILE_SUCCESS:
            return {...state, loading: false };
        case UPDATE_PROFILE_FAILURE:
            return {...state, loading: false };
        default:
            return state;
    }
};

export default reducer;