import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
} from '../../constants/action_constant';
const initialState = require('./loginInitialState').default;

const reducer = (state = null, action) => {
    if (!state) return initialState;
    switch (action.type) {
        case LOGIN_REQUEST:
            return {...state, loading: true};
        case LOGIN_SUCCESS:
            return {...state, loading: false };
        case LOGIN_FAILURE:
            return {...state, loading: false };
        default:
            return state;
    }
};

export default reducer;