import {
    FORGET_PASWORD_REQUEST,
    FORGET_PASWORD_SUCCESS,
    FORGET_PASWORD_FAILURE,
    FORGET_PASWORD_KEY_CHANGE,
} from '../../constants/action_constant';
const initialState = require('./forgetPasswordInitialState').default;

const reducer = (state = null, action) => {
    if (!state) return initialState;
    switch (action.type) {
        case FORGET_PASWORD_KEY_CHANGE:
            return {...state, email: action.email, message: ''};
        case FORGET_PASWORD_REQUEST:
            return {...state, loading: true};
        case FORGET_PASWORD_SUCCESS:
            return {...state, email: '', loading: false, message: action.message};
        case FORGET_PASWORD_FAILURE:
            return {...state, loading: false, message: action.message};
        default:
            return state;
    }
};

export default reducer;
