import ActionTypes from "../../constants/action-types";

const initialState = {
    me: {},
    you: {
        id: null,
        data: {},
    },
}

const pure = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.CHANGE_WHO_I_AM:
            return Object.assign({}, state, {
                me: {
                    id: action.payload.id,
                    data: action.payload.data,
                },
            });
        case ActionTypes.CHANGE_WHO_YOU_ARE:
            return Object.assign({}, state, {
                you: {
                    id: action.payload.id,
                    data: action.payload.data,
                },
            });
        default:
            return state;
    }
}

export default pure;


