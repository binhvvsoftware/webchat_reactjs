import { combineReducers } from 'redux';
import login from './auth/loginReducer';
import forgetPassword from './auth/forgetPasswordReducer';

/* Control reducers pseudo */
import utilityControl from './control/utility';
import chatControl from './control/chat';

import profile from './control/profile';
import timeline from './control/timeline';
import template from './control/template';
import home from './control/home';
import webView from './control/webview';
import favorite from './control/favorite';
import download from './control/download';
import main from './control/main';

const rootReducer = combineReducers({
    main,
    login,
    forgetPassword,
    profile,
    timeline,
    template,
    home,
    utilityControl,
    chatControl,
    webView,
    favorite,
    download,
});

export default rootReducer;
