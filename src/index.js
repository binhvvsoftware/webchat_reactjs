import React from 'react';
import ReactDOM from 'react-dom';
import moment from "moment";
import { Router } from 'react-router-dom';
import { composeWithDevTools } from 'redux-devtools-extension';
import './statics/scss/index.scss';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import reducer from './reducers';
import rootSaga from './sagas';
import RouterFile from './routers';
import history from './utils/history';
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true; // eslint-disable-line

/* Redux store
 * with saga middleware
 * HAVE TO: review
 */
// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
);

window.store = store;
window._ = require('lodash'); // eslint-disable-line
window.moment = moment;
sagaMiddleware.run(rootSaga);

require('./utils/mobile-detect');

// Render the application
ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <RouterFile location={document.location}/>
        </Router>
    </Provider>,
    document.getElementById('root'),
);
