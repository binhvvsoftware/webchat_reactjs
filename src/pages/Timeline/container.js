import React, { Component } from 'react';
import { connect } from "react-redux";
import './index.scss';
import PropTypes from "prop-types";
import debounce from 'lodash/debounce';
import { Link } from 'react-router-dom';
import { ActivityIndicator } from 'react-native-web';
import Popup from 'react-popup';
import storage from '../../utils/storage';
import CommentForm from "../BuzzDetail/comment-form";
import BuzzDetail from '../BuzzDetail/buzz';
import * as endpoint from '../../constants/endpoint_constant';
import MediaService from '../../services/media';
import TimelineService from '../../services/timeline';
import BannedWordService from '../../services/bannedWord';
import MainLayout from '../../components/MainLayout';
import { changeActiveType } from '../../reducers/control/timeline'
import { updateAttentionNumbers } from '../../actions/Chat';
import UserService from '../../services/user';
import { checkLoadMore, checkOrientationImage } from '../../utils/helpers';
import {
    NETWORK_ERROR,
    CANNOT_UPLOAD_YOUR_AVATAR_MESSAGE,
    BUZZ_SEGMENT_MYPOSTS,
    BUZZ_SEGMENT_FAVORITES_TITLE,
    ALL_TIMELINE,
} from '../../constants/messages';

class Timeline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: this.props.history.action === "PUSH" ? 1 : this.props.activeType,
            myTimeline: [],
            valueAddBuzz: '',
            uploadImage: null,
            bannedWord: [],
            hasLoadMore: true,
            loading: false,
            isFocus: false,
            isLoading: false,
        };
        this.handleCheckLoadMore = debounce(this.handleLoadMore, 350);
        this.textInput = null;
        this.setTextInputRef = element => {
            this.textInput = element;
        };
    }

    componentWillMount(){
        if ( this.props.history.action === "PUSH" && this.props.history.location && this.props.history.location.state && this.props.history.location.state.typeTimeline) {
            this.setState({
                isActive: 0,
            })
            this.props.changeActiveType(0);
        }
    }

    componentDidMount() {

        TimelineService.getBuzzTimeline(this.state.isActive)
            .then(response => {
                this.setState({
                    myTimeline: response.data,
                })
            });

        BannedWordService.getBannedWord()
            .then(response => {
                this.setState({
                    bannedWord: response.data.timeline,
                });
            })
        window.addEventListener('scroll', this.handleCheckLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleCheckLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (checkLoadMore(event)) {
            this.handleCallApi();
        }
    }

    handleCallApi = (target = this.state.isActive) => {
        const { myTimeline } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            TimelineService.getBuzzTimeline(target, myTimeline.length).then(response => {
                this.setState({
                    myTimeline: myTimeline.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    }

    onFocus = () => {
        this.handleValueBuzz();
        this.setState({
            isFocus: true,
        });
    }

    onBlur = () => {
        this.handleValueBuzz(true);
        this.setState({
            isFocus: false,
        });
    }

    handelClick(tab, targetLoad = null) {
        if (tab !== this.state.isActive || targetLoad === 'loading') {
            this.setState({
                isActive: tab,
                myTimeline: [],
                loading: false,
                hasLoadMore: true,
            }, () => {
                this.props.changeActiveType(tab)
                this.handleCallApi(this.state.isActive);
            });
        }

    }

    handleValueBuzz(isAddBuzz = false){
        const el = document.getElementById("submit-buzz");
        if (isAddBuzz) {
            el.style.opacity = "0";
            return;
        }
        if(this.textInput.value.length !== 0 || (this.state.uploadImage !== null && this.state.uploadImage.preview)) {
            el.style.opacity = "1";
        } else {
            el.style.opacity = "0.4";
        }
    }

    handleCheckBannedWord(value) {
        const checkBannedWord = [];
        const { bannedWord } = this.state;

        bannedWord.forEach(word => {
            if (value.indexOf(word) !== -1) {
                checkBannedWord.push(word);
            }
        })
        return checkBannedWord;
    }

    handleAddBuzz() {
        const { uploadImage } = this.state;
        const valueAddBuzz = this.textInput.value;
        const checkBannedWord = this.handleCheckBannedWord(valueAddBuzz);

        if (valueAddBuzz.length === 0 && !uploadImage) {
            return;
        }

        if (checkBannedWord.length !== 0) {
            Popup.alert(`("${checkBannedWord[0]}")は使えない単語です。再入力してください。`);
            return;
        }

        this.textInput.value = '';

        if (uploadImage) { // eslint-disable-line
            this.setState({
                isLoading: true,
            });
            this.fileInput.value = '';
            MediaService.uploadImageBase64(uploadImage.preview, endpoint.IMAGE_PUBLIC, uploadImage.orientation)
                .then(response => {
                    const data = {
                        api: 'add_buzz',
                        buzz_val: response.data.image_id,
                        buzz_type: 1,
                        cmt_val: valueAddBuzz,
                    };
                    TimelineService.addBuzzTimeline(data)
                        .then(() => {
                            this.handleAfterAddBuzz();
                            this.setState({
                                uploadImage: null,
                            })
                        })
                        .catch(() => Popup.alert(NETWORK_ERROR))
                        .then(() => {
                            this.setState({ isLoading: false });
                        });
                })
            return;
        }
        const data = {
            api: 'add_buzz',
            buzz_val: valueAddBuzz,
            buzz_type: 0,
        };

        TimelineService.addBuzzTimeline(data)
            .then(() => {
                this.handleAfterAddBuzz();
                this.handleValueBuzz(true);
            }).catch(() => Popup.alert(NETWORK_ERROR))
    }

    handleAfterAddBuzz() {
        const { isActive } = this.state;
        UserService.getAttentionNumber().then(responseAttention => {
            this.props.updateAttentionNumbers({ attentionNumbers: responseAttention.data });
        });

        if (isActive === 3) {
            this.setState({
                isActive: 1,
            }, () => {
                this.handelClick(1, "loading");
            })
        } else {
            this.handelClick(isActive, "loading");
        }
        this.setState({
            valueAddBuzz: '',
        })
    }

    uploadImage(e) {
        const image = e.target.files[0];
        if (image) {
            if (!/(\.png|\.gif|\.jpg|\.jpeg)$/i.test(image.name)) {
                Popup.alert(CANNOT_UPLOAD_YOUR_AVATAR_MESSAGE)
            } else {
                // e.target.value = '';
                this.setState({
                    isLoading: true,
                });
                checkOrientationImage(image, (preview, orientation) => {
                    this.setState({
                        isLoading: false,
                        uploadImage: {
                            preview,
                            orientation,
                        },
                    },() => {
                        this.handleValueBuzz();
                        this.textInput.focus();
                    });
                });
            }
        } else {
            Popup.alert(CANNOT_UPLOAD_YOUR_AVATAR_MESSAGE)
        }
    }

    handleAddComment = (comment, indexBuzz) => {
        const { myTimeline } = this.state;
        myTimeline[indexBuzz].comment = myTimeline[indexBuzz].comment.concat(comment);
        this.setState({
            myTimeline,
        })
    }

    handleFavorite = (buzz) => {
        const { myTimeline } = this.state;
        myTimeline.map((item) => {
            if (item.user_id === buzz.user_id) {
                item.isFavorite = buzz.isFavorite; // eslint-disable-line
            }
            return '';
        })
        this.setState({
            myTimeline,
        })
    }

    handleDeleteBuzz = (indexBuzz) => {
        const { myTimeline } = this.state;
        myTimeline[indexBuzz].isDeleted = true;
        this.setState({
            myTimeline,
        })
    }

    render() {
        const { isActive, uploadImage, myTimeline, isLoading } = this.state;
        const myUser = storage.getUserInfo();

        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list three">
                    <li onClick={this.handelClick.bind(this, 1)} className={`sub-header__item ${isActive === 1 ? 'is_active' : ''}`}>
                        {ALL_TIMELINE}
                    </li>
                    <li onClick={this.handelClick.bind(this, 3)} className={`sub-header__item ${isActive === 3 ? 'is_active' : ''}`}>
                        {BUZZ_SEGMENT_FAVORITES_TITLE}
                    </li>
                    <li onClick={this.handelClick.bind(this, 0)} className={`sub-header__item ${isActive === 0 ? 'is_active' : ''}`}>
                        {BUZZ_SEGMENT_MYPOSTS}
                    </li>
                </ul>
            </section>
        )

        const displayBuzz = myTimeline.map((item, index) => {
            const placeHolder = item.gender === myUser.gender ? `コメントを入力...` : `コメントを入力...(${item.comment_buzz_point}pts消贄します)`;
            const commentForm = item.cmt_num > 4 ? (<Link className="buzz-comment-form" to={{ pathname: `/buzz-detail/${item.buzz_id}` }}>
                <div className="buzz-comment-form__icon">
                    <span className="icon icon-comment-grey" />
                </div>
                <input
                    className="buzz-comment-form__input"
                    readOnly
                    placeholder={placeHolder}
                />
            </Link>) : <CommentForm indexBuzz={index} buzz={item} handleAddComment={this.handleAddComment} placeHolder={placeHolder} />


            return (<div className="buzz__item" key={index}>
                <BuzzDetail buzz={item} handleFavorite={this.handleFavorite} handleDeleteBuzz={this.handleDeleteBuzz} indexBuzz={index} />
                {!item.isDeleted ? commentForm : null}
            </div>)
        });

        return (
            <MainLayout className={`timeline-page has_sub-header ${this.state.isFocus ? `position-overlay-timeline` : ``}`} title="タイムライン" hasFooter={false} subHeader={subHeader}>
                {isLoading && <div className="buzz__loading" />}
                <div className="form-add-buzz">
                    <div className="upload-file-custom">
                        <input className="upload-file" type="file" ref={input => {this.fileInput=input}} onChange={(e) => this.uploadImage(e)} onFocus={this.onFocus} onBlur={this.onBlur} />
                        {uploadImage === null ? (
                            <button className="btn-upload-file" type="button"><span className="icon icon-camera-pink"></span></button>
                        ) : (
                            <div className="btn-upload-file img-uploaded">
                                <img className = {`rotate-${uploadImage.orientation}`} src={uploadImage.preview} alt='' />
                            </div>
                        )}
                    </div>
                    <div className="text-box"  >
                        <textarea
                            maxLength="500"
                            onFocus={this.onFocus}
                            onBlur={this.onBlur}
                            autoFocus={this.state.isFocus}   // eslint-disable-line
                            ref={this.setTextInputRef}
                            onChange={()=> this.handleValueBuzz()}
                            placeholder="今なにしてる?"
                        />
                        <div className="overlay" />
                        <div className="btn-add-buzz" id="submit-buzz">
                            <span className="icon icon-post-pink" onClick={() => this.handleAddBuzz()}></span>
                        </div>
                    </div>
                </div>
                <div className="buzz__list">{displayBuzz}</div>
                <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'} >
                    <ActivityIndicator animating color="black" size="large" />
                </div>
            </MainLayout>
        )
    }
}

Timeline.propTypes = {
    activeType: PropTypes.number.isRequired,
    changeActiveType: PropTypes.func.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    history : PropTypes.object,
};

const mapStateToProps = (state) => ({
    activeType: state.timeline.activeType,
});

const mapDispatchToProps = (dispatch) => ({
    changeActiveType: (value) => dispatch(changeActiveType(value)),
    updateAttentionNumbers: (payload) => dispatch(updateAttentionNumbers(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Timeline);
