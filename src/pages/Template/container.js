import React, {Component} from 'react';
import TemplateComponent from '../../components/Template';

class Template extends Component {
    render() {
        return (
            <TemplateComponent />
        );
    }
}

export default Template;
