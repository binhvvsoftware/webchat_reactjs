import React, { Component } from 'react';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import PropTypes from 'prop-types';
import isObject from 'lodash/isObject';
import Popup from 'react-popup';
import Slider from 'react-slick';
import { ActivityIndicator } from 'react-native-web';
import './index.scss';
import { Link } from 'react-router-dom';
import Image from '../../components/Image';
import * as constantsMainExitTime from '../../constants/main_exit_time_constant';
import BuzzDetail from '../BuzzDetail/buzz';
import ButtonDownload from '../../components/ButtonDownload';
import storage from '../../utils/storage';
import UtilityContainer from '../../components/container/UtilityContainer';
import * as constantsRegion from '../../constants/region_constant';
import { JOBS_MEN } from '../../constants/job_constant';
import MainLayout from '../../components/MainLayout';
import CommentForm from '../BuzzDetail/comment-form';
import MediaService from '../../services/media';
import TopMenuProfile from '../../components/TopMenuProfile';
import ReportImage from '../../components/ReportImage';
import Badge from '../../components/Badge';
import history, { getLocationHistory } from '../../utils/history';
import ProfileService from '../../services/profile';
import UserService from '../../services/user';
import TimelineService from '../../services/timeline';
import HeaderProfile from './header';
import { openBuyPoint } from '../../actions/index';
import Favorite from '../../components/Favorite';
import FullScreenModal from '../../components/FullScreenModal';
import Memo from '../../components/Memo';
import { timeAgo, checkOrientationImage, checkLoadMore } from '../../utils/helpers';
import {
    toggleMenuSettingTop,
} from '../../reducers/control/profile';
import {
    DELETE_PHOTO_MESSAGE,
    BUTTON_ALERT_DONT_DELETE_TITLE,
    ADD_TO_FAVORITES,
    BTN_TITLE_FAVORITED,
    UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
    UPLOAD_AVATAR_SUCCESSFULLY_TITLE,
    ACCOUNT_DEACTIVE_MESSAGE,
    ALERT_MESSAGE_UPLOAD_IMAGE_ERROR,
} from '../../constants/messages';

class Profile extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        const locationHistory = getLocationHistory();
        let userInfo;

        try {
            userInfo = locationHistory.current.state.user;

        } catch (e) {
            //
        }
        if (!isObject(userInfo)) {
            userInfo = {};
        }

        this.state = {
            buzzData: [],
            userInfo,
            imagePublicIndex: 0,
            imageStageIndex: 0,
            openDialogImagePublic: false,
            openDialogAllImagePublic: false,
            openDialogImageBackstage: false,
            displayImagePublicDetail: {},
            isLikeBuzz: false,
            indexGetBuzz: '',
            openBackStage: false,
            dataCheckUnlock: {},
            listImagePublic: [],
            listImageBackStage: [],
            isActive: 'profile',
            liked: false,
            clicked: false,
            isOpenMenu: true,
            hasLoadMore: true,
            loading: false,
            isFavorite: userInfo.isFavorite,
            statusPopupReportImage: false,
            countImage: true,
            isBuyPoint: false,
            loadUserProfile: !userInfo.user_id,
            bckstgTime: 0,
            memo: '',
            pointUser: 0,
            fileData: {},
            isMyProfile: false,
        };
        this.handleCheckLoadMore = debounce(this.handleLoadMore, 100);
    }

    componentDidMount = () => {
        const userId = this.props.match.params.id;
        const myProfile = storage.getUserInfo();
        if (userId === myProfile.user_id) {
            this.setState({
                isMyProfile: true,
            });
        }

        if (!userId) {
            Popup.create({
                content: ACCOUNT_DEACTIVE_MESSAGE,
                buttons: {
                    left: [{
                        text: 'はい',
                        className: 'ok',
                        action: () => {
                            Popup.close();
                            history.push('/home');
                        },
                    }],
                },
            });
            return;
        }

        this.getUserInfo(userId);
        this.handleCallApi(userId);
        window.scrollTo(0,0);
        window.addEventListener('scroll', this.handleCheckLoadMore, true);
        window.addEventListener('scroll', this.handleCheckKeepHeader, true);
    };

    componentWillUnmount = () => {
        this.isUnmounted = true;
        this.props.toggleMenuSettingTop(false);
        window.removeEventListener('scroll', this.handleCheckLoadMore, true);
        window.removeEventListener('scroll', this.handleCheckKeepHeader, true);
        document.body.classList.remove('keep-header-profile');
    };

    openDialogReportImage = () => {
        this.setState({
            statusPopupReportImage: true,
        });
    };

    onCloseReportImage = () => {
        this.setState({
            statusPopupReportImage: false,
        });
    };

    handleCheckKeepHeader = () => {
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        if (scrollTop >= document.getElementById('profile-content').offsetTop - 50) {
            document.body.classList.add('keep-header-profile');
        } else {
            document.body.classList.remove('keep-header-profile');
        }
    };

    handelClick(tab) {
        this.setState({
            isActive: tab,
        });
        const elm = document.getElementById(`${tab}-content`);
        if (elm) {
            const offsetTop = elm.offsetTop - (tab === 'profile' ? 55 : 145);
            window.scrollTo(0, offsetTop);
            setTimeout(this.handleCheckKeepHeader, 500);
        }
    }

    getUserInfo(userId) {
        UserService.getUserInfo(userId)
            .then(response => {
                const listImageBackStage = Array(response.data.bckstg_num).fill('');
                this.setState({
                    userInfo: response.data,
                    isFavorite: response.data.isFavorite,
                    memo: response.data.memo,
                    listImagePublic: response.data.lst_public_image,
                    listImageBackStage,
                });
            })
            .catch((error) => {
                if (error.code !== 3) {
                    Popup.create({
                        content: ACCOUNT_DEACTIVE_MESSAGE,
                        buttons: {
                            left: [{
                                text: 'はい',
                                className: 'ok',
                                action: () => {
                                    Popup.close();
                                    history.push('/home');
                                },
                            }],
                        },
                    });
                }
            })
            .then(() => {
                this.setState({
                    loadUserProfile: false,
                });
            });
    }

    handleCallApi = (userId) => {
        const { buzzData } = { ...this.state };
        if (this.state.loading || !this.state.hasLoadMore) {
            return;
        }
        this.setState({
            loading: true,
        }, () => {
            ProfileService.getBuzzUser(userId, buzzData.length).then(response => {
                this.setState({
                    buzzData: buzzData.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    };

    handleLoadMore = (event) => {
        if (this.state.openDialogImagePublic) {
            return;
        }
        if (checkLoadMore(event)) {
            this.handleCallApi(this.state.userInfo.user_id);
        }
    };

    handleOpenImagePublic = (image, indexPublic) => {
        const { buzzData } = this.state;
        if (image.buzz_id) {
            buzzData.map((item, index) => {
                if (item.buzz_id === image.buzz_id || item.img_id === image.buzz_id) {
                    this.setState({
                        displayImagePublicDetail: Object.assign({}, image, item),
                        indexGetBuzz: index,
                        isLikeBuzz: item.is_like === 1,
                    });
                }
                return '';
            });
            this.setState({ openDialogImagePublic: true });
            this.handleCloseAllImagePublic();
        }

        this.setState({
            imagePublicIndex: indexPublic,
            openDialogImageBackstage: false,
            buzzIdPublic: image.buzz_id,
            openDialogImagePublic: true,
        });

    };

    handleOpenDialogImageBackstage = (index) => {
        this.setState({
            openDialogImageBackstage: true,
            imageStageIndex: index,
        });
    };

    handleCloseDialogImageBackstage = () => {
        this.setState({
            openDialogImageBackstage: false,
        });
    };

    handleClose = () => {
        this.setState({
            openDialogImagePublic: false,
        });
    };

    handleCloseAfterReport = () => {
        this.handleClose();
        if (this.state.openBackStage) {
            this.handleUnlockBackStage();
        } else {
            window.location.reload();
        }
    };

    handleOpenAllImagePublic = () => {
        this.setState({
            openDialogAllImagePublic: true,
            openDialogImagePublic: false,
        });
    };

    handleCloseAllImagePublic = () => {
        this.setState({
            openDialogAllImagePublic: false,
        });
    };

    handleLikeBuzz = (buzzId) => {
        const data = {
            api: 'like_buzz',
            buzz_id: buzzId,
            like_type: this.state.isLikeBuzz ? 0 : 1,
        };
        this.setState({
            isLikeBuzz: !this.state.isLikeBuzz, // eslint-disable-line
        }, () => {
            ProfileService.likeBuzz(data).then(response => {
                this.handleAfterLikeBuzz(response);
            });
        });
    };

    handleAfterLikeBuzz = (response) => {
        if (response.data) {
            this.setState({
                displayImagePublicDetail: {
                    ...this.state.displayImagePublicDetail,// eslint-disable-line
                    is_like: 1,
                },
            }, () => {
                this.setDataInfo();
            });
        } else {
            this.setState({
                displayImagePublicDetail: {
                    ...this.state.displayImagePublicDetail,// eslint-disable-line
                    is_like: 0,
                },
            }, () => {
                this.setDataInfo();
            });
        }
    };

    setDataInfo = () => {
        const { buzzData, indexGetBuzz } = this.state;
        buzzData[indexGetBuzz] = this.state.displayImagePublicDetail;
        this.setState({
            buzzData,
        });
    };

    handleUnlockBackStage = () => {
        const { dataCheckUnlock, userInfo } = this.state;
        if (dataCheckUnlock.is_unlck === 1) {
            ProfileService.getListBackStage(userInfo.user_id)
                .then(response => {
                    this.setState({
                        listImageBackStage: response.data,
                    });
                });
        }
    };

    closeBuyPoint = () => {
        this.setState({
            isBuyPoint: false,
        });
    };

    openBackStage(userId) {
        if (this.state.isMyProfile) {
            return;
        }
        this.setState({
            openBackStage: true,
        });

        ProfileService.checkUnLock(userId)
            .then(response => {
                this.setState({
                    dataCheckUnlock: response.data,
                }, () => {
                    this.handleUnlockBackStage();
                });
            }).catch(err => err);
    }

    closeBackStage = () => {
        this.setState({
            openBackStage: false,
        });
    };

    handleUserUnlock(userId) {
        ProfileService.unLockBackStage(userId)
            .then(response => {
                this.setState({
                    dataCheckUnlock: {
                        ...this.state.dataCheckUnlock, // eslint-disable-line
                        is_unlck: 1,
                        point: response.data.point,
                    },
                }, () => {
                    this.handleUnlockBackStage();
                });
            })
            .catch(err => {
                if (err.code === 70) {
                    this.props.openBuyPoint();
                }
            });
    }

    handleRouter = () => {
        const location = getLocationHistory();
        if (/^\/(chat|profile|buzz-detail)\//.test(location.previous.pathname)) {
            return history.push('/home');
        }

        try {
            const propsDataUser = this.props.location.state;
            if (propsDataUser) {
                return history.push('/home');
            }
        } catch (e) {
            //
        }
        return history.checkAndBack();
    };

    handleChangeUser = (indexUser) => {
        try {
            const { dataUser } = this.props.location.state;
            const user = dataUser[indexUser];
            this.setState({
                buzzData: [],
                hasLoadMore: true,
                loading: false,
            }, () => {
                this.handleCallApi(user.user_id);
            });
            this.getUserInfo(user.user_id);
            history.push(user.profile_url, { indexUser, dataUser, user });
        } catch (e) {
            //
        }
    };

    confirmDelete = (buzzId) => {
        Popup.create({
            content: DELETE_PHOTO_MESSAGE,
            buttons: {
                left: [{
                    text: BUTTON_ALERT_DONT_DELETE_TITLE,
                    className: 'ok custom-popup-profile',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: 'はい',
                    className: 'ok',
                    action: () => {
                        this.deleteBuzzImage(buzzId);
                        Popup.close();
                    },
                }],
            },
        });
    };

    deleteBuzzImage = (buzzId) => {
        this.setState({
            loadUserProfile: true,
        }, () => {
            TimelineService.deleteBuzz(buzzId)
                .then(() => {
                    this.setState({
                        loadUserProfile: false,
                    });
                    this.setBuzzAfterDelete();
                });
        });
    };

    setBuzzAfterDelete = () => {
        const { imagePublicIndex, listImagePublic } = this.state;
        listImagePublic.splice(imagePublicIndex, 1);
        this.setState({
            listImagePublic,
            openDialogImagePublic: false,
        });
    };

    handleClickAvatar = (avaId, isMyAvatar) => {
        if (!avaId && !isMyAvatar) {
            return;
        }

        if (!avaId && isMyAvatar) {
            this.fileInput.click();
        }

        this.state.listImagePublic.map((image, index) => {
            if (image.img_id === avaId) {
                this.handleOpenImagePublic(image, index);
            }
            return '';
        });
    };

    handleFileChange = (event) => {
        const file = event.target.files[0];

        if (!file || (file && !/(\.png|\.gif|\.jpg|\.jpeg)$/i.test(file.name))) {
            Popup.alert(ALERT_MESSAGE_UPLOAD_IMAGE_ERROR);
            return;
        }

        this.setState({ loadUserProfile: true });
        checkOrientationImage(file, (imgBase64, orientation) => {
            this.setState({ fileData: { preview: imgBase64, orientation } }, () => {
                this.handleSubmitAvatar(this.state.fileData.preview, this.state.fileData.orientation);
            });
        });
    };

    handleSubmitAvatar = (preview, orientation) => {
        const userInfo = this.state;
        let isUploadImage = true;

        if (!preview) {
            return;
        }
        MediaService.uploadImageBase64(preview, 3, orientation)
            .then(res => {
                userInfo.ava_id = res.data.image_id;
                isUploadImage = res.data.isApprove;
                if (isUploadImage) {
                    this.setState({
                        userInfo,
                        avatarId: res.data.image_id,
                    });
                } else {
                    Popup.create({
                        title: UPLOAD_AVATAR_SUCCESSFULLY_TITLE,
                        content: UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
                        buttons: {
                            left: [{
                                text: 'はい',
                                className: 'ok',
                                action: () => {
                                    Popup.close();
                                },
                            }],
                        },
                    });
                }
            })
            .then(() => this.setState({ loadUserProfile: false }));
    };

    handleDeleteBuzz = (indexBuzz) => {
        const { buzzData } = this.state;
        buzzData[indexBuzz].isDeleted = true;
        this.setState({
            buzzData,
        });
    };

    handleFavorite = (buzz) => {
        const { buzzData, userInfo } = this.state;
        buzzData.map((item) => {
            if (item.user_id === buzz.user_id) {
                item.isFavorite = buzz.isFavorite; // eslint-disable-line
            }
            return '';
        });

        let { isFavorite } = this.state;
        if (buzz.user_id === userInfo.user_id) {
            userInfo.isFavorite = buzz.isFavorite;
            isFavorite = buzz.isFavorite; // eslint-disable-line
        }
        this.setState({
            buzzData,
            isFavorite,
            userInfo,
        });
    };

    handleAddComment = (comment, indexBuzz) => {
        const { buzzData } = this.state;
        buzzData[indexBuzz].comment = buzzData[indexBuzz].comment.concat(comment);
        this.setState({
            buzzData,
        });
    };

    handleUpdateMemo = (memo) => {
        this.setState({ memo });
    };

    handleChangeSliderPublicIndex = (newIndex) => {
        const { buzzData,listImagePublic } = this.state;
        const newImage = listImagePublic[newIndex];
        buzzData.map((buzz) => {
            if (newImage.buzz_id === buzz.buzz_id) {
                this.setState({
                    displayImagePublicDetail: buzz,
                })
            }
            return '';
        })

        this.setState({
            imagePublicIndex : newIndex,
        })
    };

    handleChangeSliderStageIndex = (newIndex) => {
        this.setState({
            imageStageIndex: newIndex,
        });
    };

    render() {
        const {
            isActive,
            userInfo,
            imagePublicIndex,
            imageStageIndex,
            displayImagePublicDetail,
            dataCheckUnlock,
            listImageBackStage,
            buzzData,
            loadUserProfile,
            listImagePublic,
        } = this.state;
        const propsDataUser = this.props.location.state; // eslint-disable-line
        const myUser = storage.getUserInfo();
        const bckstgTime = myUser.bckstg_time || this.state.bckstgTime;
        let indexUser = '';
        let dataUser = '';
        const {
            isOpenMenuSettingTop,
        } = this.props

        if (propsDataUser) {
            indexUser = propsDataUser.indexUser; // eslint-disable-line
            dataUser = propsDataUser.dataUser; // eslint-disable-line
        }

        dataUser = Array.isArray(dataUser) ? dataUser : [];

        const isOnline = userInfo.is_online ? (
            <div className="user-status">
                <span className="online-icon"></span>オンライン
            </div>
        ) : (
            <div className="user-status">
                <span className="offline-icon"></span>{timeAgo(userInfo.last_login_time)}
            </div>
        );

        const displayBackStage = listImageBackStage.map((image, index) =>
            (
                <div
                    className="profile-gallery__item is_backstage"
                    key={ image + index + dataCheckUnlock.is_unlck }
                    onClick={ () => this.handleOpenDialogImageBackstage(index) }>
                    <Image isAvatar className="profile-gallery__image" imageId={image} alt="img" />
                </div>
            )
        );

        const sliderBackstage = listImageBackStage.map((image, index) =>
            (
                <div className="backstage-profile" key={ image + index + dataCheckUnlock.is_unlck }>
                    <Image imageId={image} alt={image} imgKind='2' isBackground/>
                </div>
            )
        );

        const displayImagePublic = [];
        const sliderImagePublic = [];
        const galleryProfile = [];

        listImagePublic.forEach((data, index) => {
            displayImagePublic.push(
                <div className="img-list__item" key={`${index}${data.buzz_id}`} onClick={() => this.handleOpenImagePublic(data, index)}>
                    <Image className="thumbnail-img" imageId={data.img_id} alt={data.buzz_id} imgKind="1" />
                </div>
            );

            sliderImagePublic.push(
                <div className="backstage-profile" key={index}>
                    <Image imageId={data.img_id} alt={data.buzz_id} imgKind={2} isBackground/>
                </div>
            );

            galleryProfile.push(
                <div
                    className="profile-gallery__item"
                    key={ `${index}${data.buzz_id}` }
                    onClick={ () => this.handleOpenImagePublic(data, index) }>
                    <Image
                        className="profile-gallery__image"
                        isBackground
                        imageId={ data.img_id }
                        alt={ data.buzz_id }
                        imgKind="1"/>
                </div>
            );
        });

        const getJobUser = JOBS_MEN[userInfo.job];

        const displayBuzz = buzzData.map((item,index) => {
            const placeHolder = item.gender === myUser.gender ? `コメントを入力...` : `コメントを入力...(${item.comment_buzz_point}pts消贄します)`;
            const commentForm = item.cmt_num > 4 ? (<Link className="buzz-comment-form" to={{ pathname: `/buzz-detail/${item.buzz_id}` }}>
                <div className="buzz-comment-form__icon">
                    <span className="icon icon-comment-grey" />
                </div>
                <input
                    className="buzz-comment-form__input"
                    readOnly
                    placeholder={placeHolder}
                />
            </Link>) : <CommentForm indexBuzz={index} buzz={item} handleAddComment={this.handleAddComment} placeHolder={placeHolder} />


            return (<div className="buzz__item" key={item.buzz_id}>
                <BuzzDetail buzz={item} handleFavorite={this.handleFavorite} handleDeleteBuzz={this.handleDeleteBuzz} indexBuzz={index} />
                {!item.isDeleted ? commentForm : null}
            </div>)
        });


        const HeaderContent = <HeaderProfile
            getUserInfo={() => { this.getUserInfo(this.state.userInfo.user_id) }}
            imageId={userInfo.ava_id}
            alt={userInfo.user_name}
            isMyUser = {userInfo.user_id === myUser.user_id}
        />;

        const footerDialog = displayImagePublicDetail  ? (
            <div className="bottom-menu-option">
                <Link to={{ pathname: `/buzz-detail/${displayImagePublicDetail.buzz_id}` }} className='button-link-message bottom-menu-item'>
                    <div className="gallery-user-btn ">
                        <i className="icon icon-comment-ol" />
                        <span className="d-title">コメント {displayImagePublicDetail.cmt_num}</span>
                    </div>
                </Link>
                <button type="button" className="bottom-menu-item" onClick={() => this.handleLikeBuzz(displayImagePublicDetail.buzz_id)}>
                    <div className="gallery-user-btn ">
                        <i className="icon icon-fav-black" />
                        <span className="d-title">{displayImagePublicDetail.is_like ? "取り消す" : "いいね" }</span>
                    </div>
                </button>
                <div className="bottom-menu-item" onClick={() => this.handleClose()}>
                    <Image isAvatar imageId={userInfo.ava_id} alt="img" />
                </div>
                <ButtonDownload className="bottom-menu-item" imageId={displayImagePublicDetail.img_id} />
                <button type="button" className="bottom-menu-item" onClick={() => this.openDialogReportImage()} >
                    <div className="gallery-user-btn ">
                        <i className="icon icon-report" />
                        <span className="d-title">写稟の鞭告</span>
                    </div>
                </button>
            </div>
        ) : null ;

        const sliderPublicSettings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.handleChangeSliderPublicIndex,
            initialSlide: imagePublicIndex,
        };

        const sliderBackstageSettings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.handleChangeSliderStageIndex,
            initialSlide: imageStageIndex,
        };

        /* eslint-disable no-return-assign */

        return (
            <MainLayout
                className="profile-page"
                title={userInfo.user_name}
                hasFooter={false}
                hasSidebarLeft={false}
                hasBackButton
                backFunction={this.handleRouter}
                contentHeader={HeaderContent}
            >
                {loadUserProfile && <div className="profile-page__loading" />}
                <TopMenuProfile isOpen={isOpenMenuSettingTop} user={this.state.userInfo} onClose={() => { this.props.toggleMenuSettingTop(false) }} />
                <div className="profile-page__wrapper" key = {this.props.match.params.id}>
                    <div className="profile-page__header">
                        <div className='profile-page__cover'>
                            <div key={userInfo.ava_id} onClick={() => this.handleClickAvatar(userInfo.ava_id,userInfo.user_id === myUser.user_id)}>
                                <Image isAvatar imageId={userInfo.ava_id ? userInfo.ava_id : this.state.avatarId} alt={userInfo.ava_id} imgKind = {2}/>
                            </div>
                            <input type="file" style = {{display : 'none'}} name="avatar" accept="image/*" ref={input => { this.fileInput = input }} onChange={(e) => this.handleFileChange(e)} />
                            {!!propsDataUser && !!dataUser.length &&
                                <div className={`arrow-buttons ${indexUser === 0 ? 'arrow-end' : ''}`}>
                                    {
                                        indexUser !== 0 && <span onClick={() => this.handleChangeUser(indexUser - 1)}>
                                            <i className="icon icon-chevron-left-white"></i>
                                        </span>
                                    }
                                    {
                                        indexUser !== dataUser.length - 1 && <span onClick={() => this.handleChangeUser(indexUser + 1)} >
                                            <i className="icon icon-chevron-right-white"></i>
                                        </span>
                                    }
                                </div>
                            }

                            <div className="bottom">
                                <div className="info">
                                    <div className="info-left">
                                        {isOnline}
                                        <p>{userInfo.appeal_comment}</p>
                                    </div>
                                    <Favorite user={this.state.userInfo} isMyUser = {userInfo.user_id === myUser.user_id} favoriteCallback={this.handleFavorite} className="info-right" classNameActive="info-right">
                                        <span className={this.state.isFavorite ? 'icon icon-reactionActive' : 'icon icon-reaction'} />
                                        <span>{this.state.isFavorite ? BTN_TITLE_FAVORITED : ADD_TO_FAVORITES}</span>
                                    </Favorite>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="user-profile">
                        <div className="listUserImg">
                            <div className="img-list">
                                {displayImagePublic}
                            </div>
                            {userInfo.bckstg_num === 0 ? (
                                <div className="img-list__item private">
                                    <div>
                                        <span className="icon icon-private"></span>
                                        <span>ヒ三ツの写第</span>
                                        <span className="badge notify profile-notify">{userInfo.bckstg_num}</span>
                                    </div>
                                </div>) :
                                <div className="img-list__item private" onClick={() => this.openBackStage(userInfo.user_id)}>
                                    <div>
                                        <Badge className="notify profile-notify" number={userInfo.bckstg_num} />
                                        <span className="icon icon-private"></span>
                                        <span>ヒ三ツの写第</span>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className="userInfo text-center">
                            <p>{userInfo.user_name}<br />{userInfo.age}歳 {constantsRegion.AREA_REGION[userInfo.region]}</p>
                        </div>
                        <div>
                            <ul className="profile-tabs">
                                <li className={`profile-tabs__item ${isActive === 'profile' ? 'is_active' : null}`} onClick={this.handelClick.bind(this, 'profile')} >
                                    プロフィール
                                </li>
                                <li className={`profile-tabs__item ${isActive === 'timeline' ? 'is_active' : null}`} onClick={this.handelClick.bind(this, 'timeline')} >
                                    タイムライン
                                    {userInfo.buzz_number === 0 ? <span className="badge notify">{userInfo.buzz_number}</span> : <Badge className="notify" number={userInfo.buzz_number} />}
                                </li>
                            </ul>

                            <div className="tabs-content" id="profile-content">
                                <div className="form-row">
                                    <label htmlFor="label" className="col-6">職業</label>
                                    <div className="col-6 w-break">{userInfo.length !== 0 && userInfo.job !== -1 ? getJobUser : "未設定"}</div>
                                </div>

                                <div>
                                    <div className="form-row">
                                        <label htmlFor="label" className="col-6">夕イプの男性</label>
                                        <div className="col-6 w-break">{userInfo.type_of_man ? userInfo.type_of_man : "自由にご記入ください。"}</div>
                                    </div>
                                    <div className="form-row">
                                        <label htmlFor="label" className="col-6">チヤ一ムポイン卜</label>
                                        <div className="col-6 w-break">{userInfo.fetish ? userInfo.fetish : "未設定"}</div>
                                    </div>
                                    <div className="form-row">
                                        <label htmlFor="label" className="col-6">主な出没時間</label>
                                        <div className="col-6 w-break">{userInfo.join_hours ? constantsMainExitTime.MAIN_ENTRY_EXIT_TIMES[userInfo.join_hours] : "未設定"}</div>
                                    </div>
                                </div>

                                <div className="form-row about-me">
                                    <label htmlFor="label" className="col-12">自己紹介</label>
                                    <div className="col-12 w-break">{userInfo.abt ? userInfo.abt : "未設定"}</div>
                                </div>
                            </div>

                            <div className="timeline-page" id="timeline-content">
                                {buzzData.length !== 0 && displayBuzz}
                                <div className="buzz__not-data">{buzzData.length === 0 && "投稿がありません"}</div>
                                <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'} >
                                    <ActivityIndicator animating color="black" size="large" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <UtilityContainer friend={this.state.userInfo} />
                </div>

                <FullScreenModal
                    title={ `${imagePublicIndex + 1}/${listImagePublic.length}` }
                    isOpen={ this.state.openDialogImagePublic }
                    onClose={this.handleClose}
                    titleChildren={ <button className={this.state.countImage ? "btn-view-all" : 'hidden_element'} type="button" onClick={() => this.handleOpenAllImagePublic()}>全て見る</button>}>
                    {
                        this.state.openDialogImagePublic && (
                            <Slider { ...sliderPublicSettings } ref={slider => this.slider = slider}>
                                { sliderImagePublic }
                            </Slider>
                        )
                    }

                    { this.state.openDialogImagePublic && (
                        <footer className="k-footer">
                            {this.state.isMyProfile ? (
                                <button className="btn-delete-photo" type="button" onClick={loadUserProfile ? null : () => this.confirmDelete(this.state.buzzIdPublic)}>
                                    <i className="icon icon-recycle-grey" />
                                    <span className="d-block">写真を削除</span>
                                </button>
                            ) : footerDialog }
                        </footer>
                    )}
                </FullScreenModal>

                <FullScreenModal
                    title="ギャラリー"
                    isOpen={ this.state.openDialogAllImagePublic }
                    onClose={this.handleCloseAllImagePublic}
                >
                    <div className="profile-gallery">
                        { galleryProfile }
                    </div>
                </FullScreenModal>

                <FullScreenModal
                    className="profile-backstage"
                    title="ヒミツの写真"
                    isOpen={ this.state.openBackStage }
                    onClose={this.closeBackStage}
                >
                    <div className='text-title'>{userInfo.user_name}さんのヒミツの写真を<br />{bckstgTime} 時間だけ見ることができます!!</div>
                    <div className="profile-gallery">
                        {displayBackStage}
                    </div>
                    {dataCheckUnlock.is_unlck === 0 ? (
                        <button
                            className="btn-active btn-unlock"
                            type="button"
                            onClick={() => this.handleUserUnlock(userInfo.user_id)}
                        >
                            ロック解除：{dataCheckUnlock.bckstg_pri}pts
                        </button>
                    ) : ''}

                    <Image className="profile-backstage__background" isAvatar imageId={userInfo.ava_id} alt={userInfo.user_name} />
                    <div className="profile-gallery__background" />

                    <FullScreenModal
                        className="gallery"
                        fullScreen
                        isOpen={this.state.openDialogImageBackstage}
                        onClose={this.handleCloseDialogImageBackstage}
                        hasBackButton
                        title={`${(imageStageIndex + 1)}/${listImageBackStage.length}`}
                    >
                        {
                            this.state.openDialogImageBackstage ? (
                                <>
                                    <Slider { ...sliderBackstageSettings } ref={slider => this.slider = slider}>
                                        { sliderBackstage }
                                    </Slider>
                                    <div className="bottom-menu-option is_backstage">
                                        <ButtonDownload className="bottom-menu-item" imageId={listImageBackStage[imageStageIndex]} />
                                        <div className="bottom-menu-item" onClick={() => this.openDialogReportImage()} >
                                            <div className="gallery-user-btn ">
                                                <i className="icon icon-report" />
                                                <span className="d-title">写稟の鞭告</span>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            ) : ''
                        }
                    </FullScreenModal>
                </FullScreenModal>

                <Memo user={this.state.userInfo} content={this.state.memo} callBackUpdate={this.handleUpdateMemo} />

                {this.state.statusPopupReportImage ? (
                    <ReportImage
                        user={this.state.userInfo}
                        myImage={displayImagePublicDetail.img_id || listImageBackStage[imageStageIndex]}
                        onCloseReportImage={this.onCloseReportImage}
                        onCloseDialogImage={this.handleCloseAfterReport} />
                ) : ''}
            </MainLayout>
        );
    }
}

Profile.propTypes = {
    location: PropTypes.object,
    match: PropTypes.object,
    isOpenMenuSettingTop: PropTypes.bool.isRequired,
    toggleMenuSettingTop: PropTypes.func.isRequired,
    openBuyPoint: PropTypes.func.isRequired,
}

Profile.defaultProps = {
    location: {},
    match: {},
}

const mapStateToProps = (state) => ({
    isOpenMenuSettingTop: state.profile.isOpenMenuSettingTop,

});

const mapDispatchToProps = (dispatch) => ({
    toggleMenuSettingTop: (value) => dispatch(toggleMenuSettingTop(value)),
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile)

