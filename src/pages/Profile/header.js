import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import Image from '../../components/Image';
import {
    toggleMenuSettingTop,
    toggleDialogMemo,
} from '../../reducers/control/profile'

class HeaderProfile extends Component {

    handleToggleMemo = () => {
        if (this.props.isMyUser) {
            return;
        }
        this.props.getUserInfo();
        this.props.toggleDialogMemo();
    }

    render() {
        return (
            <>
                <span className="nav-bar__icon nav-bar__icon--left is_avatar" key={this.props.imageId}>
                    <Image isAvatar className="header-avatar" imageId={this.props.imageId} alt={this.props.alt} />
                </span>
                <span className="nav-bar__icon nav-bar__icon--right is_options" onClick={() => this.props.toggleMenuSettingTop(true)} key={1}>
                    <span className="icon icon-etc-menu" />
                </span>
                <span className="nav-bar__icon nav-bar__icon--right is_memo" onClick={this.handleToggleMemo} key={2}>
                    <span className="icon icon-memo" />
                </span>
            </>
        );
    }
}


HeaderProfile.propTypes = {
    toggleMenuSettingTop: PropTypes.func.isRequired,
    toggleDialogMemo: PropTypes.func.isRequired,
    getUserInfo: PropTypes.func.isRequired,
    imageId: PropTypes.string,
    alt: PropTypes.string,
    isMyUser: PropTypes.bool,
}

HeaderProfile.defaultProps = {
    isMyUser: false,
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
    toggleMenuSettingTop: (value) => dispatch(toggleMenuSettingTop(value)),
    toggleDialogMemo: () => dispatch(toggleDialogMemo()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderProfile)
