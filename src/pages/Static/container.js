

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../../components/Header';
import Loading from '../../components/Loading';
import HttpRequest from '../../utils/http-request';
import history from '../../utils/history';
import { ROOT_URL, STATIC_PAGE } from '../../constants/endpoint_constant';
import { termOfUser } from '../../services/webview';

class StaticPage extends Component {
    constructor(props) {
        super(props);
        let title;
        let pageType;

        switch (props.match.params.page) {
            case 'privacy-policy':
                title = 'プライバーシーポリシー';
                pageType = 1;
                break;
            case 'terms-of-use' :
                title = '特定商取引法に基づ く表示';
                pageType = null;
                break;
            case 'terms-of-service' :
                title = '利用規約';
                pageType = 0;
                break;
            default:
        }

        this.state = {
            title,
            pageType,
            content: '',
            loading: true,
        };
    }

    componentDidMount() {

        HttpRequest.post(ROOT_URL, {
            api: STATIC_PAGE,
            page_type: this.state.pageType,
        }).then((response) => {
            this.setState({
                loading: false,
                content: response.data,
            });
        }).catch(() => {
            this.setState({
                loading: false,
                content: '',
            });
        });
    }

    handleRouter() {
        history.checkAndBack();
    }

    render() {
        return (
            <div className="register">
                <Loading isLoading={this.state.loading} />
                <Header title= {this.state.title}>
                    <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.handleRouter()}>
                        <span className="icon icon-arrow-left" />
                    </span>
                </Header>

                <main className="k-main">
                    {this.state.pageType || this.state.pageType === 0 ? <div className="static-content" dangerouslySetInnerHTML={{ __html: this.state.content }} /> : (
                        <div style={{width : "100%"}}>
                            <iframe
                                src={termOfUser()}
                                title="Term of user"
                                id="TermOfUser"
                                sandbox="allow-same-origin allow-scripts allow-forms allow-pointer-lock allow-popups allow-top-navigation"
                            >
                            </iframe>
                        </div>
                    )}
                </main>
            </div>
        );
    }
}

StaticPage.propTypes = {
    match: PropTypes.object,
};

StaticPage.defaultProps = {
    match: {},
};

export default StaticPage;
