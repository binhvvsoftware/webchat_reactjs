import React, { Component } from 'react';
import Popup from 'react-popup';
import './index.scss';
import logo from '@images/logo.png';
import history from '../../utils/history';
import storage from '../../utils/storage';
import AuthService from '../../services/authentication';

class Splash extends Component {
    componentDidMount() {
        Popup.close();
        if (!storage.getToken()) {
            storage.destroy();
            history.push('/login');
            return;
        }

        AuthService.getInfo()
            .then(() => {
                history.push('/home');
            })
            .catch(() => {
                storage.destroy();
                history.push('/login');
            });
    }

    render() {
        return (
            <div className="splash-page">
                <div className="splash-page__loading" />
                <img className="splash-page__logo" src={ logo } alt=""/>
            </div>
        );
    }
}

export default Splash;
