import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Image from '../../components/Image';
import BuzzService from '../../services/timeline';
import {
    ALERT_BUTTON_YES_TITLE,
    ALERT_BUTTON_NO_TITLE,
    CONFIRM_TO_DELETE_BUZZ_TITLE,
    MESSAGE_DELETE_SUB_COMMENT,
} from '../../constants/messages';


class CommentDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subComment: props.subComment,
            isLoading: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            subComment: nextProps.subComment,
        });
    }

    handleDeleteSubComment (subCommentId){
        const { subComment } = this.state;
        const { buzzId, cmtId } = this.props;

        if (!subComment.isApprove || !subComment.canDelete) {
            return;
        }

        Popup.create({
            title: CONFIRM_TO_DELETE_BUZZ_TITLE,
            content: MESSAGE_DELETE_SUB_COMMENT,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_NO_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.setState({
                            isLoading: true,
                        });

                        BuzzService.deleteSubComment(buzzId, cmtId, subCommentId)
                            .then(() => {
                                subComment.isDeleted = true;
                                this.setState({ subComment });
                            })
                            .catch(e => e)
                            .then(() => {
                                this.setState({ isLoading: false });
                            });
                    },
                }],
            },
        });
    }

    render() {
        const { subComment, isLoading  } = this.state;

        return (
            !subComment.isDeleted &&
            <div className={ `buzz-comment__item ${subComment.isApprove ? '' : 'is_pending'}` }>
                { isLoading && <div className="buzz-comment__loading" /> }
                <div className="buzz-comment__header">
                    <Link className="buzz-comment__header-link" to={ { pathname: `/profile/${subComment.user_id}`, state: { user: subComment } } }>
                        <div className="user-avatar">
                            <Image isAvatar imageId={ subComment.ava_id }/>
                        </div>
                        <div className="user-info">
                            <p className="name">{ subComment.user_name }</p>
                            <span className="time">
                                <span className="icon icon-time" />
                                { subComment.commentTime }
                            </span>
                        </div>
                    </Link>

                    <div className="buzz-comment__header-reaction">
                        { subComment.canDelete && <span className="icon icon-delete ml_15" onClick={ () => this.handleDeleteSubComment(subComment.sub_comment_id) }/> }
                    </div>
                </div>

                <div className="buzz-comment__content">
                    { subComment.value }
                </div>
            </div>
        );
    }
}

CommentDetail.propTypes = {
    subComment: PropTypes.object,
    buzzId : PropTypes.string,
    cmtId : PropTypes.string,
}

CommentDetail.defaultProps = {
    subComment : [],
    buzzId : '',
    cmtId : '',
}

export default CommentDetail;
