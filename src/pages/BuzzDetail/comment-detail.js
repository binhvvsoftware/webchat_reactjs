import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Image from '../../components/Image';
import BuzzService from '../../services/timeline';
import SubComment from './subComment';
import {
    ALERT_BUTTON_YES_TITLE,
    ALERT_BUTTON_NO_TITLE,
    CONFIRM_TO_DELETE_BUZZ_TITLE,
    CONFIRM_TO_DELETE_BUZZ_MESSAGE,
} from '../../constants/messages';

class CommentDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: props.comment,
            isLoading: false,
            isAllSubComment : false,
            isSubComment : true,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comment: nextProps.comment,
        });
    }

    handleDeleteComment = () => {
        const { comment } = this.state;

        if (!comment.isApprove || !comment.canDelete) {
            return;
        }

        Popup.create({
            title: CONFIRM_TO_DELETE_BUZZ_TITLE,
            content: CONFIRM_TO_DELETE_BUZZ_MESSAGE,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_NO_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.setState({
                            isLoading: true,
                        });

                        BuzzService.deleteComment(comment.cmt_id, this.props.buzzId)
                            .then(() => {
                                comment.isDeleted = true;
                                this.setState({ comment });
                            })
                            .catch(e => e)
                            .then(() => {
                                this.setState({ isLoading: false });
                            });
                    },
                }],
            },
        });
    }

    getAllSubComment(cmtId){
        BuzzService.getListSubComment(this.props.buzzId,cmtId)
            .then (response => {
                const { comment } = this.state;
                comment.subComment = response.data;
                this.setState({
                    comment,
                    isAllSubComment : true,
                })
            })
    }

    addSubcomment(indexComment){
        this.props.subCommentBuzzDetail(indexComment);
    }

    render() {
        const { comment, isLoading  } = this.state;
        const { buzzId } = this.props;
        const loadSubComment = (
            <Link className="buzz-comment__load-sub" to={ { pathname: `/buzz-detail/${buzzId}`} }>
                過去の返信をすべて表示…
            </Link>
        );

        const btnReply = comment.showReply && !this.props.isBuzzDetail ? (<Link to={ { pathname: `/buzz-detail/${buzzId}`, state: { comment }, isFocus : true, isSubComment : true, indexComment : this.props.indexComment } }>
            <span className = "text-pink">返信</span>
        </Link>) : <div className = "text-pink" onClick= {() => this.addSubcomment(this.props.indexComment)}>返信</div>

        const subComment = comment.subComment.map((sub,index) =>
            <SubComment subComment={sub} key={index} buzzId = {buzzId} cmtId = {comment.cmt_id} isSubComment = {this.state.isSubComment}/>
        );


        return (
            !comment.isDeleted &&
            <div className={ `buzz-comment__item ${comment.isApprove ? '' : 'is_pending'}` }>
                { isLoading && <div className="buzz-comment__loading" /> }
                <div className="buzz-comment__header">
                    <Link className="buzz-comment__header-link" to={ { pathname: `/profile/${comment.user_id}`, state: { user: comment } } }>
                        <div className="user-avatar">
                            <Image isAvatar imageId={ comment.ava_id }/>
                        </div>
                        <div className="user-info">
                            <p className="name">{ comment.user_name }</p>
                            <div className="comment-time">
                                <span className="icon icon-time" />
                                <span className="text-comment">{ comment.commentTime }</span>
                            </div>
                        </div>
                    </Link>

                    <div className="buzz-comment__header-reaction">
                        { btnReply }
                        { comment.canDelete && <span className="icon icon-delete ml_15" onClick={  this.handleDeleteComment }/> }
                    </div>
                </div>

                <div className="buzz-comment__content">
                    <div>{ comment.cmt_val }</div>
                    {  !this.props.isBuzzDetail && comment.sub_comment_number !== 0 && loadSubComment }
                    { this.props.isBuzzDetail && (
                        <div className = "buzz-comment__subcomment">
                            {comment.sub_comment_number > 4 && !this.state.isAllSubComment && <div className = "text-pink" onClick = {() => this.getAllSubComment(comment.cmt_id)}>過去の返信をすべて表示...</div>}
                            { subComment }
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

CommentDetail.propTypes = {
    comment: PropTypes.object,
    isBuzzDetail: PropTypes.bool,
    buzzId: PropTypes.string,
    indexComment : PropTypes.number,
    subCommentBuzzDetail : PropTypes.func,
}

CommentDetail.defaultProps = {
    comment: {},
    isBuzzDetail: true,
    buzzId : '',
}

export default CommentDetail;
