import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Image from '../../components/Image';
import Favorite from '../../components/Favorite';
import CommentDetail from './comment-detail';
import BuzzService from '../../services/timeline';
import history from '../../utils/history';
import {
    ALERT_BUTTON_YES_TITLE,
    ALERT_BUTTON_NO_TITLE,
    CONFIRM_TO_DELETE_BUZZ_TITLE,
    CONFIRM_TO_DELETE_BUZZ_MESSAGE,
} from '../../constants/messages';
import './index.scss';

class Buzz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buzz: props.buzz,
            isLoading: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            buzz: nextProps.buzz,
        });
    }

    handleUserLike = () => {
        const { buzz } = this.state;

        if (!buzz.isApproveAll) {
            return;
        }

        const likeType = buzz.isLike ? 0 : 1;
        this.setState({
            isLoading: true,
        });

        BuzzService.like(buzz.buzz_id, likeType)
            .then(() => {
                buzz.isLike = !buzz.isLike;
                buzz.like_num += buzz.isLike ? 1 : -1;
                buzz.like_num = buzz.like_num < 0 ? 0 : buzz.like_num;

                this.setState({
                    buzz,
                });
            })
            .catch(e => e)
            .then(() => {
                this.setState({ isLoading: false });
            });
    };

    openDialogDetailImage(buzz) {
        const { isBuzzDetail } = this.props;

        if (isBuzzDetail) {
            this.props.openDialogDetailImage(buzz);
        }
    }

    replaceBuzzValue = (value) => {
        if (value.length === 0) {
            return '';
        }

        return value.split('\n').map((item) => <span>{ item }<br/></span>);
    }

    handleDelete = () => {
        const { buzz } = this.state;
        if (!buzz.isApprove) {
            return;
        }

        Popup.create({
            title: CONFIRM_TO_DELETE_BUZZ_TITLE,
            content: CONFIRM_TO_DELETE_BUZZ_MESSAGE,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_NO_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.setState({
                            isLoading: true,
                        });

                        BuzzService.deleteBuzz(buzz.buzz_id)
                            .then(() => {
                                buzz.isDeleted = true;
                                this.setState({
                                    buzz,
                                });
                            })
                            .catch(e => e)
                            .then(() => {
                                this.setState({ isLoading: false });
                                if (this.props.isBuzzDetail) {
                                    history.checkAndBack();
                                } else {
                                    this.props.handleDeleteBuzz(this.props.indexBuzz);
                                }
                            });
                    },
                }],
            },
        });
    };

    render() {
        const { buzz, isLoading } = this.state;
        const { isBuzzDetail, key } = this.props;

        if (!buzz || buzz.length === 0) {
            return '';
        }

        const comments = buzz.comment && buzz.comment.map((comment, index) => <CommentDetail
            subCommentBuzzDetail={ this.props.subCommentBuzzDetail } indexComment={ index } comment={ comment }
            buzzId={ buzz.buzz_id } isBuzzDetail={ this.props.isBuzzDetail } key={ index }/>);
        return (
            !buzz.isDeleted &&
            <div key={ `${key}${buzz.isFavorite}` }>
                { isLoading && <div className="buzz__loading"/> }
                <div className="buzz__item-header">
                    <Link className="buzz__item-header-link" to={{ pathname: `/profile/${buzz.user_id}`, state: { user: buzz } }}>
                        <div className="user-avatar">
                            <Image isAvatar imageId={ buzz.ava_id }/>
                        </div>
                        <div className="user-info">
                            <p className="user-info-name">{ buzz.user_name }</p>
                            <div className="time-position">
                                <span className="icon icon-time"/>
                                <span className='text-buzz-time'>{ buzz.buzzTime }</span>
                                <span className="icon icon-position"/>
                                <span className='text-position'>{ buzz.region_name }</span>
                            </div>
                        </div>
                    </Link>

                    <div className="buzz__item-header-reaction">
                        {
                            buzz.isAuthor ? (
                                <span className="icon icon-delete" onClick={ this.handleDelete }/>
                            ) : (
                                <Favorite className="icon icon-heart-ol-sm" classNameActive="icon icon-heart-sm" user={ buzz } favoriteCallback={ this.props.handleFavorite } key={ key }/>
                            )
                        }
                    </div>
                </div>

                <div className="buzz__content">
                    { buzz.isApprove && !isBuzzDetail ? (
                        <Link to={ { pathname: `/buzz-detail/${buzz.buzz_id}` } } className='link-content-buzz'>
                            <div>
                                <div
                                    className={ `buzz__status ${buzz.isApproveAll ? '' : 'is_pending'}` }>{ buzz.buzz_type === 0 ? this.replaceBuzzValue(buzz.buzz_val) : this.replaceBuzzValue(buzz.caption) }</div>
                                { buzz.buzz_type === 1 ? (
                                    <div
                                        className={ `buzz__image ${buzz.isApprove ? '' : 'is_pending'} type_${buzz.buzzType}` }
                                        onClick={ () => this.openDialogDetailImage(buzz) }>
                                        {
                                            !buzz.isApprove && buzz.buzzType === 'image' &&
                                            <div className="buzz__text-hint">
                                                この画像はスタッフによって承認された後、<br/>
                                                他のユーザーへ公開されます。
                                            </div>
                                        }
                                        <Image imageId={ buzz.buzz_val } isBackground imgKind="2"/>
                                    </div>) : ''
                                }
                            </div>
                        </Link>
                    ) : (
                        <div>
                            <div
                                className={ `buzz__status ${buzz.isApproveAll ? '' : 'is_pending'}` }>{ buzz.buzz_type === 0 ? this.replaceBuzzValue(buzz.buzz_val) : this.replaceBuzzValue(buzz.caption) }</div>
                            { buzz.buzz_type === 1 ? (
                                <div
                                    className={ `buzz__image ${buzz.isApprove ? '' : 'is_pending'} type_${buzz.buzzType}` }
                                    onClick={ () => this.openDialogDetailImage(buzz) }>
                                    {
                                        !buzz.isApprove && buzz.buzzType === 'image' &&
                                        <div className="buzz__text-hint">
                                            この画像はスタッフによって承認された後、<br/>
                                            他のユーザーへ公開されます。
                                        </div>
                                    }
                                    <Image imageId={ buzz.buzz_val } isBackground imgKind="2"/>
                                </div>) : ''
                            }
                        </div>
                    )}
                </div>

                <div className="buzz__footer">
                    { buzz.isApproveAll && !isBuzzDetail ? (
                        <Link to={ { pathname: `/buzz-detail/${buzz.buzz_id}` } }>
                            <span className="icon icon-comment "/>{ buzz.cmt_num }
                        </Link>
                    ) : (
                        <span>
                            <span className="icon icon-comment"/>{ buzz.cmt_num }
                        </span>
                    ) }
                    <div className={ buzz.isLike ? 'box-like text_pink' : 'box-like' } onClick={ this.handleUserLike }>
                        <span className={ `icon icon-${buzz.isLike ? 'liked' : 'like'}` }/>
                        <span className='num-like'>{ buzz.like_num }</span>

                    </div>
                </div>

                <div className="buzz-comment__list">{ comments }</div>
                {
                    buzz.cmt_num > 4 && !isBuzzDetail ?
                        <Link className="buzz__load-comment" to={ { pathname: `/buzz-detail/${buzz.buzz_id}` } }>
                            その他のコメントを表示…
                        </Link> : ''
                }
            </div>
        );
    }
}

Buzz.propTypes = {
    buzz: PropTypes.object,
    openDialogDetailImage: PropTypes.func,
    isBuzzDetail: PropTypes.bool,
    subCommentBuzzDetail: PropTypes.func,
    handleFavorite: PropTypes.func,
    handleDeleteBuzz: PropTypes.func,
    indexBuzz: PropTypes.number,
    key: PropTypes.number,
};

Buzz.defaultProps = {
    buzz: [],
    isBuzzDetail: false,
};

export default Buzz;
