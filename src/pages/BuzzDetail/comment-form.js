import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import BuzzService from '../../services/timeline';
import {
    BANNED_WORDS_MESSAGE_FORMAT,
} from '../../constants/messages';
import { openBuyPoint } from '../../actions/index';
import BannedWordService from '../../services/bannedWord';

class CommentForm extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            comment: props.comment || '',
            isFocus : this.props.isFocus,
        };
        this.contentRef = React.createRef();
    }

    componentDidMount(){
        if (this.props.isSubCmt) {
            this.contentRef.current.focus();
            this.setState({
                isFocus : false,
            })
        }

        BannedWordService.getBannedWord()
            .then( response => {
                if (!this.isUnmounted) {
                    this.setState({
                        bannedWord : response.data.timeline,
                    })
                }
            })
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    handleCheckBannedWord(value){
        const checkBannedWord = [];
        const {bannedWord} = this.state;

        bannedWord.map((word) => {
            if (value.indexOf(word) !== -1) {
                checkBannedWord.push(word);
            }
            return '';
        })
        return checkBannedWord;
    }

    valueComment(e) {
        const { value } = e.target;

        if (value.length > 500) {
            return;
        }

        this.setState({
            comment : value,
        })
    }

    checkCommentDelete(arrComment) {
        const newComment = []
        arrComment.map((comment,index) => {
            if (comment.isDeleted) {
                newComment.push(arrComment.splice(index,1));
            }
            return '';
        })
        return newComment;
    }

    addComment(e){
        e.preventDefault();
        this.contentRef.current.blur();
        const { buzz,cmtId,indexCmt,indexBuzz } = this.props;
        const checkBannedWord = this.handleCheckBannedWord(this.state.comment);
        if (checkBannedWord.length !== 0) {
            const messages = BANNED_WORDS_MESSAGE_FORMAT.replace(/%s/,checkBannedWord[0])
            Popup.alert(messages)
            return ;
        }

        this.setState({
            comment : '',
        })

        if (this.props.isSubCmt) {
            BuzzService.addSubComment(buzz.buzz_id,cmtId,this.state.comment)
                .then(() => {
                    if (buzz.comment[indexCmt])
                        BuzzService.getListSubComment(buzz.buzz_id,cmtId,buzz.comment[indexCmt].sub_comment.length,1)
                            .then(res => this.props.handleAddSubComment(res.data,indexCmt,indexBuzz))
                            .catch((err) => {
                                if (err.code === 70) {
                                    this.props.openBuyPoint();
                                }
                            })
                })
            return;
        }

        const commentDeleted = this.checkCommentDelete(buzz.comment);

        BuzzService.addComment(this.state.comment,buzz.buzz_id)
            .then(() => {
                BuzzService.getListComment(buzz.buzz_id,buzz.comment.length - commentDeleted.length,1)
                    .then(res => {
                        this.props.handleAddComment(res.data,this.props.indexBuzz)
                    })
            }).catch((err) => {
                if (err.code === 70) {
                    this.props.openBuyPoint();
                }
            })

    }

    render() {

        if (this.props.isSubCmt && !this.state.isFocus) {
            this.contentRef.current.focus();
        }

        return (
            <form name = "formSubmit" className="buzz-comment-form" onSubmit = {(e) => this.addComment(e)}>
                <div className="buzz-comment-form__icon">
                    <span className="icon icon-comment-grey" />
                </div>
                <input
                    maxLength = "500"
                    ref={ this.contentRef }
                    name = 'valueComment'
                    disabled = {!this.props.buzz.isApproveAll}
                    value = {this.state.comment}
                    className="buzz-comment-form__input"
                    placeholder={ this.props.placeHolder }
                    onChange = {(e) => this.valueComment(e)}
                />
            </form>
        );
    }
}

CommentForm.propTypes = {
    placeHolder: PropTypes.string,
    handleAddComment : PropTypes.func,
    buzz : PropTypes.object,
    comment : PropTypes.object,
    indexBuzz : PropTypes.number,
    isSubCmt : PropTypes.bool,
    cmtId : PropTypes.string,
    indexCmt : PropTypes.number,
    handleAddSubComment :  PropTypes.func,
    isFocus : PropTypes.bool,
    openBuyPoint: PropTypes.func.isRequired,
}

CommentForm.defaultProps = {
    placeHolder: 'コメントを入力...',
    isSubCmt : false,
    isFocus : false,
}

const mapDispatchToProps = (dispatch) => ({
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
});

export default connect(null, mapDispatchToProps)(CommentForm);
