import React, { Component } from 'react';
import PropTypes, { object } from 'prop-types';
import Popup from 'react-popup';
import { Link } from 'react-router-dom';
import MainLayout from '../../components/MainLayout';
import DetailImage from '../../components/DetailImage';
import Buzz from './buzz';
import storage from '../../utils/storage';
import CommentForm from "./comment-form";
import history from '../../utils/history';
import TimelineService from '../../services/timeline';
import {
    ALERT_BUZZ_INVALID,
} from '../../constants/messages';

class BuzzDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buzz: [],
            openDialogDetailImage: false,
            isSubComment : props.location.isSubComment || false,
            indexCmt : props.location.indexComment,
            cmtId : props.location.state ? props.location.state.comment.cmt_id : '',
            isFocus : props.location.isFocus,
        };

        this.handleOpenDialogDetailImage = this.handleOpenDialogDetailImage.bind(this);
        this.handleCloseDialogDetailImage = this.handleCloseDialogDetailImage.bind(this);
    }

    componentDidMount(){
        TimelineService.getBuzzDetail(30,this.props.match.params.id)
            .then (response => {
                let { buzz } = this.state;
                buzz = buzz.concat(response.data);
                this.setState({
                    buzz,
                })
            }).catch(() => {
                Popup.create({
                    content: ALERT_BUZZ_INVALID,
                    buttons: {
                        left: [{
                            text: 'はい',
                            className: 'ok',
                            action: () => {
                                Popup.close();
                                history.checkAndBack();
                            },
                        }],
                    },
                });
            })
    }

    handleCloseDialogDetailImage(buzzId) {
        TimelineService.getBuzzDetail(30, buzzId)
            .then(responseBuzzDetail => {
                const dataBuzz = [];
                dataBuzz.push(responseBuzzDetail.data);
                this.setState({
                    openDialogDetailImage: false,
                    buzz: dataBuzz,
                });
            });
    }

    handleOpenDialogDetailImage() {
        this.setState({
            openDialogDetailImage: true,
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            buzz: nextProps.buzz,
        });
    }

    handleRouter(){
        return history.checkAndBack();
    }

    handleAddComment = (comment) => {
        const { buzz } = this.state;
        buzz[0].comment = buzz[0].comment.concat(comment[0]);
        this.setState({
            buzz,
        })
    }

    subCommentBuzzDetail = (indexCmt) => {
        const { buzz } = this.state;
        this.setState({
            isSubComment : true,
            indexCmt,
            cmtId : buzz[0].comment[indexCmt].cmt_id,
        })
    }

    handleAddSubComment = (subComment,indexCmt) => {
        const { buzz } = this.state;
        buzz[0].comment[indexCmt].subComment = buzz[0].comment[indexCmt].subComment.concat(subComment);
        this.setState({
            buzz,
            isSubComment : false,
        })
    }

    render() {
        const { buzz,indexCmt,cmtId,isFocus } = this.state;
        const myUser = storage.getUserInfo();
        const isSubCmt =  this.state.isSubComment;

        if (!buzz || buzz.length === 0) {
            return '';
        }

        const display = buzz.map((item,index) => {
            const isBuzzDetail = true;
            const placeHolder = item.gender === myUser.gender ? `コメントを入力...` : `コメントを入力...(${item.comment_buzz_point}pts消贄します)`;
            return (<div className="buzz__item" key = {index}>
                <Buzz subCommentBuzzDetail = {this.subCommentBuzzDetail} isBuzzDetail = {isBuzzDetail} buzz={ item } key={ index } openDialogDetailImage={this.handleOpenDialogDetailImage}/>
                {item.cmt_num > 4 && !isBuzzDetail ? (<Link className="buzz-comment-form" to={ { pathname: `/buzz-detail/${item.buzz_id}` } }>
                    <div className="buzz-comment-form__icon">
                        <span className="icon icon-comment-grey" />
                    </div>
                    <input
                        className="buzz-comment-form__input"
                        readOnly
                        placeholder={ placeHolder }
                    />
                </Link>) : <CommentForm
                    indexCmt = {indexCmt}
                    cmtId = {cmtId}
                    isSubCmt = {isSubCmt}
                    isBuzzDetail = {isBuzzDetail}
                    buzz={item}
                    handleAddComment={this.handleAddComment}
                    placeHolder={placeHolder}
                    handleAddSubComment={this.handleAddSubComment}
                    isFocus= {isFocus}
                />
                }
            </div>)
        });

        return (
            <MainLayout
                className="buzz-detail-page"
                title="詳細"
                hasFooter={false}
                hasSidebarLeft={false}
                hasSidebarRight={false}
                hasBackButton
                backFunction={this.handleRouter}
            >
                <div className="k-main buzz-detail">
                    <div className="buzz__list">{ display }</div>
                </div>
                {this.state.openDialogDetailImage ? <DetailImage isImageMyUser={buzz[0].user_id === myUser.user_id} buzz={buzz[0]} handleCloseDialogDetailImage={this.handleCloseDialogDetailImage} /> : ''}
            </MainLayout>
        );
    }
}


BuzzDetail.propTypes = {
    buzz: PropTypes.array,
    match : object,
    location : object,
}

BuzzDetail.defaultProps = {
    buzz: [],
}

export default BuzzDetail;
