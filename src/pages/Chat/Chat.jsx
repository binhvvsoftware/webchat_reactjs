import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './Chat.scss';
import ChatContainer from '../../components/container/ChatContainer';
import MediaService from '../../services/media';
import * as endpoint from '../../constants/endpoint_constant';
import storage, { keys } from '../../utils/storage';
import { MessageTypes, MessageFileTypes } from '../../constants/types';
import MessageSocket, { StatusSocket } from '../../entities/MessageSocket';
import { getLocationHistory } from '../../utils/history';
import { closeChatConversation } from '../../actions';
import {
    initChat,
    updateShowHideButtonBack,
    changeWhoYouAre,
    loadChatPool,
    pushChatPool,
    openBuyingPointPromotion,
    closeBuyingPointPromotion,
    commitFileSending,
    updateAttentionNumbers,
    updateConversation,
    closeTemplate,
    openFooterKeyboard,
    commitRawMessageHandling,
    updateChatPool,
} from '../../actions/Chat';

import BuyPoint from '../../components/BuyPoint';
import UserService from '../../services/user';
import ChatService from '../../services/chat';
import { sendMessage } from '../../actions/socket';

const mapStateToProps = (state) => ({
    isSocketNewMessageListenersLoadedFully: state.chatControl.isSocketNewMessageListenersLoadedFully,
    you: state.chatControl.you,
    pendingFileMessage: state.chatControl.pendingFileMessage,
    isShowPopupBuyPoint: state.chatControl.isShowPopupBuyPoint,
    filterConversation: state.chatControl.filterConversation,
    arrivedRawMessage: state.chatControl.arrivedRawMessage,
    attentionNumbers: state.chatControl.clientAttentionNumbers,
    listConversation: state.chatControl.listConversation,
});
const mapDispatchToProps = (dispatch) => ({
    initChat: payload => dispatch(initChat(payload)),
    updateShowHideButtonBack: payload => dispatch(updateShowHideButtonBack(payload)),
    changeWhoYouAre: payload => dispatch(changeWhoYouAre(payload)),
    loadChatPool: payload => dispatch(loadChatPool(payload)),
    pushChatPool: payload => dispatch(pushChatPool(payload)),
    openBuyingPointPromotion: payload => dispatch(openBuyingPointPromotion(payload)),
    closeBuyingPointPromotion: payload => dispatch(closeBuyingPointPromotion(payload)),
    closeChatConversation: payload => dispatch(closeChatConversation(payload)),
    commitFileSending: payload => dispatch(commitFileSending(payload)),
    updateAttentionNumbers: payload => dispatch(updateAttentionNumbers(payload)),
    updateConversation: payload => dispatch(updateConversation(payload)),
    closeTemplate: payload => dispatch(closeTemplate(payload)),
    openFooterKeyboard: payload => dispatch(openFooterKeyboard(payload)),
    commitRawMessageHandling: payload => dispatch(commitRawMessageHandling(payload)),
    updateChatPool: payload => dispatch(updateChatPool(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

/* eslint-disable no-console */
class ConnectedChat extends Component {
    getContainer = () => <ChatContainer/>;

    getBuyingPromotion = () => this.props.isShowPopupBuyPoint ? (
        <BuyPoint closeBuyPoint={ this.props.closeBuyingPointPromotion }/>
    ) : null;

    // ATTENTION: do NOT use componentWillReceiveProps!
    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.id !== this.props.match.params.id) {
            this.props.closeChatConversation();
            this.props.loadChatPool({ data: [] });
            this.props.closeTemplate();
            this.props.openFooterKeyboard();
            this.handleCallApi(nextProps.match.params.id);
        }

        const location = getLocationHistory();
        let isShowButtonBack = location.previous.pathname.indexOf(`/profile/${nextProps.match.params.id}`) === 0;
        if (location.previous.pathname === '' || location.previous.pathname.indexOf(`/chat/${nextProps.match.params.id}`) === 0) {
            isShowButtonBack = !!storage.get(keys.CHAT_MENU_BUTTON);
        }
        storage.set(keys.CHAT_MENU_BUTTON, isShowButtonBack);

        this.props.updateShowHideButtonBack({
            isShowButtonBack,
        });
    }

    componentWillUnmount() {
        this.props.initChat();
    }

    componentDidUpdate() {
        const messageDataObject = this.props.arrivedRawMessage;
        if (!messageDataObject) {
            return;
        }
        this.props.commitRawMessageHandling();
        // Unsent message (may be lacked of point)
        if (messageDataObject.mds && messageDataObject.mds.msg_status === StatusSocket.UNSENT.code) {
            this.props.openBuyingPointPromotion();
            if (this.props.pendingFileMessage) {
                this.props.commitFileSending({ message: this.props.pendingFileMessage });
            }

            this.props.updateChatPool({
                message: {
                    sendError: true,
                    id: messageDataObject.mds.msg_id,
                },
            });
            return;
        }

        // Can send, but have a pending message (FILE)
        if (messageDataObject.mds && this.props.pendingFileMessage && messageDataObject.mds.msg_status === StatusSocket.SENT.code) {
            if (messageDataObject.mds.msg_id === this.props.pendingFileMessage.msg_id) {
                console.log('%cThe new message is a file sending confimation...', 'font-weight: bold; color: orange;');
                /* IMAGE FILE */
                if (this.props.pendingFileMessage.file.msg_content_type === MessageFileTypes.PHOTO) {
                    this.handleUploadImage();
                    /* VIDEO FILE */
                } else if (this.props.pendingFileMessage.file.msg_content_type === MessageFileTypes.VIDEO) {
                    this.handleUploadVideo();
                }
            }
            return;
        }

        if (messageDataObject.msg_type === MessageTypes.FILE && messageDataObject.file && messageDataObject.file.file_id) {
            if (typeof(messageDataObject.isUnlock) === 'undefined') {
                let apiName = 'checkVideoUnlock';
                if (messageDataObject.file.msg_content_type === MessageFileTypes.PHOTO) {
                    apiName = 'checkImageUnlock';
                }

                ChatService[apiName](this.props.you.id, messageDataObject.file.file_id)
                    .then((checkFileUnlockAPIResponse) => {
                        const newMessage = new MessageSocket(Object.assign({}, messageDataObject, { isUnlock: checkFileUnlockAPIResponse.data.is_unlck === 1 }));
                        this.props.pushChatPool({ message: newMessage });
                    });
            }
        }

        const newMessage = new MessageSocket(Object.assign({}, messageDataObject));
        if (newMessage.type === MessageTypes.PP || newMessage.type === MessageTypes.STK || newMessage.type === MessageTypes.GIFT || (newMessage.type === MessageTypes.FILE && newMessage.file.file_id)) {
            this.props.pushChatPool({ message: newMessage });
        }
    }

    componentDidMount() {
        this.handleCallApi(this.props.match.params.id);
    }

    handleUploadImage = () => {
        const messageDataObject = this.props.arrivedRawMessage;
        const handleSendImage = (imageFileId) => {
            if (!this.props.pendingFileMessage) {
                return;
            }
            const imageFileData = {
                to: this.props.pendingFileMessage.receiver,
                file: {
                    file_id: imageFileId,
                    msg_content_type: this.props.pendingFileMessage.file.msg_content_type,
                    msg_id: this.props.pendingFileMessage.msg_id,
                },
            };

            const message = new MessageSocket(Object.assign({}, this.props.pendingFileMessage, imageFileData, {
                fileStatus: messageDataObject.mds.status_file,
            }));
            this.props.sendMessage(message);
            this.props.commitFileSending({message });
        };

        ChatService.checkImageExist(this.props.pendingFileMessage.file.meta.time, this.props.pendingFileMessage.file.meta.size).then((checkImageFileExistResponse) => {
            console.log(checkImageFileExistResponse);
            if (checkImageFileExistResponse.data.is_existed) {
                // file is currently exist, don't upload it, just use it
                console.log('%cFile is currently exist, don\'t upload it, just use it...', 'color: orange;');
                return handleSendImage(checkImageFileExistResponse.data.image_id, true);
            }

            // file is not exist, upload it
            console.log('%cFile is not exist, upload it...', 'color: orange;');
            const file = this.props.pendingFileMessage.file.meta;
            return MediaService.uploadImageBase64(file.data, endpoint.IMAGE_CHAT, file.orientation, file.time, file.size)
                .then((uploadImageResponse) => {
                    handleSendImage(uploadImageResponse.data.image_id);
                });
        });
    };

    handleUploadVideo = () => {
        const messageDataObject = this.props.arrivedRawMessage;
        const getURLVideo = (videoFileId) => {
            MediaService.getURLVideo(videoFileId)
                .then((getVideoUrlResponse) => {
                    this.props.updateChatPool({
                        message: {
                            videoUrl: getVideoUrlResponse.data.url,
                            id: messageDataObject.mds.msg_id,
                        },
                    });
                });
        }

        const uploadNewVideo = () => {
            // file is not exist, upload it
            console.log('%cFile is not exist, upload it...', 'color: orange;');
            MediaService.updateVideo(this.props.pendingFileMessage.file.meta).then((videoUploadingResponse) => {
                const videoFileId = videoUploadingResponse.data.file_id;
                const videoFileData = {
                    to: this.props.pendingFileMessage.receiver,
                    file: {
                        file_id: videoFileId,
                        msg_content_type: this.props.pendingFileMessage.file.msg_content_type,
                        msg_id: this.props.pendingFileMessage.msg_id,
                    },
                };

                MediaService.updateThumbnailVideo(this.props.pendingFileMessage.file.meta.poster, videoFileId)
                    .then(response => {
                        console.log(response);
                    })
                    .catch(errors => {
                        console.log(errors);
                    })
                    .finally(() => {
                        const message = new MessageSocket(Object.assign({}, this.props.pendingFileMessage, videoFileData, {
                            fileStatus: messageDataObject.mds.status_file,
                        }));
                        this.props.sendMessage(message);
                        this.props.commitFileSending({message });
                        getURLVideo(videoFileId);
                    });
            });
        };

        ChatService.checkVideoExist(this.props.pendingFileMessage.file.meta.time, this.props.pendingFileMessage.file.meta.size).then((checkVideoFileExistResponse) => {
            if (checkVideoFileExistResponse.data.is_existed) {
                // file is currently exist, don't upload it, just use it
                console.log('%cFile is currently exist, don\'t upload it, just use it...', 'color: orange;');
                console.log(checkVideoFileExistResponse);
                const videoFileId = checkVideoFileExistResponse.data.file_id;
                const videoFileData = {
                    to: this.props.pendingFileMessage.receiver,
                    file: {
                        file_id: videoFileId,
                        msg_content_type: this.props.pendingFileMessage.file.msg_content_type,
                        msg_id: this.props.pendingFileMessage.id,
                    },
                };


                const message = new MessageSocket(Object.assign({}, this.props.pendingFileMessage, videoFileData, {
                    fileStatus: messageDataObject.mds.status_file,
                }));
                this.props.sendMessage(message);
                this.props.commitFileSending({message });
                getURLVideo(videoFileId);
            } else {
                uploadNewVideo();
            }
        }).catch(errors => {
            console.error(errors);
            uploadNewVideo();
        });
    };

    handleCallApi = (youId) => {
        /* Load user's friend information */
        UserService.getUserInfo(youId).then((response) => {
            // dispatch action
            this.props.changeWhoYouAre({ id: response.data.user_id, data: response.data });

            /* Load chat history */
            ChatService.getChatHistory(youId).then((chatHistoryResponse) => {
                this.handleUpdateConversations();
                this.props.loadChatPool({ data: chatHistoryResponse.data });
            });
        });
    };

    handleUpdateConversations = () => {
        UserService.getAttentionNumber().then(getAttentionNumbersResponse => {
            this.props.updateAttentionNumbers({ attentionNumbers: getAttentionNumbersResponse.data });
        });

        ChatService.getConversations(this.props.filterConversation)
            .then(responseConversation => {
                this.props.updateConversation({
                    data: responseConversation.data,
                });
            });
    };

    render() {
        return (
            <div id="chat-root">
                { this.props.pendingFileMessage && <div className="chat-root__loading"/> }
                { this.getContainer() }
                { this.getBuyingPromotion() }
            </div>
        );
    }
}

ConnectedChat.propTypes = {
    initChat: PropTypes.func.isRequired,
    updateShowHideButtonBack: PropTypes.func.isRequired,
    you: PropTypes.object.isRequired,
    changeWhoYouAre: PropTypes.func.isRequired,
    loadChatPool: PropTypes.func.isRequired,
    pushChatPool: PropTypes.func.isRequired,
    openBuyingPointPromotion: PropTypes.func.isRequired,
    closeBuyingPointPromotion: PropTypes.func.isRequired,
    commitFileSending: PropTypes.func.isRequired,
    closeChatConversation: PropTypes.func.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    updateConversation: PropTypes.func.isRequired,
    openFooterKeyboard: PropTypes.func.isRequired,
    closeTemplate: PropTypes.func.isRequired,
    commitRawMessageHandling: PropTypes.func.isRequired,
    match: PropTypes.object,
    isShowPopupBuyPoint: PropTypes.bool.isRequired,
    pendingFileMessage: PropTypes.object,
    arrivedRawMessage: PropTypes.object,
    filterConversation: PropTypes.string.isRequired,
    updateChatPool: PropTypes.func.isRequired,
    sendMessage: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConnectedChat);


