import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { TextInput } from 'react-native-web';
import Popup from 'react-popup';
import Header from '../../components/Header'
import FullScreenModal from '../../components/FullScreenModal'
import AccountTakeover from '../../components/AccountTakeover'
import Auth from '../../services/authentication'
import {
    ALERT_TITLE_DEACTIVE,
    DEACTIVATE_ALERT_SHOW_TEXT,
} from '../../constants/messages';
import { disconnectSocket } from '../../utils/socket-io';
import history from '../../utils/history'
import storage from '../../utils/storage'
import './index.scss';


class Deactivate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '退会',
            withdraw: '',
            openWithdraw: false,
            openHandover: false,
        };
    }

    handleClose = (hasEmail) => {
        this.setState({ openHandover: false })
        history.push('/settings', { hasEmail });
    }

    handleOpenWithdraw = () => {
        this.setState({
            openWithdraw: true,
        });
    }

    handleOpenHandover = () => {
        this.setState({
            openHandover: true,
        });
    }

    handleChangeWithdrawContent = (text) => {
        this.setState({
            withdraw: text.trim(),
        });
    }

    handleSubmitWithdraw = () => {
        if (this.state.withdraw.length === 0) {
            Popup.alert(DEACTIVATE_ALERT_SHOW_TEXT, ALERT_TITLE_DEACTIVE);
            return;
        }

        Auth.deactivate(this.state.withdraw);
        disconnectSocket();
        history.push('/');
    }

    render() {
        return (
            <div className="deactivate-page">
                <Header title={this.state.title} backUrl={this.handleClose} />
                <main className="k-main">
                    <div className="text-note">
                        {(this.props.userInfo.fb_id) ? (
                            <div>
                                <p>
                                    退会すると 『所持ポイントを含む全てのアカウントデータ』が削除されます。
                                </p>
                                <p>
                                    再度Kyuunを利用する可能性がある場合は退会せずにヽ 「データ弓ーき継ぎ」 を行つてからアプリをアンインス トール
                                    (削除) してください。 なお、 畷種変更する場合も同様にヽ 事前に、 「 デ一タ引き継ぎ」 を行-〕」・〔 ください~ デ一タ
                                    引き継ぎはこららから
                                </p>
                                <p>
                                    ※ 「Facebookで口グイ ン的に弓ーき継がれ董すされている場合は退会しても自動9。
                                </p>
                            </div>
                        ) : (
                            <div>
                                <p>
                                    退会すると 『所持ポイントを含む全てのアカウントデータ』が削除されます。
                                </p>
                                <p>
                                    再度Kyuunを利用する可能性がある場合は退会せずに、「データ引き継ぎ」を行ってからアプリをアンインストール（削除）してください。
                                    なお、機種変更する場合も同様に、事前に、「データ引き継ぎ」を行ってください。<br/>⇒データ引き継ぎは<span className="cursor-pointer text-primary" onClick={this.handleOpenHandover}>こちら</span>から
                                </p>
                                <p>
                                    ※「Facebookでログイン」されている場合は退会しても自動的に引き継がれます。
                                </p>
                            </div>
                        )}
                        <Link className='btn-link' to='/settings'>
                            <button type="button" className="btn-custom btn-active btn-login">退会しません</button>
                        </Link>
                        <p>
                            <button type="button" className="btn-custom btn-default" onClick={this.handleOpenWithdraw}>退会する</button>
                        </p>
                    </div>

                    <FullScreenModal title={this.state.title} isOpen={this.state.openWithdraw} onClose={this.handleClose}>
                        <div className="static-content text-note">
                            Kyuunをご使用頂き誠にありがとうございました。最後に、退会される理由（Kyuunの改善点等）をご入力頂けましたら幸いでございます。また機会がございましたら、Kyuunを宜しくお願い申し上げます。

                            <TextInput
                                className="draw-content"
                                multiline
                                numberOfLines={5}
                                autoCapitalize="sentences"
                                defaultValue={this.state.withdraw}
                                onChangeText={ (text) => this.handleChangeWithdrawContent(text) } />

                            <p className="text-center">
                                <button type="button" className="btn-custom btn-active btn-login" onClick={this.handleSubmitWithdraw}>退会する</button>
                            </p>
                        </div>
                    </FullScreenModal>

                    <AccountTakeover isOpen={this.state.openHandover} onClose={this.handleClose} />
                </main>
            </div>
        );
    }
}

Deactivate.propTypes = {
    userInfo: PropTypes.object,
};

Deactivate.defaultProps = {
    userInfo: storage.getUserInfo(),
};

export default Deactivate;
