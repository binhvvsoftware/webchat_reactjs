import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isMobile from 'ismobilejs';
import FullScreenModal from '../../components/FullScreenModal';
import { closeDownload } from '../../actions/download';
import avatarDefault from '../../statics/images/avatar-default.png';

class PageDownload extends Component {
    closeDownload = () => {
        this.props.onClose();
    };

    render() {
        const img = <img
            src={ this.props.downloadURL }
            alt={ this.props.fileName }
            name={ this.props.fileName }
            onError={ (e) => e.target.src = avatarDefault }/>; // eslint-disable-line
        const downloadLink = ( // eslint-disable-next-line jsx-a11y/anchor-has-content
            <a
                className="download-link" href={ this.props.downloadURL }
                download={ this.props.fileName }
                target="_blank">
                { img }
            </a>
        );

        return (
            <FullScreenModal
                className="modal-download"
                title="ダウンロード"
                isOpen={ this.props.isOpenDownload }
                onClose={ this.closeDownload }>
                { isMobile.any ? img : downloadLink }
            </FullScreenModal>
        );
    }
}

PageDownload.propTypes = {
    isOpenDownload: PropTypes.bool.isRequired,
    downloadURL: PropTypes.string.isRequired,
    fileName: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    isOpenDownload: state.download.isOpenDownload,
    downloadURL: state.download.downloadURL,
    fileName: state.download.fileName,
});

const mapDispatchToProps = (dispatch) => ({
    onClose: () => dispatch(closeDownload()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PageDownload);

