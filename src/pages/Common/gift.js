import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import FullScreenModal from '../../components/FullScreenModal';
import ChatGiftPool from '../../components/presentational/Chat/ChatGiftPool';

class Gift extends Component {
    render() {
        return (
            <FullScreenModal isOpen={this.props.isGiftPoolOpened}>
                { this.props.isGiftPoolOpened ? <ChatGiftPool active={this.props.isGiftPoolOpened} /> : <></> }
            </FullScreenModal>
        );
    }
}

const mapStateToProps = state => ({
    isGiftPoolOpened: state.chatControl.isGiftPoolOpened,
});

const mapDispatchToProps = () => ({});

Gift.propTypes = {
    isGiftPoolOpened: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Gift);

