/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import PropTypes from 'prop-types';
import storage from '../../utils/storage';
import Auth from '../../services/authentication';
import PublicRoutes from '../../routers/public';
import history, { getLocationHistory } from '../../utils/history';
import { connectSocket, disconnectSocket, updateAuthenticate } from '../../utils/socket-io';
import Visibility from '../../utils/visibility';
import { MessageTypes } from '../../constants/types';
import {
    changeWhoIAm,
    commitSocketIONewMessageListeners,
    clearSocketIONewMessageListeners,
    fireNewMessageRawData,
    updateAttentionNumbers,
    updateConversation,
    updateChatTyping,
    updateChatPool,
    pushChatPool,
} from '../../actions/Chat';
import { pushNotification } from '../../actions';
import { sendMessage } from '../../actions/socket';
import { SocketEvents } from '../../constants/events';
import {
    NOTI_GAVE_GIFT_NEW,
    NOTI_NEW_CHAT_MSG_TEXT_NEW,
    NOTI_NEW_CHAT_MSG_PHOTO_NEW,
} from '../../constants/messages';
import UserService from '../../services/user';
import ChatService from '../../services/chat';
import './index.scss';
import MessageSocket from '../../entities/MessageSocket';

const visibility = new Visibility();

class Socket extends Component {
    constructor(props) {
        super(props);

        this.handleUpdateConversations = debounce(this.updateConversations, 2000);
    }

    componentWillMount() {
        visibility.onChange((visible) => {
            if (visible) {
                this.reloadData();
            } else {
                disconnectSocket();
                this.props.clearSocketIONewMessageListeners();
            }
        });

        history.listen(() => {
            this.reloadData();
        });
        this.reloadData();
    }

    checkPublicRoute = () => {
        const location = getLocationHistory();
        let isPublic = false;
        PublicRoutes.forEach(route => {
            if (location.current.pathname.indexOf(route) === 0) {
                isPublic = true;
            }
        });

        return isPublic;
    };

    reloadData = () => {
        if (this.checkPublicRoute()) {
            return;
        }

        const token = storage.getToken();
        if (token) {
            Auth.getInfo().then(response => {
                this.props.changeWhoIAm({ id: response.data.user_id, data: response.data });
            });
            this.connectSocket();
            this.handleGetNewMessage();
        } else {
            this.handleRedirectLogin();
        }
    };

    handleGetNewMessage = () => {
        const location = getLocationHistory();
        const match = /^(\/chat\/)([0-9a-f]+)(\/)?$/.exec(location.current.pathname);

        if (Array.isArray(match) && match[2]) {
            ChatService.getChatHistory(match[2]).then((response) => {
                // clientChain
                response.data.reverse().forEach(message => {
                    if (!this.props.clientChain[message.id]) {
                        this.props.pushChatPool({ message, history: true });
                    }
                });
            });
        }
    }

    handleUpdateConversation(message) {
        const notify = {
            sender: message.sender,
        };

        switch (message.type) {
            case MessageTypes.GIFT:
                notify.message = NOTI_GAVE_GIFT_NEW;
                break;

            case MessageTypes.STK:
            case MessageTypes.PP:
            case MessageTypes.TEMPLATE:
                notify.message = NOTI_NEW_CHAT_MSG_TEXT_NEW;
                break;

            case MessageTypes.FILE:
                notify.message = NOTI_NEW_CHAT_MSG_PHOTO_NEW;
                break;
            default:
                break;
        }

        if (notify.message) {
            this.props.pushNotification({ data: notify });
        }

        this.handleUpdateConversations();
    }

    handleCheckMDSMessage = (message) => {
        if (this.handleCheckPageChatWithFriend(message)) {
            this.props.updateChatPool({
                message: Object.assign({}, {
                    id: message.mds.msg_id,
                    read_time: new Date().getTime(),
                    statusCode: 'rd',
                }),
            });
        }

        if (message.mds && message.mds.point !== undefined) {
            const user = this.props.me;
            user.data.point = message.mds.point;
            this.props.changeWhoIAm(user);
        }
    };

    updateConversations = () => {
        UserService.getAttentionNumber().then(getAttentionNumbersResponse => {
            this.props.updateAttentionNumbers({ attentionNumbers: getAttentionNumbersResponse.data });
        });

        ChatService.getConversations(this.props.filterConversation)
            .then(responseConversation => {
                this.props.updateConversation({
                    data: responseConversation.data,
                });
            });
    };

    /* eslint-disable no-console */
    connectSocket = () => {
        const token = storage.getToken();
        const currentUser = storage.getUserInfo();
        this.props.changeWhoIAm({ id: currentUser.user_id, data: currentUser });

        ((dispatch) => {
            const socket = connectSocket();
            if (!this.props.isSocketNewMessageListenersLoadedFully) {
                // Load Socket IO
                if (socket.id && dispatch && socket.connected && socket.isAuthenticate) {
                    console.log('Already have a socket connection, just use it: ID: %s Object: %o', socket, socket.id);
                    dispatch(socket);

                    return;
                }

                socket.on(SocketEvents.CONNECT, () => {
                    console.log('%cSOCKETIO CONNECTED!', 'font-weight: bold; color: blue;', socket);
                    if (dispatch) dispatch(socket);
                    socket.removeAllListeners(SocketEvents.NEW_MESSAGE);
                    socket.on(SocketEvents.NEW_MESSAGE, (messageData) => {
                        const messageDataObject = new MessageSocket(JSON.parse(messageData), 'socket');
                        console.log('%cRESPONSE FROM SOCKET IO, newMessage EVENT: %o', 'font-weight: bold; color: #71d008', messageDataObject);
                        this.handleCheckTyping(messageDataObject);

                        if (
                            messageDataObject.type !== MessageTypes.PRC &&
                            messageDataObject.type !== MessageTypes.AUTH &&
                            !messageDataObject.pendingAttachmentFileMessage
                        ) {
                            this.handleUpdateConversation(messageDataObject);
                        }

                        if (messageDataObject.type === MessageTypes.AUTH) {
                            updateAuthenticate(currentUser.user_id);
                            window.queueSocet = window.queueSocet || [];
                            window.queueSocet.forEach(message => {
                                if (message.file && message.file.meta) {
                                    delete message.file.meta;
                                }
                                delete message.data;
                                socket.emit('newMessage', message);
                                console.log('%cEMIT Socket in queue', 'font-weight: bold; color: green', message);
                            });

                            window.queueSocet = [];
                        }

                        if (messageDataObject.type === MessageTypes.MDS && messageDataObject.mds && messageDataObject.mds.msg_id) {
                            this.handleCheckMDSMessage(messageDataObject);
                        }

                        /* A new message has come, dispatch it as a new action */
                        this.props.fireNewMessageRawData({ messageDataObject });
                    });
                });

                socket.on('disconnect', (msg) => {
                    this.props.clearSocketIONewMessageListeners();
                    console.log(`%cSOCKETIO DISCONNECT [${msg}]`, 'font-weight: bold; color: red;');
                });
            } else {
                console.log('Already have a socket connection, with complete new message listener, just use it: ID: %s Object: %o', socket, socket.id);
                dispatch(null);
            }
        })((socket) => {
            if (socket) {
                // this.props.loadSocketIO({ authMessage, socket });
                this.props.sendMessage({
                    type: MessageTypes.AUTH,
                    content: token,
                });
                this.props.commitSocketIONewMessageListeners();
            }
        });
    };

    handleCheckPageChatWithFriend = (message) => {
        const location = getLocationHistory();
        const match = /^(\/chat\/)([0-9a-f]+)(\/)?$/.exec(location.current.pathname);
        if (Array.isArray(match) && message && match[2] === message.sender) {
            return true;
        }

        return false;
    };

    handleCheckTyping = (message) => {
        if (!this.handleCheckPageChatWithFriend(message)) {
            return;
        }
        clearTimeout(window.clearTyping);
        this.props.updateChatTyping({ isTyping: false });

        if (message.type === MessageTypes.PRC) {
            this.props.updateChatTyping({ isTyping: message.isTyping });

            if (message.isTyping) {
                window.clearTyping = setTimeout(() => {
                    this.props.updateChatTyping({ isTyping: false });
                }, 3000);
            }
        }
    };

    handleRedirectLogin = () => {
        history.push('/login');
    };

    render() {
        return (
            <></>
        );
    }
}


const mapStateToProps = (state) => ({
    isSocketNewMessageListenersLoadedFully: state.chatControl.isSocketNewMessageListenersLoadedFully,
    me: state.chatControl.me,
    you: state.chatControl.you,
    clientChain: state.chatControl.clientChain,
    filterConversation: state.chatControl.filterConversation,
    arrivedRawMessage: state.chatControl.arrivedRawMessage,
    updateAttentionNumbers: PropTypes.func.isRequired,
    updateConversation: PropTypes.func.isRequired,
    commitSocketIONewMessageListeners: PropTypes.func.isRequired,
});

const mapDispatchToProps = (dispatch) => ({
    pushNotification: (payload) => dispatch(pushNotification(payload)),
    updateAttentionNumbers: payload => dispatch(updateAttentionNumbers(payload)),
    updateConversation: payload => dispatch(updateConversation(payload)),
    commitSocketIONewMessageListeners: payload => dispatch(commitSocketIONewMessageListeners(payload)),
    clearSocketIONewMessageListeners: payload => dispatch(clearSocketIONewMessageListeners(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
    fireNewMessageRawData: payload => dispatch(fireNewMessageRawData(payload)),
    changeWhoIAm: payload => dispatch(changeWhoIAm(payload)),
    updateChatTyping: payload => dispatch(updateChatTyping(payload)),
    updateChatPool: payload => dispatch(updateChatPool(payload)),
    pushChatPool: payload => dispatch(pushChatPool(payload)),
});


Socket.propTypes = {
    me: PropTypes.object,
    clientChain: PropTypes.array.isRequired,
    isSocketNewMessageListenersLoadedFully: PropTypes.bool.isRequired,
    commitSocketIONewMessageListeners: PropTypes.func.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    updateConversation: PropTypes.func.isRequired,
    filterConversation: PropTypes.string.isRequired,
    pushNotification: PropTypes.func.isRequired,
    fireNewMessageRawData: PropTypes.func.isRequired,
    changeWhoIAm: PropTypes.func.isRequired,
    clearSocketIONewMessageListeners: PropTypes.func.isRequired,
    updateChatTyping: PropTypes.func.isRequired,
    updateChatPool: PropTypes.func.isRequired,
    pushChatPool: PropTypes.func.isRequired,
    sendMessage: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(Socket);
