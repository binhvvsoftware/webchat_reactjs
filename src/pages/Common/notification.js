import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import Image from '../../components/Image';
import { updateNotification } from '../../actions';
import history, { getLocationHistory } from '../../utils/history';
import UserService from '../../services/user';
import { getUserFromCache } from '../../utils/cache';

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: null,
            timeoutHide: 5000,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.notification.length) {
            const location = getLocationHistory();
            if (location.current.pathname.indexOf('/chat') === 0) {
                return;
            }
            const notification = nextProps.notification[nextProps.notification.length - 1];
            const user = getUserFromCache(notification.sender);

            if (user) {
                this.handleUpdateMessage(nextProps.notification, user);

                return;
            }

            UserService.getUserInfo(notification.sender).then((response) => {
                this.handleUpdateMessage(nextProps.notification, response.data);
            });
        }
    }

    handleUpdateMessage = (notificationList, user) => {
        if (notificationList.length === 0) {
            return;
        }

        const notification = notificationList[notificationList.length - 1];
        notification.message = notification.message.replace('%username%', user.user_name);
        clearTimeout(this.clearNotification);
        this.setState({
            notification: null,
        }, () => {
            this.setState({
                notification: Object.assign({}, user, notification),
            }, () => {
                this.clearNotification = setTimeout(this.handleClearNotification, this.state.timeoutHide);
            });
        });
    };

    handleClearNotification = () => {
        this.props.updateNotification({ data: [] });
        this.setState({
            notification: null,
        });
    };

    handleClick = () => {
        if (this.state.notification) {
            this.setState({
                notification: null,
            });
            history.push(`/chat/${this.state.notification.sender}`);
        }
    };

    render() {
        const { notification } = this.state;

        const div = (
            <div className="notification-bottom">
                <div className="notification-bottom__icon">
                    <i className="icon icon-chat-pink"/>
                </div>
                <div className="notification-bottom__message">
                    { notification && notification.message }
                </div>
                <div className="notification-bottom__arrow">
                    <i className="icon icon-chevron-right-white"/>
                </div>
            </div>
        );
        return (
            <>
                <CSSTransitionGroup
                    transitionName="notification-transition"
                    transitionEnterTimeout={ 500 }
                    transitionLeaveTimeout={ 300 }
                >
                    { notification ? (
                        <div className="notification-bottom" onClick={ this.handleClick }>
                            <div className="notification-bottom__icon">
                                <div className="notification-bottom__avatar">
                                    <Image isAvatar imageId={ notification.ava_id } isSystem={ notification.isSystem }/>
                                </div>
                            </div>
                            <div className="notification-bottom__message">
                                { notification.message }
                            </div>
                            <div className="notification-bottom__arrow">
                                <i className="icon icon-chevron-right-white"/>
                            </div>
                        </div>
                    ) : null }
                </CSSTransitionGroup>
            </>
        );
    }
}

const mapStateToProps = state => ({
    notification: state.main.notification,
});

const mapDispatchToProps = (dispatch) => ({
    updateNotification: (payload) => dispatch(updateNotification(payload)),
});

Notification.propTypes = {
    notification: PropTypes.array.isRequired,
    updateNotification: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Notification);

