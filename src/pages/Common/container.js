import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import WebView from '../../components/WebView';
import Gift from './gift';
import Download from './download';
import Notification from './notification';
import BuyPoint from './buypoint';
import Socket from './socket';
import './index.scss';

class Common extends Component {
    componentWillReceiveProps(nextProps) {
        const sidebarOpen = nextProps.isOpenSidebarMenu || nextProps.isOpenChatConversation;
        const className = nextProps.isOpenWebView ? 'sidebar-web-view_open' : 'sidebar_open';
        const { classList } = document.body;

        if (sidebarOpen) {
            classList.remove('sidebar_open', 'sidebar-web-view_open');
            classList.add(className);
        } else {
            classList.remove('sidebar_open', 'sidebar-web-view_open');
        }
    }

    render() {
        return (
            <>
                <Gift/>
                <WebView/>
                <Download/>
                <Notification/>
                <BuyPoint/>
                <Socket/>
                {
                    this.props.isLoadingFavorite

                    && <div className="loading-full"/>
                }
            </>
        );
    }
}

const mapStateToProps = state => ({
    isLoadingFavorite: state.favorite.isLoadingFavorite,
    isOpenSidebarMenu: state.main.isOpenSidebarMenu,
    isOpenChatConversation: state.main.isOpenChatConversation,
    isOpenWebView: state.webView.isOpenWebView,
});

const mapDispatchToProps = () => ({});

Common.propTypes = {
    isOpenSidebarMenu: PropTypes.bool.isRequired,
    isOpenChatConversation: PropTypes.bool.isRequired,
    isLoadingFavorite: PropTypes.bool.isRequired,
    isOpenWebView: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Common);

