import React, { Component } from 'react';
import { connect } from 'react-redux';
import './index.scss';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { buyPoint } from '../../services/webview';
import {
    TITLE_BUYPOINT,
    CONTENT_BUYPOINT,
    BUTTON_BUYPOINT_YES,
    BUTTON_BUYPOINT_NO,
} from '../../constants/messages';
import { openWebView } from '../../actions/webview';
import { closeBuyPoint } from '../../actions/index';

class BuyPoint extends Component {

    componentWillReceiveProps(nextProps){
        if (!nextProps.isOpenBuyPoint) {
            return;
        }

        const contentBuyPoint = this.replaceText(CONTENT_BUYPOINT);
        Popup.create({
            title: TITLE_BUYPOINT,
            content: contentBuyPoint,
            buttons: {
                left: [{
                    text: BUTTON_BUYPOINT_NO,
                    className: 'ok',
                    action: () => {
                        this.props.closeBuyPoint();
                        Popup.close();
                    },
                }],
                right: [{
                    text: BUTTON_BUYPOINT_YES,
                    className: 'ok',
                    action: () => {
                        this.props.openWebView({
                            url: buyPoint(),
                            title: 'ホイント第入',
                            backButton: true,
                        });
                        this.props.closeBuyPoint();
                        Popup.close();
                    },
                }],
            },
        });

    }

    replaceText(text){
        return text.split('\n').map((item, key) => <span key={key}>{item}<br/></span>)
    }

    render() {
        return (
            <></>
        )

    }
}

BuyPoint.propTypes = {
    openWebView: PropTypes.func.isRequired,
    isOpenBuyPoint: PropTypes.bool.isRequired,
    closeBuyPoint: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    isOpenBuyPoint: state.main.isOpenBuyPoint,
});

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
    closeBuyPoint: payload => dispatch(closeBuyPoint(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BuyPoint);
