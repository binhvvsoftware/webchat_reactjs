import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Popup from 'react-popup';
import UserService from '../../services/user';
import Auth from '../../services/authentication';
import BannedWordService from '../../services/bannedWord';
import { aboutPayment } from '../../services/webview';
import MainLayout from '../../components/MainLayout';
import Loading from '../../components/Loading';
import Image from '../../components/Image';
import {
    PENDDING_APPEAL_COMMENT_NOTI,
    BANNED_WORDS_MESSAGE_FORMAT,
} from '../../constants/messages';
import storage from '../../utils/storage';
import { numberFormat } from '../../utils/helpers';
import './index.scss';
import { openWebView } from '../../actions/webview';

class MyPage extends Component {
    constructor(props) {
        super(props);

        const user = storage.getUserInfo();

        this.state = {
            user,
            appealComment: user.appeal_comment || '',
            bannedWord: [],
            isUpdateAppeal: false,
            loading: true,
        };
    }

    handleOpenWebView = (url, title) => {
        this.props.openWebView({
            url,
            title,
            backButton: true,
        });
    };

    componentDidMount() {
        Auth.getInfo()
            .then(response => {
                this.setState({
                    user: response.data,
                    appealComment: response.data.appeal_comment || '',
                    loading: false,
                });
            });

        BannedWordService.getBannedWord().then(response => {
            this.setState({
                bannedWord: response.data.appeal_comment,
            });
        });
    }

    handleCheckBannedWord(value) {
        const checkBannedWord = [];
        const { bannedWord } = this.state;

        bannedWord.forEach(word => {
            if (value.indexOf(word) !== -1) {
                checkBannedWord.push(word);
            }
        });

        return checkBannedWord;
    }

    updateAppeal(e) {
        const comment = e.target.value.replace(/[\r\n\v]+/g, '');
        if (comment.length > 40) {
            return;
        }
        if (e.key === "Enter") {
            this.handleUpdateAppeal();
        }
        const { user } = this.state;

        this.setState({
            appealComment: comment,
            isUpdateAppeal: user.appeal_comment !== comment,
        });
    }

    handleUpdateAppeal() {
        const { appealComment } = this.state;
        const checkBannedWordAppeal = this.handleCheckBannedWord(appealComment.trim());

        if (checkBannedWordAppeal.length !== 0) {
            Popup.alert(BANNED_WORDS_MESSAGE_FORMAT.replace('%s', checkBannedWordAppeal[0]));
            return;
        }

        UserService.updateAppealComment(appealComment)
            .then(response => {
                if (!response.data.isApprove) {
                    Popup.alert(PENDDING_APPEAL_COMMENT_NOTI);
                }
            });
        this.setState({
            isUpdateAppeal: false,
        });
    }

    render() {
        const { user, appealComment } = this.state;
        const { attentionNumbers } = this.props;
        const lengthComment = 40 - appealComment.length;
        const contentHeader = (
            <div className="my-page__cover">
                <Link className="my-page__avatar" to="/edit-profile">
                    <Image isAvatar imageId={user.ava_id} />
                </Link>
                <div className="my-page__user-name">{user.user_name}</div>
                <div className="my-page__appeal-comment">
                    <textarea wrap="soft" maxLength="40" placeholder="自分のアピールポイントをつぶやこう !" value={appealComment} onChange={(e) => this.updateAppeal(e)} onKeyUp={(e) => this.updateAppeal(e)}/>
                    <div className="my-page__appeal-count" onClick={() => this.handleUpdateAppeal()}>
                        {lengthComment}
                        {this.state.isUpdateAppeal ? <span className="icon icon-send" /> : ''}
                    </div>
                </div>
            </div>
        );

        return ( !this.state.loading &&
            <MainLayout className="my-page" title="マイページ" hasFooter={false} contentHeader={contentHeader}>
                <Loading isLoading={this.state.loading} />
                <div className="my-page__options">
                    <div className="my-page__row">
                        <Link className="my-page__item" to={{ pathname: '/timeline', state: { typeTimeline: 'owner' } }}>
                            <span className="my-page__item-content">
                                {attentionNumbers.buzz}
                            </span>
                            <span className="my-page__item-title">タイムライン</span>
                        </Link>
                        <div className="my-page__item" onClick={() => this.handleOpenWebView(aboutPayment())}>
                            <strong className="my-page__item-content">
                                { numberFormat(user.point) }pts
                            </strong>
                            <span className="my-page__item-title">保持ポイント</span>
                        </div>
                        <Link className="my-page__item" to="/my-gallery">
                            <span className="my-page__item-content">{attentionNumbers.backstage}</span>
                            <span className="my-page__item-title">ヒミツの写真</span>
                        </Link>
                    </div>
                    <div className="my-page__row">
                        <Link className="my-page__item" to="/edit-profile">
                            <span className="icon icon-user" />
                            <span className="my-page__item-title">プロフィール</span>
                        </Link>
                        <Link className="my-page__item" to="/online-alert">
                            <span className="icon icon-report-ol-notify" />
                            <span className="my-page__item-title">オンライン通知</span>
                        </Link>
                        <Link className="my-page__item" to="/template">
                            <span className="icon icon-template" />
                            <span className="my-page__item-title">テンプレート</span>
                        </Link>
                    </div>
                    <div className="my-page__row">
                        <Link className="my-page__item" to="/call-log">
                            <span className="icon icon-call-log" />
                            <span className="my-page__item-title">通話ログ</span>
                        </Link>
                        <div className="my-page__item" onClick={() => this.handleOpenWebView(aboutPayment())}>
                            <span className="icon icon-point" />
                            <span className="my-page__item-title">ポイント</span>
                        </div>
                        <Link className="my-page__item" to="/call">
                            <span className="icon icon-mobile" />
                            <span className="my-page__item-title">通話アプリ接続</span>
                        </Link>
                    </div>
                </div>

                <Link className="form__control" to={{ pathname: user.profile_url, state: { user } }}>
                    <div className="form__label">相手から見た自分のプロフィールを確認</div>
                    <div className="form__group-input">
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </Link>
            </MainLayout >
        );
    }
}

MyPage.propTypes = {
    openWebView: PropTypes.func.isRequired,
    attentionNumbers: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
});
const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyPage);
