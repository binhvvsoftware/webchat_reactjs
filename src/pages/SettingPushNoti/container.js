import React, { Component } from 'react';
import SettingService from '../../services/settings';
import Header from '../../components/Header';
import history from '../../utils/history';
import SelectBox from '../../components/SelectBox';
import { SETTING_PUSH_NOTI } from '../../constants/setting_push_noti';
import './index.scss';

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myAlert : 0,
            notiBuzz : 0,
            chat : 0,
            loading: true,
        };
    }

    componentDidMount(){
        SettingService.getNotiSetting()
            .then(response => {
                this.setState({
                    myAlert : response.data.setting.andg_alt,
                    notiBuzz : response.data.setting.noti_buzz,
                    chat : response.data.setting.chat + 1,
                    loading: false,
                })
            })
    }

    handleRouter() {
        history.push("/settings");
    }

    changeSetting(e,target){

        const value = e.target.checked ? 1 : 0;

        this.setState({
            [target] : value,
        })
    }

    handleSettingChat = (value) => {
        this.setState({
            chat : value,
        })
    }

    handleSubmit(){
        const data = {
            api : 'noti_set',
            noti_buzz : this.state.notiBuzz,
            andg_alt : this.state.myAlert,
            chat : this.state.chat -1,
        }

        SettingService.setNotiSetting(data)
            .then(() => {
                history.push("/settings")
            })
    }

    render() {

        return ( !this.state.loading &&
            <div className="setting-push-page">
                <Header showMenu title="設定">
                    <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.handleRouter()}>
                        <span className="icon icon-arrow-left">&nbsp;</span>
                    </span>
                    <button type = "button" className = "btn-save-noti" onClick = {() => this.handleSubmit()}>完了</button>
                </Header>
                <main className="k-main">
                    <div className="form-group">
                        <label className="form-group-left">Kyuunからのお知らせ</label>
                        <div className="form-group-right">
                            <label className="switch">
                                <input type="checkbox" className="switch__input" checked = {this.state.myAlert === 1} onChange = {(e) => this.changeSetting(e,"myAlert")}/>
                                <div className="switch__toggle">
                                    <div className="switch__handle"></div>
                                </div>
                            </label>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="form-group-left">タイムライン投稿（お気に入りのみ）</label>
                        <div className="form-group-right">
                            <label className="switch">
                                <input type="checkbox" className="switch__input" checked = {this.state.notiBuzz === 1} onChange = {(e) => this.changeSetting(e,"notiBuzz")}/>
                                <div className="switch__toggle">
                                    <div className="switch__handle"></div>
                                </div>
                            </label>
                        </div>
                    </div>

                    <div className="form-group form-setting-noti">
                        <label className="form-group-left">プッシュ通知</label>
                        <div className="form-group-right">
                            <SelectBox options={SETTING_PUSH_NOTI} selected={this.state.chat} onSelected={this.handleSettingChat} />
                        </div>
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                    <div className="footer-setting-noti">アプリを強制終了すると<br/> 通知が遅れたり受信できない場合があります。</div>
                </main>
            </div>
        )
    }
}

export default Settings;
