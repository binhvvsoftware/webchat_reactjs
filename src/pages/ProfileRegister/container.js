/* eslint-disable */
import React, { Component } from 'react';
import moment from 'moment/moment';
import DatePicker from 'react-mobile-datepicker';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { TextInput } from 'react-native-web';
import storage, { keys } from '../../utils/storage';
import { checkOrientationImage } from '../../utils/helpers';
import history, { getLocationHistory } from '../../utils/history';
import Header from '../../components/Header';
import SelectBox from '../../components/SelectBox';
import AreaRegion from '../../components/AreaRegion';
import Image from '../../components/Image';
import MediaService from '../../services/media';
import { JOBS_MEN } from '../../constants/job_constant';
import { AREA_REGION } from '../../constants/region_constant';
import {
    CHECK_NAME_IS_EMPTY_MESSAGE,
    USER_NAME_IS_TOO_LONG_MESSAGE,
    USER_NAME_IS_INVALID,
    ALERT_MUST_REQUIRE,
    UPLOAD_AVATAR_SUCCESSFULLY_CONTENT,
    UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
    UPLOAD_AVATAR_SUCCESSFULLY_TITLE,
    ALERT_MESSAGE_UPLOAD_IMAGE_ERROR,
    ALERT_TEXT_DONT_VIEW,
    TABLE_CELL_BIRTHDAY_TITLE,
    PLEASE_SELECT,
    BIRTHDAY_IS_EMPTY,
    YOU_FINISHED_REGIST_MSG,
    YOU_FINISH_REGIST_MSG_TITLE,
} from '../../constants/messages';
import UserService from '../../services/user';
import AuthService from '../../services/authentication';
import BannedWordService from '../../services/bannedWord';
import './index.scss';

class RegisterProfile extends Component {
    constructor(props) {
        super(props);
        const location = getLocationHistory();
        const isPageEditProfile = location.current.pathname === '/edit-profile';
        const birthday = moment(moment().subtract(20, 'years').format('YYYY-12-31')).add(1, 'days').toDate();

        let user;
        try {
            // Convert user from login fb
            user = props.location.state.user || {};
        } catch (e) {
            user = {};
        }

        this.state = {
            title: isPageEditProfile ? 'プロフィールの入力' : 'プロフィール作成',
            user,
            birthday,
            appeal_comment: '',
            appeal_comment_max_length: 40,
            user_name_max_length: 14,
            user_name: user.name || '',
            job: '',
            type_of_men: '',
            main_exit_time: '',
            region: '',
            self_introduction: '',
            charm_point: '',
            isShowAreaRegion: false,
            fileData: {},
            loading: true,
            bannedWord: [],
            checkBannedWord: [],
            displayDateOfBirth: '',
            isPageEditProfile,
        };
        this.handleFileChange = this.handleFileChange.bind(this);
    }

    componentDidMount() {
        const { isPageEditProfile } = this.state;
        const registerToken = storage.get(keys.TOKEN_REGISTER);
        const token = storage.getToken();

        if (!registerToken && !token && !isPageEditProfile) {
            history.push('/register', { user: {} });
            return;
        }

        const location = getLocationHistory();
        if (['', '/register'].indexOf(location.previous.pathname) === -1 && !this.state.isPageEditProfile) {
            Popup.alert(YOU_FINISHED_REGIST_MSG, YOU_FINISH_REGIST_MSG_TITLE);
        }

        if (isPageEditProfile) {
            AuthService.getInfo()
                .then(res => {
                    this.setState({
                        user: res.data,
                        appeal_comment: res.data.appeal_comment || '',
                        user_name: res.data.user_name,
                        type_of_men: res.data.type_of_man,
                        charm_point: res.data.fetish,
                        self_introduction: res.data.abt,
                        main_exit_time: res.data.join_hours,
                        region: res.data.region,
                        job: res.data.job === -1 ? '' : res.data.job,
                        displayDateOfBirth: moment(res.data.bir).format('YYYY年MM月DD日'),
                        loading: false,
                    });
                });
        } else {
            this.setState({
                loading: false,
            });
        }

        BannedWordService.getBannedWord()
            .then(response => {
                this.setState({
                    bannedWord: response.data,
                });
            });
    }

    handleChangeAppealComment(text) {
        const lengthComment = text.target.value.length;
        if (lengthComment > 40) {
            return;
        }
        this.setState({ appeal_comment: text.target.value });
    }

    handleChangeNickname(text) {
        if (text.target.value.replace(/\s/g, '').length > 14) {
            return;
        }
        this.setState({ user_name: text.target.value.replace(/\s/g, '') });
    }

    handleChangeSelfIntroduction(text) {
        if (text.target.value.replace(/\s/g, '').length > 200) {
            return;
        }
        this.setState({ self_introduction: text.target.value });
    }

    showAreaRegion = () => {
        this.setState({ isShowAreaRegion: true });
    };

    handleSelectAreaRegion = (region) => {
        this.setState({ region, isShowAreaRegion: false });
    };

    handleSelectJob = (job) => {
        this.setState({ job, isShowAreaRegion: false });
    };

    handleClickDatePicker = () => {
        this.setState({ isOpenDatePicker: true });
    };

    handleCancelDatePicker = () => {
        this.setState({ isOpenDatePicker: false });
    };

    handleSelectDatePicker = (birthday) => {
        this.setState({
            birthday,
            isOpenDatePicker: false,
            displayDateOfBirth: moment(birthday).format('YYYY年MM月DD日'),
        });
    };

    handleCheckBannedWord(value, target) {
        const { checkBannedWord } = this.state;
        const { bannedWord } = this.state;

        if (!value) {
            return;
        }

        if (target === 'appeal_comment') {
            bannedWord.appeal_comment.forEach(word => {
                if (value.indexOf(word) !== -1) {
                    checkBannedWord.push(word);
                }
            });
        } else {
            bannedWord.profile.forEach(word => {
                if (value.indexOf(word) !== -1) {
                    checkBannedWord.push(word);
                }
            });
        }
        this.setState({
            checkBannedWord,
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.state.user_name === '') {
            return Popup.alert(CHECK_NAME_IS_EMPTY_MESSAGE);
        }

        if (this.state.user_name.length > this.state.user_name_max_length) {
            return Popup.alert(USER_NAME_IS_TOO_LONG_MESSAGE);
        }

        if (!/^[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf\sa-z0-9-!@#$%^&*(),.?":{}|<>\-\/「」¥\[\]+_€';=\\]+$/gi.test(this.state.user_name)) {
            return Popup.alert(USER_NAME_IS_INVALID);
        }

        if (this.state.region === '') {
            return Popup.alert(ALERT_MUST_REQUIRE);
        }

        if (this.state.displayDateOfBirth === '') {
            return Popup.alert(BIRTHDAY_IS_EMPTY);
        }

        this.handleCheckBannedWord(this.state.appeal_comment, 'appeal_comment');
        this.handleCheckBannedWord(this.state.user_name);
        this.handleCheckBannedWord(this.state.type_of_men);
        this.handleCheckBannedWord(this.state.charm_point);
        this.handleCheckBannedWord(this.state.self_introduction);

        if (this.state.checkBannedWord.length !== 0) {
            Popup.alert(`("${this.state.checkBannedWord[0]}")は使えない単語です。再入力してください。`);
            this.setState({
                checkBannedWord: [],
            });
            return '';
        }

        const params = {
            gender: 0,
            user_name: this.state.user_name,
            auto_region: 0,
            abt: this.state.self_introduction,
            type_of_man: this.state.type_of_men,
            fetish: this.state.charm_point,
            job: this.state.job,
            region: this.state.region,
            join_hours: this.state.main_exit_time,
            husband_status: '',
            marital_status: '',
            cute_type: '',
            gps_adid: '',
            style: '',
            hobby: '',
            bir: moment(this.state.displayDateOfBirth, 'YYYY/MM/DD').format('YYYYMMDD'),
        };

        if (!this.state.isPageEditProfile) {
            params.token = storage.get(keys.TOKEN_REGISTER);
        }

        this.setState({ loading: true });
        (async () => {
            try {
                const response = await UserService.updateInfo(params);
                if (!this.state.isPageEditProfile) {
                    storage.remove(keys.GENDER);
                    storage.remove(keys.BIRTHDAY);
                    storage.remove(keys.REGISTER_TIME);
                    storage.remove(keys.TOKEN_REGISTER);
                    storage.setToken(response.token || params.token);
                }

                let isApproveImage = true;
                const user = response.data;

                if (this.state.isPageEditProfile) {
                    await UserService.updateAppealComment(this.state.appeal_comment);
                }

                let { preview } = this.state.fileData;
                const { orientation } = this.state.fileData;

                if (this.state.user.fb_id && !preview) {
                    const responseAvatar = await MediaService.getBase64FromFbId(this.state.user.fb_id);
                    preview = responseAvatar.data;
                }

                if (preview) {
                    const responseUpload = await MediaService.uploadImageBase64(preview, 3, orientation);
                    user.ava_id = responseUpload.data.image_id;
                    isApproveImage = responseUpload.data.isApprove;
                }

                this.setState({ loading: false });

                if (this.state.isPageEditProfile) {
                    this.handleUpdateSuccess(user, isApproveImage);

                    return;
                }

                if (this.props.location.state && this.props.location.state.bckstg_time) {
                    user.bckstg_time = this.props.location.state.bckstg_time;
                }

                storage.setUserInfo(user);
                this.handleRegisterSuccess(user, isApproveImage);
            } catch (err) {
                console.error(err); // eslint-disable-line
                this.setState({ loading: false });
                if (err.code === 15) {
                    Popup.alert(BIRTHDAY_IS_EMPTY);
                    return;
                }
                Popup.alert(ALERT_MESSAGE_UPLOAD_IMAGE_ERROR);
            }
        })();
        return true;
    }

    handleRegisterSuccess = (user, isApproveImage) => {
        if (isApproveImage) {
            history.push('/home');
            return;
        }

        Popup.create({
            title: UPLOAD_AVATAR_SUCCESSFULLY_TITLE,
            content: UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
            buttons: {
                left: [{
                    text: 'はい',
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        history.push('/home');
                    },
                }],
            },
        });
    };

    handleUpdateSuccess = (user, isApproveImage) => {
        if (isApproveImage) {
            Popup.create({
                content: UPLOAD_AVATAR_SUCCESSFULLY_CONTENT,
                buttons: {
                    left: [{
                        text: '見ない',
                        className: 'ok',
                        action: () => {
                            Popup.close();
                            history.push('/my-page');
                        },
                    }],
                    right: [{
                        text: '見る',
                        className: 'ok',
                        action: () => {
                            Popup.close();
                            history.push(`/profile/${user.user_id}`, { user });
                        },
                    }],
                },
            });
            return;
        }

        Popup.create({
            title: UPLOAD_AVATAR_SUCCESSFULLY_TITLE,
            content: UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
            buttons: {
                left: [{
                    text: 'OK',
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        Popup.create({
                            content: UPLOAD_AVATAR_SUCCESSFULLY_CONTENT,
                            buttons: {
                                left: [{
                                    text: ALERT_TEXT_DONT_VIEW,
                                    className: 'ok',
                                    action: () => {
                                        Popup.close();
                                        history.push('/my-page');
                                    },
                                }],
                                right: [{
                                    text: '見る',
                                    className: 'ok',
                                    action: () => {
                                        Popup.close();
                                        history.push(`/profile/${user.user_id}`, { user });
                                    },
                                }],
                            },
                        });
                    },
                }],
            },
        });
    };

    handleFileChange(event) {
        this.setState({ loading: true });
        checkOrientationImage(event.target.files[0], (imgBase64, orientation) => {
            this.setState({ fileData: { preview: imgBase64, orientation }, loading: false });
        });
    }

    handleRouter = () => {
        const location = getLocationHistory();
        if (['/callback-login', '/register', ''].indexOf(location.previous.pathname) !== -1) {
            return history.push('/register', { user: {} });
        }

        return history.checkAndBack();
    };

    render() {
        const maxDate = moment(moment().subtract(18, 'years').format('YYYY-12-31')).add(1, 'days').toDate();
        const minDate = moment().subtract(120, 'years').toDate();
        const dateConfig = {
            'year': {
                format: 'YYYY年',
                caption: '年',
                step: 1,
            },
            'month': {
                format: 'MM月',
                caption: '月',
                step: 1,
            },
            'date': {
                format: 'DD日',
                caption: '日',
                step: 1,
            },
        };
        const regionName = AREA_REGION[this.state.region] || <span className="grey-text">{ PLEASE_SELECT }</span>;
        let previewImage = {};

        if (this.state.fileData.preview || this.state.user.picture) {
            previewImage = { backgroundImage: `url(${this.state.fileData.preview || this.state.user.picture})` };
        }

        const classRotateImage = `rotate-${this.state.fileData.orientation || 0}`;

        const appealComment = (
            <div className="status-edit-profile">
                <TextInput
                    maxLength={ this.state.appeal_comment_max_length }
                    multiline
                    numberOfLines={ 2 }
                    autoCapitalize="sentences"
                    value={ this.state.appeal_comment }
                    placeholder="自分のアピールポイントをつぶやこう!アダルトな内容や規約に違反する内容は公開されません。"
                    onChange={ (text) => this.handleChangeAppealComment(text) }/>
                <span
                    className="number">{ this.state.appeal_comment ? this.state.appeal_comment_max_length - this.state.appeal_comment.length : this.state.appeal_comment_max_length }</span>
            </div>
        );

        return (
            <div className="register-profile">
                { this.state.loading && <div className="register-profile__loading"/> }
                <Header title={ this.state.title }>
                    <span className="nav-bar__icon nav-bar__icon--left" onClick={ this.handleRouter }>
                        <span className="icon icon-arrow-left"/>
                    </span>
                </Header>

                <main className="k-main">
                    { this.props.location.pathname === '/register-profile' &&
                    <div className="header-breadcrumb">
                        <ul className="breadcrumb">
                            <li>
                                <div className="number-active">1</div>
                                ユーザー情報入力
                            </li>
                            <li>
                                <div className="number-active">2</div>
                                プロフィール作成
                            </li>
                            <li>
                                <div className="number-default">3</div>
                                登録完了
                            </li>
                        </ul>
                    </div> }
                    <form className="form" onSubmit={ (e) => this.handleSubmit(e) }>
                        <div className="form-upload">
                            <div className="upload-img">
                                <input type="file" name="avatar" accept="image/*" ref={ input => {
                                    this.fileInput = input;
                                } } onChange={ this.handleFileChange }/>
                                {
                                    this.state.user.ava_id && !previewImage.backgroundImage ? (
                                        <div className="edit-profile-avatar" onClick={ () => this.fileInput.click() }>
                                            <Image isAvatar imageId={ this.state.user.ava_id }/>
                                        </div>
                                    ) : (
                                        <div
                                            className={ `preview-avatar ${classRotateImage} ${ previewImage.backgroundImage ? 'is-circle' : '' } ` }
                                            style={ previewImage } onClick={ () => this.fileInput.click() }/>
                                    ) }
                            </div>
                            { this.state.isPageEditProfile ? appealComment : <span className="text">プロフィール写真<br/>(あなたの写真を登録してね)</span> }
                        </div>
                        <div className="form-group-text">
                            <span className="text-require">（<span className="require-text">※</span>）は必須項目です。</span>
                        </div>

                        <div>
                            <div className="form__control border-t">
                                <div className="form__label">ハンドルネーム <span className="require-text">※</span></div>
                                <div className="form__group-input pink-text">
                                    <TextInput
                                        className="text-right"
                                        value={ this.state.user_name }
                                        placeholder="なまえの入力"
                                        maxLength={ this.state.user_name_max_length }
                                        onChange={ (text) => this.handleChangeNickname(text) }/>
                                </div>
                            </div>

                            <div className="form__control">
                                <div className="form__label">{ TABLE_CELL_BIRTHDAY_TITLE }<span
                                    className="require-text">※</span></div>
                                <div className="form__group-input pink-text" tabIndex="0" role="button"
                                     onKeyPress={ this.handleClickDatePicker } onClick={ this.handleClickDatePicker }>
                                    <DatePicker
                                        value={ this.state.birthday }
                                        isOpen={ this.state.isOpenDatePicker }
                                        onSelect={ this.handleSelectDatePicker }
                                        onCancel={ this.handleCancelDatePicker }
                                        confirmText="完了"
                                        cancelText="キャンセル"
                                        dateConfig={ dateConfig }
                                        min={ minDate }
                                        max={ maxDate }
                                        showHeader={ false }
                                        theme="ios"
                                        defaultShow={ minDate }
                                    />
                                    <span
                                        className="nameSex">{ this.state.displayDateOfBirth }</span>
                                    <span className="form__icon-right">&nbsp;</span>
                                </div>
                            </div>

                            <div className="form__control">
                                <div className="form__label">地域 <span className="require-text">※</span></div>
                                <div className="form__group-input pink-text" onClick={ this.showAreaRegion }>
                                    { regionName }
                                    <span className="form__icon-right">&nbsp;</span>
                                </div>
                            </div>

                            <div className="form__control">
                                <div className="form__label">職業</div>
                                <div className="form__group-input is_select">
                                    <SelectBox options={ JOBS_MEN }
                                               textEmpty={ PLEASE_SELECT } selected={ this.state.job }
                                               onSelected={ this.handleSelectJob }>
                                    </SelectBox>
                                    <span className="form__icon-right">&nbsp;</span>
                                </div>
                            </div>

                            <div className="form__control multiple_line mb_30">
                                <div className="form__label">自己紹介</div>
                                <div className="form__group-input">
                                    <TextInput
                                        multiline
                                        numberOfLines={ 4 }
                                        value={ this.state.self_introduction }
                                        placeholder="自由にこ記入ください。"
                                        maxLength={ 200 }
                                        onChange={ (e) => this.handleChangeSelfIntroduction(e) }/>
                                </div>
                            </div>
                        </div>
                        <div className='btn-submit-form'>
                            <button type="button" className="btn-custom btn-active"
                                    onClick={ (e) => this.handleSubmit(e) }>完了
                            </button>
                        </div>
                    </form>
                </main>

                <AreaRegion selected={ this.state.region } isOpen={ this.state.isShowAreaRegion }
                            onSelected={ this.handleSelectAreaRegion }/>
            </div>
        );
    }
}


RegisterProfile.propTypes = {
    location: PropTypes.object,
};
export default RegisterProfile;
