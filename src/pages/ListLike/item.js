import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import history from '../../utils/history';

class ListLikeItem extends Component {

    handleGotoProfile = (e) => {
        e.preventDefault();
        const { like } = this.props;
        history.push(`/profile/${like.noti_user_id}`, { state: { user: like } });
    };

    render() {
        const { like } = this.props;

        return (
            <Link to={ { pathname: `/buzz-detail/${like.noti_buzz_id}` } }>
                <div className="like-noti-item">
                    <div className="left">
                        <span className="icon icon-fav-like"></span>
                    </div>
                    <div className="center">
                        <span className="text-noti">
                            <span onClick={this.handleGotoProfile} className="pink-text custom-text-like cursor-pointer">{ like.noti_user_name }</span>
                            さんがあなたの投稿に「いいね」をしました
                        </span>
                        <span className="time">{ like.time_like }</span>
                    </div>

                    <div className="right">
                        <span className="icon icon-chevron-right"></span>
                    </div>
                </div>
            </Link>
        );
    }
}

ListLikeItem.propTypes = {
    like: PropTypes.object,
};

ListLikeItem.defaultProps = {
    like: {},
};

export default ListLikeItem;
