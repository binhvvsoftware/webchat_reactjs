import React, { Component } from 'react';
import ListLikeService from '../../services/listLike';
import MainLayout from '../../components/MainLayout';
import ListLikeItem from './item';
import './index.scss';

class ListLike extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listLike: [],
            loading: true,
        };
    }

    componentDidMount() {
        ListLikeService.getListLike()
            .then(response => {
                this.setState({
                    listLike: response.data,
                    loading: false,
                });
            });
    }

    render() {
        const { listLike, loading } = this.state;

        const displayListLike = listLike.map((like, index) => <ListLikeItem like={ like } key={ index }/>);

        return (
            <MainLayout
                className="list-like"
                title="いいね"
                hasFooter={ false }
                hasSidebarLeft={ false }
                hasBackButton
                backFunction='/home'
            >
                <div className="list-like">
                    { this.state.listLike.length === 0 && !loading &&
                        <div className="text-default">表示出来る項目がありません。</div>
                    }
                    { loading && <div className="list-like__loading"/>}
                    { displayListLike }
                </div>
            </MainLayout>
        );
    }
}

export default ListLike;
