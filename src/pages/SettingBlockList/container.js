import React, { Component } from 'react';
import Popup from 'react-popup';
import Image from '../../components/Image';
import SettingService from '../../services/settings';
import Header from '../../components/Header';
import {
    LIST_BLOCKED_USERS_TITLE,
    UN_BLOCK_UNSER_TITLE,
    UN_BLOCK_USER_MSG,
    BLOCK_USER_MSG_TITLE,
    BLOCK_USER_MSG_CONTENT,
    ALERT_BUTTON_YES_TITLE,
} from '../../constants/messages';
import history from '../../utils/history';
import './index.scss';

class SettingBlockList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blockList: [],
            isModalUnBlock: false,
            nameUnBlock: '',
            userIdUnBlock: '',
            isUnBlock: true,
            isModalBlock: false,
            nameBlock: '',
            userIdBlock: '',
        };
    }

    componentDidMount(){
        const data = {
            api : "lst_blk",
            skip : 0,
            take : 30,
        }
        SettingService.getBlockList(data)
            .then( response => {
                this.setState({
                    blockList : response.data,
                })
                response.data.map((user,index) =>
                    this.setState({
                        [index] : true,
                    })
                )
            })
    }

    handleRouter(){
        history.push("/settings");
    }

    unBlockUser(name, userId, index) {
        const contentMessage = name + UN_BLOCK_USER_MSG;
        Popup.create({
            title: UN_BLOCK_UNSER_TITLE,
            content: contentMessage,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.handleUnBlock(userId, index)
                    },
                }],
            },
        });
    }

    handleClose() {
        this.setState({
            isModalUnBlock: false,
            isModalBlock: false,
        })
    }

    handleUnBlock(id, index) {
        const data = {
            api: "rmv_blk",
            blk_user_id: id,
        }
        SettingService.unBlockUser(data)
            .then(() => {
                this.setState({
                    [index]: false,
                })
            })
        this.handleClose();
    }

    blockUser(name, userId, index) {
        const contentMessage = BLOCK_USER_MSG_CONTENT.replace(/%s/,name);
        Popup.create({
            title: BLOCK_USER_MSG_TITLE,
            content: contentMessage,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.handleBlock(userId, index)
                    },
                }],
            },
        });
    }

    handleBlock(userId, index) {
        const data = {
            api: 'add_blk',
            req_user_id: userId,
        }
        SettingService.blockUser(data)
            .then(() => {
                this.setState({
                    [index]: true,
                })
            })
        this.handleClose();
    }

    render() {

        const displayBlockList = this.state.blockList.map((user,index) =>
            <div className="user-block" key = {index}>
                <div className="block-left">
                    <div className="user-img"><Image isAvatar imageId={ user.ava_id ? user.ava_id : null}/></div>
                </div>
                <div className="block-right">
                    <span className="user-name">{user.user_name}</span>
                    { this.state[index] ?
                        <button type = "button" className = "btn-block" onClick = {() => this.unBlockUser(user.user_name, user.user_id, index)}>変更</button>
                        :
                        <button type = "button" className = "btn-block" onClick = {() => this.blockUser(user.user_name, user.user_id, index)}>ブ口ツク</button>
                    }
                </div>
            </div>
        )

        return (
            <div className="setting-block-page">
                <Header showMenu title={LIST_BLOCKED_USERS_TITLE}>
                    <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.handleRouter()}>
                        <span className="icon icon-arrow-left">&nbsp;</span>
                    </span>
                </Header>

                <main className="k-main">
                    {this.state.blockList.length === 0 ? <div className = "main-block">現在プロツクしているユ一ザ一はいません</div> : displayBlockList}
                </main>
            </div>
        )
    }
}

export default SettingBlockList;
