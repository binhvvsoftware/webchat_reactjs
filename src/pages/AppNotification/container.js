import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import logo from '@images/logo.png';
import * as AppCall from '../../utils/app-call';
import history from '../../utils/history';
import Notice from '../../entities/Notice';
import Auth from '../../services/authentication';
import './index.scss';
import { openWebView } from '../../actions/webview';

class AppNotification extends Component {
    componentDidMount() {
        /* eslint-disable no-console */
        try {
            const params = this.props.match.params.data;
            const data = AppCall.decryption(params);

            Auth.loginEmail(data.email, data.password, true).then(() => {
                const notify = new Notice(data.data);
                console.log(notify);
                if (notify.isWebView) {
                    this.props.openWebView(notify.webView);

                    return history.push('/notification');
                }

                return history.push(notify.pathname, notify.state);
            }).catch((e) => {
                console.log(e);
                history.push('/');
            });

        } catch (e) {
            console.log(e);
            history.push('/home');
        }
    }

    render() {
        return (
            <div className="splash-page">
                <div className="splash-page__loading"/>
                <img className="splash-page__logo" src={ logo } alt=""/>
            </div>
        );
    }
}

AppNotification.propTypes = {
    match: PropTypes.object,
    openWebView: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(null, mapDispatchToProps)(AppNotification);
