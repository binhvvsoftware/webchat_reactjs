import React, { Component } from 'react';
import { connect } from "react-redux";
import Popup from 'react-popup';
import { ActivityIndicator, TextInput } from 'react-native-web';
import history from '../../utils/history';
import Image from '../../components/Image';
import { UserDisplayList } from '../../components/UserList';
import Header from '../../components/Header';
import SearchService from '../../services/search';
import { EMPTY_CONTENT_SEARCH, EMPTY_CONTENT_TITLE } from '../../constants/messages';
import { checkLoadMore } from '../../utils/helpers';
import './index.scss';

class SearchByName extends Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: '',
            userName: '',
            dataUser: [],
            hasLoadMore: true,
            loading: false,
        };
    }

    backHandler() {
        this.setState({ keyword: "", dataUser: [] })
        history.checkAndBack();
    }

    handleCallApi = () => {
        const { dataUser } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore || this.state.userName.length === 0) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            SearchService.searchName(this.state.userName, dataUser.length).then(response => {
                this.setState({
                    dataUser: dataUser.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    }

    handleLoadMore = (event) => {
        if (this.state.isOpenMessage || this.state.isOpenMenu || this.state.isOpenSearch) {
            return;
        }

        if (checkLoadMore(event)) {
            this.handleCallApi();
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        const { keyword, userName } = this.state;
        if (!keyword.length) {
            Popup.alert(EMPTY_CONTENT_SEARCH, EMPTY_CONTENT_TITLE);
            return;
        }

        if (keyword === userName) {
            return;
        }

        this.setState({
            userName: keyword,
            dataUser: [],
            hasLoadMore: true,
        }, () => {
            this.handleCallApi()
        });
    }

    handleChangeKeyword = (text) => {
        this.setState({
            keyword: text.trim(),
        });
    }

    render() {
        let displayList;

        if (this.state.dataUser.length || this.state.loading) {
            displayList = (
                <div className="user-result-wrap">
                    <UserDisplayList dataUser={this.state.dataUser} />
                    <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'} >
                        <ActivityIndicator animating color="black" size="large" />
                    </div>
                </div>
            );
        } else {
            displayList = <Image isAvatar className="background-empty" key="" />
        }

        return (
            <div className="setting-block-page">
                <Header showMenu title="ニックネームで検索">
                    <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.backHandler()}>
                        <span className="icon icon-arrow-left">&nbsp;</span>
                    </span>
                </Header>

                <main className="k-main">
                    <form className="form-search-name" onSubmit={(e) => this.handleSubmit(e)}>
                        <div className="form__control search-input">
                            <div className="form__group-input no_icon-right has_icon-left">
                                <span className="icon icon-search">&nbsp;</span>
                                <TextInput
                                    defaultValue={this.state.keyword}
                                    placeholder="こここに検索結果が表示されます"
                                    keyboardType="search"
                                    onChangeText={(text) => this.handleChangeKeyword(text)} />
                            </div>
                        </div>
                        {displayList}
                    </form>
                </main>
            </div>
        );
    }
}

export default connect(null, null)(SearchByName);
