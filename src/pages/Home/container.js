import React, { Component } from 'react';
import isMobile from 'ismobilejs';
import { ActivityIndicator } from 'react-native-web';
import debounce from 'lodash/debounce';
import './index.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import BonusPopup from '../../components/BonusPopup';
import FullScreenModal from '../../components/FullScreenModal';
import { UserDisplayGrid, UserDisplayList } from '../../components/UserList';
import Search from '../../components/Search';
import SearchService from '../../services/search';
import storage from '../../utils/storage';
import { checkLoadMore } from '../../utils/helpers';
import MainLayout from '../../components/MainLayout';
import { SEARCH_INFO, DISPLAY_LIST_GRID_USER, DISPLAY_DIALOG_SEARCH_USER } from '../../constants/storage_constant';
import BannerTop from '../../components/BannerTop';
import WarningPopup from '../WarningPopup';

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            displayGrid: storage.get(DISPLAY_LIST_GRID_USER) || true,
            isOpenSearch: storage.get(DISPLAY_DIALOG_SEARCH_USER) || false,
            searchInfo: Object.assign({
                sortBy: 'login',
                isTimeLogin: false,
                fromAge: 18,
                toAge: 120,
                region: [],
            }, storage.get(SEARCH_INFO)),
            hasLoadMore: true,
            loading: false,
            dataUser: [],
            target: 'all-user',
            showWarningPopup: false,
            updated: true,
            skip: 0,
        };
        this.takeUser = isMobile.tablet || !isMobile.any ? 50 : 24;
        this.handleCheckLoadMore = debounce(this.handleLoadMore, 300);
    }

    componentDidMount() {
        this.setState({
            displayGrid: !!(storage.get(DISPLAY_LIST_GRID_USER) || true),
        });

        this.handleCallApi();

        const user = storage.getUserInfo();

        if (user.flag_warning_popup) {
            this.setState({
                showWarningPopup: true,
            })
        }

        window.addEventListener('scroll', this.handleCheckLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleCheckLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (this.state.isOpenSearch || this.state.loading || !this.state.hasLoadMore) {
            return;
        }

        if (checkLoadMore(event)) {
            this.handleCallApi();
        }
    };

    handleDisplay = () => {
        let { displayGrid } = this.state;
        displayGrid = !displayGrid;
        storage.set(DISPLAY_LIST_GRID_USER, displayGrid);
        this.setState({
            displayGrid,
        });
    };

    handleCloseSearch = () => {
        this.setState({
            isOpenSearch: false,
        }, () => storage.set(DISPLAY_DIALOG_SEARCH_USER, false));
    };

    handleSearch = (data) => {
        storage.set(SEARCH_INFO, data);
        this.setState({
            isOpenSearch: false,
            searchInfo: data,
            dataUser: [],
            loading: false,
            hasLoadMore: true,
            scrollTop: 0,
        }, () => {
            storage.set(DISPLAY_DIALOG_SEARCH_USER, false)
            this.handleCallApi()
        });
    };

    handleCallApi = () => {
        if ((this.state.loading || !this.state.hasLoadMore)) {
            return;
        }

        let filter;
        const { dataUser } = { ...this.state };

        switch (this.state.target) {
            case 'call-waiting':
                filter = 2;
                break;
            case 'new-user':
                filter = 1;
                break;
            default:
                filter = 0;
                break;
        }

        const sortType = this.state.searchInfo.sortBy === 'login' ? 1 : 2;

        const data = {
            is_new_login: this.state.searchInfo.isTimeLogin,
            distance: 2,
            lower_age: this.state.searchInfo.fromAge,
            upper_age: this.state.searchInfo.toAge,
            skip: this.state.skip,
            region: this.state.searchInfo.region,
            filter,
            sort_type: sortType,
            is_sent_chat: 2,
            is_received_chat: 2,
        };

        this.setState({
            loading: true,
        }, () => {
            SearchService.searchInfo(data)
                .then(response => {
                    const {skip} = this.state;
                    this.setState({
                        dataUser: dataUser.concat(response.data),
                        hasLoadMore: response.hasLoadMore,
                    });
                    if (response.data.length) {
                        this.setState({
                            skip: skip + this.takeUser,
                        })
                    }
                })
                .then(() => {
                    this.setState({
                        loading: false,
                    });
                });
        });
    };

    handleGetUser = (target) => {
        if (target !== this.state.target) {
            this.setState({
                target,
                dataUser: [],
                loading: false,
                hasLoadMore: true,
                isOpenSearch: false,
                scrollTop: 0,
                skip: 0,
            }, this.handleCallApi);
        }
    };

    handleOpenModalSearch = () => {
        this.setState({
            isOpenSearch: true,
        }, () => { storage.set(DISPLAY_DIALOG_SEARCH_USER, true) });
    };

    render() {
        const viewType = this.state.displayGrid ? 'icon icon-list-view' : 'icon icon-grid-view';
        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list">
                    <li className="sub-header__item text_left" onClick={this.handleOpenModalSearch}>
                        <span className="form-search">
                            <span className="icon icon-search">&nbsp;</span>
                        </span>
                    </li>
                    <li className={`sub-header__item ${this.state.target === 'call-waiting' ? 'is_active' : ''}`} onClick={() => this.handleGetUser('call-waiting')}>
                        通話待機中
                    </li>
                    <li className={`sub-header__item ${this.state.target === 'new-user' ? 'is_active' : ''}`} onClick={() => this.handleGetUser('new-user')}>
                        新人
                    </li>
                    <li className={`sub-header__item ${this.state.target === 'all-user' ? 'is_active' : ''}`} onClick={() => this.handleGetUser('all-user')}>
                        すべて
                    </li>
                </ul>
            </section>
        );

        const contentHeader = (
            <span className="nav-bar__icon nav-bar__icon--right" onClick={this.handleDisplay}>
                <span className={viewType}>&nbsp;</span>
            </span>
        );

        return (
            <MainLayout hasFooter className="home-page has_footer" title="TOP" subHeader={subHeader} contentHeader={contentHeader}>
                {this.state.updated && <BannerTop />}
                {this.state.displayGrid ? <UserDisplayGrid dataUser={this.state.dataUser} /> : <UserDisplayList dataUser={this.state.dataUser} />}

                <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'}>
                    <ActivityIndicator animating color="black" size="large" />
                </div>

                <FullScreenModal onClose={this.handleCloseSearch} isOpen={this.state.isOpenSearch} title="ユーザー検索">
                    <Search
                        fromAge={this.state.searchInfo.fromAge}
                        toAge={this.state.searchInfo.toAge}
                        isTimeLogin={this.state.searchInfo.isTimeLogin}
                        sortBy={this.state.searchInfo.sortBy}
                        region={this.state.searchInfo.region}
                        onSearch={this.handleSearch} />
                </FullScreenModal>

                {this.state.updated && <BonusPopup />}
                {this.state.showWarningPopup && <WarningPopup />}
            </MainLayout>
        );
    }
}

export default Home;
