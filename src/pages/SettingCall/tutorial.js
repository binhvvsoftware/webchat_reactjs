import React, { Component } from 'react';
import Slider from 'react-slick';
import TutorialService from '../../services/tutorial';
import FullScreenModal from '../../components/FullScreenModal';
import storage from '../../utils/storage';
import {
    TUTORIAL_CALL_WAITING_BEGIN_CONTENT,
} from '../../constants/messages';
import './index.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const SHOW_TUTORIAL_SETTING = 'tutorial_setting';

class SettingCall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowTutorial: false,
            openTutorialStep2: false,
        };
    }

    componentDidMount() {
        const user = storage.getUserInfo();
        const isShowTutorial = (Array.isArray(user.accessed_pages) && user.accessed_pages.indexOf(2) === -1 && storage.get(SHOW_TUTORIAL_SETTING) !== false) || (!Array.isArray(user.accessed_pages) && storage.get(SHOW_TUTORIAL_SETTING) !== false);

        if (isShowTutorial) {
            this.setState({
                isShowTutorial: true,
            });
        }
    }

    finishTutorial() {
        TutorialService.closeTutorial(2)
            .then(() => {
                storage.set(SHOW_TUTORIAL_SETTING, false);
                this.setState({
                    isShowTutorial: false,
                    openTutorialStep2: false,
                });
            });
    }

    openTutorialStep2() {
        this.setState({
            isShowTutorial: false,
            openTutorialStep2: true,
        });
    }

    render() {
        const { toggleSetting } = this.state;
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
        };

        const step1 = (
            <>
                <div className="bonus-popup-setting-call__content tutorial-setting-call">
                    <div className="tutorial-setting-call__title">このページでは、<br/>通話待ち設定をすることができます。</div>
                    <div className="tutorial-setting-call__content">{ TUTORIAL_CALL_WAITING_BEGIN_CONTENT }</div>
                </div>
                <div className="group-btn-tutorial">
                    <button type="button" className="btn-next-step2" onClick={ () => this.openTutorialStep2() }>チュートリアルを見る
                    </button>
                    <button type="button" className="btn-cancel-tutorial" onClick={ () => this.finishTutorial() }>見ない
                    </button>
                </div>
            </>
        );

        const step2 = (
            <div className="bonus-popup-setting-call__content">
                <Slider { ...settings }>
                    <div className="tutorial-setting-call__step1">
                        <button type="button" className="header-tutorial-btn" disabled>
                            完了
                        </button>
                        <div className="main-content k-main">
                            <div className="form-group">
                                <label className="form-group-left">音声通話の受信許可</label>
                                <div className="form-group-right">
                                    <label className="switch">
                                        <input type="checkbox" className="switch__input"/>
                                        <div className="switch__toggle">
                                            <div className="switch__handle"/>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="form-group-left">ビデオ通話の受信許可</label>
                                <div className="form-group-right">
                                    <label className="switch">
                                        <input type="checkbox" className="switch__input"/>
                                        <div className="switch__toggle">
                                            <div className="switch__handle"/>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <span className="message-step1">チェックをオンにして、<br/>
                                <span className="hightlight">「完了」</span>を押すと、通話待ち状態になります。<br/>
                                通話待ち状態になっている間は、お相手から通話がかかってくることがあります。
                            </span>
                        </div>
                        <button type="button" className="btn-cancel-step1" onClick={ () => this.finishTutorial() }>チュートリアルを終了する
                        </button>
                    </div>
                    <div className="k-main tutorial-setting-call__step2">
                        <div className="message-step2">
                            <span><span className="hightlight">「＋」</span>を押すと、通話可能な時間帯を選択できます。<br/>より細かく設定したいときに便利な機能です。</span>
                            <span>設定方法のチュートリアルはこれでおしまいです。<br/>設定をしてお相手との通話を楽しみましょう♪</span>
                        </div>
                        <div className="form-group" style={ { backgroundColor: toggleSetting ? '#f8f8f8' : '', position: 'relative' } }>
                            <label className="form-group-left">
                                <span>通話可能時間設定</span>
                                <span className="msg-text">通話着信を受ける場合は、着信設定をしてください。</span>
                            </label>
                            <div className="form-group-right">
                                <i className="icon icon-plus"/>
                            </div>
                        </div>
                        <button type="button" className="btn-cancel-step1 btn-cancel-step2" onClick={ () => this.finishTutorial() }>通話待ち設定ページに戻る
                        </button>
                    </div>
                </Slider>
            </div>
        );

        return (
            <FullScreenModal className="bonus-popup-setting-call" isOpen={ this.state.openTutorialStep2 || this.state.isShowTutorial }>
                { this.state.isShowTutorial && step1 }
                { this.state.openTutorialStep2 && step2 }
            </FullScreenModal>
        );
    }
}

export default SettingCall;
