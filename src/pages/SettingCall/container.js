import React, {Component} from 'react';
import Popup from 'react-popup';
import SelectBox  from '../../components/SelectBox';
import history from '../../utils/history';
import { SETTING_NOTI_CALL } from '../../constants/setting_noti_call_constant';
import SettingCallService from '../../services/settingCall';
import BannedWordService from '../../services/bannedWord';
import UserService from '../../services/user';
import Header from '../../components/Header';
import Tutoria from './tutorial';
import {
    PENDDING_APPEAL_COMMENT_NOTI,
    BANNED_WORDS_MESSAGE_FORMAT,
} from '../../constants/messages';

import './index.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class SettingCall  extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '通話設定',
            backUrl: '/home',
            toggleSetting: false,
            isCallVoid: false,
            isCallVideo: false,
            lengthComment: 40,
            onCallTime: [],
            valueSettingNotify: 0,
            appealComment: '',
            isTextSettingNotify: true,
            timeWaitingNotify: '',
            isCallNow: false,
            isUpdateAppeal: false,
            bannedWord: [],
            loading: true,
        };
    }

    componentDidMount() {
        const getDataSettingCall = {
            api: 'get_call_waiting',
        };

        const getAppealComment = {
            api: 'get_appeal_comment',
        };

        SettingCallService.getCallWaiting(getDataSettingCall)
            .then(response => {
                response.data.on_call_times.map((value, key) => {// eslint-disable-line
                    this.setState({
                        [value]: true,
                    });
                    response.data.on_call_times[key] = value.toString();
                });

                this.setState({
                    isCallVoid: response.data.voice_call_waiting,
                    isCallVideo: response.data.video_call_waiting,
                    valueSettingNotify: response.data.appeal_call_waiting,
                    timeWaitingNotify: response.data.appeal_call_waiting_time,
                    isCallNow: response.data.on_call_now,
                    onCallTime: response.data.on_call_times,
                    loading: false,
                });
            });

        SettingCallService.getAppealComment(getAppealComment)
            .then(response => {
                if (Object.keys(response.data).length) {
                    this.setState({
                        appealComment: response.data.appeal_comment,
                    });
                }
            });

        BannedWordService.getBannedWord()
            .then(response => {
                this.setState({
                    bannedWord: response.data.appeal_comment,
                });
            });
    }

    handleSettingNotify = (value)  => {
        this.setState({
            valueSettingNotify: value,
        });
    }

    toggleSetting() {
        const { toggleSetting } = this.state;

        this.setState({
            toggleSetting: !toggleSetting,
        });
    }

    handleComment(e) {
        const lengthComment = e.target.value.replace(/\s/g, '').length;
        if (lengthComment > 40) {
            return;
        }
        this.setState({
            lengthComment: 40 - lengthComment,
            appealComment: e.target.value.replace(/\s/g, ''),
            isUpdateAppeal: true,
        });
    }

    handleCheckBannedWord(value) {
        const checkBannedWord = [];
        const { bannedWord } = this.state;

        bannedWord.forEach(word => {
            if (value.indexOf(word) !== -1) {
                checkBannedWord.push(word);
            }
        });
        return checkBannedWord;
    }

    handleSubmit() {
        const { onCallTime, isCallVideo, isCallVoid, valueSettingNotify, appealComment, isUpdateAppeal } = this.state;
        const dataSettingCall = {
            video_call_waiting: isCallVideo,
            voice_call_waiting: isCallVoid,
            on_call_times: onCallTime,
            appeal_call_waiting: valueSettingNotify,
            api: 'set_call_waiting',
        };

        const checkBannedWordAppeal = this.handleCheckBannedWord(appealComment);

        if (checkBannedWordAppeal.length !== 0) {
            Popup.alert(BANNED_WORDS_MESSAGE_FORMAT.replace('%s', checkBannedWordAppeal[0]));
            return;
        }

        SettingCallService.setCallWaiting(dataSettingCall);
        if (isUpdateAppeal) {
            UserService.updateAppealComment(appealComment).then(response => {
                if (response.data.isApprove) {
                    history.push('/home');
                } else {
                    Popup.create({
                        content: PENDDING_APPEAL_COMMENT_NOTI,
                        buttons: {
                            left: [{
                                text: 'はい',
                                className: 'ok',
                                action: () => {
                                    Popup.close();
                                    history.push('/home');
                                },
                            }],
                        },
                    });
                }
            });
            return;
        }
        history.push('/home');
    }

    handleCheckInput = (value)  => {
        const { onCallTime } = this.state;

        const key = onCallTime.indexOf(value);
        if (key === -1) {
            onCallTime.push(value);
        } else {
            onCallTime.splice(key, 1);
        }

        this.setState({ onCallTime });
    }

    changeStatusCall(e) {
        const value = e.target.checked;
        const name = e.target.value;

        this.setState({
            [name]: value,
        });
    }

    handleToggleCheckAll = () => {
        let { onCallTime } = this.state;

        if (onCallTime.length === 24) {
            onCallTime = [];
        } else {
            onCallTime = Array.from(Array(24).keys()).map(hour => String(hour));
        }

        this.setState({ onCallTime });
    }

    render() {
        const {
            toggleSetting,
            isTextSettingNotify,
            appealComment,
            isCallVoid,
            valueSettingNotify,
            isCallVideo,
            isCallNow,
            onCallTime,
        } = this.state;
        let {timeWaitingNotify} = this.state;
        const lengthComment = appealComment.length === 0 ? this.state.lengthComment : 40 - appealComment.length ;
        const overTimeSetting = timeWaitingNotify > 0 && timeWaitingNotify < 360;
        const isDisable = isCallVideo || isCallVoid;

        if (timeWaitingNotify > 60) {
            timeWaitingNotify =  (Math.floor(timeWaitingNotify/60) + ' 時' +  Math.floor(timeWaitingNotify % 60)) // eslint-disable-line
        }
        const settingTime = Array.from(Array(24).keys()).map(item => {
            const hour = String(item);

            return (
                <label className="setting-checkbox__item" htmlFor={`time-${hour}`} key={hour}>
                    <input className="radio-button" type="checkbox" name="region" value={hour} id={`time-${hour}`} onChange={ () => this.handleCheckInput(hour) } checked={onCallTime.indexOf(hour) !== -1}/>
                    <label htmlFor={`time-${hour}`}>
                        <span className="radio-button__icon">&nbsp;</span>
                    </label>
                    {hour}時～
                </label>
            )
        })

        return ( !this.state.loading &&
            <div className="setting-call-page">
                <Header title={ this.state.title } backUrl={ this.state.backUrl }>
                    <button type="button" className="nav-bar__button" onClick={ () => this.handleSubmit() }>
                        完了
                    </button>
                </Header>

                <main className="k-main">
                    <div className="form-group">
                        <label className="form-group-left">音声通話の受信許可</label>
                        <div className="form-group-right">
                            <label className="switch">
                                <input type="checkbox" className="switch__input" checked= {this.state.isCallVoid} onChange={(e) => this.changeStatusCall(e)} value = "isCallVoid"/>
                                <div className="switch__toggle">
                                    <div className="switch__handle" />
                                </div>
                            </label>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="form-group-left">ビデオ通話の受信許可</label>
                        <div className="form-group-right">
                            <label className="switch">
                                <input type="checkbox" className="switch__input" checked= {this.state.isCallVideo} onChange={(e) => this.changeStatusCall(e)} value = "isCallVideo"/>
                                <div className="switch__toggle">
                                    <div className="switch__handle" />
                                </div>
                            </label>
                        </div>
                    </div>

                    <div className="form-group d-block">
                        <label>通話待ちアピール</label>
                        <div className="timeAppear">
                            <SelectBox enabled={ isDisable } options={SETTING_NOTI_CALL} selected={valueSettingNotify} onSelected={this.handleSettingNotify} />
                            <div className = "form-group-right">
                                <span className="arrow-right icon icon-chevron-right "></span>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="text-note" >
                            ボタンをONにした通話方法のみ着信を受け付けます。<br/> ボタンをOFFにすると着信拒否となります。<br/>
                            {(isCallVoid || isCallVideo) && isTextSettingNotify && overTimeSetting ? <span>通話待ちアピール状態 : あと {timeWaitingNotify}分</span> : ''}
                        </div>
                    </div>

                    <div className={ `form-group ${toggleSetting ? 'is_open' : ''}` }>
                        <label className="form-group-left">
                            <span>通話可能時間設定</span>
                            {isCallNow ? <span className="msg-text msg-notice">只今の時間は通話着信を許可しています。</span> : <span className="msg-text">通話着信を受ける場合は、着信設定をしてください。</span> }

                        </label>
                        <div className="setting-call__btn-toggle" onClick = {() => this.toggleSetting()}>
                            <i className= {toggleSetting ? "icon icon-minus" : "icon icon-plus"} />
                        </div>
                    </div>

                    <div className={ `setting-call-time__box ${toggleSetting ? 'is_open' : ''}` }>
                        <div className="setting-call-time__note">
                            通話可能な時間帯をチェックし、右上の「完了」を押してください。<br/>
                            設定した時間帯になると、[着信設定]でチェックした通話方法が自動的に着信許可されます。
                        </div>

                        <label className="setting-checkbox__item" htmlFor="check-all">
                            <input className="radio-button" type="checkbox" name="region" id="check-all" onChange={ this.handleToggleCheckAll } checked={onCallTime.length === 24}/>
                            <label htmlFor="check-all">
                                <span className="radio-button__icon">&nbsp;</span>
                            </label>
                            一括チェック
                        </label>

                        <div className="setting-checkbox__list">
                            { settingTime }
                        </div>
                    </div>

                    <div className="app-comment">
                        <label>アピール一言コメント</label>
                        <div className="comment">
                            <textarea
                                maxLength="40"
                                placeholder="自分のアピールポイントをつぶやこう!アダルトな内容や規約に違反する内容は公開されません。"
                                value = {appealComment}
                                onChange = {(e) => this.handleComment(e)} />
                            <span className="number">{lengthComment}</span>
                        </div>
                    </div>
                </main>

                <Tutoria />
            </div>
        );
    }
}

export default SettingCall;
