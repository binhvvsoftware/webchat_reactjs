import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import Header from '../../components/Header';
import ProfileService from '../../services/profile';
import SettingNoticePopup from '../../components/SettingNoticePopup';
import Image from '../../components/Image';
import './index.scss';

class OnlineAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'オンライン通知',
            backUrl: '/my-page',
            users: [],
            userSettingNotice: {},
            listDelete: {},
            isOpenSettingNotice: false,
            isLoading: true,
        };
        this.handleCloseSettingNotice = this.handleCloseSettingNotice.bind(this);
    }

    componentDidMount() {
        ProfileService.listOnlineAlert()
            .then(response => {
                this.setState({
                    users: response.data,
                    isLoading: false,
                });
            });
    }

    handleCloseSettingNotice(value) {
        const { listDelete, userSettingNotice } = this.state;
        listDelete[userSettingNotice.user_id] = !value;

        this.setState({
            isOpenSettingNotice: false,
            listDelete,
        });
    }

    handleOpenSettingNotice(user) {
        this.setState({
            userSettingNotice: user,
            isOpenSettingNotice: true,
        });
    }

    render() {
        const { users, listDelete } = this.state;
        const list = users.map((user) =>
            <div className={ `list-user__item ${listDelete[user.user_id] ? 'is_hidden' : '' }` } key={user.user_id} >
                <Link to={ {pathname: user.profile_url, state: { user } } }>
                    <div className="list-user__thumbnail">
                        <div className="list-user__thumbnail-img">
                            <Image isAvatar imageId={ user.ava_id }/>
                        </div>
                    </div>
                </Link>

                <div className="list-user__info">
                    <Link className="username" to={ {pathname: user.profile_url, state: { user } } }>
                        { user.user_name }
                    </Link>
                    <div className="setting-online">
                        <button type="button" onClick = {() => this.handleOpenSettingNotice(user)} className='button-setting'>更変</button>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="online-alert">
                { this.state.isLoading && <div className="online-alert__loading" /> }
                <Header title={this.state.title} backUrl={this.state.backUrl} />
                <main className="k-main">
                    <div className="online-alert__note">オンラインになると通知が届き ます。</div>
                    <div className="list-user is_list ">
                        {list}
                    </div>
                    <SettingNoticePopup isOpen={this.state.isOpenSettingNotice} user={this.state.userSettingNotice} onClose={this.handleCloseSettingNotice} />
                </main>
            </div>
        );
    }
}

export default OnlineAlert;
