import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-web';
import Header from '../../components/Header';
import { EMAIL_IS_EMPTY_MESSAGE, EMAIL_IS_WRONG_MESSAGE, PASSWORD_IS_EMPTY_MESSAGE, PASSWORD_OUT_OF_RANGE } from '../../constants/messages';
import { submitLogin, loginFailure } from '../../actions/login';
import './index.scss';
import { checkEmail } from '../../utils/helpers';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            title: 'ログイン',
            backUrl: '/login',
        };
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmail(text) {
        this.setState({
            email: text.trim(),
        });
    }

    handleChangePassword(text) {
        this.setState({
            password: text,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.email === '') {
            return Popup.alert(EMAIL_IS_EMPTY_MESSAGE);
        }

        if (!checkEmail(this.state.email)) {
            return Popup.alert(EMAIL_IS_WRONG_MESSAGE);
        }

        if (this.state.password === '') {
            return Popup.alert(PASSWORD_IS_EMPTY_MESSAGE);
        }

        if (this.state.password.length < 6 || this.state.password.length > 12) {
            return Popup.alert(PASSWORD_OUT_OF_RANGE);
        }

        return this.props.submitLogin(this.state.email, this.state.password);
    };

    render() {
        return (
            <div className="sign-in-page">
                { this.props.login.loading && <div className="sign-in-page__loading" />}
                <Header title={ this.state.title } backUrl={ this.state.backUrl }/>
                <main className="k-main">
                    <form className="form" onSubmit={ this.handleSubmit }>
                        <div className="form__control ">
                            <div className="form__group-input">
                                <TextInput
                                    defaultValue={this.state.email}
                                    placeholder="メールアドレス"
                                    onChangeText={ (text) => this.handleChangeEmail(text) } />
                            </div>
                        </div>

                        <div className="form__control ">
                            <div className="form__group-input">
                                <TextInput
                                    defaultValue={this.state.password}
                                    placeholder="パスワード"
                                    secureTextEntry
                                    onChangeText={ (text) => this.handleChangePassword(text) } />
                            </div>
                        </div>

                        <div className="mt_30 text-center">
                            <button type="submit" className="btn-custom btn-active">ログイン</button>

                            <p className="mt_20">
                                <Link className="text-center text-link underline" to='/forget-password'>パスワードはお忘れですか？</Link>
                            </p>
                        </div>
                    </form>
                </main>
            </div>
        );
    }
}

SignIn.propTypes = {
    submitLogin: PropTypes.func.isRequired,
    login: PropTypes.shape({
        loading: PropTypes.bool,
    }),
};

export default connect(state => ({ login: state.login }), { submitLogin, loginFailure })(SignIn);
