import loadable from '@loadable/component';

export const LoginOption = loadable(() => import('./welcome'));

export const SignIn = loadable(() => import('./container.js'));
