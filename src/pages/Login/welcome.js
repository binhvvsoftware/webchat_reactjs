import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import './index.scss';
import logo from '@images/logo.png';
import background from '@images/welcome-bg.svg';
import { FACEBOOK_APP_ID, FACEBOOK_SELECT_FIELDS } from '../../constants/facebook_constant';
import {
    WOMEN_CAN_NOT_LOGIN, YOU_FINISH_REGIST_MSG_TITLE, YOU_FINISHED_REGIST_MSG,
} from '../../constants/messages';
import { HTTP_WOMEN_CAN_NOT_LOGIN } from '../../constants/response_status_constant';
import history  from '../../utils/history';
import AuthService from '../../services/authentication';
import storage, { keys } from '../../utils/storage';
import { TOKEN_REGISTER } from '../../constants/storage_constant';

/* global FB */
class Welcome extends Component {
    constructor(props) {
        super(props);
        this.buttonFb = React.createRef();
        this.state = {
            loading: false,
        };
    };

    render() {
        /* eslint-disable */
        window.fbAsyncInit = function() {
            FB.init({
                appId: FACEBOOK_APP_ID,
                cookie: true,  // enable cookies to allow the server to access the session
                xfbml: true,  // parse social plugins on this page
                version: 'v3.2', // use graph api version 3.2
            });
        };

        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        // Here we run a very simple test of the Graph API after login is
        // successful.  See statusChangeCallback() for when this call is made.
        window.getFacebook = (accessToken) => {
            console.log('Welcome!  Fetching your information.... ');
            FB.api(`/me/?fields=${FACEBOOK_SELECT_FIELDS}&access_token=${accessToken}`, (response) => {
                let birthday;
                switch (response.birthday) {
                    case 'male':
                    case 1:
                        birthday = 0;
                        break;
                    case 'female':
                    case 0:
                        birthday = 1;
                        break;
                    default:
                        birthday = '';
                        break;
                }

                const params = {
                    id: response.id,
                    fb_id: response.id,
                    email: response.email,
                    name: response.name,
                    picture: `https://graph.facebook.com/${response.id}/picture?type=large`,
                    gender: response.gender,
                    birthday,
                };

                this.setState({
                    loading: true,
                }, () => {
                    AuthService.loginFb(params.fb_id).then((response) => {
                        let user = response.data;
                        if (!user.user_name.length) {
                            user = Object.assign(params, user);
                            storage.set(TOKEN_REGISTER, user.token);
                            storage.set(keys.GENDER, user.gender);
                            storage.set(keys.BIRTHDAY, user.reg_date);
                            storage.set(keys.TUTORIAL, user.accessed_pages);
                            storage.set(keys.REGISTER_TIME, String(new Date().getTime()));
                            history.push('/register-profile', { user });
                            Popup.alert(YOU_FINISHED_REGIST_MSG, YOU_FINISH_REGIST_MSG_TITLE);
                            return;
                        }

                        history.push('/home');
                    }).catch((errors) => {
                        if (errors.code === HTTP_WOMEN_CAN_NOT_LOGIN) {
                            Popup.alert(WOMEN_CAN_NOT_LOGIN);
                            return;
                        }

                        history.push('/register', { user: params });
                    });
                })
            });

        }

        /* eslint-enable */
        const connectFb = () => {
            window.callbackLoginFb = responseLogin => {
                if (responseLogin.status === 'connected') {
                    window.getFacebook(responseLogin.authResponse.accessToken);
                    return;
                }

                console.warn(responseLogin); // eslint-disable-line
            };

            FB.login(window.callbackLoginFb);
        };

        return (
            <div>
                <div className="login-page">
                    { this.state.loading && <div className="login-page__loading" /> }

                    <img className="login-page__background" src={background} alt=""/>
                    <div className="login-page__content">
                        <div className="app-logo">
                            <img className="app-logo__img" src={ logo } alt=""/>
                        </div>

                        <div className="btns-begin text-center">
                            <Link to='/register' className="btn-custom btn-active">
                                さっそくはじめる（無料）
                            </Link>

                            <button type="button" ref={this.buttonFb} onClick={ connectFb } className="btn-custom btn-fb-login">
                                <span className="icon icon-fb" />
                                <span>Facebookでログイン</span>
                            </button>
                            <p className="grey-text">Facebookに利用状況が公開されることはありません。</p>
                        </div>

                        <div className="btns-login">
                            <Link className="pink-text" to='/sign-in'>ログイン</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Welcome.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func,
    }),
};

export default Welcome;
