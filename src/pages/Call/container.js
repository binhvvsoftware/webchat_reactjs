import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MainLayout from '../../components/MainLayout';
import KyuunCall from '../../statics/images/KYUN-CALL.png'
import * as AppCall from '../../utils/app-call';
import './index.scss';

class Call extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);

        this.state = {
            appStoreURL: AppCall.buildLinkAppStore(),
            connectURL: AppCall.buildLinkConnect(),
        };
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    render() {
        const {
            appStoreURL,
            connectURL,
        } = this.state;
        return (
            <MainLayout className="page-call" title="通話アプリ設定方法" hasFooter={false}>
                <div className="page-call-1">
                    <div className="main__title">インストール手順</div>
                    <div className="section">
                        <div className="section__title"><span className="number-active">1</span>通話アプリのインストール</div>
                        <div>下記のボタンからアプリをダウンロードしてください。</div>
                        <img style={{ width: "100%", margin: "20px 0px" }} src={KyuunCall} alt="" />
                        <div className="section__btn">
                            <a href={appStoreURL} target="_blank">
                                <button className="btn-custom btn-active" type="button">ストアからダウンロード</button>
                            </a>
                        </div>
                        <div className="text-note">※ 上記タップ際は、ホーム画面にアプリがダウンロードされているかご確認ください。</div>
                        <div>インストール完了後、このページに戻って下記の2の設定完了ボタンをタップして完了してください。</div>
                        <div className="section__title"><span className="number-active">2</span>設定完了</div>
                        <div>下記のボタンをタップして「登録しました」のページが表示れたら設定完了です♪</div>
                        <div className="section__btn">
                            <a href={connectURL} target="_blank">
                                <button className="btn-custom btn-active" type="button">コチラをタップして設定完了</button>
                            </a>
                        </div>
                    </div>
                    <div className="section no-padding ">
                        <div className="txt-info">あとはアナタの好みの子を狙って、今すぐリアル映像を体感♪</div>
                        <Link className="form__control" to="/home">
                            <div className="form__label">今すぐ話せる女の子</div>
                            <div className="form__group-input" >
                                <span className="form__icon-right">&nbsp;</span>
                            </div>
                        </Link>
                        <Link className="form__control" to="/favorite">
                            <div className="form__label">お気に入りの女の子</div>
                            <div className="form__group-input" >
                                <span className="form__icon-right">&nbsp;</span>
                            </div>
                        </Link>
                    </div>
                    <div className="main__title">注意事項</div>
                    <div className="section">
                        <ul className="section__list">
                            <li className="section__list__item">
                                <div><span className="number-default">1</span></div>
                                <div>インターネット回線を通じての通話となりますので、携帯キャリアへの通話料は発生しません。</div>
                            </li>
                            <li className="section__list__item">
                                <div><span className="number-default">2</span></div>
                                <div>「アプリ利用設定許可」が「利用する」の設定になっていない場合は、携帯キャリアへの通話料は発生しますのでご注意下さい。</div></li>
                            <li className="section__list__item">
                                <div><span className="number-default">3</span></div>
                                <div>サイト内のポイントは通常通り消費しますので、事前に利用ポイント表をご確認下さい。</div></li>
                            <li className="section__list__item">
                                <div><span className="number-default">4</span></div>
                                <div>その他ご不明な点は、よくある質問ページをご確認下さい。</div></li>
                            <li className="section__list__item">
                                <div><span className="number-default">5</span></div>
                                <div>よくある質問ページで解決できないような場合には、事務局までご連絡下さい。</div></li>
                            <li className="section__list__item">
                                <div><span className="number-default">6</span></div>
                                <div>ビデオ通話アプリの品質は完全保証いたしかねますので、お客様のご利用にご納得がいかない状況が発生したとしても、ポイントの変換には対応したしかねますので、ご了承下さいませ。</div></li>
                        </ul>
                    </div>
                </div>
            </MainLayout>
        );
    }
}

export default Call;
