import React, { Component } from 'react';
import MainLayout from '../../components/MainLayout';
import './index.scss';

class Disconnect extends Component {
    render() {
        return (
            <MainLayout className="page-call" title="Call" hasFooter={false}>
                <div className="page-call-2">
                    <div className="main__title"><span className="icon icon-mobile"></span> 通話アプリ利用設定</div>
                    <div className="intro">
                        <div>現在、<span style={{fontWeight:"bold", fontSize:"16px"}}>「通話アプリを利用する」</span><br></br>設定になっています。</div>
                    </div>
                    <div className="section__btn">
                        <button className="btn-custom btn-active" type="button">通話アプリを利用しないに変更する</button>
                    </div>
                    <div className="section">
                        <div className="note" ><span className="icon icon-caution"></span> ご注意</div>
                        <p> (1) 機種変更をした方<br></br>
                            (2) 通話アプリをアンインストールした方<br></br>
                            (3) 通話アプリを再インストールしたい方<br></br>
                        </p>
                        <div><span style={{color:"#ec008c"}}>こちら</span>から再度通話アプリのダウンロードをお試し下さい。</div>
                    </div>
                </div>
            </MainLayout>
        );
    }
}

export default Disconnect;
