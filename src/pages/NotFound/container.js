import React, { Component } from 'react';
import Header from '../../components/Header';

class NotFound extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '404 Not found',
            backUrl: '/home',
        };
    }

    render() {
        return (
            <div className="register">
                <Header title={this.state.title} backUrl={this.state.backUrl} />

                <main className="k-main">
                    <div className="static-content">
                        URLの有効期限を過ぎています。
                    </div>
                </main>
            </div>
        );
    }
}

export default NotFound;
