import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-web';
import { connect } from 'react-redux';
import PropTypes  from 'prop-types';
import MainLayout from '../../components/MainLayout';
import CallLogItem from '../../components/CallLog';
import FootPrintService from '../../services/footPrint';
import { checkLoadMore } from '../../utils/helpers';
import './index.scss';

class FootPrint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'あしあと',
            active: 'lst_chk_out',
            dataFootPrint: [],
            hasLoadMore: true,
            loading: false,
        };
    }

    componentDidMount() {
        this.handleCallApi();
        window.addEventListener('scroll', this.handleLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (checkLoadMore(event)) {
            this.handleCallApi(this.state.active);
        }
    };

    handleCallApi = (target = 'lst_chk_out') => {
        const { dataFootPrint } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            FootPrintService.getMyFootPrint(target, dataFootPrint.length).then(response => {
                this.setState({
                    dataFootPrint: dataFootPrint.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    };

    changeTitle(target) {
        if (target === 'lst_chk_out') {
            this.setState({
                title: 'あしあと',
            });
        } else if (target === 'lst_my_footprint') {
            this.setState({
                title: '自分のあしあと',
            });
        }
        if (target !== this.state.active) {
            this.setState({
                active: target,
                dataFootPrint: [],
                loading: false,
                hasLoadMore: true,
            }, () => this.handleCallApi(target));
        }
    }

    render() {
        const { active, dataFootPrint } = this.state;
        const { attentionNumbers } = this.props;
        const listDataFootPrint = dataFootPrint.map((user,index) => <CallLogItem type="FootPrint" user={user} key={index}/>);

        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list two">
                    <li className={ `sub-header__item ${active === 'lst_chk_out' ? 'is_active' : ''}` } onClick={ () => this.changeTitle('lst_chk_out') }>
                        あしあと {attentionNumbers.checkout}
                    </li>
                    <li className={ `sub-header__item ${active === 'lst_my_footprint' ? 'is_active' : ''}` } onClick={ () => this.changeTitle('lst_my_footprint') }>
                        自分のあしあと {attentionNumbers.myFootprint}
                    </li>
                </ul>
            </section>
        );

        return (
            <MainLayout
                className="foot-print"
                title={ this.state.title }
                hasFooter={ false }
                hasSidebarLeft={ false }
                hasBackButton
                backFunction='/home'
                subHeader={subHeader}
            >
                <div className="list-users">
                    <div className="text-note">あなたをお気に入り登録している人です</div>
                    { dataFootPrint.length === 0 ? <div className="text-notice">あなたをお気に入り登録している人はいません</div> : '' }
                    { listDataFootPrint }
                    <div className={ this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden' }>
                        <ActivityIndicator animating color="black" size="large"/>
                    </div>
                </div>
            </MainLayout>
        );
    }
}

FootPrint.propTypes = {
    attentionNumbers: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
});

export default connect(mapStateToProps)(FootPrint);
