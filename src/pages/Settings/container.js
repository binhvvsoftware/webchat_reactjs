import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AccountTakeover from '../../components/AccountTakeover'
import SettingService from '../../services/settings';
import { APP_VERSION } from '../../constants/endpoint_constant';
import MainLayout from '../../components/MainLayout';
import storage from '../../utils/storage';
import { LIST_BLOCKED_USERS_TITLE } from '../../constants/messages'
import { termOfUser, aboutPayment } from '../../services/webview';
import './index.scss';
import { openWebView } from '../../actions/webview';

class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpenAccountTakeover: false,
            hasEmail: false,
            joinRanking: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        const user = storage.getUserInfo();
        let { hasEmail } = user;
        if (!hasEmail && nextProps.location.state && nextProps.location.state.hasEmail) {
            hasEmail = true;
        }

        this.setState({
            hasEmail,
            joinRanking: user.joinRanking,
        });
    }

    handleOpenWebView = (url, title) => {
        this.props.openWebView({
            url,
            title,
            backButton: true,
        });
    };

    componentDidMount() {
        const user = storage.getUserInfo();
        this.setState({
            hasEmail: user.hasEmail,
        })
    }

    handleOpenAccountTakeover = () => {

        this.setState({
            isOpenAccountTakeover: true,
        })
    }

    handleCloseAccountTakeover = (hasEmail = false) => {
        this.setState({
            isOpenAccountTakeover: false,
            hasEmail,
        })
    }

    handleChangeJoinRanking = () => {
        let { joinRanking } = this.state;
        joinRanking = !joinRanking;
        this.setState({
            joinRanking,
        }, () => {
            SettingService.updateJoinRanking(joinRanking);
            const user = storage.getUserInfo();
            user.joinRanking = joinRanking;
            storage.setUserInfo(user);
        })
    }

    render() {
        return (
            <MainLayout title="設定" className="setting-page" hasFooter={false} hasSidebarRight={false}>
                <div className="setting-title">チャットおよび通知設定</div>

                <Link to="/setting-push-noti">
                    <div className="form-group form-setting">
                        <label className="form-group-left">プッシュ通知</label>
                        <div className="form-group-right">
                            <span className="form__icon-right">&nbsp;</span>
                        </div>
                    </div>
                </Link>

                <div className="setting-title">アカウント設定</div>
                <div className="cursor-pointer" onClick={this.handleOpenAccountTakeover}>
                    <div className="form-group form-setting">
                        <label className="form-group-left">アカウントデータ引き継ぎ</label>
                        <div className="form-group-right">
                            <span className="setting-hanover-label">{this.state.hasEmail ? '' : '未登録'}</span>
                            <span className="form__icon-right">&nbsp;</span>
                        </div>
                    </div>
                </div>
                <Link to="/setting-block-list">
                    <div className="form-group form-setting">
                        <label className="form-group-left">{LIST_BLOCKED_USERS_TITLE}</label>
                        <div className="form-group-right">
                            <span className="form__icon-right">&nbsp;</span>
                        </div>
                    </div>
                </Link>

                <Link to="/deactivate">
                    <div className="form-group form-setting">
                        <label className="form-group-left">退会</label>
                        <div className="form-group-right">
                            <span className="form__icon-right">&nbsp;</span>
                        </div>
                    </div>
                </Link>

                <div className="setting-title">料金設定</div>
                <div className="form-group form-setting" onClick={() => this.handleOpenWebView(aboutPayment())}>
                    <label className="form-group-left">料金について</label>
                    <div className="form-group-right">
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="setting-title">サービス規約</div>
                <Link className="form-group form-setting" to={{ pathname: '/pages/terms-of-service' }}>
                    <label className="form-group-left">利用規約</label>
                    <div className="form-group-right">
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </Link>

                <Link className="form-group form-setting" to={{ pathname: '/pages/privacy-policy' }}>
                    <label className="form-group-left">プライバシーポリシー</label>
                    <div className="form-group-right">
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </Link>

                <div className="form-group form-setting" onClick={() => this.handleOpenWebView(termOfUser(), '特定商取引法に基づく表示')}>
                    <label className="form-group-left">特定商取引法に基づく表示</label>
                    <div className="form-group-right">
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="info-version">バージョン{APP_VERSION}</div>

                <AccountTakeover hasEmail={this.state.hasEmail} isOpen={this.state.isOpenAccountTakeover} onClose={this.handleCloseAccountTakeover} />
            </MainLayout>
        );
    }
}

Settings.propTypes = {
    location: PropTypes.object,
    openWebView: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(null, mapDispatchToProps)(Settings);
