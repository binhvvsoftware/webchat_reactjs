import React, { Component } from 'react';
import { TextInput } from 'react-native-web';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import PopupRegister from '@images/popup-register.svg';
import FullScreenModal  from '../../components/FullScreenModal';
import Header from '../../components/Header';
import Loading from '../../components/Loading';
import history from '../../utils/history';
import Auth from '../../services/authentication'
import { checkEmail } from '../../utils/helpers';
import {
    EMAIL_IS_EMPTY_MESSAGE,
    EMAIL_IS_WRONG_MESSAGE,
    EMAIL_IS_TOO_LONG_MESSAGE,
    RETYPE_PASSWORD_IS_NOT_THE_SAME,
    PASSWORD_OUT_OF_RANGE,
    PASSWORD_IS_EMPTY_MESSAGE,
    EMAIL_HAS_BEEN_USED_PART2,
    NETWORK_ERROR,
} from '../../constants/messages';
import storage, { keys } from '../../utils/storage';
import './index.scss';

class Register extends Component {
    constructor(props) {
        super(props);
        let user;

        try {
            // Convert user from login fb
            user = props.location.state.user || {}
        } catch (e) {
            user = {};
        }

        this.state = {
            title: 'ユーザー情報入力',
            backUrl: '/login',
            loading: false,
            gender: 0,
            user,
            registerData: {
                newPwd: '',
                confirmPwd: '',
                email: '',
            },
            isConfirmAge: false,
            showMessagePopup: false,
            bckstgTime: '',
        };
    }

    handleChangeHandoverContent = (key, text) => {
        const { registerData } = this.state;
        registerData[key] = key === 'email' ? text.trim() : text;

        this.setState({
            registerData,
        });
    }

    confirmAge = (e) => {
        this.setState({
            isConfirmAge: e.target.checked,
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const { isConfirmAge, registerData } = this.state;
        if (!isConfirmAge){
            return '';
        }
        if (registerData.email.length === 0 ) {
            return Popup.alert(EMAIL_IS_EMPTY_MESSAGE);
        }

        if (registerData.newPwd.length === 0){
            return Popup.alert(PASSWORD_IS_EMPTY_MESSAGE);
        }

        if (registerData.confirmPwd.length === 0){
            return Popup.alert(RETYPE_PASSWORD_IS_NOT_THE_SAME);
        }

        if (registerData.email.length && !checkEmail(registerData.email)) {
            return Popup.alert(EMAIL_IS_WRONG_MESSAGE);
        }

        if (registerData.email.length > 150) {
            return Popup.alert(EMAIL_IS_TOO_LONG_MESSAGE);
        }

        if (registerData.newPwd !== registerData.confirmPwd) {
            return Popup.alert(RETYPE_PASSWORD_IS_NOT_THE_SAME);
        }

        if (registerData.newPwd.length > 0) {
            if (registerData.newPwd.length < 6 || registerData.newPwd.length > 12) {
                return Popup.alert(PASSWORD_OUT_OF_RANGE);
            }
        }

        const params = {
            gender: this.state.gender,
            fb_id: this.state.user.fb_id,
            email: registerData.email,
            pwd: registerData.newPwd,

        };
        this.setState({ loading: true });

        return Auth.register(params).then((response) => {
            storage.set(keys.TOKEN_REGISTER, response.data.token);
            storage.set(keys.TUTORIAL, response.data.accessed_pages);
            storage.set(keys.REGISTER_TIME, String(new Date().getTime()));
            this.setState({
                showMessagePopup: true,
                bckstgTime: response.data.bckstg_time,
                loading: false,
            })
        }).catch((err) => {
            this.setState({ loading: false });
            if (err.code === 12) {
                return Popup.alert(this.replaceBuzzValue(EMAIL_HAS_BEEN_USED_PART2));
            }
            return Popup.alert(NETWORK_ERROR);
        });
    };

    handleClosePopup = () => {
        this.setState({showMessagePopup: false})
        history.push('/register-profile', { user: this.state.user, bckstg_time: this.state.bckstgTime });
    }

    replaceBuzzValue = value => value.split('\n').map((item) => <span>{ item }<br/></span>);

    render() {
        return (
            <div className="register">
                <Loading isLoading={this.state.loading} />
                <Header title={this.state.title} backUrl={this.state.backUrl} />

                <main className="k-main">
                    <div className="text-center">
                        <div className="header-breadcrumb">
                            <ul className="breadcrumb">
                                <li><div className="number-active" >1</div>ユーザー情報入力</li>
                                <li><div className="number-default">2</div>プロフィール作成</li>
                                <li><div className="number-default">3</div>登録完了</li>
                            </ul>
                        </div>
                        <form className="form form-register" onSubmit={(e) => this.handleSubmit(e)}>
                            <div className="form__control">
                                <div className="form__group-input no_icon-right">
                                    <TextInput
                                        placeholder="メール"
                                        onChangeText={ (text) => this.handleChangeHandoverContent('email', text) }
                                    />
                                </div>
                            </div>
                            <div className="form__control">
                                <div className="form__group-input no_icon-right">
                                    <TextInput
                                        placeholder="パスワード"
                                        secureTextEntry
                                        onChangeText={ (text) => this.handleChangeHandoverContent('newPwd', text) }
                                    />
                                </div>
                            </div>
                            <div className="form__control">
                                <div className="form__group-input no_icon-right">
                                    <TextInput
                                        placeholder="パスワード（確認用）"
                                        secureTextEntry
                                        onChangeText={ (text) => this.handleChangeHandoverContent('confirmPwd', text) }
                                    />
                                </div>
                            </div>
                        </form>
                        <div className="register__text-message">本サービスをご利用いただくためのパスワード設定をお願いします。</div>
                        <div className='form-confirm'>
                            <label className="setting-checkbox__item" htmlFor="check-all">
                                <input className="radio-button" type="checkbox" id="check-all" onChange={this.confirmAge}/>
                                <label htmlFor="check-all">
                                    <span className="radio-button__icon">&nbsp;</span>
                                </label>
                            </label>
                            <div className='form-confirm__text-confirm'> <label className="setting-checkbox__item" htmlFor="check-all">私は満18歳以上（高校生を除く）の独身者で <Link to={{ pathname: '/pages/terms-of-service' }}>利用規約</Link>および<Link to={{ pathname: '/pages/privacy-policy' }}>プライバーシーポリシー</Link>に同意します</label></div>
                        </div>
                        <div className="section__btn">
                            <button className={`btn-custom btn-active ${this.state.isConfirmAge ? '':  'btn-disable'}`} type="button" onClick={(e) => this.handleSubmit(e)}>登録する</button>
                        </div>
                    </div>
                </main>
                <FullScreenModal className="message-popup" isOpen={this.state.showMessagePopup}>
                    <div className="message-popup__content">
                        <div className="message-popup__header-content" style={{position:"relative"}}>
                            <div className="message-popup__title-content">メールアドレスとパスワードの登録が<br/>
                                完了しました。<br/>
                            以降、ご登録情報にてログインできます。</div>
                            <div className="message-popup__background"><img  src={PopupRegister} alt=""/></div>
                            <button onClick={this.handleClosePopup} type="button" className="message-popup__btn">
                                次へ進む
                            </button>
                        </div>
                    </div>
                </FullScreenModal>
            </div>
        );
    }
}

Register.propTypes = {
    location: PropTypes.object,
};

export default Register;
