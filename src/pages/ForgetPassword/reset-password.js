import React, { Component } from 'react';
import { TextInput } from 'react-native-web';
import Popup from 'react-popup';
import Header from '../../components/Header';
import {
    CODE_VERIFY_EMPTY_MESSAGE,
    PASSWORD_OUT_OF_RANGE,
    TOKEN_IS_WRONG_MESSAGE,
    ERROR_SYSTEM,
} from '../../constants/messages';
import ResetPasswordService from '../../services/resetPassword';
import AuthService from '../../services/authentication';
import history, { getLocationHistory } from '../../utils/history';
import { EMAIL } from '../../constants/storage_constant';
import storage from '../../utils/storage';
import './index.scss';

class ChangePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: 'パスワード再設定',
            token: '',
            password: '',
            backUrl: '/forget-password',
            loading: false,
        };

        let email;
        this.handleChangeToken = this.handleChangeToken.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        try {
            const location = getLocationHistory();
            email = location.current.state.email || '';
        } catch (e) {
            email = '';
        }

        if (!email) {
            history.push('/forget-password');
        }
    }

    handleChangeToken(text) {
        this.setState({
            token: text.trim(),
        });
    }

    handleChangePassword(text) {
        this.setState({
            password: text,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.password === '' || this.state.token === '') {
            return Popup.alert(CODE_VERIFY_EMPTY_MESSAGE);
        }

        if (this.state.password.length < 6 || this.state.password.length > 12) {
            return Popup.alert(PASSWORD_OUT_OF_RANGE);
        }

        this.setState({ loading: true });
        const email = storage.get(EMAIL);
        ResetPasswordService.resetPassword(this.state.token, this.state.password, email)
            .then(() => {
                AuthService.loginEmail(email, this.state.password).then(() => {
                    history.push('/home');
                    storage.remove(EMAIL);
                });
            })
            .catch(error => {
                if (error.code === 90) {
                    return Popup.alert(TOKEN_IS_WRONG_MESSAGE);
                }

                return Popup.alert(ERROR_SYSTEM);
            })
            .then(() => {
                this.setState({ loading: false });
            });

        return true;
    };

    render() {
        return (
            <div className="change-password">
                { this.state.loading && <div className="form-reset__loading"/> }
                <Header title={ this.state.title } backUrl={ this.state.backUrl }/>

                <main className="k-main">
                    <div className="form-reset">
                        <form className="form" onSubmit={ this.handleSubmit }>
                            <div className="form__control">
                                <div className="form__label">認証コード</div>
                                <div className="form__group-input">
                                    <TextInput
                                        defaultValue={ this.state.token }
                                        className="text-right"
                                        placeholder="ここに入力"
                                        onChangeText={ (text) => this.handleChangeToken(text) }/>
                                </div>
                            </div>
                            <div className="form__control">
                                <div className="form__label">新パスワード</div>
                                <div className="form__group-input">
                                    <TextInput
                                        className="text-right"
                                        defaultValue={ this.state.password }
                                        placeholder="ここに入力"
                                        secureTextEntry
                                        onChangeText={ (text) => this.handleChangePassword(text) }/>
                                </div>
                            </div>
                            <button type='submit' className="btn-custom  btn-active">パスワード再設定</button>
                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

export default ChangePassword;
