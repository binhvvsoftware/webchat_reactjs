import loadable from '@loadable/component';

export const ForgetPassword = loadable(() => import('./container.js'));

export const ResetPassword = loadable(() => import('./reset-password.js'));
