import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-web';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Header from '../../components/Header';
import {
    EMAIL_IS_WRONG_MESSAGE_FOGOTPASS,
    GUIDE_FORGOT_MESSAGE,
    EMAIL_IS_NOT_EMPTY,
    NAVIGATION_FORGOT_TITLE,
    BUTTON_RESET_PASSWORD_TITLE,
} from '../../constants/messages';
import { checkEmail } from '../../utils/helpers';
import './index.scss';

import { submitForgetPassword, forgetPasswordKeyChange } from '../../actions/forgetPassword';


class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            title: NAVIGATION_FORGOT_TITLE,
            backUrl: '/sign-in',
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.data !== nextProps.data) {
            this.setState({
                email: nextProps.data.email,
            });
        }
    }

    handleChangeEmail(text) {
        this.setState({ email: text.trim() });
    }

    handlerSubmit(event) {
        event.preventDefault();
        if (this.state.email === '') {
            Popup.alert(EMAIL_IS_NOT_EMPTY);
            return;
        }
        if (!checkEmail(this.state.email)) {
            Popup.alert(EMAIL_IS_WRONG_MESSAGE_FOGOTPASS);
            return;
        }

        this.props.submitForgetPassword(this.state.email);
    }

    render() {
        return (
            <div className="reset-password">
                { !!this.props.data.loading && <div className="form-reset__loading" />}
                <Header title={this.state.title} backUrl={this.state.backUrl} />

                <main className="k-main">
                    <form className="form form-reset" onSubmit={(e) => this.handlerSubmit(e)}>
                        <div className="form__control multiple_line mb_30">
                            <div className="form__label">{GUIDE_FORGOT_MESSAGE}</div>
                            <div className="form__group-input mt_20">
                                <TextInput
                                    defaultValue={this.state.email}
                                    placeholder="メールアドしス"
                                    onChangeText={(text) => this.handleChangeEmail(text)} />
                            </div>
                        </div>

                        <button className="btn-custom  btn-active" disabled={this.props.data.loading} type="submit">
                            {BUTTON_RESET_PASSWORD_TITLE}
                        </button>
                    </form>
                </main>
            </div>
        );
    }
}

ResetPassword.propTypes = {
    submitForgetPassword: PropTypes.func.isRequired,
    data: PropTypes.object,
};

const mapStateToProps = state => ({ data: state.forgetPassword });

export default connect(mapStateToProps, { submitForgetPassword, forgetPasswordKeyChange })(ResetPassword);
