import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-web';
import MainLayout from '../../components/MainLayout';
import CallLogItem from '../../components/CallLog';
import CallLogService from '../../services/callLog';
import { checkLoadMore } from '../../utils/helpers';
import {
    CALL_LOG_TITLE,
    LOG_OUTGOING_TAB,
} from '../../constants/messages';
import './index.scss';

class CallLog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: CALL_LOG_TITLE,
            active: 2,
            histories: [],
            hasLoadMore: true,
            loading: false,
        };
    }

    componentDidMount() {
        this.getCallLog();
        window.addEventListener('scroll', this.handleLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (checkLoadMore(event)) {
            this.getCallLog();
        }
    };

    getCallLog = (type = 2) => {
        const { histories } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            CallLogService.getCallLog(type, histories.length)
                .then(response => {
                    this.setState({
                        histories: histories.concat(response.data),
                        hasLoadMore: response.hasLoadMore,
                    });
                })
                .catch(() => {
                    this.setState({
                        hasLoadMore: false,
                    });
                })
                .then(() => {
                    this.setState({
                        loading: false,
                    });
                });
        });
    };

    changeTitle(type) {
        if (type !== this.state.active) {
            this.setState({
                active: type,
                histories: [],
                loading: false,
                hasLoadMore: true,
                scrollTop: 0,
            }, () => this.getCallLog(type));
        }
    }

    render() {
        const { active, histories } = this.state;
        const listHistories = histories.map((user, index) => <CallLogItem type="CallLog" user={user} key={index} />);

        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list two">
                    <li className={`sub-header__item ${active === 2 ? 'is_active' : ''}`} onClick={() => this.changeTitle(2)}>
                        {CALL_LOG_TITLE}
                    </li>
                    <li className={`sub-header__item ${active === 1 ? 'is_active' : ''}`} onClick={() => this.changeTitle(1)}>
                        {LOG_OUTGOING_TAB}
                    </li>
                </ul>
            </section>
        );

        return (
            <MainLayout
                className="call-log"
                title={this.state.title}
                hasFooter={false}
                hasSidebarLeft={false}
                hasBackButton
                backFunction='/home'
                subHeader={subHeader}
            >
                <div className="list-users">
                    {listHistories}
                    <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'}>
                        <ActivityIndicator animating color="black" size="large" />
                    </div>
                </div>
            </MainLayout>
        );
    }
}


export default CallLog;
