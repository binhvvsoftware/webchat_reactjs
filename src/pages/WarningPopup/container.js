import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './index.scss';
import FullScreenModal from '../../components/FullScreenModal';
import WarningPopupService from '../../services/warningPopup';
import { openWebView } from '../../actions/webview';

class WarningPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listWarningPopup: [],
            showModal: false,
            read: false,
        };
    }

    componentWillMount() {
        WarningPopupService.getWarningPopup()
            .then(response => {
                const listWarningPopup = response.data;
                this.setState({
                    listWarningPopup,
                    showModal: !!listWarningPopup.length,
                });
            });
    }

    handleReadWarning = (read) => {
        this.setState({
            read: !read,
        });
    };

    handleSubmit = () => {
        const { read, listWarningPopup } = this.state;

        if (!read) {
            return;
        }

        WarningPopupService.updateWarningPopup(listWarningPopup[0].popup_id)
            .then(() => {
                listWarningPopup.splice(0, 1);
                this.setState({
                    listWarningPopup,
                    read: false,
                    showModal: !!listWarningPopup.length,
                });
            });
    };

    handleClickLink = (event) => {
        event.preventDefault();
        try {
            this.props.onOpenWebView({
                url: event.target.getAttribute('href'),
                backButton: true,
            });
        } catch (error) {
            //
        }
    };

    render() {
        const { showModal, listWarningPopup, read } = this.state;

        if (listWarningPopup.length === 0) {
            return '';
        }

        const warningPopup = listWarningPopup[0];
        setTimeout(() => {
            const tags = document.querySelectorAll('.main-popup-warning a');

            tags.forEach((item) => {
                if (!item.getAttribute('data-add-event')) {
                    item.addEventListener('click', this.handleClickLink, true);
                    item.setAttribute('data-add-event', 'true');
                }
            });
        }, 500);

        return (
            <FullScreenModal className="popup-warning" isOpen={ showModal }>
                <div className="main-popup-warning">
                    <div className="title-popup-warning"><span className="icon icon-warning"></span> 警告!</div>
                    <div className="my-content-popup">
                        <div className="content-popup-warning" dangerouslySetInnerHTML={ { __html: warningPopup.content } }/>

                        <div className="read-popup-warning">
                            <label className="">
                                <input
                                    className="radio-button"
                                    type="radio"
                                    name="region"
                                    id="warning-popup-id"
                                    onChange={ () => this.handleReadWarning(read) }
                                    checked={ read }/>
                                <label htmlFor="warning-popup-id" className="">
                                    <span className="radio-button__icon"/>
                                </label>
                                同意します
                            </label>
                        </div>
                    </div>
                    <div className="footer-popup">
                        <button
                            disabled={ !read }
                            type="button"
                            className="btn-popup-warning"
                            onClick={ () => this.handleSubmit() }>
                            閉じる
                        </button>
                    </div>
                </div>
            </FullScreenModal>
        );
    }
}

WarningPopup.propTypes = {
    onOpenWebView: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
    onOpenWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WarningPopup);


