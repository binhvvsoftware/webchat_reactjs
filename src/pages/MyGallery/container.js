import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-web';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Slider from 'react-slick';
import union from 'lodash/union';
import Header from '../../components/Header';
import Image from '../../components/Image';
import * as endpoint from '../../constants/endpoint_constant';
import MyGalleryService from '../../services/myGallery';
import MediaService from '../../services/media';
import { checkLoadMore, checkOrientationImage } from '../../utils/helpers';
import history,{ getLocationHistory } from '../../utils/history';
import FullScreenModal from '../../components/FullScreenModal';
import './index.scss';
import storage from '../../utils/storage';
import {
    DELETE_PHOTO_TITLE,
    DELETE_PHOTO_MESSAGE,
    ALERT_MESSAGE_UPLOAD_IMAGE_ERROR,
    UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE,
} from '../../constants/messages';

class MyGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [],
            userInfo: storage.getUserInfo(),
            title: 'ヒミツの写真',
            backUrl: history.checkAndBack,
            openDialog: false,
            imageIndex: '',
            hasLoadMore: true,
            loading: false,
            loadingImage: false,
            showImageDetail: null,
        };
    }

    componentDidMount() {
        const locationHistory = getLocationHistory();
        let showImageDetail = null;
        if (locationHistory.current.state && locationHistory.current.state.imageId) {
            showImageDetail = locationHistory.current.state.imageId;
        }
        this.setState({ showImageDetail }, this.handleCallApi);
        window.addEventListener('scroll', this.handleLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLoadMore, true);
    }

    handleCallApi = () => {
        const { images, showImageDetail } = { ...this.state };

        if (this.state.loadingImage || !this.state.hasLoadMore) {
            return;
        }

        this.setState({
            loadingImage: true,
        }, () => {
            MyGalleryService.getMyListBackStage(images.length).then(response => {
                const { data } = response;
                const newImages = union(images.concat(data));
                if (showImageDetail) {
                    if (data.indexOf(showImageDetail) === -1) {
                        data.push(showImageDetail);
                    }

                    this.setState({
                        openDialog: true,
                        imageIndex: newImages.indexOf(showImageDetail),
                    });

                }

                this.setState({
                    images: newImages,
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loadingImage: false,
                });
            });
        });
    };

    handleLoadMore = (event) => {
        if (this.state.isOpenMessage) {
            return;
        }
        if (checkLoadMore(event)) {
            this.handleCallApi();
        }
    };

    handleGallery = (index) => {
        this.setState({
            openDialog: true,
            imageIndex: index,
        });
    };

    handleChangeIndex = (newIndex) => {
        this.setState({ imageIndex: newIndex });
    };

    handleClose = () => {
        if (this.state.showImageDetail) {
            history.checkAndBack();
        } else {
            this.setState({
                openDialog: false,
                loading: false,
            });
        }
    };

    confirmDelete = () => {
        Popup.create({
            title: DELETE_PHOTO_TITLE,
            content: DELETE_PHOTO_MESSAGE,
            buttons: {
                left: [{
                    text: 'キャンセル',
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: 'OK',
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        this.deleteImage();
                    },
                }],
            },
        });
    };

    deleteImage = () => {
        const { images, imageIndex } = this.state;

        this.setState({ loading: true }, () => {
            MyGalleryService.deleteBackStage(images[imageIndex])
                .then(() => {
                    images.splice(imageIndex, 1);
                    this.setState({
                        images,
                    });
                })
                .catch(e => e)
                .then(this.handleClose);
        });
    };

    handleFileChange = (event) => {
        const file = event.target.files[0];

        if (!file || (file && !/(\.png|\.gif|\.jpg|\.jpeg)$/i.test(file.name))) {
            Popup.alert(ALERT_MESSAGE_UPLOAD_IMAGE_ERROR);
            return;
        }

        this.setState({ loading: true }, () => {
            checkOrientationImage(file, (imgBase64, orientation) => {
                this.setState({ fileData: { preview: imgBase64, orientation } }, () => {
                    this.handleUploadImage();
                });
            });
        });
    };

    handleUploadImage = () => {
        MediaService.uploadImageBase64(this.state.fileData.preview, endpoint.IMAGE_BACKSTAGE, this.state.fileData.orientation)
            .then(response => {
                if (response.data.isApprove) {
                    const { images } = this.state;
                    images.unshift(response.data.image_id);
                    this.fileInput.value = '';
                    this.setState({
                        images,
                    }, () => {
                        this.forceUpdate();
                    });
                } else {
                    Popup.alert(UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE);
                }
            })
            .then(() => this.setState({ loading: false }));
    };

    render() {
        const { images, imageIndex, userInfo, loading } = this.state;
        const locationHistory = getLocationHistory();
        if(locationHistory.current.state && locationHistory.current.state.imageId && images.length === 0) {
            return  <div className="gallery__loading"/>;
        }
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.handleChangeIndex,
            initialSlide: imageIndex,
        };

        const displayImage = images.map((image, index) =>
            (<div className="my-backstage__item" key={ image + index } onClick={ () => this.handleGallery(index) }>
                <Image isAvatar className="my-backstage__image" imageId={ image }/>
            </div>),
        );

        const sliderContent = images.map((image, index) =>
            <div className="my-photo" key={ image + index } data-id={ image }>
                <Image imageId={ image } imgKind={ 2 } isBackground alt="img"/>
            </div>,
        );

        /* eslint-disable no-return-assign */
        return (
            <div className="gallery">
                { loading && <div className="gallery__loading"/> }
                <Image isAvatar className="gallery-background" imageId={ userInfo.ava_id } imgKind={ 2 }/>
                <Header title={ this.state.title } backUrl={ this.state.backUrl }/>
                <main className="k-main">
                    <div className="my-backstage">
                        <div className="my-backstage__item">
                            <div className="my-backstage__button" onClick={ () => this.fileInput.click() }>
                                <div>
                                    <input type="file" name="avatar" accept="image/*" ref={ input => { this.fileInput = input } } onChange={ (e) => this.handleFileChange(e) }/>
                                    <i className="icon icon-plus"/>
                                    <span>写真の追加</span>
                                </div>
                            </div>
                        </div>
                        { displayImage }
                    </div>
                    <div className={ this.state.loadingImage && this.state.hasLoadMore ? 'loading-box' : 'hidden' }>
                        <ActivityIndicator animating color="black" size="large"/>
                    </div>

                    <FullScreenModal
                        className="gallery"
                        fullScreen
                        isOpen={ this.state.openDialog }
                        onClose={ this.handleClose }
                        hasBackButton
                        title={ `${imageIndex + 1}/${images.length}` }
                    >
                        {
                            this.state.openDialog && (
                                <Slider { ...settings } ref={ slider => this.slider = slider }>
                                    { sliderContent }
                                </Slider>
                            )
                        }
                        <button className="btn-delete-photo" type="button" onClick={ () => this.confirmDelete() }>
                            <i className="icon icon-recycle-grey"/>
                            <span className="d-block">写真の削除</span>
                        </button>
                    </FullScreenModal>
                </main>
            </div>
        );
    }
}

MyGallery.propTypes = {
    location: PropTypes.object,
};

export default MyGallery;
