import React, { Component } from 'react';
import Popup from 'react-popup';
import './index.scss';
import Image from '../../components/Image';
import MainLayout from '../../components/MainLayout';
import RankingService from '../../services/ranking';
import history from '../../utils/history';

class Ranking extends Component {
    constructor(props) {
        super(props);
        this.state={
            isLoading: true,
            listRanking : [],
            target : 'daily',
            listTopRanking : [],
        };
    }

    componentDidMount(){
        this.handleCallApi();
    }

    handleGetRanking(target) {
        this.setState({
            target,
            isLoading: true,
        }, () => {
            this.handleCallApi();
        });
    }

    handleCallApi = () => {
        RankingService.getList(this.state.target).then(response => {
            const listTopRanking = response.data.splice(0, 3);
            this.setState({
                listRanking: response.data,
                listTopRanking,
                isLoading: false,
            });
        });
    }

    redirectToUser = (user) => {
        if (user.flag) {
            return history.push(user.profile_url);
        }

        return Popup.alert('このユーザーは退会しました', 'エラー');
    }

    render() {
        const { listRanking,target,listTopRanking } = this.state;
        const daily = target === 'daily' ? 'is_active' : '';
        const weekly = target === 'weekly' ? 'is_active' : '';
        const monthly = target === 'monthly' ? 'is_active' : '';

        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list three">
                    <li className={ `sub-header__item ${daily}` } onClick={ () => this.handleGetRanking('daily') }>
                        前日
                    </li>
                    <li className={ `sub-header__item ${weekly}` } onClick={ () => this.handleGetRanking('weekly') }>
                        週間
                    </li>
                    <li className={ `sub-header__item ${monthly}` } onClick={ () => this.handleGetRanking('monthly') }>
                        月間
                    </li>
                </ul>
            </section>
        );

        const handleStatusUser = (user) => {
            if (user.video_call_waiting || user.voice_call_waiting) {
                return <div className="on-status"><span className="icon icon-time-white" />電話できます</div>
            }

            if (user.current_rank === 1 || user.current_rank === 2 || user.current_rank === 3) {
                return <div className="off-status"><span className="icon icon-time-white" />{user.lastOnline}</div>
            }

            return <div className="off-status"><span className="icon icon--time-grey" />{user.lastOnline}</div>
        };

        const displayListRanking = listRanking.map((user, index) => {
            const position = index + listTopRanking.length + 1;

            return (
                <div className="ranking-item" key={user.user_id} onClick={() => this.redirectToUser(user)}>
                    <div className="ranking-number">
                        <strong>{position}</strong>位
                    </div>
                    <div className="user-img">
                        <Image isAvatar imageId={ user.ava_id }/>
                    </div>
                    <span className="thumbnail-icon">
                        <span className={user.icon_status} />
                    </span>
                    <div className="user-info">
                        {handleStatusUser(user)}
                        <div className ="info">
                            <span style={{marginRight: "5px"}}>{user.user_name}</span>
                            <span className="">{user.age}歳</span>
                        </div>
                    </div>
                    <div className="ranking-info" dangerouslySetInnerHTML={{ __html: user.position }} />
                </div>
            )
        });

        const rankingTop = (user, position) => user &&
            <div className={ `ranking-top top-${position}` } onClick={() => this.redirectToUser(user)}>
                <div className="ranking-r">
                    <Image isAvatar className="user-pic" imageId={ user.ava_id }/>
                </div>

                <div className="user-name">{user.user_name}</div>
                <div className="ranking-info" dangerouslySetInnerHTML={{ __html: user.position }} />
                {handleStatusUser(user)}
            </div>

        return (
            <MainLayout className="ranking" title="ランキング" subHeader={subHeader} hasFooter={false}>
                { this.state.isLoading ? <div className="ranking__loading" /> : null }
                { listTopRanking.length !== 0 &&
                <div className="top-ranking">
                    { rankingTop(listTopRanking[0], 1) }
                    { rankingTop(listTopRanking[1], 2) }
                    { rankingTop(listTopRanking[2], 3) }
                </div>
                }
                <div className="list-ranking">
                    {displayListRanking}
                </div>
            </MainLayout>
        );
    }
}

export default Ranking;
