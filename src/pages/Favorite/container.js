import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-web';
import MainLayout from '../../components/MainLayout';
import CallLogItem from '../../components/CallLog';
import ListFavoriteService from '../../services/favorite';
import { checkLoadMore } from '../../utils/helpers';
import './index.scss';

class Favorite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'お気に入り',
            active: 'lst_fvt',
            favNum: 0,
            fvtNum: 0,
            dataFavorite: [],
            hasLoadMore: true,
            loading: false,
        };
    }

    componentDidMount() {
        const getNumberFavorite = {
            api: 'get_connection_number',
        };

        ListFavoriteService.getNumberFavorite(getNumberFavorite)
            .then(response => {
                this.setState({
                    favNum: response.data.fav_num,
                    fvtNum: response.data.fvt_num,
                });
            });
        ListFavoriteService.getListFavorite('lst_fvt')
            .then(response => {
                this.setState({
                    dataFavorite: response.data,
                });
            });
        window.addEventListener('scroll', this.handleLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (checkLoadMore(event)) {
            this.handleCallApi(this.state.active);
        }
    };

    handleCallApi = (target = 'lst_fav') => {
        const { dataFavorite } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            ListFavoriteService.getListFavorite(target, dataFavorite.length).then(response => {
                this.setState({
                    dataFavorite: dataFavorite.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    };

    changeTitle(target) {
        if (target === 'lst_fav') {
            this.setState({
                title: 'お気に入り',
            });
        } else if (target === 'lst_fvt') {
            this.setState({
                title: 'フォロワー',
            });
        }

        if (target !== this.state.active) {
            this.setState({
                active: target,
                dataFavorite: [],
                loading: false,
                hasLoadMore: true,
            }, () => this.handleCallApi(target));
        }
    }

    render() {
        const { active, favNum, fvtNum, dataFavorite } = this.state;
        const text = ( this.state.active === 'lst_fvt' ) ? <div className="text-notice">あなたをお気に入り登録している人はいません</div> : <div className="text-notice">お気に入り登録がありません。<br/>気になる人をお気に入りに追加してください。</div>
        const listDataFavorite = dataFavorite.map((user,index) => <CallLogItem type="Favorite" user={user} key={index}/>);
        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list two">
                    <li className={ `sub-header__item ${active === 'lst_fvt' ? 'is_active' : ''}` } onClick={ () => this.changeTitle('lst_fvt') }>
                        フォロワー  {fvtNum}
                    </li>
                    <li className={ `sub-header__item ${active === 'lst_fav' ? 'is_active' : ''}` } onClick={ () => this.changeTitle('lst_fav') }>
                        お気に入り  {favNum}
                    </li>
                </ul>
            </section>
        );

        return (
            <MainLayout
                className="favorite-page"
                title={ this.state.title }
                hasFooter={ false }
                hasSidebarLeft={ false }
                hasBackButton
                backFunction='/home'
                subHeader={subHeader}
            >
                <div className="list-users">
                    <div className="text-note">あなたをお気に入り登録している人です</div>
                    { dataFavorite.length === 0 ? <div className="text-notice">{ text }</div> : ''}
                    {listDataFavorite}
                    <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden' } >
                        <ActivityIndicator animating color="black" size="large"/>
                    </div>
                </div>
            </MainLayout>
        );
    }
}


export default Favorite;
