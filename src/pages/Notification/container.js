import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native-web';
import debounce from 'lodash/debounce';
import NewsComponent from '../../components/News';
import NoticeComponent from '../../components/Notice';
import SearchService from '../../services/search';
import MainLayout from '../../components/MainLayout';
import { updateAttentionNumbers } from '../../actions/Chat';
import { checkLoadMore } from '../../utils/helpers';
import UserService from '../../services/user';

class Notification extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            hasLoadMore: true,
            loading: false,
            target: 'notice',
            news: [],
            notices: [],
        };
        this.handleCheckLoadMore = debounce(this.handleLoadMore, 350);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleCheckLoadMore, true);
        this.getNoticeData();
        this.getNewsData();
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleCheckLoadMore, true);
        this.isUnmounted = true;
    };

    handleTab = (target) => {
        this.setState({
            target,
        });
    }

    handleLoadMore = (event) => {
        if (this.state.isOpenMessage || this.state.isOpenMenu || this.state.target !== 'notice') {
            return;
        }

        if (checkLoadMore(event)) {
            this.getNoticeData();
        }
    };

    getNewsData = () => {
        if (this.state.news.length) {
            return;
        }

        this.setState({ loading: true });

        SearchService.getNews().then(response => {
            if (!this.isUnmounted) {
                this.setState({
                    news: response.data,
                    loading: false,
                });
            }
        }).catch(() => {
            if (!this.isUnmounted) {
                this.setState({ loading: false });
            }
        });
    };

    getNoticeData = () => {
        this.setState({ loading: true });

        const { notices } = this.state;

        const timestamp = notices[notices.length - 1] ? notices[notices.length - 1].time : null;

        SearchService.getNoticeData(timestamp).then(response => {
            if (!this.isUnmounted) {
                this.setState({
                    notices: notices.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                    loading: false,
                });
            }

            UserService.getAttentionNumber().then(responseAttention => {
                this.props.updateAttentionNumbers({ attentionNumbers: responseAttention.data });
            });
        }).catch(() => {
            if (!this.isUnmounted) {
                this.setState({ loading: false });
            }
        });
    };

    render() {
        const subHeader = (
            <section className="sub-header">
                <ul className="sub-header__list two">
                    <li className={ `text-center sub-header__item ${this.state.target === 'news' ? 'is_active' : ''}` } onClick={ () => this.handleTab('news') }>
                        最新ニュース
                    </li>
                    <li className={ `text-center sub-header__item ${this.state.target === 'notice' ? 'is_active' : ''}` } onClick={ () => this.handleTab('notice') }>
                        システム通知
                    </li>
                </ul>
            </section>
        );

        return (
            <MainLayout hasFooter={false} className="has_footer has_sub-header" title="お知らせ" subHeader={subHeader}>
                <div className="news-notice">
                    { this.state.target === 'news' ?
                        <NewsComponent news={ this.state.news }/>
                        :
                        <NoticeComponent notice={ this.state.notices }/>
                    }

                    <div className={ this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden' }>
                        <ActivityIndicator animating color="black" size="large"/>
                    </div>
                </div>
            </MainLayout>
        );
    }
}

Notification.propTypes = {
    updateAttentionNumbers: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
    updateAttentionNumbers: (payload) => dispatch(updateAttentionNumbers(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
