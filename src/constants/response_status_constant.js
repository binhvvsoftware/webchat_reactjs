import {
    HTTP_SUCCESS,
    HTTP_UNKNOWN_ERROR,
    HTTP_WRONG_DATA_FORMAT,
    HTTP_INVALID_TOKEN,
    HTTP_CHANGE_SETTING_TOKEN,
    HTTP_INVALID_ACCOUNT,
    HTTP_EMAIL_NOT_FOUND,
    HTTP_INVALID_EMAIL,
    HTTP_EMAIL_REGISTERED,
    HTTP_EMAIL_REGISTED,
    HTTP_SEND_MAIL_FAIL,
    HTTP_INVALID_USER_NAME,
    HTTP_INVALID_BIRTHDAY,
    HTTP_UNUSABLE_EMAIL,
    HTTP_DUPLICATE_USER_NAME,
    HTTP_INCORRECT_PASSWORD,
    HTTP_INVALID_PASSWORD,
    HTTP_NOT_FOR_USERS,
    HTTP_UPLOAD_IMAGE_ERROR,
    HTTP_UPLOAD_FILE_ERROR,
    HTTP_ACCESS_DENIED,
    HTTP_INVALID_FILE,
    HTTP_FILE_NOT_FOUND,
    HTTP_WRONG_VERIFICATION_CODE,
    HTTP_USER_NOT_EXIST,
    HTTP_LOCKED_USER,
    HTTP_LOCK_FEATURE,
    HTTP_NOT_ENOUGHT_POINT,
    HTTP_PATNER_NOT_ENOUGHT_POINT,
    HTTP_BLOCK_USER,
    HTTP_WRONG_URL,
    HTTP_EMPTY_DATA,
    HTTP_SERVER_MAINTAIN,
    HTTP_OUT_OF_DATE,
    HTTP_DISABLE_EMAIL,
    HTTP_IMAGE_NOT_FOUND,
    HTTP_BUZZ_NOT_FOUND,
    HTTP_COMMENT_NOT_FOUND,
    HTTP_EXISTS_DATA,
    HTTP_CM_CODE_EXISTS,
    HTTP_PERMISSION_DENIED,
    HTTP_OUT_OF_DATE_API,
    HTTP_UNUSABLE_VERSION_APPLICATION,
    HTTP_INVALID_APPLICATION_NAME,
    HTTP_CATEGORY_NAME_EXIST,
    HTTP_NO_CHANGE,
    HTTP_TRANSACTION_EXIST,
    HTTP_NOT_DOWNLOAD,
    HTTP_WRONG_FILE_TYPE,
    HTTP_UPDATE_NEWS_TOKEN,
    HTTP_CAN_NOT_DELETE_APPLICATION,
    HTTP_MISSION_LOCKED,
    HTTP_MISSION_NOT_COMPLETE,
    HTTP_PUSH_SUCCESS,
    HTTP_PUSH_ERROR,
    HTTP_OUT_OF_LIMIT_WARNING,
    HTTP_POINT_CAMPAIGN_INVALID,
    HTTP_GET_TUTORIAL_POINT_BEFORE,
} from './messages';


export const SUCCESS = 0;
export const UNKNOWN_ERROR = 1;
export const WRONG_DATA_FORMAT = 2;
export const INVALID_TOKEN = 3;
export const CHANGE_SETTING_TOKEN = 7;
export const INVALID_ACCOUNT = 9;

export const EMAIL_NOT_FOUND = 10;
export const INVALID_EMAIL = 11;
export const EMAIL_REGISTED = 12;
export const EMAIL_REGISTERED = 12;
export const SEND_MAIL_FAIL = 13;
export const INVALID_USER_NAME = 14;
export const INVALID_BIRTHDAY = 15;
export const UNUSABLE_EMAIL = 16;
export const DUPLICATE_USER_NAME = 17;

export const INCORRECT_PASSWORD = 20;
export const INVALID_PASSWORD = 21;
export const NOT_FOR_USERS = 23;

export const UPLOAD_IMAGE_ERROR = 30;
export const UPLOAD_FILE_ERROR = 35;

export const ACCESS_DENIED = 41;
export const INVALID_FILE = 45;
export const FILE_NOT_FOUND = 46;

export const WRONG_VERIFICATION_CODE = 90;
export const USER_NOT_EXIST = 80;
export const LOCKED_USER = 81;

export const LOCK_FEATURE = 50;
export const NOT_ENOUGHT_POINT = 70;
export const PATNER_NOT_ENOUGHT_POINT = 71;
export const BLOCK_USER = 60;
export const WRONG_URL = 13;
export const EMPTY_DATA = 79;

// back end
export const SERVER_MAINTAIN = 8;
export const OUT_OF_DATE = 9;
export const DISABLE_EMAIL = 15;
export const IMAGE_NOT_FOUND = 30;
export const BUZZ_NOT_FOUND = 40;
export const COMMENT_NOT_FOUND = 43;
export const EXISTS_DATA = 75;
export const CM_CODE_EXISTS = 70;
export const PERMISSION_DENIED = 999;

// main
export const OUT_OF_DATE_API = 5;
export const UNUSABLE_VERSION_APPLICATION = 6;
export const INVALID_APPLICATION_NAME = 7;
export const CATEGORY_NAME_EXIST = 70;
export const NO_CHANGE = 4;
export const TRANSACTION_EXIST = 99;
export const NOT_DOWNLOAD = 98;

export const WRONG_FILE_TYPE = 97;

export const UPDATE_NEWS_TOKEN = 96;

export const CAN_NOT_DELETE_APPLICATION = 51;

export const MISSION_LOCKED = 100;                               // ITS#23766
export const MISSION_NOT_COMPLETE = 101;                         // ITS#23766

export const PUSH_SUCCESS = 200;
export const PUSH_ERROR = 400;

export const OUT_OF_LIMIT_WARNING = 37;                            // ITS#28147

export const POINT_CAMPAIGN_INVALID = 110;                       // ITS#27959

export const GET_TUTORIAL_POINT_BEFORE = 112;

export const HTTP_WOMEN_CAN_NOT_LOGIN = 'women_can_not_login';

export function getMessageFromCode(code) {
    switch (code) {
        case SUCCESS:
            return HTTP_SUCCESS;
        case UNKNOWN_ERROR:
            return HTTP_UNKNOWN_ERROR;
        case WRONG_DATA_FORMAT:
            return HTTP_WRONG_DATA_FORMAT;
        case INVALID_TOKEN:
            return HTTP_INVALID_TOKEN;
        case CHANGE_SETTING_TOKEN:
            return HTTP_CHANGE_SETTING_TOKEN;
        case INVALID_ACCOUNT:
            return HTTP_INVALID_ACCOUNT;
        case EMAIL_NOT_FOUND:
            return HTTP_EMAIL_NOT_FOUND;
        case INVALID_EMAIL:
            return HTTP_INVALID_EMAIL;
        case EMAIL_REGISTERED:
            return HTTP_EMAIL_REGISTERED;
        case EMAIL_REGISTED:
            return HTTP_EMAIL_REGISTED;
        case SEND_MAIL_FAIL:
            return HTTP_SEND_MAIL_FAIL;
        case INVALID_USER_NAME:
            return HTTP_INVALID_USER_NAME;
        case INVALID_BIRTHDAY:
            return HTTP_INVALID_BIRTHDAY;
        case UNUSABLE_EMAIL:
            return HTTP_UNUSABLE_EMAIL;
        case DUPLICATE_USER_NAME:
            return HTTP_DUPLICATE_USER_NAME;
        case INCORRECT_PASSWORD:
            return HTTP_INCORRECT_PASSWORD;
        case INVALID_PASSWORD:
            return HTTP_INVALID_PASSWORD;
        case NOT_FOR_USERS:
            return HTTP_NOT_FOR_USERS;
        case UPLOAD_IMAGE_ERROR:
            return HTTP_UPLOAD_IMAGE_ERROR;
        case UPLOAD_FILE_ERROR:
            return HTTP_UPLOAD_FILE_ERROR;
        case ACCESS_DENIED:
            return HTTP_ACCESS_DENIED;
        case INVALID_FILE:
            return HTTP_INVALID_FILE;
        case FILE_NOT_FOUND:
            return HTTP_FILE_NOT_FOUND;
        case WRONG_VERIFICATION_CODE:
            return HTTP_WRONG_VERIFICATION_CODE;
        case USER_NOT_EXIST:
            return HTTP_USER_NOT_EXIST;
        case LOCKED_USER:
            return HTTP_LOCKED_USER;
        case LOCK_FEATURE:
            return HTTP_LOCK_FEATURE;
        case NOT_ENOUGHT_POINT:
            return HTTP_NOT_ENOUGHT_POINT;
        case PATNER_NOT_ENOUGHT_POINT:
            return HTTP_PATNER_NOT_ENOUGHT_POINT;
        case BLOCK_USER:
            return HTTP_BLOCK_USER;
        case WRONG_URL:
            return HTTP_WRONG_URL;
        case EMPTY_DATA:
            return HTTP_EMPTY_DATA;
        case SERVER_MAINTAIN:
            return HTTP_SERVER_MAINTAIN;
        case OUT_OF_DATE:
            return HTTP_OUT_OF_DATE;
        case DISABLE_EMAIL:
            return HTTP_DISABLE_EMAIL;
        case IMAGE_NOT_FOUND:
            return HTTP_IMAGE_NOT_FOUND;
        case BUZZ_NOT_FOUND:
            return HTTP_BUZZ_NOT_FOUND;
        case COMMENT_NOT_FOUND:
            return HTTP_COMMENT_NOT_FOUND;
        case EXISTS_DATA:
            return HTTP_EXISTS_DATA;
        case CM_CODE_EXISTS:
            return HTTP_CM_CODE_EXISTS;
        case PERMISSION_DENIED:
            return HTTP_PERMISSION_DENIED;
        case OUT_OF_DATE_API:
            return HTTP_OUT_OF_DATE_API;
        case UNUSABLE_VERSION_APPLICATION:
            return HTTP_UNUSABLE_VERSION_APPLICATION;
        case INVALID_APPLICATION_NAME:
            return HTTP_INVALID_APPLICATION_NAME;
        case CATEGORY_NAME_EXIST:
            return HTTP_CATEGORY_NAME_EXIST;
        case NO_CHANGE:
            return HTTP_NO_CHANGE;
        case TRANSACTION_EXIST:
            return HTTP_TRANSACTION_EXIST;
        case NOT_DOWNLOAD:
            return HTTP_NOT_DOWNLOAD;
        case WRONG_FILE_TYPE:
            return HTTP_WRONG_FILE_TYPE;
        case UPDATE_NEWS_TOKEN:
            return HTTP_UPDATE_NEWS_TOKEN;
        case CAN_NOT_DELETE_APPLICATION:
            return HTTP_CAN_NOT_DELETE_APPLICATION;
        case MISSION_LOCKED:
            return HTTP_MISSION_LOCKED;
        case MISSION_NOT_COMPLETE:
            return HTTP_MISSION_NOT_COMPLETE;
        case PUSH_SUCCESS:
            return HTTP_PUSH_SUCCESS;
        case PUSH_ERROR:
            return HTTP_PUSH_ERROR;
        case OUT_OF_LIMIT_WARNING:
            return HTTP_OUT_OF_LIMIT_WARNING;
        case POINT_CAMPAIGN_INVALID:
            return HTTP_POINT_CAMPAIGN_INVALID;
        case GET_TUTORIAL_POINT_BEFORE:
            return HTTP_GET_TUTORIAL_POINT_BEFORE;
        default:
            return HTTP_UNKNOWN_ERROR;
    }
}
