import keyMirror from 'keymirror';

export default keyMirror({
    UPDATE_FREE_PAGE: null,
    OPEN_CHAT_CONVERSATION: null,
    CLOSE_CHAT_CONVERSATION: null,
    OPEN_SIDEBAR_MENU: null,
    CLOSE_SIDEBAR_MENU: null,
    PUSH_NOTIFICATION: null,
    UPDATE_NOTIFICATION: null,
    UPDATE_LIST_ACCOUNT_SYSTEM: null,
    OPEN_BUYPOINT: null,
    CLOSE_BUYPOINT: null,
});
