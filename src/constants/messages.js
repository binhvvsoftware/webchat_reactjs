/*
  Localizable.strings
  Kyuun

  Copyright © 2018 NTQ-Solution. All rights reserved.
*/

// Common ---------------------------------------------------------
export const ALERT_BUTTON_YES_TITLE = 'はい';
export const ALERT_BUTTON_NO_TITLE = 'いいえ';
export const ALERT_BUTTON_CANCEL_TITLE = 'キヤンセル';
export const OK = 'OK';
export const LOADING = 'ローディング...';
export const PROCESSING = '処理中...';
export const SAVING = '保存中…';
export const PULL_TO_REFRESH = '下に引いて更新してください...';
export const RELEASE_TO_REFRESH = 'リリースして更新してください...';
//----------------------------------------------------------------

// Navagation bar ------------------------------------------------
export const DONE = '完了';
export const FORGOT_PASS_BUTTON = 'パスワードはお忘れですか？';
export const LOGIN_BUTTON = 'ログイン';
export const EMAIL_IS_EMPTY_MESSAGE = 'メールを入力してください';
export const EMAIL_IS_WRONG_MESSAGE = 'メールが無効です';
export const EMAIL_IS_NOT_REGITTER_MESSAGE = 'メールアドレスが登録されていません';
export const FORGET_PASSWORD_SUCCESS_MESSAGE = '認証メ一ルを送信しました。 スパムフオルダもチエックしてください';
export const ERROR_SYSTEM = 'システムエラー';
export const EMAIL_IS_TOO_LONG_MESSAGE = 'メールが長すぎる。';
export const PASSWORD_IS_EMPTY_MESSAGE = 'パスワードを入力してください';
export const TOKEN_IS_EMPTY_MESSAGE = 'デ一タ形式が不正です。';
export const TOKEN_IS_WRONG_MESSAGE = '認証コードが正しくありません。';
export const PASSWORD_OUT_OF_RANGE = 'パスワードの長さは6から12までです!';
export const EMAIL_HAS_BEEN_USED_PART2 = 'ご入力されたメールアドレスで \n既にアカウントが登録されています。'
//----------------------------------------------------------------

// TOP -----------------------------------------------------------
export const TOP = 'TOP';
export const CALL_WAITING = '通話待機中';
export const NEW_PERSON = '新人';
export const ALL = 'すべて';

//----------------------------------------------------------------

// xxx -----------------------------------------------------------
export const LOGIN_FROM_OTHER_SYSTEM = '提携サイトでログイン';
export const CONTENT_ALERT_VOICE = '音声通話を発信します';
export const CONTENT_ALERT_VIDEO = 'ビデオ通話を発信します';
export const FREE_POINT_END_TITLE = '無料ポイントGET';
export const FREE_POINT_END = '無料ポイント（%d pts）\nをGETしました！';
export const BUTTON_GET_FREE_POINT_END = '続ける';
export const GET_ON_CREDIT_CARD = 'クレジットカード登録でGET';
export const GET_ON_PHONE_NUMBER = '電話番号認証でGET';
export const NOT_FREE_POINT_GET = '無料ポイントGETしない';
export const CONFIRM_AGE_TITLE = '身分証登録(年齢認証)';
export const CONFIRM_AGE_HEADER = 'ご登録ありがとうございます\n続いて身分証登録による年齢認証を行って下さい';
export const CONFIRM_AGE_NOTE = '※本サービスは18歳以上の方のみご利用いただけます';
export const CONFIRM_AGE_BUTTON = '年齢認証に進む';
export const PREPARE_ONE_POINT_TITLE = '身分証を1点ご用意ください';
export const FACE_PHOTO_ID = '顔写真付き身分証明書';
export const CONTENT_FACE_PHOTO_ID = '・運転免許証\n・住民基本台帳カード(顔写真付き)\n・パスポート';
export const NOT_FACE_PHOTO_ID = '顔写真付き身分証明書がない場合';
export const CONTENT_NOT_FACE_PHOTO_ID = '・健康保険証\n・住民基本台帳カード(顔写真なし)';
export const CONFIRM_AGE_NOTE_BOTTOM = '※身分証明書は有効期限内のものをご用意ください。\n※いずれか1点をご用意ください。';
export const GET_FREE_POINT_TITLE = 'ご登録ありがとうございます\nお好みの方法で無料ポイントをGETできます';
export const GET_FREE_POINT = '無料ポイントGET';
export const TOTAL_POINT_TITLE = '所持ポイント';
export const TITLE_INCOMING_CALL_WAITING = '通話設定';
export const TITLE_CALL_WAITING = '着信設定';
export const RECEIVE_AUDIO_CALL = '音声通話の受信許可';
export const RECEIVE_VIDEO_CALL = 'ビデオ通話の受信許可';
export const GUIDE_CALL_WAITING = 'チェックした通話方法のみ着信を受け付けます。\nチェックをはずすと着信拒否となります。';
export const LOG_INCOMMING_CALL_TAB = '着信ログ';
export const LOG_OUTGOING_TAB = '発信ログ';
export const CALL_LOG_TITLE = '通話ログ';
export const TITLE_ALLOW_USER_CALL = '通話待ちアピール';
export const GUIDE_ALLOW_USER_CALL = 'ボタンをONにした通話方法のみ着信を受け付けます。\nボタンをOFFにすると着信拒否となります。';
export const GUIDE_ALLOW_USER_CALL_TIME = '通話待ちアピール状態：あと';
export const ITEM_0 = '設定しない';
export const ITEM_1 = '30分';
export const ITEM_2 = '1時間';
export const ITEM_3 = '2時間';
export const ITEM_4 = '3時間';
export const ITEM_5 = '4時間';
export const ITEM_6 = '5時間';
export const ITEM_7 = '6時間';
export const SET_WARNING_CALL_RECORD = 'ビデオ通話の映像での性器露出は禁止となります。';
export const SET_WARNING_CALL_RECORD_TITLE = '使い方';
export const NOTICE_DEALS_FROM_KYUUN = 'Kyuunからのお知らせ';
export const TIMELINE_POST = 'タイムライン投稿（お気に入りのみ）';
export const MESSAGE_WAITING = 'メッセージ受信';
export const FAVORITE_ONLY = 'お気に入りのみ';
export const GUID_PUSH_NOTIFICATION = 'アプリを強制終了すると、\n通知が遅れたり受信できない場合があります。';
export const LOGIN_SUCCESS_MESSAGE_CODE = 'ログインに成功しました';
export const UNKNOWN_ERROR_MESSAGE_CODE = '不明なエラー。後ほどやり直してください。';
export const WRONG_DATA_FORMAT_MESSAGE_CODE = 'データフォーマットが違います';
export const INVALID_EMAIL_MESSAGE_CODE = 'メールアドレス、もしくはパスワードが違います。';
export const INVALID_USERNAME_MESSAGE_CODE = 'ユーザー名が無効です';
export const DUPLICATE_USERNAME_MESSAGE_CODE = '既に使用されているなまえです。\n他のなまえを入れてください。';
export const INCORRECT_PASSWORD_MESSAGE_CODE = 'メールアドレス、もしくはパスワードが違います。';
export const EMAIL_NOT_FOUND_MESSAGE_CODE = 'メールが登録されていません';
export const EMAIL_REGISTERED_MESSAGE_CODE = '登録に失敗しました。既に存在するメールです。';
export const SEND_EMAIL_FAIL_MESSAGE_CODE = 'メール送信に失敗しました';
export const INCORRECT_CODE_MESSAGE_CODE = 'コードが無効です';
export const INVALID_TOKEN_MESSAGE_CODE = 'トークンが無効です';
export const INVALID_PASSWORD_MESSAGE_CODE = 'パスワードが無効です';
export const CHANGE_PASSWORD_SUCCESS_MESSAGE_CODE = 'パスワード変更しました';

export const PASSWORD_AND_MAIL_EMPTY_MESSAGE = 'メールアドレスとパスワードを人力してください';
export const EMAIL_INVALID = 'メ一ルが無効です';
export const PASSWORD_CONFIRM_NEW_DIFFERENT_MESSAGE = '新しいパスワードと確認用パスワ一ドが異なります';
export const CHANGE_PASSWORD_EMAIL_SUCCESS_MESSAGE = 'メールアドレスとパスワー ドの登録に 成功しま した';
export const CHANGE_PASSWORD_EMAIL_ERROR_MESSAGE = 'メールアドレスもしくはパスワードが違います。';
export const EMAIL_HAS_BEEN_USED = 'このメールアドレスは既に登録されています。';
export const REGISTER_FAILD_EMAIL_HAS_BEEN_USED = '登録に失敗しました。既に存在するメールです。';

export const TITLE_BLOCK_MESSAGE = 'ブ口ツク';
export const CONTENT_BLOCK_MESSAGE = 'さんをブ口ツクしますか?';
export const TITLE_REPORT_MESSAGE = ' ユ-ザ一の報告';
export const SUCCESS_CONTENT_REPORT_MESSAGE = ' ご報告ありがとうございました。 まもなくスタッフにて審査させていただきます。';

export const CHANGE_PASSWORD_SUCCESS_MESSAGE = 'パスワー ドが変更されま した';
export const TITE_MESSAGE_SETTING_NOTICE = 'オンライン通知';
export const MESSAGE_SETTING_NOTICE_NO_ONLINE = 'さんのオンライ ンを通知 しません。';

export const CHECK_NAME_IS_EMPTY_MESSAGE = '氏名を入力してください';
export const CHECK_BIRTHDAY_IS_REQUIRED_MESSAGE = '誕生日を選択してください';
export const CAMERA_NOT_SUPPORTED_MESSAGE = 'カメラを利用できません';
export const DEVICE_DOES_NOT_SUPPORT_CAMERA_MESSAGE = 'このデバイスにはカメラがありません';
export const UPLOADING_MESSAGE = 'アップロード中';
export const SUCCESS_MESSAGE = '成功';
export const VERIFY_CHANGE_PASSWORD_MESSAGE = '認証メールを送信しました。スパムフォルダもチェックしてください。';
export const CODE_VERIFY_EMPTY_MESSAGE = '認証コードが空です';
export const LOGOUT_APPLICATION_MESSAGE = '本当にログアウトしますか？';
export const OLD_PASSWORD_IS_EMPTY_MESSAGE = '古いパスワードを入力してください';
export const NEW_PASSWORD_IS_EMPTY_MESSAGE = '新しいパスワードを入力してください';
export const CHANGE_PASSWORD_ALERT_TITLE = 'パスワードの変更';
export const ARE_YOU_SURE_MESSAGE = '本当に変更しますか？';
export const PASSWORD_CHANGED_MESSAGE = 'パスワードが変更されました';
export const GUIDE_FORGOT_MESSAGE = '登録した際のメールアドレスを入力して下さい。';
export const PASSWORD_CHANGED = 'パスワードを更新しました。新しいパスワードにてログインしてください。';
export const BLOCK_USER_MSG_TITLE = 'ユーザーブロック';
export const BLOCK_USER_MSG_CONTENT = '%sさんをブロックしますか？';
export const CANNOT_CONNECT_TO_THE_HOST = 'サーバーに接続できません。';
export const NETWORK_ERROR = '「ネットワークエラー」ネットワーク状態を確認した後、やり直してください';
export const ERROR_TITLE = 'エラー';
export const CANNOT_ACCESS_PROFILE = '現在このプロフィールにアクセス出来ません。やり直してください';
export const ACCOUNT_IS_LOCKED = 'このアカウントはロックされています。管理者へ連絡してください。';
export const BIRTHDAY_INVALID = '誕生日が無効です。日付を確認してください。';
export const CHECK_REGION_IS_REQUIRED_MESSAGE = '地域登録してください';
export const OPEN_A_DELETED_VIDEO_FILE_IN_CHAT_MESSAGE = '再生期限が終了致しました。\nこちらの動画は再生が行なえません。';
export const REPORTVIEW_CANCEL = 'キャンセル';
export const REPORTVIEW_DONE = '完了';
export const CLOSE = '閉じる';
export const UNLOCK_BACKSTAGE_MESSAGE_ALERT_TEXT = 'この秘密の写真にアクセス出来ません。';
export const CANNOT_SHOW_DELETED_PIC_DETAIL_MESSAGE = ' 写真が見つかりません';
export const UPLOAD_BACKSTAGE_SUCCESSFULLY_TITLE = '秘密の写真は投稿されました';
export const UPLOAD_BACKSTAGE_SUCCESSFULLY_MESSAGE = 'この画像はスタッフによって承認された後、他のユーザーへ公開されます。';
export const UPLOAD_AVATAR_SUCCESSFULLY_CONTENT = 'プロフィール編集が完了しました。他のユーザー様からはこのように見えます。';
export const UPLOAD_AVATAR_SUCCESSFULLY_MESSAGE = 'このプロフィール画像はスタッフによって承認された後、他のユーザーへ公開されます。';
export const UPLOAD_AVATAR_SUCCESSFULLY_TITLE = 'プロフィール画像は投稿されました';

export const HEIGHT_FOOT_CELL_EXTENT = 'ﾌｨｰﾄ';
export const HEIGHT_INCH_CELL_EXTENT = 'ｲﾝﾁ';
export const COMPLATE_REGVIEW_TEXT = '<html> <head><style type';

export const LOCATION_SEVICES_OFF_TITLE = '位置情報';
export const LOCATION_SEVICES_OFF_MESSAGE = 'お使いの端末の[設定]>[プライバシー]>[位置情報サービス]にて設定を確認してください。';

export const BUTTON_SIGNUP_TITLE = 'さっそくはじめる（無料）';
export const BUTTON_LOGIN_TITLE = 'ログイン';
export const BUTTON_LOGIN_FB_TITLE = '  Facebookでログイン';
export const FB_TEXT_INTRO = 'Facebookに利用状況が公開されることはありません。';
export const BUTTON_LOGOUT_TITLE = 'ログアウト';
export const BUTTON_DONE_TITLE = '完了';
export const BUTTON_CHANGE_PASSWORD = 'パスワード変更';
export const BUTTON_RESET_PASSWORD_TITLE = '送信';
export const TABLE_CELL_NAME_TITLE = '氏名';
export const TABLE_CELL_BIRTHDAY_TITLE = '生年月日';
export const TABLE_CELL_GENDER_TITLE = '性別';
export const TABLE_CELL_INTERS_TITLE = '興味';
export const TABLE_CELL_SIGNUP_TITLE = '利用規約に同意して登録';
export const TABLE_CELL_SEND_EMAIL_SIGNUP_TITLE = '通知のメールがほしい';
export const TABLE_CELL_EMAIL_TITLE = 'メールアドレス';
export const TABLE_CELL_PASSWORD_TITLE = 'パスワード';
export const TABLE_CELL_GUIDE_RESET_YOUR_PASSWORD = '登録したメールを入力してください:';
export const TABLE_CELL_CODE_TITLE = '認証コード';
export const TABLE_CELL_NEW_PASSWORD_TITLE = '新パスワード';
export const NAVIGATION_SIGNUP_TITLE = 'プロフィールの設定';
export const NAVIGATION_BUTTON_CANCEL_TITLE = 'キャンセル';
export const NAVIGATION_BUTTON_CONTINUE_TITLE = '続ける';
export const NAVIGATION_COMP_REGIS_TITLE = 'アカウント登録';
export const NAVIGATION_BUTTON_DONE_TITLE = '完了';
export const NAVIGATION_LOGIN_TITLE = 'ログイン';
export const NAVIGATION_BUTTON_FORGOT_TITLE = 'パスワードをお忘れですか？';
export const NAVIGATION_FORGOT_TITLE = 'パスワード再設定';
export const NAVIGATION_CHANGE_PASSWORD_TITLE = 'パスワードの変更';
export const NAVIGATION_WHO_CHECKED_YOU_OUT_TITLE = '足あと';
export const NAVIGATION_VERIFY_CODE_TITLE = 'パスワード再設定';
export const TOOLBAR_BUTTON_DONE_TITLE = '完了';
export const TOOLBAR_BUTTON_CANCEL_TITLE = 'キャンセル';
export const GENDER_MAN_TITLE = '男性';
export const GENDER_WOMAN_TITLE = '女性';
export const INTERS_MEN_TITLE = '男性';
export const INTERS_WOMEN_TITLE = '女性';
export const INTERS_MEN_WOMEN_TITLE = '男と女';
export const GENDER_MALE_TILE = '男';
export const GENDER_FEMALE_TITLE = '女';
export const NAVIGATION_ADD_FRIEND_TITLE = '友達を追加';
export const NAVIGATION_BUTTON_SKIP_TITLE = 'スキップ';
export const NAVIGATION_EDIT_PROFILE_TITLE = 'プロフィールの入力';
export const BUTTON_ADD_FRIEND_CONTACT_TITLE = '連絡先';
export const CELL_NAME_PROFILE_BASIC_TITLE = 'なまえ';
export const CELL_NAME_EDIT_PROFILE_TITLE = 'ハンドルネーム';
export const CELL_GENDER_PROFILE_BASIC_TITLE = '性別';
export const CELL_AGE_PROFILE_BASIC_TITLE = '年齢';
export const TABLE_CELL_REGION_TITLE = '地域';
export const CELL_LOCATION_PROFILE_BASIC_TITLE = '位置';
export const CELL_INTERS_IN_PROFILE_BASIC_TITLE = '興味';
export const CELL_LOOKING_FOR_PROFILE_BASIC_TITLE = '探す対象';
export const CELL_ABOUT_ME_PROFILE_BASIC_TITLE = '自己紹介';
export const CELL_VIEW_FULL_PROFILE_PROFILE_BASIC_TITLE = 'すべてのプロフィールを表示';
export const CELL_RELATIONSHIP_STATUS_EXTENDED_INFO_TITLE = '交際ステータス';
export const CELL_BODY_TYPE_EXTENDED_INFO_TITLE = '体型';
export const CELL_HEIGHT_EXTENDED_INFO_TITLE = '身長';
export const CELL_ETHNICITY_EXTENDED_INFO_TITLE = '民族性';
export const CELL_INTERESTS_EXTENDED_INFO_TITLE = '興味';
export const RELATIONSHIP_STATUS_ASKME_TITLE = '未設定';
export const RELATIONSHIP_STATUS_SINGLE_TITLE = '独身';
export const RELATIONSHIP_STATUS_IN_A_RELATIONSHIP_TITLE = '交際中';
export const RELATIONSHIP_STATUS_MARRIED_TITLE = '結婚';
export const RELATIONSHIP_STATUS_IT_IS_COMPLICATED_TITLE = '複雑な関係';
export const RELATIONSHIP_STATUS_IN_A_OPEN_RELATIONSHIP_TITLE = 'オープンな関係';
export const RELATIONSHIP_STATUS_SEPARATED_TITLE = '別居中';
export const RELATIONSHIP_STATUS_DIVORCED_TITLE = '離婚';
export const RELATIONSHIP_STATUS_IN_A_CIVIL_UNION_TITLE = '同性結婚';
export const ENTH_ASKME_TITLE = '未設定';
export const ENTH_LATINA_LATIO_TITLE = 'ラティーナ/ラテン';
export const ENTH_BLACK_AFRICAN_TITLE = 'ブラック/アフリカ';
export const ENTH_NATIVE_ABORIGINAL_TITLE = 'ネイティブ/アボリジニ';
export const ENTH_ASIAN_TITLE = 'アジア系';
export const ENTH_EAST_INDIAN_TITLE = '東インド';
export const ENTH_PACIFIC_ISLANDER_TITLE = 'ポリネシアン';
export const ENTH_WHITE_CAUCASIAN_TITLE = 'ホワイト/白人';
export const ENTH_MIDDLE_EASTERN_TITLE = '中東系';
export const ENTH_MIXED_MULTI_TITLE = 'ミックス/マルチ';
export const LOOKING_FOR_FRIEND_TITE = '友達';
export const LOOKING_FOR_FILRTING_TITE = '出会い';
export const LOOKING_FOR_FUN_TITE = '楽しむ';
export const LOOKING_FOR_DATING_TITE = 'デート';
export const LOOKING_FOR_CHAT_TITE = 'チャット';
export const LOOKING_FOR_PARTY_TITE = 'パーティ';
export const LOOKING_FOR_RELATIONSHIP_TITE = '交際';
export const BODY_TYPE_SLIM_TITLE = 'スリム';
export const BODY_TYPE_PETITE_TITLE = '小柄';
export const BODY_TYPE_SLENDER_TITLE = '細身';
export const BODY_TYPE_NORMAL_TITLE = '普通';
export const BODY_TYPE_MORE_TO_LOVE_TITLE = 'ぽっちゃり型';
export const BODY_TYPE_CURVY_TITLE = 'ふくよか';
export const BODY_TYPE_SWIMMERS_BUILD_TITLE = '水泳選手体型';
export const BODY_TYPE_MUSCULAR_TITLE = '筋肉質';
export const BODY_TYPE_SKINNY_TITLE = 'ガリガリ';
export const BODY_TYPE_TALL_TITLE = '長身';
export const BODY_TYPE_ATHLETIC_TITLE = '体育系';
export const BODY_TYPE_LARGE_AND_SOLID_TITLE = '大柄でがっちり';

export const INTERESTS_MOVIE_TITLE = '映画';
export const INTERESTS_BOOKS_TITLE = '読書';
export const INTERESTS_TV_TITLE = 'テレビ';
export const INTERESTS_MUSIC_TITLE = '音楽';
export const INTERESTS_FAMILY_TITLE = '家族';
export const INTERESTS_PETS_TITLE = 'ペット';
export const INTERESTS_DRINKING_TITLE = '飲み会';
export const INTERESTS_RESTAURANTS_TITLE = '食事';
export const INTERESTS_SHOPPING_TITLE = 'ショッピング';
export const INTERESTS_WATCHING_SPORTS_TITLE = 'スポーツ鑑賞';
export const INTERESTS_PLAYING_SPORTS_TITLE = 'スポーツをする';
export const INTERESTS_BARS_TITLE = 'バー';
export const INTERESTS_DANCING_TITLE = 'ダンス';
export const INTERESTS_GAMES_TITLE = 'ゲーム';
export const LEFT_MENU_MEET_PEOPLE_TITLE = 'フレンズ検索';
export const LEFT_MENU_BUZZ_TITLE = 'タイムライン';
export const LEFT_MENU_SHAKE_TO_CHAT_TITLE = 'シェイクチャット';
export const LEFT_MENU_MY_PROFILE_TITLE = 'マイページ';
export const LEFT_MENU_CONNECTIONS_TITLE = 'マイフレンズ';
export const LEFT_MENU_MY_CHAT_TITLE = 'メッセージ一覧';
export const LEFT_MENU_NOTIFICATIONS_TITLE = 'お知らせ';
export const LEFT_MENU_POINTS_TITLE = 'ポイント購入';
export const LEFT_MENU_SETTINGS_TITLE = '設定';
export const LEFT_MENU_ANDG_PLUS_TITLE = 'Kyuun +';
export const LEFT_MENU_ANDG_BLOG_TITLE = 'Kyuun ブログ';
export const LEFT_MENU_ANDG_SAFETY_TITLE = 'Kyuunを安全にご利用';
export const LEFT_MENU_SHARE_MY_BUZZ_TITLE = 'タイムライン一覧';
export const LEFT_MENU_SHARE_SUPPORT_TITLE = 'ヘルプ';
export const LEFT_MENU_NEWS = '最新情報';
export const LEFT_MENU_MY_MENU = 'マイメニュー';
export const LEFT_MENU_ABOUT_KYUUN_FREEPAGE = 'Kyuun について';
export const LEFT_MENU_HOW_TO_USE = '使い方';
export const SEND_REQUEST_FACEBOOK_TITLE = 'リクエスト通信';
export const SEND_REQUEST_FACEBOOK_TITLE_MSG = 'Kyuunからメッセージを送ります';
export const ONLINE_PROFILE_TITLE = 'オンライン';
export const OFFLINE_PROFILE_TITLE = 'オフライン';
export const THE_BASIC_SECTION_TABLE_TITLE = '基本';
export const YOU_HAVE_WINKED_TO_MESSAGE = '%sさんに足跡をつけました';
export const CONNECTION_ALERT_NO_FRIEND_MESSAGE = '友達をKyuunに招待しませんか？\nFacebookやアドレス帳を使って友達を招待できます。';
export const CONNECTION_ALERT_NO_FAVORITE_MESSAGE = 'お気に入りに登録がありません。 \n気になる人をお気に入りに追加してください。';
export const CONNECTION_ALERT_NO_FAVORITED_YOU_MESSAGE = 'あなたをお気に入り登録している人はいません';
export const PLEASE_SELECT = '選択してね';
export const STICKER_TITLE = 'スタンプ';
export const TAP_TO_SET_YOUR_PROFILE_PICTURE_TITLE = 'タップしてプロファイル写真を設定';
export const PENDING_BUTTON_TITLE = '保留';
export const INVITE_BUTTON_TITLE = '招待';
export const FACEBOOK_CONNECT_FAIL_MESSAGE = 'Facebookに接続できません。';
export const FACEBOOK_LOGIN_FAIL_MESSAGE = 'Facebookにログインできません。';
export const IMPORTING_PROFILE_MESSAGE = 'プロフィールをインポート中';
export const EXTENDED_INFO_SECTION_TABLE_TITLE = '拡張情報';
export const DISTANCE_EDIT_PROFILE_KM = '(%0.1f km); // Ex: 15.3 km';
export const ADDRESS_NOT_FOUND_TITLE = '住所が特定出来ません';
export const CITY_NOT_FOUNDED_TITLE = '都市を検索できません';
export const LEFT_MENU_MISC_TITLE = 'その他';
export const CHAT_WITH_YOUR_FRIENDS_GUIDE_ADD_FRIEND = '友達とチャット';
export const SHOW_GROUP_OF_FRIENDS_GUIDE_ADD_FRIEND = '友達をKyuunへ招待';
export const EARN_POINT_FOR_EACH_NEW_FRIEND_GUIDE_ADD_FRIEND = '友達を招待して25ポイントを獲得しよう';
export const MY_PROFILE_EDIT_MY_PROFILE_TITLE = 'プロフィールの編集';
export const MY_PROFILE_SHARE_PIC_TITLE = '写真を共有';
export const MY_PROFILE_SHARE_STATUS_TITLE = '近況を投稿する';
export const MY_PROFILE_GIVE_GIFT_TITLE = 'ギフトを贈る';
export const OTHER_PROFILE_WINK_TITLE = 'ウィンク';
export const OTHER_PROFILE_CHAT_TITLE = 'チャット';
export const OTHER_PROFILE_ADD_TO_FAVORITE_TITLE = 'お気に入りに追加';
export const OTHER_PROFILE_GIVE_GIFT_TITLE = 'ギフト';
export const OTHER_PROFILE_MORE_TITLE = 'もっと表示';
export const OLD_PASSWORD_SETTING_TITLE = '古いパスワード';
export const NEW_PASSWORD_SETTING_TITLE = '新しいパスワード';
export const CHANGE_PASSWORD_SETTING_TITLE = 'パスワードの変更';
export const PROFILE_POINT_TITLE = '%dポイント';
export const PROFILE_BACKSTAGE_PICTURE_TITLE = '秘密の写真';
export const SETTING_CHANGE_PASSWORD = 'パスワードの変更';
export const BUTTON_FAVORITE_TITLE = 'お気に入り';
export const BUTTON_FAVORITE_TITLE1 = 'お気に入り%d';
export const BUTTON_FAVORITE_TITLES = 'お気に入り%d';

export const NO_BUTTON_WHO_TITLE = 'フォロワー';
export const BUTTON_WHO_TITLE = 'フォロワー %d';
export const BUTTON_WHO_TITLES = 'フォロワー %d';
export const BUTTON_CONFIRM_TITLE = '確認';
export const BUTTON_NOT_NOW_TITLE = '後でします';
export const BUTTON_WHO_FAVORITED_ME_TITLE = 'フォロワー';

export const THE_FIRST_PURCHASE = '期間限定キャンペーン★\n初回購入時のみ、付与ポイントx1.5倍が加算されます！\n最初の1回だけなので、沢山買ったほうがお得！';
export const THE_FIRST_PURCHASE_DONE = '初回購入なので、付与ポイントが加算さました！';

export const TITLE_PAGE_LOGOUT = '口グインに失敗しました';
export const LOGIN_FAILED_TITLE = 'ログインに失敗しました';
export const RELOGIN_TITLE = '再口グインする';
export const PLEASE_CLICK_HERE_IN_CASE_YOU_FORGOT_YOUR_EMAIL_PASSWORD = 'メールアドレス・パスワードがご不明な場合は、以下よりお問い合わせください。';
export const LOGIN_WITH_EMAIL_AND_PASSWORD = 'メールアドレスとパスワードでログインする';
export const CLICK_HERE_TO_ASK_FOR_SUPPORT = 'お問い合わせはコチラ';
export const SUPPORT_MAIL_CAN_BE_DIRECTED_TO_SPAM_MAILBOX_PLEASE_CHECK_SPAM_MAILBOX_ALSO = '※お問い合わせの際、力スタマーサポートからの返信メー ルが「迷惑フォルダ」に入る可能性があります。迷惑メー ルアォルダもこ確認ください。';

export const PLEASE_SETTING_INBOX_IN_ORDER_TO_RECEIVE_MAILS = '※ドメイン拒否などを設定している場合は【＠kyuun-app.com】からのメールを受信できるよう、メール受信設 定をご確認ください。';
export const PLEASE_ANSWER_THE_FOLLOWING_QUESTIONS_BEFORE_SENDING_EMAIL = 'メールアドレス登録されていた方は、その情報で[ログイン]してください。\nFacebook登録されていた方は、再度Facebookログインしてください。\n\n下記(1)(2)に回答後、メール送信してください。';
export const FOLLOW_BEFORE_LOGOUT = '(1)ログアウトしてしまう直前の状況：';
export const NECESSARY_INFORMATION = '(2)アカウント特定に必要な情報：';

export const REGISTER_DATE = '登録日時';
export const POINT_LEFT = '残ポイント';
export const PROFILE_AREA = 'プロフ(地域)';
export const PROFILE_JOB = 'プロフ(職業)';
export const WE_WILLREPLY_YOU_WITHIN_48_HOURS_THANKS_FOR_WAITING = 'なお、頂いたメールの確認および返信は、最長で48時間かかる場合がございます。\n誠に恐れ入りますが、今しばらくお待ちくださいませ。';
export const OCCUR_TIME = '■発生日時';
export const DEVICE_INFO = '■端末情報';
export const LOGIN_INFO = '■ログイン情報';
export const SET_AVATAR_MESSAGE_CHAT = 'プロフィール画像を設定すると異性からの好感度が大幅アップ♪';
export const SET_AVATAR_LATER_CHAT = '後で設定する';
export const SET_AVATAR_SET_NOW_CHAT = '今すぐ設定する';

export const USER_AGE = '%d歳';
export const USER_AGE_TITLE = '年齢';
export const USER_AREA_TITLE = '地域';
export const USER_JOB_TITLE = '職業';
export const NO_DATA_NOTE = '無し';
export const ADDRESS = '住まい';
export const BUTTON_FOOTPRINTS_TITLE = 'あしあと';
export const BUTTON_FOOTPRINTS_TITLES = 'あしあと';

export const BUTTON_MY_FOOTPRINTS_TITLE = '自分のあしあと';
export const BUTTON_MY_FOOTPRINTS_TITLES = '自分のあしあと';

export const GUIDE_MY_FOOTPRINTS = 'あなたがプロフィールを閲覧した方です。';
export const GUIDE_WHO_BACKSTAGE = 'あなたのヒミツの写真を閲覧した方です。';
export const NO_DATA_FOOTPRINTS = 'あなたをチェックしたユーザーはいません';
export const NO_DATA_MYFOOTPRINTS = 'あなたがチェックしたユーザーはいません';
export const NO_DATA_WHO_BACKSTAG = 'あなたの秘密写真を閲覧した方はいません。';
export const BUTTON_NEW_TAB_TITLE = '最新ニュース';
export const BUTTON_NOTIFICATION_TAB_TITLE = 'システム通知';
export const VIEW_IMAGE_POPUP_CONFIRM_TITLE = '画像の閲覧';
export const PLAY_VIDEO_POPUP_CONFIRM_TITLE = '動画の再生';
export const PLAY_AUDIO_POPUP_CONFIRM_TITLE = '音声の再生';
export const TEXT_DESRIPTION_APPEAAL_COMMENT = 'アピール一言コメント';
export const TEXT_PLACEHOLDER_APPEAL_COMMENT_POPUP = '自分のアピールポイントをつぶやこう!\n※アダルトな内容、金銭を要求する内容など規約に違反する内容は公開されません。';
export const TEXT_PLACEHOLDER_APPEAL_COMMENT = '自分のアピールポイントをつぶやこう！\nアダルトな内容や規約に違反する内容は公開されません。';
export const APPROVE_APPEAL_COMMENT_NOTI = '一言アピールコメントが承認されました。';
export const PENDDING_APPEAL_COMMENT_NOTI = 'ー嚣アピールコメントは承認後公開され漱す。'
export const DENIED_APPEAL_COMMENT_NOTI = '一言アピールコメントが否認されました。';
export const MSG_APPEAL_COMMENT_REVIEW = '一言アピールコメントは、承認後公開されます';
export const WELCOME_BACK_TEXT = 'お疲れさまです！';
export const WELCOME_BACK_TEXT_MALE = 'おかえりなさい!';
export const WHAT_IS_NEW_TEXT = '前回からの新着情報';
export const NEW_NUMBER_NOTIFICATION = '1件の新しい通知';
export const NEW_NUMBER_NOTIFICATIONS = '%d件の新しい通知';
export const MP_TOP_MENU_SEARCH = '検索';
export const MP_TOP_MENU_WHO_CHECK = '足あと';
export const MP_TOP_MENU_WINK_BOMB = 'あしあとボム';
export const MP_TOP_MENU_MY_FAVORITES = 'お気に入り';
export const LIST_VIEW = 'リストビュー';
export const GRID_VIEW = 'グリッドビュー';
export const ONLINE = 'オンライン';
export const OFFLINE = 'オフライン';
export const MALE = '男';
export const FEMALE = '女';
export const YEAR_OLD_GENDER = '%i歳%s';
export const NAME_YEAR_OLD = '%s %i歳';
export const DISTANCE_KM = '%0.1f km';
export const NAME_YEAR_OLD_GENDER = '<html><body><span style';
export const SEARCH_SETTING = 'ユーザー検索';
export const ALL_TIMELINE = 'すべて表示';
export const CANCEL = 'キャンセル';
export const SEARCH = '検索';
export const SHOW_ME = '検索の対象';
export const INTERESTED_IN = '興味の対象';
export const THAT_ARE_WITHIN = '範囲';
export const OF_AGES_BETWEEN = '年齢';
export const OF_ETHNICITY = '民族性';
export const LOWER_AND_UPPER_AGE = '%dから%d';
export const SEARCH_ETHNICITY = '民族性選択';
export const WINK_MESSAGE_FORMAT = '%sさんがあなたに足跡をのこしました!\n元気ですか？';
export const AUDIO_MESSAGE_FORMAT = 'ボイスメッセージを送りました';
export const PHOTO_MESSAGE_FORMAT = '写真を送りました';
export const VIDEO_MESSAGE_FORMAT = '動画を送りました';
export const LOCATION_MESSAGE_FORMAT = '場所を共有する';
export const LOCATION = '場所';
export const CHAT_HOLD_TO_TALK = '音声通話';
export const CHAT_RELEASE_TO_SEND = '離して送信';
export const CHAT_RELEASE_TO_DELETE = '離して削除';
export const AUDIO_RECORDER_PREPARING = '準備中...';
export const TYPING = '入力中';
export const GIFT_MESSAGE_TEXT = 'ギフト(%dpts)を送りました';
export const GIFT_MESSAGE_FORMAT = '%sから%sへギフト(%dpts)を送りました';
export const GIFT_MESSAGE_TEXT_2 = 'ギフト(%dpt)を送りました';
export const GIFT_MESSAGE_FORMAT_2 = '%sから%sへギフト(%dpt)を送りました';
export const SHARE_LOCATION_SCREEN_TITLE = '位置情報をシェア';
export const SHARE_LOCATION_CALLOUT_TITLE = '位置情報をシェア';
export const SEND_CHAT_TITLE = '送信';
export const CANT_RECORD_TITLE = 'マイクがオフになっています';
export const CANT_RECORD_MESSAGE = 'お使いの端末の[設定>プライバシー]より[マイク]をオンにしてください';
export const CANT_RECORD_OK = 'OK';
export const RECORDER_FAIL_TITLE = '音声録音';
export const RECORDER_FAIL_MESSAGE = '録音でエラーが発生しました。やり直してください。';
export const RECORDER_FAIL_CLOSE = '閉じる';
export const AUDIO_PLAYER_FAIL_TITLE = 'オーディオプレイヤー';
export const AUDIO_PLAYER_FAIL_MESSAGE = 'オーディオファイルの再生にてエラーが発生しました。やり直してください。';
export const AUDIO_PLAYER_FAIL_CLOSE = '閉じる';
export const SEND_FILE_FAIL_TITLE = 'ファイル送信';
export const SEND_FILE_FAIL_MESSAGE = 'ファイル送信にてエラーが発生しました。やり直してください。';
export const SEND_FILE_FAIL_CLOSE = '閉じる';
export const DOWLOAD_FILE_FAIL_TITLE = 'ファイルダウンロード';
export const DOWNLOAD_FILE_FAIL_MESSAGE = 'ファイルのダウンロードにてエラーが発生しました。やり直してください。';
export const DOWNLOAD_FILE_FAIL_CLOSE = '閉じる';
export const CHOOSE_FILE_ERROR_MESSAGE = 'ファイルに問題があります。別のファイルを選択してください。';
export const CHOOSE_FILE_ERROR_MESSAGE_CLOSE = 'OK';
export const CHATLIMIT_LIMITING_FORMAT = 'チャットを開始してください。%d秒後にプロフィールを見ることができます。';
export const CHATLIMIT_CAN_FAVORITE_FORMAT = 'チャットはどうでしたか？%sさんをお気に入りに追加しましょう。他の人がいいですか？シェイクしましょう。';
export const CHATLIMIT_ADD_TO_FAVORITE = 'お気に入りに追加';
export const CHATLIMIT_SHAKE_AGAIN = 'もう一度シェイクする';
export const CHATLIMIT_VIEW_BUZZ_FORMAT = '%sさんの投稿を見る';
export const SELECT_THUMBNAIL = 'サムネイルを選択';
export const SHARE_MY_BUZZ_SEND = '送信';
export const SHARE_MY_BUZZ_POSTING = '送信中...';
export const MY_BUZZ_NOW = '今';
export const MY_BUZZ_HERE = 'ここ';
export const SHARE_MY_BUZZ_SHARE_SOMETHING = 'ここに記入してください...';
export const SHARE_BUZZ_ENABLE_SHARE_TO_FACEBOOK = 'シェアする';
export const SHARE_BUZZ_DISABLE_SHARE_TO_FACEBOOK = 'シェアしない';
export const EMPTY_CONTENT_TITLE = '無効';
export const EMPTY_CONTENT_MESSAGE = '無効な入力です。空白では投稿に投稿しないでください。';
export const EMPTY_CONTENT_OK = 'OK';
export const NOT_ENOUGH_POINTS_TITLE = 'ポイントが足りません';
export const NOT_ENOUGH_POINTS_MESSAGE_FORMAT = '%2$@ の足跡をのこす為に、%1$@ポイント必要です。現在の保有ポイントは %3$@ ポイントです。ポイントを追加しますか？';
export const NOT_ENOUGH_POINTS_NO = 'いいえ';
export const NOT_ENOUGH_POINTS_YES = 'はい';
export const NOT_ENOUGH_POINT_GIFT_TITLE = 'ポイントが不足しています';
export const NOT_ENOUGH_POINT_GIFT_MESSAGE_MALE = 'このギフトを贈るには%dポイント必要です。ポイントを追加しますか？';
export const NOT_ENOUGH_POINT_GIRT_MESSAGE_FEMALE = 'このギフトを贈るには%dポイント必要です。(現在の保有ポイント：%dポイント)';
export const NOT_ENOUGH_POINT_SEND_MESSAGE_FEMALE = 'このチャットを送るには%dポイント必要です。(現在の保有ポイント：%dポイント);//21060802 - #21329 - HanhGT - Added';
export const NOT_ENOUGH_POINT_SELECT_NO = 'キャンセル';
export const NOT_ENOUGH_POINT_GO_PURCHASE = '購入ページへ';
export const NOT_ENOUGH_POINT_UNLOCK_BSCKS_MESSAGE = 'ロックを解除するには%dポイント必要です。ポイントを追加しますか？';
export const NOT_ENOUGH_POINT_COMMENT_MESSAGE = 'コメントするには%dポイント必要です。ポイントを追加しますか？';
export const NOT_ENOUGH_POINT_CHAT_MESSAGE = 'メッセージを送信するには%dポイント必要です。ポイントを追加しますか？';
export const NOT_ENOUGH_POINT_CALL_VIDEO = 'ビデオ通話を発信するには%dポイント必要です。ポイントを追加しますか？';
export const NOT_ENOUGH_POINT_CALL_AUDIO = '音声通話を発信するには%dポイント必要です。ポイントを追加しますか？';
export const RECV_DO_NOT_ENOUGH_POINT_TO_CALL = 'このユーザーはコールに必要なポイントがありません。';
export const SET_AVATAR_TITLE = 'プロフィール写真設定';
export const SET_AVATAR_MESSAGE = 'プロフィール写真が設定されていません。今すぐ設定して注目度をアップさせよう♪';
export const SET_AVATAR_LATER = 'あとで';
export const SET_AVATAR_SET_NOW = '今すぐ設定する';

// Deactivate account
export const DO_NOT_DEACTIVATE_BUTTON_TEXT = '退会しません';

export const DEACTIVATE_INFORM_TEXT = '<html> <head><style type';

export const DEACTIVATE_INFORM_TEXT_IPAD = '<html> <head><style type';

export const DEACTIVATE_INFORM_TEXT_FACEBOOK = '<html> <head><style type';


export const DEACTIVATE_INFORM_TEXT_FACEBOOK_IPAD = '<html> <head><style type';

export const DEACTIVATE_INFORM_TEXT1 = 'Kyuunをご使用頂き誠にありがとうございました。最後に、退会される理由(Kyuunの改善点等)をご入力頂けましたら幸いでございます。また機会がございましたら、Kyuunを宜しくお願い申し上げます。';
export const DEACTIVATE_BUTTON_TEXT = '退会する';
export const DEACTIVATE_ALERT_SHOW_TEXT = '続ける前にコメントを入れてください。';
export const ALERT_TITLE_DEACTIVE = '警告';
export const BUTTON_OK_TITLE = 'OK';
export const NOTHING_TO_SHOW_TEXT = '表示出来る項目がありません。';
export const LOOK_AT_ME_TITLE = 'オークション';
export const FEATURE_ME = 'オークションで自己アピール';
export const TIME_REMAINING_FORMAT = '残り%s';
export const HOUR_FORMAT = '%d時間';
export const MINUTE_FORMAT = '%d分';
export const ABOUT_1_MINUTE = '約1分';
export const PLACE_BID_TITLE = 'オークションに参加する';
export const BID_DESCRIPTION_HTML_FORMAT = '<!DOCTYPE html><html><body><p align';
export const BID_SLOGAN_FORMAT = '「オークション」について。他のユーザーと競う為には、より多い入札が必要です。入札の有効期限は%d時間です。入札をしてトップを目指してください。';
export const BID_STATUS_FORMAT = '現在1位の入札額は%dポイントです。現在の最低入札額は%dポイントです。';
export const BID_POINTS = 'ポイント';
export const BANNER_WORDS_OK = 'OK';
export const PLACE_THIS_BID = 'これを入札する';
export const BID_NOT_ENOUGH_POINTS_TITLE = 'ポイントが足りません';
export const BID_NOT_ENOUGH_POINTS_MESSAGE_FORMAT = '入札するためには%dポイント必要です。 現在の保有ポイントは%dポイントです。ポイントを追加しますか？';
export const BID_CONFIRM_TITLE = '入札確認';
export const BID_CONFIRM_MESSAGE_FORMAT = '%dポイントを入札しても宜しいですか？';
export const BID_SUCCESS_TITLE = '入札しました';
export const BID_SUCCESS_MESSAGE_FORMAT = 'あなたの入札は%dポイントです。';
export const BID_NO = 'いいえ';
export const BID_YES = 'はい';
export const BID_OK = 'OK';

export const VIEW_PICT_CONFIRM_SAVE_TITLE = '画像の保存';
export const VIEW_PICT_CONFIRM_SAVE_MESSAGE_FORMAT = 'ポイントを使って画像を保存しますか？';
export const VIEW_PICT_CONFIRM_SAVE_MESSAGE_NO_POINT = 'この画像を保存しますか？';
export const VIEW_PICT_CONFIRM_SAVE_NO = 'いいえ';
export const VIEW_PICT_CONFIRM_SAVE_YES = 'はい';
export const NOTIFICATION_MILE_FORMAT = '%0.1f ﾏｲﾙ';
export const NOTIFICATION_SOMEONE = '誰か';
export const VOIP_CANCEL = 'キャンセル';
export const VOIP_VIDEO_CALL = ' ビデオ通話';
export const VOIP_VOICE_CALL = ' 音声通話';
export const VOIP_OK = 'OK';
export const VOIP_NOT_REGISTER_SIP_SERVER_TITLE = '通話サーバに登録されていません';
export const VOIP_NOT_REGISTER_SIP_SERVER_MESSAGE = '後ほどやり直してください';
export const VOIP_OUT_GOING_CALL_FAILURE_TITLE = 'コールに失敗しました';
export const VOIP_OUT_GOING_CALL_FAILURE_MESSAGE = '新規コールに不具合が発生しました。後ほどやり直してください。';
export const VOIP_CANNOT_MAKE_CALL_BY_GMS_TITLE = '通話が出来ません。';
export const VOIP_CANNOT_MAKE_CALL_BY_GMS_MSG = 'デバイスの通話を終了してください。';
export const VOIP_ANDG_CALL = '着信中';
export const VOIP_RINGING = '呼び出し中';
export const VOIP_MESSSAGE_VIDEO_START = 'ビデオ通話中';
export const VOIP_MESSAGE_VIDEO_END = '%s';
export const VOIP_MESSAGE_VIDEO_END_CONV = '通話時間 %s';
export const VOIP_MESSAGE_VIDEO_BUSY = '通話中';
export const VOIP_MESSAGE_VIDEO_NO_ANSWER = 'キャンセル';
export const VOIP_MESSSAGE_VOICE_START = '音声通話中';
export const VOIP_MESSAGE_VOICE_END = '%s';
export const VOIP_MESSAGE_VOICE_END_CONV = '通話時間 %s';
export const VOIP_MESSAGE_VOICE_BUSY = '通話中';
export const VOIP_MESSAGE_VOICE_NO_ANSWER = 'キャンセル';
export const VOIP_END = '終了';
export const VOIP_IGNORE = '応答しない';
export const VOIP_ANSWER = '応答';
export const VOIP_ANSWER_VOICE_ONLY = '音声のみ';
export const BANNED_WORDS_MESSAGE_FORMAT = '(%s)は使えない単語です。再入力してください。';
export const REMOVE_FRIEND = '友達から削除';
export const CANCEL_TEXT = 'キャンセル';
export const MORE_OPTIONS = 'もっと';
export const REMOVE_FAVORITE = 'お気に入りから削除';
export const ADD_TO_FAVORITES = 'お気に入り';
export const ADD_TO_FAVORITES_ALERT_TITLE = 'お気に入りに追加';
export const ADD_TO_FAVORITES_ALERT_MESSAGE = '%sさんをお気に入りに追加しました。%sさんにギフトを送りますか？';
export const REMOVE_FAVORITE_ALERT_TITLE = 'お気に入りから削除';
export const REMOVE_FAVORITE_ALERT_MESSAGE = 'お気に入りリストから %s さんを削除しました';
export const ONLINE_ALERT_TEXT = '通知設定';
export const ONLINE_ALERT_TITLE = '通知設定';
export const SHAKE_TO_CHAT = 'シェイクチャット';
export const EDIT_TEXT = '履歴の削除';
export const MARK_ALL_TEXT = '全て既読にする';
export const DELETE_ALL = 'すべて削除';
export const SOMEONE_TEXT = 'だれか';
export const ANONYMOUS_TEXT = '匿名';
export const YEAR_TEXT = '年以内';
export const YEAR_TEXT_TIMELINE = '年前';
export const MONTH_TEXT = 'ヶ月以内';
export const MONTH_TEXT_TIMELINE = 'ヶ月前';
export const WEEK_TEXT = '週間以内';
export const WEEK_TEXT_TIMELINE = '週間前';
export const DAY_TEXT = '日以内';
export const DAY_TEXT_TIMELINE = '日前';
export const HOUR_TEXT = '時間以内';
export const HOUR_TEXT_TIMELINE = '時間前';
export const MINUTE_TEXT = '分以内';
export const MINUTE_TEXT_TIMELINE = '分前';
export const SECOND_TEXT = '1分以内';
export const SECOND_TEXT_TIMELINE = '1分前';
export const NOW_TEXT = '1分以内';
export const NOW_TEXT_TIMELINE = '1分前';
export const WARNING_ALERT_TEXT = '警告!';
export const DELETE_MESSAGE_ALERT_TEXT = 'すべてのメッセージを削除しますか？';
export const MARK_ALL_MESSAGE_ALERT_TEXT = '全てのメッセージを既読にしますか?';
export const NO_ONE_CHECKED_TEXT = 'あなたをチェックした人はいません';
export const RECEIVE_GIFT_TEXT = '%sさんがギフトをもらいました';
export const SHOW_MORE_RESPONSES = 'もっと見る';
export const RESPONSES_TEXT = '詳細';
export const ALERT_ADD_BUZZ_TEXT = '画像かテキストを入力してください';
export const ALERT_BUZZ_INVALID = 'この投稿は既に削除されました';
export const CANNOT_ACCESS_BUZZ = 'この投稿にアクセス出来ませんでした。後ほどやり直してください。';
export const CANNOT_ACCESS_IMAGE = '現在この画像にアクセス出来ません。やり直してください';
export const PENDING_APPROVED_PICTURE = 'この画像はスタッフによって承認された後、他のユーザーへ公開されます。';
export const EMOJI_TEXT = '絵文字';
export const STICKER_TEXT = 'スタンプ';
export const FUNCTION_LOCKED_TEXT = 'この機能はロックされています';
export const WHEN_SHOW_ALERT_LABEL = 'アラート回数';
export const HOW_SHOW_ALERT_LABEL = 'どのようなアラートがいりますか？';
export const NAME_ONLINE_ALERT_LABEL = 'さんがオンラインになる度に \nプッシュ通知を受け取ることができます。';
export const PUSH_ONLINE_ALERT_LABEL = '';
export const SETTING_ONLINE_ALERT_LABEL = '通知設定';
export const GUIDE_ONLINE_ALERT_LABEL = 'アプリを強制終了すると、 \n通知が遅れたり受信できない場合があります。';

export const WHEN_NEVER = '通知しない';
export const WHEN_EVERY = 'いつも';
export const WHEN_MAX_TEN = '１日10回まで';
export const WHEN_MAX_FIVE = '１日5回まで';
export const WHEN_MAX_ONCE = '１日1回まで';

export const HOW_EMAIL_NOTIFI = 'メールとPUSH通知';
export const HOW_NOTIFI = 'PUSH通知のみ';
export const HOW_EMAIL = 'メールのみ';
export const INFORM_ONLIEN_ALERT_LABEL = 'アラート毎に%dポイント必要です。アラート通知する度に保有ポイントから引かれます。';
export const ALERT_ONLINE_INFORM_TITLE = 'オンライン通知';
export const ALERT_ONLINE_INFORM_MESSAGE = ' さんがオンラインになる度に';
export const ALERT_ONLINE_INFORM_MESSAGE_AFTER = '通知します。';
export const ALERT_ONLINE_INFORM_MESSAGE2 = 'さんのオンラインを通知しません。';
export const ONLINE_ALERT_ENABLED_TEXT = 'オンライン通知が有効です';
export const SET_ALERT_TEXT = '通知をセット';
export const GET_ALERT_GO_ONLINE_TEXT = '%sさんがオンラインになる度に通知を受け取ります。';
export const SETTING_ONLINE_ALERTS_TEXT = 'オンライン通知';
export const THIS_PERSON_CHECK_TEXT = 'このユーザーがあなたをチェックしました。';
export const CHECKOUT_NUMBER_TEXT = '他に%d人がチェックしています。';
export const CHECKOUT_NUMBER_TEXT_ONE = '他に1 人がチェックしています。';
export const CHECKOUT_NUMBER_TEXT_ZERO = '他にもチェックしている人がいます。';
export const FIND_OUT_TEXT = '彼らの事を知りたいですか？';
export const UNLOCK_TEXT = '%d時間ロック解除する';
export const YOUR_BALANCE_TEXT = '残高:';
export const TOP_UP_YOUR_POINT = 'ポイントを追加する';
export const UNLOCK_LABEL_TEXT = 'ロック解除:';
export const THIS_PERSON_FAVORITE_ME_TEXT = 'このユーザーはあなたをお気に入り登録しました';
export const FAVORITE_NUMBER_TEXT = '他に%d人がお気に入り登録しています。';
export const FAVORITE_NUMBER_TEXT_ONE = '他に1人がお気に入り登録しています。';
export const FAVORITE_NUMBER_TEXT_ZERO = '他にもチェックしている人がいます。';
export const FIND_FAVORITE_TEXT = '彼らの事を知りたいですか？';

// Notifications
export const NOTI_CALL_REQUEST = '%sさんから、通話リクエストが届きました♪';
export const NOTI_NEW_CHAT_MSG = '%sさんがメッセージを送りました';
export const NOTI_FRIEND_CAME_ONLINE = '%sさんがオンラインになりました';
export const NOTI_FRIEND_CAME_OFFLINE = '%sさんがオフラインになりました';
export const NOTI_CHECKED_PROFILE = '%sさんがあなたをチェックしました';
export const NOTI_SOMEONE_CHECKED_PROFILE = '誰かがあなたをチェックしました';
export const NOTI_FAVORITED = '%sさんがあなたをお気に入りにしました';
export const NOTI_SOMEONE_FAVORITED = '誰かがあなたをお気に入りにしました';
export const NOTI_LIKE_YOUR_BUZZ = 'さんがあなたの投稿に「いいね」をしました';
export const NOTI_ALSO_LIKE_BUZZ = '%sさんも%sさんの投稿に「いいね」をしました';
export const NOTI_RESPONSED_YOUR_BUZZ = 'タイムラインにコメントがつきました。';
export const NOTI_ALSO_RESPONSED_BUZZ = '%sさんも%sさんの投稿にコメントしました';
export const NOTI_UNLOCK_BACKSTAGE = '%sさんがあなたの秘密の写真をロック解除しました。あなたは%sポイント獲得しました。';
export const NOTI_UNLOCK_BACKSTAGE_NO_POINT = '%sさんがあなたの秘密の写真をロック解除しました。';
export const NOTI_BECAME_FRIEND = 'あなたの友達の%sさんがKyuunに登録しました。あなたは%sポイント獲得しました。';
export const NOTI_BECAME_FRIEND_NO_POINT = 'あなたの友達の%sさんがKyuunに登録しました。';
export const NOTI_NEW_CHAT_MSG_TEXT = '新着メッセージがあります。';
export const PUSH_NOTI_MSG_TEXT = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_STICKER = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_VIDEO = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_AUDIO = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_LOCATION = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_PHOTO = '新着メッセージがあります。';
export const NOTI_ONLINE_ALERT = 'さんがオンラインになりました';
export const NOTI_GAVE_GIFT = '新着メッセージがあります。';
export const NOTI_MSG_TEMPLATE_NEW = '新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_TEXT_NEW = '%username%さんより、新着メッセージがあります。';
export const PUSH_NOTI_MSG_TEXT_NEW = 'さんより、新着メッセージがあります';
export const NOTI_NEW_CHAT_MSG_STICKER_NEW = 'さんより、新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_VIDEO_NEW = 'さんより、添付ファイルが届いています。';
export const NOTI_NEW_CHAT_MSG_AUDIO_NEW = 'さんより、添付ファイルが届いています。';
export const NOTI_EARNED_FROM_DAILY_BONUS_BEFORE = 'ログインボーナス: ';
export const NOTI_EARNED_FROM_DAILY_BONUS = 'ログインボーナス: %sポイント獲得しました';
export const NOTI_NEW_CHAT_MSG_LOCATION_NEW = 'さんより、新着メッセージがあります。';
export const NOTI_NEW_CHAT_MSG_PHOTO_NEW = '%username%さんより、添付ファイルが届いています。';
export const NOTI_GAVE_GIFT_NEW = 'さんより、ギフトが届いています。';
export const NOTI_EARNED_FROM_DAILY_BONUS_AFTER = 'ポイント獲得しました';
export const NOTI_GOT_TOP_2_LOOK_AT_ME = 'おめでとうございます。「オークション」にて%s位を獲得しました。';
export const NOTI_APPROVED_BUZZ = 'あなたの画像は承認されました。';
export const NOTI_APPROVED_BACKSTAGE = 'あなたの画像は承認されました。';
export const NOTI_VOIP_CALLING = '%sさんから着信があります。';
export const NOTI_FAVORITED_USERS_CREATE_BUZZ = 'さんがタイムラインを更新しました';
export const NOTI_DENIED_IMAGE_BUZZ = 'あなたの画像が否認されました。';
export const NOTI_DENIED_BACKSTAGE = 'あなたの画像が否認されました。';
export const NOTI_APPROVED_TEXT_BUZZ = 'あなたの投稿は承認されました。';
export const NOTI_DENIED_TEXT_BUZZ = 'あなたの投稿は否認されました。';
export const NOTI_APPROVED_COMMENT = 'あなたのコメントが承認されました。';
export const NOTI_DENIED_COMMENT = 'あなたのコメントが否認されました。';
export const NOTI_APPROVED_SUB_COMMENT = 'あなたの返信が承認されました。';
export const NOTI_DENIED_SUB_COMMENT = 'あなたの返信が否認されました。';
export const APPROVE_USER_INFOR_NOTI = 'プロフィールの変更が承認されました。';
export const DENIED_APART_OF_USER_INFO = 'プロフィールの変更が一部承認されました。';
export const DENIED_USER_INFOR_NOTI = 'プロフィールの変更が否認されました。';
export const VIEWER_PICTURE_COMMENT_LABLE_TITLE = 'コメント';
export const VIEWER_PICTURE_CHANGE_PROFILE_PICTURE_LABLE_TITLE = 'プロフ画像変更\n ';
// "viewer_picture_delete_picture_lable_title" = "写真の削除";
export const VIEWER_PICTURE_DELETE_PICTURE_LABLE_TITLE = '写真の通報';
export const VIEWER_SEE_ALL_BUTTON_TITLE = '全て見る';
export const GALLERY_LABLE_TITLE = 'ギャラリー';
export const TOTAL_PHOTOS_GALLERY_TITLE = '写真';
export const PREVIOUS_PHOTO_TITLE = '写真';
export const TOTAL_PHOTO_GALLERY_TITLE = '写真';
export const TOTAL_PHOTO_VIEWER_PICTURE_TITLE = '%d/%d';
export const VIEWER_PICTURE_SAVE_PICTURE_LABLE_TITLE = '写真の保存';
export const VIEWER_PICTURE_REPORT_PICTURE_LABLE_TITLE = '写真の報告';
export const CANNOT_SAVE_PIC_PROFILE_MESSAGE = '写真が有効ではありません。\n保存できません';
export const PRIVACY_POLICY_SETTING_TITLE = 'プライバシポリシー';
export const SAFETY_TIPS_SETTING_TITLE = '安全上の注意';
export const TERMS_OF_USE_SETTING_TITLE = '特定商取引法に基づく表示';
export const TERMS_OF_SERVICE_TITLE = '利用規約';
export const VERSION_TITLE = 'バージョン情報';
export const ACCOUNT_SETTING_HEADER_SECTION_TITLE = 'アカウント設定';
export const TERMS_OF_SERVICE_HEADER_SECTION_TITLE = 'サービス規約';
export const SAVED_SUCESS_PICTURE_MESSAGE = '画像を保存しました';
export const DELETE_PHOTO_TITLE = '写真を削除';
export const DELETE_PHOTO_MESSAGE = 'この写真を削除しますか？';
export const BUTTON_ALERT_DONT_DELETE_TITLE = 'キャンセル';
export const SET_AS_PROFILE_PIC_BUTTON_TITLE = 'プロフ写真設定';
export const DONT_REPOT_THIS_PICTURE_TITLE = 'この写真を報告しない';
export const SEXUAL_CONTENT_TITLE = '性的なコンテンツ';
export const VIOLENT_CONTENT_TITLE = '暴力的なコンテンツ';
export const HATEFUL_CONTENT_TITLE = '不愉快なコンテンツ';
export const DANGEROUS_CONTENT_TITLE = '危険なコンテンツ';
export const COPYRIGHTED_CONTENT_TITLE = '著作権保護コンテンツ';
export const THIS_IS_SPAM_OR_SCAM_TITLE = 'これは迷惑メールか詐欺です';
export const UNDERAGE_CONTENT_TITLE = '未成年コンテンツ';
export const REPORT_PICTURE_TITE = '写真の報告';
export const REPORT_PICTURE_MESSAGE = 'ご報告ありがとうございました。まもなくスタッフが審査させていただきます。';
export const REPORT_USER_TITLE = 'ユーザーの報告';
export const CONFIRM_GIFT_ALERT_TITLE = 'ギフトの確認';
export const SEND_GIFT_ALERT_MESSAGE = '%sギフトを%sさんに贈りますか？';
export const SEND_GIFT_SUCCESS_ALERT_MESSAGE = '%sギフトを%sさんに贈りました';
export const PUCHARSE_ALERT_TITLE = '購入';
export const FAIL_ALERT_TITLE = '失敗';

export const LATEST_GIFTS_SECTION_TITLE = '最新のギフト';
export const REPORT_USER_TITE = 'ユーザー報告';
export const BLOCK_USER_TITLE = 'ブロックする';
export const BLOCK_BTN_TITLE = 'ブロック';
export const UNBLOCK_BTN_TITLE = '解除';
export const NO_MORE_USERS = 'ユーザーがいません';
export const LIST_BLOCKED_USERS_TITLE = 'ブロックリスト';
export const UN_BLOCK_UNSER_TITLE = 'ブロック解除';
export const UN_BLOCK_USER_MSG = 'さんをブロック解除しますか？';
export const NO_MORE_USERS_BLOCKED = '現在ブロックしているユーザーはいません';
export const VIDEOS_PICKER_TITLE = '動画';

export const VIEW_ALL_BUZZ_CELL_TITLE = '全ての投稿を表示';
export const LATEST_BUZZ_SECTION_TITLE = 'タイムライン';
export const USER_NAME_IS_TOO_LONG_MESSAGE = 'ユーザー名が長過ぎます';
export const NO_GIFT_CELL_TITLE = 'ギフトがありません';
export const NO_BUZZ_CELL_TITLE = '投稿がありません';
export const CANNOT_UPLOAD_YOUR_AVATAR_MESSAGE = '写真をアップロードできません。後でもう一度やり直してください。';
export const MEET_PEOPEL_CANNOT_FIND_ANY_MATCHES_MESSAGE = 'フレンズ検索で一致する相手が見つかりません。検索設定を調整してください。';
export const FREE_GIFT_TITLE = '無料';

export const MORE_VIEW_CHOOSE_A_PHOTO = '写真を選択';
export const MORE_VIEW_TAKE_A_VIDEO = '動画を撮る';
export const MORE_VIEW_TAKE_A_PICTURE = '写真を撮る';
export const MORE_VIEW_TAKE_A_RECORD = 'ボイスメッセージ';
export const MORE_VIEW_CHOOSE_A_VIDEO = '動画を選択';
export const MORE_VIEW_PREVIOUS_PHOTO = '以前の写真';

export const SHARE_LOCATION_MORE_MENU_TITLE = '位置を共有';
export const CAMERA_MORE_MENU_TITLE = 'カメラ';
export const PREVIOUS_PHOTO_MORE_MENU_TITLE = '先の写真';
export const CHOOSE_A_VIDEO_MORE_MENU_TITLE = 'ビデオを選択';
export const CHOOSE_A_GIFT_MORE_MENU_TITLE = 'ギフトを贈る';
export const VIEW_PROFILE_LABLE_TITLE = 'プロファイルを表示';
export const LIKE_LABLE_TITLE = 'いいね';
export const UNLIKE_LABLE_TITLE = '取り消す';
export const NO_RESULTS_LABLE_TITLE = '検索結果がありません';
export const REPORT_BUZZ_TITE = '投稿を報告';
export const POST_STATUS_DEFAULT_BUTTON_TITLE = '    今なにしてる？';
export const BUZZ_SEGMENT_FRIEND_TITLE = '友達';
export const BUZZ_SEGMENT_FAVORITES_TITLE = 'お気に入り';
export const NO_BUZZ_TO_SHOW_TITLE = '情報がありません';
export const ADD_FRIEND_FROM_FB_BUZZ_TITLE = 'Facebookやアドレス帳を使って友達を招待しませんか？';
export const YOU_CURRENTLY_HAVE_ANY_FAVORITES_BUZZ_TITLE = 'まだお気に入りのメンバーがいません。気になる人をお気に入りに追加しよう';
export const VIEW_ALL_GIFT_PROFILE_TITLE = '全てのギフトを見る';
export const DISTANCE_IN_SETTING_TITLE = '距離の単位';
export const DISTANCE_IN_MILES_TITLE = 'ﾏｲﾙ';
export const DISTANCE_IN_KILOMETERS_TITLE = 'ｷﾛﾒｰﾄﾙ';
export const DISTANCE_MILES = '%0.1f ﾏｲﾙ';
export const DONT_REPOT_THIS_USER_TITLE = 'このユーザーを報告しない';
export const DO_YOU_WANT_UPLOAD_PHOTO_MESSAGE = '送信します。よろしいですか？';
export const BUY_POINTS_MESSAGE = 'このギフトを購入する為に%dポイント必要です。現在のポイントは%dポイントです。ポイントを追加しますか？';
export const BUY_POINTS_BACKSTAGE_MESSAGE = 'この秘密の写真をロック解除する為に%dポイント必要です。 現在の保有ポイントは%dポイントです。ポイントを追加しますか？';
export const BUY_POINTS_SAVE_PIC_MESSAGE = 'この写真を保存する為に%dポイント必要です。現在の保有ポイントは%dポイントです。ポイントを追加しますか？';
export const NOT_ENOUGHT_TO_VOICE_CALL = '音声通話を継続するにはポイントが足りません。現在のポイントは%dポイントです。ポイントを追加してください。';
export const NOT_ENOUGHT_TO_VIDEO_CALL = 'ビデオ通話を継続するにはポイントが足りません。現在のポイントは%dポイントです。ポイントを追加してください。';
export const NOT_ENOUGH_TO_SEND_MSG = 'メッセージを送るにはポイントが足りません。';
export const BUY_POINT_BUTTON_TITLE = 'ポイントを購入';
export const EDIT_YOUR_BACKSTAGE_TITLE = 'ヒミツの写真';
export const ADD_PIC_BACKSTAGE_TEXT = '写真の追加';
export const BACKSTAGE_SET_PRICE_BTN_TITLE = 'ロック解除';
export const BACKSTAGE_TITLE = 'ヒミツの写真';
export const YOU_WILL_EARN_PICKER_TITLE = '(%dポイント獲得します)';
export const BACKSTAGE_UNLOCKED_FOR_24_DAYS_TITLE = '%sさんのヒミツの写真を\n%d時間だけ見ることができます!!';
export const UNLOCK_POINT_TITLE = 'ロック解除 :';
export const UNLOCK_USER_BACKSTAGE_FOR_24_HOURS_TITLE = '秘密の写真を%d時間解除';
export const USER_WILL_EARN_POINTS_TITLE = '%sさんが%dポイント獲得します';
export const CHAT_AND_NOTIFICATIONS_ALERT_TITLE = 'チャット及び通知設定';
export const VIBRATE_TITLE = 'バイブ設定';
export const PLAY_SOUND_TITLE = 'サウンド設定';
export const RATE_APP_TEXT = 'レビューを書く';

export const DEACTIVE_ACCOUNT_TEXT = '退会';

export const NOTIFICATIONS_TITLE = 'プッシュ通知';
export const PUSH_NOTIFICATION_TITLE = 'プッシュ通知設定';
export const SEND_EMAIL_NOTIFICATION_TITLE = 'メール送信通知';
export const UPDATE_TO_MY_BUZZ_TITLE = '私に関連する投稿';
export const MY_SUBMISSIONS_TITLE = '私の投稿';
export const AND_G_ALERT_TITLE = 'Kyuunからの通知';
export const CHAT_MESSAGE_ALERT_TITLE = 'チャットメッセージ';
export const SOMEON_CHECKS_ME_OUT_TITLE = '誰かが私をチェックした時';
export const EMAIL_NOTIFICATIONS_TITLE = 'メール通知';
export const PUSH_NOTIFICATIONS_TITLE = 'PUSH通知';
export const NONE_TITLE = 'なし';
export const FRIENDS_AND_FAVORITES_TITLE = 'お気に入りのみ';
export const LOOK_AT_ME_GET_POINT_MESSAGE = '「オークション」でアピールしてトップを目指しましょう。';
export const WHO_CHECK_ME_OUT_TITLE = '私をチェックした人';
export const WHO_CHECK_ME_OUT_GET_POINT_MESSAGE = 'ロック解除であなたをチェックした人を確認しましょう。';
export const WINK_BOMB_GET_POINT_MESSAGE = '数百のユーザーへ一度にメッセージを送る「ウィンクボム」';
export const WHO_FAVORITES_ME_TITLE = '誰がお気に入り登録しているか？';
export const WHO_FAVORITES_ME_GET_POINT_MESSAGE = 'あなたをお気に入り登録している人をチェックできます。';
export const SEND_GIFT_GET_POINT_MESSAGE = '仮想のギフトを送って打ち解けよう。休日や誕生日にどうぞ。';
export const YOUR_TOTAL_POINTS_TITLE = 'トータルポイント';
export const BUY_POINT_TITLE = 'ポイント';
export const FORMAT_BUY_POINT_CELL_LABEL = '%dポイントは%sです';
export const DO_YOU_REALLY_WANT_TO_SET_THIS_PICTURE_MESSAGE = 'この画像をプロフ写真に設定しますか？';
export const YOU_CURRENTLY_POINT_MESSAGE = '現在の保有ポイントは%dポイントです。';
export const PURCHASE_FAIL_TITLE = '購入失敗';
export const CONNECT_TO_ITUNES_FAIL_MESSAGE = 'iTunesへの接続に失敗しました';
export const CONNECT_TO_SERVICE_FAIL_MESSAGE = 'サービスへの接続に失敗しました';

export const TITLE_COMFIRM_SAVE_IMAGE = '写真を保存';
export const CONTENT_COMFIRM_SAVE_IMAGE = '%s ポイント使ってこの画像を保存しますか?'

export const BACKSTAGE_INFOMATION_TITLE = 'ここに追加した写真は\n会員様が閲覧するたび%s獲得できます';

export const USER_LIST_INFOMATION = '<img src';

export const RATING_BASE_ON_REVIEWER_TITLE = '評価 (%dのレビューに基づいています)';
export const PLEASE_TAP_A_STAR_TO_RATE_IT_MESSAGE = '星をタップして評価してください';
export const ACTIONSHEET_PROFILE = 'プロフィールを見る';
export const ACTIONSHEET_BLOCK = 'ブロック';
export const ACTIONSHEET_REPORT = '報告';

export const CHAT_DATE_FORMAT_SHORT = 'HH:mm';
export const CHAT_DATE_FORMAT_LONG = 'M月d日 (E)';
export const NEED_UPDATE_THE_APP_MSG = 'AppStoreからアプリを最新のバージョンに更新してください。';
export const FILE_IST_UPLOADING = 'このファイルはアップロード中です。お待ちください。';

export const CHANGE_EMAIL_SCREEN_CURRENT_EMAIL_TITLE = '現在このメールアドレスを使っています:';
export const CHANGE_EMAIL_SCREEN_NEW_EMAIL_TITLE = '新しいアドレス';
export const CHANGE_EMAIL_SCREEN_NEW_EMAIL_FIELD_PLACE_HOLDER = 'メールアドレス';
export const CHANGE_EMAIL_SCREEN_PASSWORD_TITLE = '現在のパスワード';
export const CHANGE_EMAIL_SCREEN_PASSWORD_FIELD_PLACE_HOLDER = 'パスワード';
export const CHANGE_EMAIL_SCREEN_UPDATE_BUTTON_TITLE = '変更';
export const CHANGE_EMAIL_SCREEN_TITLE = 'メールアドレス';

export const CHANGE_EMAIL_SUCCESS_MESSAGE = 'メールアドレスを変更しました。';
export const CHANGE_EMAIL_EMAIL_REGISTED_ERROR_MESSAGE = '既に登録されています。';
export const BUTTON_TITLE_CHANGE = '変更';

export const RETYPE_PASSWORD_IS_NOT_THE_SAME = '新しいパスワードと確認用パスワードが異なります';
export const RETYPE_NEW_PASSWORD = 'パスワードの確認';

export const PROFILE_ADD_AVATAR = 'プロフィール写真\n（あなたの写真を登録してね）';

export const CELL_BLOOD_TYPE_TITLE = '血液型';
export const CELL_REGION_TITLE = '地域';
export const CELL_THREE_SIZE_TITLE = '3サイズ';
export const CELL_ETCH_TITLE = 'エッチ度';
export const CELL_MAN_OF_TYPE = 'タイプの男性';
export const CELL_FETISH_TITLE = 'チャームポイント';
export const CELL_PROUND_TITLE = '自慢できること';
export const CELL_HOPPY_TITLE = '趣味';
export const CELL_MESSAGE_TITLE = '自己紹介';
export const CELL_SEF_INTRODUCTION = '自己紹介';
export const TABLE_CELL_OCCUPATION_TITLE = '職業';

export const JOB_STUDENT = '学生';
export const JOB_OL = 'OL';
export const JOB_PART_TIME = 'アルバイター';
export const JOB_PART_TIME_WORKER = 'フリーター';
export const JOB_NURSE = '看護師';
export const JOB_CHILDMINDER = '保育士';
export const JOB_SALESPERSON = '販売員';
export const JOB_MODEL = 'モデル';
export const JOB_TEACHER = '教師';
export const JOB_DOMESTIC = '家事手伝い';
export const JOB_HOUSEWIFE = '主婦';
export const JOB_OTHERS_WORKER = '飲食系';
export const JOB_APPAREL_SYSTEM = 'アパレル系';
export const JOB_BEAUTY_SYSTEM = '美容系';
export const JOB_YOUR_WATER_SYSTEM = 'お水系';
export const JOB_SYSTEM = 'ヒミツ';
export const JOB_OTHER = 'その他';

export const MAIL_JOB_EMPLOYMENT = '会社員';
export const MAIL_JOB_MANAGEMENT = '経営者';
export const MAIL_JOB_SENIOR_MANAGEMENT = '経営幹部';
export const MAIL_JOB_PART_TIME_WORKER = 'フリーター';
export const MAIL_JOB_STUDENT = '学生';
export const MAIL_JOB_ENGINEER = 'エンジニア';
export const MAIL_JOB_MANUFACTURING = '製造系';
export const MAIL_JOB_TRANSPORTATION_SYSTEM = '運輸系';
export const MAIL_JOB_FINANCE_SYSTEM = '金融系';
export const MAIL_JOB_REAL_ESTATE_SYSTEM = '不動産系';
export const MAIL_JOB_LEGAL_SYSTEM = '法律系';
export const MAIL_JOB_IT_SYSTEM = 'IT系';
export const MAIL_JOB_MEDICAL = '医療系';
export const MAIL_JOB_EDUCATION_SYSTEM = '教育系';
export const MAIL_JOB_WELFARE_SYSTEM = '福祉系';
export const MAIL_JOB_APPAREL_SYSTEM = 'アパレル系';
export const MAIL_JOB_MEDIA_SYSTEM = 'メディア系';
export const MAIL_JOB_FINE_ARTS = '芸術系';
export const MAIL_JOB_OTHER = 'その他';
export const MAIL_JOB_SECRET = 'ヒミツ';

export const LOCATION_INFOMATION_AVAILABLE = '位置情報利用';
export const EDIT_LOCATION_INFO = '位置情報利用を「OK」すると手動で地域を選択する手 間を省くことができます。地域の表示は都道府県単位 となります。例:東京都 と表示されます。';
export const SELECT_REGION_TITLE = '地域選択';
export const SELECT_REGION_MANUALLY = '手動で地域を選択する';

export const CELL_CUP_SIZE_TITLE = 'カップ数';

export const PROFILE_REG_CUTE_TYPE_NEAT = '清楚系';
export const PROFILE_REG_CUTE_TYPE_UNPUSSY = 'おっとり系';
export const PROFLE_REG_CUTE_TYPE_LOLITA = 'ロリ系';
export const PROFILE_REG_CUTE_TYPE_HEALING = '癒し系';
export const PROFILE_REG_CUTE_TYPE_BEAUTIFUL = 'きれい系';
export const PROFILE_REG_CUTE_TYPE_CUTE = 'かわいい系';
export const PROFILE_REG_CUTE_TYPE_GAL = 'ギャル系';
export const PROFILE_REG_CUTE_TYPE_COMEDY = 'お笑い系';
export const PROFILE_REG_CUTE_TYPE_NATURAL = '天然系';
export const PROFILE_REG_CUTE_TYPE_SISTER_1 = 'お姉系';
export const PROFILE_REG_CUTE_TYPE_SISTER_2 = '妹系';
export const PROFILE_REG_CUTE_TYPE_QUEEN = '女王様系';
export const PROFILE_REG_CUTE_TYPE_SECRET = 'ヒミツ';

export const NO_MARRY = '未婚';
export const TO_1_YEAR = '１年未満';
export const TO_3_YEAR = '１～3年';
export const TO_5_YEAR = '3年～5年';
export const TO_10_YEAR = '5年～10年';
export const GREATER_THAN_10_YEAR = '10年以上';
export const DIVORCE = '離婚';

export const LIVE_IN = '同居';
export const LIVE_SEPARATELY = '別居';
export const BUSINESS_TRIP = '単身赴任';
export const LIVE_OTHER = 'その他';

export const SLENDER = 'スレンダー';
export const GLAMOROUS = 'グラマー';
export const GENERAL = '普通';
export const LET_FAT = 'ややぽっちゃり';
export const FAT = 'ぽっちゃり';
export const SECRET = '秘密';

export const CELL_CUTE_TYPE_TITLE = 'タイプ';
export const SORT_TYPE_LAST_LOGIN = 'オンライン順';
export const SORT_TYPE_NEW_REGISTER = '登録順';

export const MARRED_TITLE_PROFILE = '結婚歴';
export const HUSBAND_TITLE_PROFILE = '旦那とは？';
export const STYLE_TITLE_PROFILE = 'スタイル';

export const REGION_SEARCH_SETTING_NEAR_REGION_TEXT = '自分の場所から検索(付近)';
export const REGION_SEARCH_SETTING_CITY_REGION_TEXT = '自分の場所から検索(都市)';
export const REGION_SEARCH_SETTING_STATE_REGION_TEXT = '都道府県から検索';
export const REGION_SEARCH_SETTING_COUNTRY_REGION_TEXT = '自分の場所から検索(国)';
export const REGION_SEARCH_SETTING_WORLD_REGION_TEXT = '全てのユーザー';
export const REGION_SEARCH_SETTING_ALL_REGION_TEXT = 'すべての地域';

export const PROFILE_BIRTHDAY_FORMAT = '%s (%d歳)';

export const CELL_JOIN_HOUR_TITLE = '主な出没時間';

export const JOIN_HOUR_MORNING = '午前中';
export const JOIN_HOUR_MORNING_DAYTIME = '朝～昼間';
export const JOIN_HOUR_EVENING = '夕方';
export const JOIN_HOUR_NIGHT_MIDNIGHT = '夜～深夜';
export const JOIN_HOUR_LATE_NIGHT_EARLY_MORNING = '深夜～早朝';
export const JOIN_HOUR_IRREGULAR = '不定期';
export const JOIN_HOUR_THE_CALL_BY_E_MAIL = 'メールで呼んで';
export const JOIN_HOUR_SECRET = 'ヒミツ';

export const EDIT_PROFILE_BTN_TITLE = 'ポイント';
export const MYPAGE_EDIT_PROFILE_TITLE = 'プロフィール';
export const MYPAGE_SETTING_TITLE = '通話待ち設定';
export const MYPAGE_BLOCK_TITLE = 'ブロックリスト';
export const TIMELINE_POSTS_TITLE = 'タイムライン';
export const MYPAGE_GIFT_TITLE = 'GETしたギフト';
export const CHECK_MYPAGE_WHEN_OTHER_VIEW = '相手から見た自分のプロフィールを確認';
export const ALERT_REGION_DISALLOW = '許可しない';
export const ALERT_REGION_OK = 'OK';
export const ALERT_REGION_MESSAGE = 'マップは現在の位置情報を利用します。\nよろしいですか？';
export const BUZZ_SEGMENT_MYPOSTS = '自分の投稿';
export const CALLLOG_FAILED_CALL_TEXT = '通話失敗';
export const CALLLOG_DID_CALLED_TEXT = '通話時間 %s';
export const BTN_TITLE_FAVORITED = 'お気に入り済';
export const CONV_BTN_DELETE_SEL = '選択削除';
export const FREE_FILL_TITLE = '自由にご記入ください。';
export const EDIT_PROFILE_ENTER_NAME = 'なまえの入力';
export const BIRTHDAY_IS_EMPTY = '誕生日が空白です';
export const GENDER_IS_EMPTY = '性別が空白です';
export const CHECK_AGE_SIGNUP_MESSAGE = '18歳未満の方は登録できません';

export const CONFIRM_TO_DELETE_BUZZ_MESSAGE = 'この投稿を削除しますか？';
export const CONFIRM_TO_DELETE_BUZZ_TITLE = '投稿を削除';
export const CONFIRM_TO_DELETE_COMMENT_MESSAGE = 'このコメントを削除しますか?';
export const CONFIRM_TO_DELETE_COMMENT_TITLE = 'コメントを削除';
export const PLEASE_SELECT_JOB = '職業を選択してください';
export const PLEASE_SELECT_THREESIZE = '3サイズを選択してください';
export const PLEASE_SELECT_CUPSIZE = 'カップを選択してください';
export const PLEASE_SELECT_CUTE_TYPE = 'タイプを選択してください';
export const PLEASE_SELECT_JOIN_HOURS = '主な出没時間を選択してください';
export const PLEASE_FILL_TYPEMAN = 'タイプの男性を入力してください';
export const PLEASE_FILL_FETISH = 'フェチを入力してください';
export const PLEASE_FILL_ABOUT_ME = '自己紹介を入力してください';
export const CURRENT_EMAIL_TITLE = '現在のアドレス';
export const PLEASE_INPUT_EMAIL_OR_PASSWORD_TO_CHANGE = 'メールアドレスとパスワードを入力してください';
export const REGISTER_EMAIL_PASS_SUCCESS_MSG = 'メールアドレスとパスワードの登録に成功しました';
export const REGIST_ID_SCREEN_TITLE = 'アカウント引き継ぎ';
export const CONFIRM_VIEW_PROFILE_AFTER_EDIT_MSG = 'プロフィール編集が完了しました。他のユーザー様からはこのように見えます。';
export const ALERT_TEXT_VIEW = '見る';
export const ALERT_TEXT_DONT_VIEW = '見ない';
export const BASIC_INFO_PART_TITLE = 'プロフィール';
export const PLEASE_SELECT_AT_LEAST_ONE_REGION = '都道府県を一つ以上選択してください。';
export const SELECT_GROUP_REGION = ' まとめて選択 ';
export const UN_SELECT_GROUP_REGION = ' 選択を解除 ';
export const CONFIRM_OPEN_GET_FREE_POINT_MSG = 'Kyuunへようこそ。まずは無料で遊びませんか?';
export const DONT_ENOUGH_POINT_TO_SEND_COMMENT_MSG = 'コメントに十分なポイントがありません';
export const YOU_FINISHED_REGIST_MSG = '続いてプロフィールをご入力ください。';
export const YOU_FINISH_REGIST_MSG_TITLE = '登録完了';
export const WAITING_FOR_VERIFY_AGE_MSG = '只今、年齢認証中です。\n数分後に結果をご連絡いたしますので今しばらくお待ちくださいませ。';
export const NEED_TO_REVERIFY_AGE_MSG = '年齢認証のお手続きにおいて、身分証画像に不備がございました。\n再度年齢認証のお手続きを行ってください。';
export const KYUUN_GENERAL_MSG_TITLE = 'Kyuunからのお知らせ';
export const ALERT_BTN_BACK_TO_VERIFY_AGE = '年齢認証の手続きに進む';
export const DID_ACCEPT_VERIFY_AGE_MSG = '身分証による年齢認証が完了しました。';
export const ALERT_TITLE_SHOW_HOW_TO_USE = 'Kyuunへようこそ。';
export const ALERT_MESSAGE_HOW_TO_USE = 'まずは「使い方」ページをご覧ください。';
export const ALERT_MESSAGE_HOW_TO_USE_MALE = 'はじめての方は、まずは使い方ページをご覧ください。';
export const ALERT_MESSAGE_DONT_SHOW_HOW_TO_USE = 'プロフィール画像を設定すると、チャット返信率がアップ♪';
export const ALERT_MESSAGE_DONT_SHOW_HOW_TO_USE_MALE = 'Kyuunの遊び方を見る場合は、\n左メニュー内の「使い方」をタップしてください。';
export const ALERT_MESSAGE_DONT_SHOW_GET_FREE_POINT = '無料で遊ぶ場合は左メニュー内の\n「無料ポイント」をタップしてください';
export const LEFT_MENU_GET_FREE_POINT = '無料ポイント';

export const VOIP_PARTNER_GOT_REALCALL = '相手が電話を着信しました。しばらくしてからもう一度かけ直してください。';
export const SET_ONLINE_ALERT = '設定済み';
export const NOT_SET_ONLINE_ALERT = 'オンライン通知';
export const GUIDE_FAVOURITE = 'あなたがお気に入り登録している人です';
export const GUIDE_FOLLOWER = 'あなたをお気に入り登録している人です';
export const GUIDE_FOOT_PRINT = 'あなたのプロフィールを閲覧した方です。';
export const GUIDE_ONLINE_ALERT_LIST = 'オンラインになると通知が届きます。';
export const ABOUT_PAYMENT = '料金について';
export const PAY_MENT = '料金設定';
export const ABOUT_KYUUN = 'Kyuunとは';
export const CANNOT_DETECT_REGION = '位置情報を検出することが出来ません。やりなおしてください。';
export const TEXT_RESEND = '再送信';
export const TEXT_DELETE = '削除';
export const EMPTY_CONTENT_SEARCH = '無効な入力。すべての項目を入力してください。';
export const VOIP_CANT_START_VIDEO_CALL = '回線状況が不安定のため切断されました。時間をあけておかけ直し頂くか、Wi-Fi回線に変更してください。';

export const ALERT_MESSAGE_UPLOAD_IMAGE_ERROR = '画像をアップロードできませんでした。';
export const TITLE_BUTTON_SETTING_SCREEN_TOP = '通話設定';
export const TITLE_CLOSE_APP = 'あとで';
export const TITLE_VERSION_UP_ENTERPRISE = 'バージョンアップする';

export const CHAT_CONFIRM_MSG_VIEW_PICTURE_FOR_JUS_POINT = 'ポイントを使ってこの写真を見ますか？';
export const CHAT_CONFIRM_MSG_VIEW_VOICE_FOR_JUS_POINT = 'ポイントを使ってこの音声を聞きますか？';
export const CHAT_CONFIRM_MSG_VIEW_VIDEO_FOR_JUS_POINT = 'ポイントを使ってこの動画を見ますか？';
export const MESSAGE_UNLOCK_IMAGE_POINT = '画像を表示するには%dポイント必要です。現在の保有ポイントは%dポイントです。ポイントを追加しますか？';
export const MESSAGE_UNLOCK_VOICE_POINT = '音声を聞くには%dポイント必要です。現在の保有ポイントは%dポイントです。ポイントを追加しますか？';
export const MESSAGE_UNLOCK_VIDEO_POINT = '動画を表示するには%dポイント必要です。現在の保有ポイントは%dポイントです。ポイントを追加しますか？';

export const MESSGE_HOW_TO_USE_POINT = '無料で遊ぶ場合は左メニュー内の\n「無料ポイント」をタップしてください';

export const TITLE_TEMPLATE_FUCTION = 'テンプレート';
export const CONDITON_TEMPALTE_EMPTY = '最大10個まで登録できます。 \n右下の＋を押してよく使う内容を登録して下さい。';
export const TEMPLATE_TITLE = 'テンプレート（%d/10）';
export const TEMPLATE_TITLE_EMPTY = 'タイトルを入力してください';
export const TEMPLATE_CONTENT_EMPTY = '本文を入力してください';
export const TEMPLATE_CONTEN_AND_TEMPLATE_TITLE_EMPTY = 'タイトルを入力してください';
export const TEMPLATE_DELETE_BUTTON_TITLE = 'このテンプレートを削除してもよろしいですか？';
export const BUTTON_NO_TEMPLATE = 'いいえ';
export const BUTTON_YES_TEMPLATE = 'はい';
export const TITLE_EDIT_TEMPLATE = 'テンプレート編集';
export const MESSAGE_SHOW_ALL_REPLY = '過去の返信をすべて表示する';
export const MESSAGE_REPLY_COMMENT = '返信する';
export const NOTI_REPLY_YOUR_COMMENT = 'タイムラインに返信がありました';
export const MESSAGE_DELETE_SUB_COMMENT = 'この返信を削除しますか？';
export const MESSAGE_SUB_COMMENT_NOT_FOUND = 'このコメントは削除されました';
export const NOT_ENOUGH_POINT_REPLY_COMMENT_MESSAGE = 'コメントを返信するには%dポイント必要です。ポイントを追加しますか?';
export const PLACEHOLDER_REPLY_COMMENT = '返信してみよう';
export const MESSAGE_UPLOAD_FILE_OR_IMAGE_FAIL = 'アップロードに失敗しました';
export const ACCOUNT_BLOCK_MESSAGE = 'ブロックされているユーザーの為、実行できません。';
export const ACCOUNT_DEACTIVE_MESSAGE = '現在このプロフィールにアクセル出来ません。やり直してください。';
export const MESAGE_DOWNLOAD_VIDEO_FAIL = '動画が読み込めませんでした。\nOKボタンで再度試してください。';
export const MESSAGE_REVIEW_UPDATE_USER_INFO = '編集したプロフィールは承認後公開されます';
export const NOTI_DENIED_ATTACHMENT_FILE_IN_CHAT = 'この添付ファイルは削除されました。';

/* message request call */

export const MESAGE_WHEN_BOTH_VOICE_CALL_AND_VIDEO_CALL_TOGETHER_OFFLINE = '%sさんは現在通話ができません。\nリクエストしてお誘いしましょう(無料)。';
export const MESSAGE_WHEN_JUST_VOICE_CALL_OFFLINE = '%sさんは現在音声通話ができません。\nリクエストしてお誘いしましょう(無料)。';
export const MESSAGE_WHEN_JUST_VIDEO_CALL_OFFLINE = '%sさんは現在ビデオ通話ができません。\nリクエストしてお誘いしましょう(無料)。';
export const MESSAGE_NOTIFICATION_WHEN_USER_VIDEO_CALL = '%sさん、ビデオ通話しませんか？';
export const MESSAGE_NOTIFICATION_WHEN_USER_VOICE_CALL = '%sさん、音声通話しませんか？';
export const MESSAGE_REQUEST_CALL_UPDATE = 'さんから、通話リクエストが届きました♪';
export const READ_MESSAGE = '既読';
export const TITLE_BTN_WHEN_SETTING_VIDEO_CALL_OFFLINE = ' ビデオ通話\n リクエスト';
export const TITLE_BTN_WHEN_SETTING_VOICE_CALL_OFFLINE = ' 音声通話\n リクエスト';

export const STRING_VERSION = 'バージョン';
export const NOTI_SEND_TEMPLATE_MESSAGE = '%s さんに送信しました';
export const RESTORE_CONFIRM_TRANSACTION = '前回のAppstore決済が失敗しました。こちらの表示を閉じた後、AppleIDを入力する表示がされますので、入力後に自動的に前回失敗分のポイントが加算されます。';
export const WAITING_CALL_SETTING = '時～';
export const WAITING_CALL_NOTI_NO_CHECK_BOX = '通話着信を受ける場合は、着信設定をしてください。';
export const WAITING_CALL_NOTI_ONLINE = '只今の時間は通話着信を許可しています。';
export const WAITING_CALL_NOTI_OFFLINE = '只今の時間は通話着信を拒否しています。';
export const TITLE_NOTI_SAVE_SETTING_CALL = '[!]設定が変更されています';
export const MSG_NOTI_SAVE_SETTING_CALL = '保存せずにページを閉じますか？';
export const SAVE = '保存する';
export const NOTI_SETTING_DESCRIPTION = '通話可能な時間帯をチェックし、右上の「完了」を押してください。\n設定した時間帯になると、[着信設定]でチェックした通話方法が自動的に着信許可されます。';
export const CALLING_IS_BUSY_SEND_CALL_REQUEST = 'お相手は、ただいま通話中です。\n通話リクエストを送りますか？';
export const END_CALL_TITLE = '通話が終了しました';
export const YOU_ARE_OUT_OF_POINT = 'ポイントが不足しています';
export const REFILL = 'ポイント補充';
export const PLEASE_RATE_US = 'お楽しみいただけた方は、是非 \nレビューで応援をお願いします☆';
export const NOT_FUN = '楽しくなかった';
export const KEEP_UP_THE_GOOD_WORK = '応援してあげる';
export const CALL_ENDED = '通話が終了しました。\n引き続きお楽しみください。';
export const NOTI_SERSER_MAINTAIN_RESTART = 'メンテナンス中です。間もなく完了します。';
export const TITLE_MAINTAIN = 'メンテナンス';

export const POST_IMAGE_TITLE = '画像投稿';
export const LOGIN_BONUS_TITLE = '毎日ログインでポイントGET';

// Tutorial
export const TUTORIAL_CLOSE = 'チュートリアルを終了する';
// Top
export const TUTORIAL_TOP_BEGIN_TITLE = '1分でわかるKyuunの使い方';
export const TUTORIAL_TOP_BEGIN_CONTENT = 'この度は、ご登録ありがとうございます。\n\nこのアプリは、異性とチャットや音声通話、ビデオ通話を通じて恋人感覚で仲良くお話できるアプリです。';
export const TUTORIAL_TOP_1 = '/highlight_text/\nこの3つがメインの楽しむ方法です。';
export const TUTORIAL_TOP_1_YELLOW = '①チャット（メッセージ）\n②音声通話\n③ビデオ通話';
export const TUTORIAL_TOP_2 = '他にも、\n/highlight_text/\nなど楽しめる要素もたくさんあります。';
export const TUTORIAL_TOP_2_YELLOW = '・お相手の日常を垣間見えるタイムライン\n・普段見れない姿を見れるヒミツの写真';
export const TUTORIAL_TOP_3 = '/highlight_text/\nなど、無料で楽しめるコンテンツも多数ご用意！';
export const TUTORIAL_TOP_3_YELLOW = '・プロフィール閲覧\n・タイムライン投稿\n・オンライン通知';
export const TUTORIAL_TOP_4 = 'さて、早速使い方を説明してまいります。\n\n今開いているページは、異性が並ぶページです。';
export const TUTORIAL_TOP_4_YELLOW = '';
export const TUTORIAL_TOP_5 = '/highlight_text/を押すと、すべてのお相手が並びます。';
export const TUTORIAL_TOP_5_YELLOW = '「すべて」';
export const TUTORIAL_TOP_6 = '/highlight_text/を押すと、\n通話待ち中のお相手が並びますので、\n通話したい相手を探す時はこのタブを選択しましょう。';
export const TUTORIAL_TOP_6_YELLOW = '「通話待ち」';
export const TUTORIAL_TOP_7 = '/highlight_text/を押すと、\n登録後間もないお相手のみが表示されます。優しく接してあげましょう。';
export const TUTORIAL_TOP_7_YELLOW = '「新人」';
export const TUTORIAL_TOP_8 = '/highlight_text/を押すと、\n地域や年齢で好きなお相手を探すことができます。';
export const TUTORIAL_TOP_8_YELLOW = '「検索」';
export const TUTORIAL_TOP_9 = '/highlight_text/を押すと、\nあなたのプロフィールを見たお相手を確認できます。';
export const TUTORIAL_TOP_9_YELLOW = '「足あと」';
export const TUTORIAL_TOP_10 = '/highlight_text/を押すと、\nあなたの投稿したタイムラインにいいね！したお相手を確認できます。';
export const TUTORIAL_TOP_10_YELLOW = '「フォロワー」';
export const TUTORIAL_TOP_11 = '/highlight_text/を押すと、\nあなたがお気に入りしたお相手を確認できます。';
export const TUTORIAL_TOP_11_YELLOW = '「通話設定」';
export const TUTORIAL_TOP_12 = 'ここまで見てくださった方に、お得なお知らせがあります！\n\nはじめてご利用の方に限り、\n無料でアプリ体験できるポイントをプレゼントしています。';
export const TUTORIAL_TOP_12_YELLOW = '';
export const TUTORIAL_TOP_13 = '左上のメニューを開いて、\n/highlight_text/から、\n無料体験ポイントを手に入れてください。';
export const TUTORIAL_TOP_13_YELLOW = '「無料ポイントページ」';
export const TUTORIAL_TOP_14 = '左上のメニューを開いて、\n/highlight_text/から、\n無料体験ポイントを手に入れてください。';
export const TUTORIAL_TOP_14_YELLOW = '「無料ポイントページ」';

export const TUTORIAL_TOP_FINISH_CONTENT = 'チュートリアルはこれでおしまいです。\nそれでは、お好きなお相手を選んで、\n早速チャットして見ましょう♪';
export const TUTORIAL_TOP_CANCEL = 'アプリの使い方は、\n左メニュー「使い方」をチェック★';

// Profile
export const TUTORIAL_PROFILE_BEGIN_TITLE = 'ここから、チャットを送信したり、\n通話を発信したり、お相手の\nタイムラインや秘密の写真を\n閲覧したりできます。';
export const TUTORIAL_PROFILE_BEGIN_CONTENT = 'プロフィールのチュートリアルを見ますか？';
// Chat button
export const TUTORIAL_PROFILE_1 = '/highlight_text/ボタンを押すと、文字で\nチャットができます。';
export const TUTORIAL_PROFILE_1_YELLOW = '「チャット」';
// Voice call button
export const TUTORIAL_PROFILE_2 = '/highlight_text_1/ボタンを押すと、\n音声通話を発信できます。\n/highlight_text_2/';
export const TUTORIAL_PROFILE_2_YELLOW_1 = '「音声通話」';
export const TUTORIAL_PROFILE_2_YELLOW_2 = '※相手が通話待ち設定していない\nときは、リクエスト送信できます。';
// Video call button
export const TUTORIAL_PROFILE_3 = '/highlight_text_1/ボタンを押すと、\nビデオ通話を発信できます。\n/highlight_text_2/';
export const TUTORIAL_PROFILE_3_YELLOW_1 = '「ビデオ通話」';
export const TUTORIAL_PROFILE_3_YELLOW_2 = '※相手が通話待ち設定していない\nときは、リクエスト送信できます。';
// Favorite button
export const TUTORIAL_PROFILE_4 = '/highlight_text/ボタンを押すと、お相手\nをお気に入り登録することができます。\nチュートリアルはこれでおしまいです。\nそれではお気に入りのお相手を見つけて\nお話してみましょう♪';
export const TUTORIAL_PROFILE_4_YELLOW = '「お気に入り」';
// Give gift button
export const TUTORIAL_PROFILE_5 = '/highlight_text/ボタンを押すと、お相手にギフトを贈ることができます♪';
export const TUTORIAL_PROFILE_5_YELLOW = '「ギフトを贈る」';
// Online alert button
export const TUTORIAL_PROFILE_6 = '右上から2つめのアイコンを押して項\n目を開き/highlight_text/ボタンを押すと、お相手\nがオンラインになった通知を受け取れます。';
export const TUTORIAL_PROFILE_6_YELLOW = '「オンライン通知」';

export const TUTORIAL_PROFILE_FINISH_BUTTON_TITLE = 'プロフィールページに戻る';

// Call waiting settings
export const TUTORIAL_CALL_WAITING_BEGIN_TITLE = 'このページでは、\n通話待ち設定をすることができます。';
export const TUTORIAL_CALL_WAITING_BEGIN_CONTENT = '設定方法のチュートリアルを見ますか？';
export const TUTORIAL_CALL_WAITING_1 = 'チェックをオンにして、\n/highlight_text/を押すと、通話待ち状態になります。\n通話待ち状態になっている間は、お相手から通話が\nかかってくることがあります。';
export const TUTORIAL_CALL_WAITING_1_YELLOW = '「完了」';
export const TUTORIAL_CALL_WAITING_2 = '/highlight_text/を押すと、通話可能な時間帯を選択できます。\nより細かく設定したいときに便利な機能です。\n\n設定方法のチュートリアルはこれでおしまいです。設定をしてお相手との通話を楽しみましょう♪';
export const TUTORIAL_CALL_WAITING_2_YELLOW = '「＋」';

export const TUTORIAL_CALL_WAITING_FINISH_BUTTON_TITLE = '通話待ち設定ページに戻る';

// Template
export const TUTORIAL_TEMPLATE_BEGIN_TITLE = 'テンプレートを登録しておくと、\n便利にチャットができるようになります♪';
export const TUTORIAL_TEMPLATE_BEGIN_CONTENT = '作成方法のチュートリアルをみますか？';
export const TUTORIAL_TEMPLATE_1 = ' ここを押して新規作成して見ましょう。';
export const TUTORIAL_TEMPLATE_2_TITLE = '入力して保存しましょう。';
export const TUTORIAL_TEMPLATE_2_CONTENT = '(例)\nタイトル：はじめての挨拶メッセージ\n本文：初めまして★\nよかったら仲良くしてください♪';
export const TUTORIAL_TEMPLATE_3 = '/highlight_text_1/を選択して\nまだチャットしたことのない男性の  プロフィールを開くと、設定しているテンプレートの内容が挿入された状態になっています。/highlight_text_2/を押すだけで送信ができてとても便利です♪\n\n作成方法のチュートリアルはこれでおしまいです。他にもテンプレートを作成してみましょう♪';
export const TUTORIAL_TEMPLATE_3_YELLOW_1 = '「自動入力」';
export const TUTORIAL_TEMPLATE_3_YELLOW_2 = '「送信」';

export const TUTORIAL_TEMPLATE_FINISH_BUTTON_TITLE = 'テンプレート作成ページに戻る';

export const CREATE_TEMPLATE_BUTTON_TITLE = '新規作成';
export const AUTO_FILL_TEMPLATE_BUTTON_TITLE = '自動入力';

export const ALLUSER_TITLE = 'すべて';
export const UNREAD_TITLE = '未読';
export const DELETE_TITLE = '削除';

export const MISSION_START = 'ミッションをはじめる';
export const MISSION_DOING = 'OK';
export const MISSION_FINISH = '報酬を受け取る';
export const MISSION_BASIC_FINISH = 'OK';
export const MISSION_LEVEL_BASIC = '基本編';
export const MISSION_LEVEL_ADVANCE = '応用編';
export const MISSION_BASIC_FINISH_NOTE = '応用編が解放されました。 \nさらに報酬GETのチャンス!!';
export const MISSION_ADVANCE_ITEM_ALERT = '未達成のミッションを\nクリアすると開放されます。';
export const MISSION_ADVANCE_TAB_ALERT = '基本編のミッションを\n全てクリアすると開放されます。';
export const MISSION_FINISHED = '報酬を受け取りました！';
export const FINISHED_LIST_BASIC_MISSION = '応用編が解放されました。\nさらに報酬GETのチャンス!!';
export const NOTI_MISSION_COMPLETE = 'ミッションクリア☆さっそく報酬をGETしよう♪';
export const TITLE_MISSION_VIEW = '72時間限定ミッション!!';
export const METTER_MISSION = '挑戦中';
export const LB_POINT_MISSION = '挑戦する';
export const LB_POINT_START = '挑戦する';
export const LB_POINT_PROCESS = '詳細確認';
export const LB_POINT_CLEAR = '報酬受取';
export const LB_ROW = '基本編 ';
export const LB_STAR = '応用編 ';
export const DEFAULT_METTER = '挑戦前';
export const TITLE_LEFT_CELL_BASIC = '基本編';
export const TITLE_LEFT_CELL_AVD = '応用編';

export const SEND_NUMBER = '送信回数:%s';
export const REPLY_NUMBER = '受信回数:%s';
export const PERCENTAGE = '返信率:%.1f%%';
export const PERCENTAGE0 = '返信率:%.0f%%';

export const TITLE_DELETED_IMAGE = '閲覧期限が終了致しました。\n こちらの画像は閲覧が行なえません。';
export const MEDIA_DENIED = '添付ファイルは審査待ち状態です。';
export const DENIED_FILE_ATTACHMENT_CHAT = 'この添付ファイルは削除されました';
export const DENIED_FILE_ATTACHMENT = '添付ファイルが規約違反に触れる内容のため削除されました。今後の送信はご注意ください。';

export const BUTTON_LIST_BACKSTAGE = 'ヒミツの写真のあしあと';
export const CONTENT_FIRST_CELL = 'イージーギフトは、プレゼントを送ることが出来るサービスです。\nお相手が貰って喜ぶギフトを事務局が厳選してご用意。\nお気に入りの方にぜひプレゼントしてみてはいかがでしょうか？';
export const CONTENT_LAST_CELL = '※何らかの理由でお相手がギフトを受け取れない場合、商品分のポイントを返還致します。\n\n※在庫切れが発生した場合、商品分のポイントを返還致します。\n\n※ご申請後のキャンセルは受付致しかねます。\n\n※お相手が48時間以内に受理をしなかった場合、自動的にキャンセルとなり商品分のポイントを返還致します。\n\n※ギフト到着まで、ご申請から1週間〜10日ほどのお時間を要します。';
export const TITLE_FIRST_CELL = 'Kyuun Giftについて';
export const TITLE_PATH_CELL = 'ギフトが届くまでの流れ';
export const TITLE_LAST_CELL = '免責事項';
export const TITLE_LIST_GIFT = 'Kyuun Gift 一覧';
export const TITLE_ABOUT_GIFT = 'Kyuun Giftについて';
export const TITLE_HISTORY_GIFT = 'プレゼント履歴';
export const MESS_NOT_INPUT = '必要情報を入力してください';
export const MESS_NOT_FOUND_ZIPCODE = '該当する郵便番号がありませんでした。\n確認の上再度入力してください。';
export const MESS_NOT_NUMBER = '半角数字のみで入力してください';
export const MESS_7_CHARACTER = '7桁の半角数字で入力してください';
export const TITLE_ADD_VIEW = '受け取り先情報入力';
export const SORT_HIGH = '高い順';
export const SORT_LOW = '安い順';
export const SORT_DEFAULT = '---';
export const PRICE_RANGE_1 = '~5000pts';
export const PRICE_RANGE_2 = '5,001pts~10,000pts';
export const PRICE_RANGE_3 = '10,001pts~20,000pts';
export const PRICE_RANGE_4 = '20,001pts~30,000pts';
export const PRICE_RANGE_5 = '30,001pts~40,000pts';
export const PRICE_RANGE_6 = '40,001pts~50,000pts';
export const PRICE_RANGE_7 = '50,001pts~';
export const NOTIFICATION_RECEIVED_REAL_GIFT = '%sさんへギフトを発送しました！';
export const NOTIFICATION_CONFIRM_RECEIVING_REAL_GIFT = '%sさんがギフトを受理しました！';
export const NOTIFICATION_CANCELED_REAL_GIFT = '女性が受け取りを拒否されました。';
export const CANCEL_CHAT_CONVER = 'ギフトをキャンセルしました。';
export const EXPIRED_DATA_CONVER = 'ギフト受付の有効期限が切れました。';
export const SEND_SUCESS_CONVER = 'ギフトを受け取りました';
export const NOTI_CONFIRM_GIFT = 'さんがギフトを受理しました！;           //%sさんがギフトを受理しました！';
export const RECEIVED_REAL_GIFT = 'さんへのギフト発送手続きが完了しました！; //%sさんへのギフト発送手続きが完了しました！';
export const LAST_CHAT_MESS_SEND_GIFT_MALE = 'ギフトが贈られました。';
export const LAST_CHAT_MESS_SEND_GIFT_FMALE = 'ギフトの配送が完了しました。';
export const PLACEHOLDER_INPUT_CM = 'コメントを入力...';
export const PLACEHOLDER_INPUT_CM_FOR_OTHER_USER = 'コメントを入力...（%s消費します)';
export const HINT_CHAT = 'この男性は●歳、住まいは●です。挨拶だけではなく、男性のプロフィール情報をチェックして興味あることに絡めたメッセージを送るとより返信率が高まるかもしれません♪ご自身が写った写真や動画を送るのも効果的かも！？';
export const ASK_ME_TITLE = '未設定';
export const GIFT = 'ギフト';
export const FAVORITED = 'お気に入り済み';
export const BLOCK = 'ブロック';
export const REPORT = '報告';
export const NECESSARY = '(※)必須';
export const OK_ALERT_TEXT = 'OK';
export const ALERT_MUST_REQUIRE = '(※)は必須項目です。';
export const EMPTY_SEARCH_BY_NAME = '該当ユーザーがいません';
export const EMAIL_NOT_FOUND_MESSAGE_CODE_LOGIN = 'メールアドレスが見つかりません。';
export const ALERT_TITLE_VALIDATE_EMAIL_LOGIN = 'ログイン出来ません';
export const EMAIL_IS_WRONG_MESSAGE_FOGOTPASS = 'メールアドレスの形式が無効です';
export const EMAIL_IS_NOT_EMPTY = 'メールを入力してください';
export const ALERT_VALIDATE_FOGOTPASS_TITLE = 'パスワード再設定';
// 20180801 - ITS#29367 - DucNc - Add - Start
// tutorialMale
export const TUTORIAL_TOP_FEMALE_STEP1 = 'ログイン順に女性が並びます \n好きな女性を見つけて、 \nタップしてみましょう！';
export const TUTORIAL_TOP_FEMALE_STEP2 = '気になる女の子に \nチャットや通話ができます。 \nまずはチャットから仲良くなりましょう';
export const TUTORIAL_TOP_FEMALE_STEP3 = '「可愛いね」「仲良くなりたいです」 \nなど、挨拶メッセージを \n送ってみよう！';
export const FIRST_TUTORIAL_ON_TOP = 'Kyuunは大人の女性と\n チャットやビデオ通話が\n 楽しめるアプリです。';
// 20180801 - ITS#29367 - DucNc - Add - End

// How to use
export const ALERT_TITLE_SHOW_HOW_TO_USE_MEET = 'Kyuunへようこそ。';
export const ALERT_MESSAGE_DONT_SHOW_HOW_TO_USE_MALE_MEET = 'Kyuunの遊び方を見る場合は、\n左メニュー内の「使い方」をタッ \nプしてください。';
export const NOTICE_DEALS_FROM_KYUUN_NOTI = 'Kyuunからのお知らせ';

export const BTN_VOIP_VOICE_END_TITLE = '終了';
export const BTN_VOIP_VIDEO_END_TITLE = '終了';

// Buy point
export const TITLE_CONTENT_1 = '気軽にお試し！チャット体験はこちら';
export const TITLE_CONTENT_2 = 'ビデオ通話も試したい方はこちら';
export const TITLE_CONTENT_3 = '一番人気!長く楽しみたい方はこちら';
export const TITLE_CONTENT_4 = 'いろいろな女性と仲良くなりたいならこちら';
export const TITLE_CONTENT_5 = 'ポイントを気にせず遊びたい方はこちら';

// Buy point Ver 2
export const TITLE_BUYPOINT = '【ポイントが不足しています】';
export const CONTENT_BUYPOINT = 'ポイントを補充して、\n女の子との会話を楽しみましょう♪';
export const BUTTON_BUYPOINT_YES = ' [購入ページへ進む]';
export const BUTTON_BUYPOINT_NO = '閉じる';

// Real gift
export const GIFT_STATUS_SENDING_MALE = 'お相手へ受け取り確認中です。';
export const GIFT_STATUS_SENDED_MALE = 'お相手がギフトを受理しました。現在発送準備中です';
export const GIFT_STATUS_RECEIVED_MALE = '商品発送が完了しました。';
export const GIFT_STATUS_CANCEL_MALE = '相手が商品の受け取りをキャンセルしました。';
export const GIFT_STATUS_OUTOFSTOCK_MALE = '商品の在庫切れです。';

export const GIFT_STATUS_SENDING_FEMALE = '受け取り先の指定待ちです。';
export const GIFT_STATUS_SENDED_FEMALE = '商品の発送準備中です。しばらくお待ち下さい。';
export const GIFT_STATUS_RECEIVED_FEMALE = '商品発送が完了しました。';
export const GIFT_STATUS_CANCEL_FEMALE = '商品の受け取りをキャンセルしました。';
export const GIFT_STATUS_OUTOFSTOCK_FEMALE = '申し訳ございません。商品の在庫切れです。';
export const GIFT_ERROR_OUT_OF_STOKE = '在庫切れにより注文できません。他の商品をお選びください。';
export const GIFT_ERROR_OUT_OF_STOKE_FEMALE = '在庫切れによりアクセスできません。時間を空けてお試しください。';
export const GIFT_ERROR_NOT_ENOUGH_POINT = 'ポイントが不足しております。';
export const GIFT_BUY = '購入する';
export const GIFT_NO_ONE_USER_FOR_SEND_GIFT = 'ギフトを贈れる相手がいません。';
export const GIFT_NOT_CHAT_WITH_ANYONE = '贈れる相手がまだいません。ギフトを贈りたい相手と、チャットや通話をしてください';
// 20180801 - ITS_#27991 - HuyNguyen - Add - Start
export const MENU_RANKING_TITLE = 'ランキング';
export const MENU_RANKING_SETTING = 'ランキングへ参加';
export const RANKING_TAB_DAY = '前日';
export const RANKING_TAB_WEEK = '週間';
export const RANKING_TAB_MONTH = '月間';
export const RANKING_NUMBER = '%d位';
export const RANKING_CALL = '電話できます';
export const RANKING_CHAT = 'ログイン%d分前';
export const RANKING_LAST_RANK = '前回%d位';
export const RANKING_DEACTIVE = 'このユーザーは退会しました';
// 20180801 - ITS_#27991 - HuyNguyen - Add - End

// 20181212 - LocDT - Add - Start
export const PURCHASE_POINTS_TO_TALK_WITH_GIRL = 'ポイント購入すると女の子と会話ができるようになります。';
export const GO_TO_BUY_POINT_PAGE = 'ポイント購入ページへ';
// 20181212 - LocDT - Add - End
/*
 *
 * Web define
 */

export const HTTP_SUCCESS = 'Success';
export const HTTP_UNKNOWN_ERROR = 'Unknown error';
export const HTTP_WRONG_DATA_FORMAT = 'Wrong data format';
export const HTTP_INVALID_TOKEN = 'Invalid token';
export const HTTP_CHANGE_SETTING_TOKEN = 'Change setting token';
export const HTTP_INVALID_ACCOUNT = 'Invalid account';

export const HTTP_EMAIL_NOT_FOUND = 'Email not found';
export const HTTP_INVALID_EMAIL = 'Invalid email';
export const HTTP_EMAIL_REGISTED = 'このメールアドレスは既に登録されています。';
export const HTTP_EMAIL_REGISTERED = 'Email registered';
export const HTTP_SEND_MAIL_FAIL = 'Send mail fail';
export const HTTP_INVALID_USER_NAME = 'Invalid user name';
export const HTTP_INVALID_BIRTHDAY = 'Invalid birthday';
export const HTTP_UNUSABLE_EMAIL = 'Unusable email';
export const HTTP_DUPLICATE_USER_NAME = 'Duplicate user name';

export const HTTP_INCORRECT_PASSWORD = 'Incorrect password';
export const HTTP_INVALID_PASSWORD = 'Invalid password';
export const HTTP_NOT_FOR_USERS = 'Not for users';

export const HTTP_UPLOAD_IMAGE_ERROR = 'Upload image error';
export const HTTP_UPLOAD_FILE_ERROR = 'Upload file error';

export const HTTP_ACCESS_DENIED = 'Access denied';
export const HTTP_INVALID_FILE = 'Invalid file';
export const HTTP_FILE_NOT_FOUND = 'File not found';

export const HTTP_WRONG_VERIFICATION_CODE = 'Wrong verification code';
export const HTTP_USER_NOT_EXIST = 'User not exist';
export const HTTP_LOCKED_USER = 'Locked user';

export const HTTP_LOCK_FEATURE = 'Lock feature';
export const HTTP_NOT_ENOUGHT_POINT = 'Not enought point';
export const HTTP_PATNER_NOT_ENOUGHT_POINT = 'Patner not enought point';
export const HTTP_BLOCK_USER = 'Block user';
export const HTTP_WRONG_URL = 'Wrong url';
export const HTTP_EMPTY_DATA = 'Empty data';

// back end
export const HTTP_SERVER_MAINTAIN = 'Server maintain';
export const HTTP_OUT_OF_DATE = 'Out of date';
export const HTTP_DISABLE_EMAIL = 'Disable email';
export const HTTP_IMAGE_NOT_FOUND = 'Image not found';
export const HTTP_BUZZ_NOT_FOUND = 'Buzz not found';
export const HTTP_COMMENT_NOT_FOUND = 'Comment not found';
export const HTTP_EXISTS_DATA = 'Exists data';
export const HTTP_CM_CODE_EXISTS = 'Cm code exists';
export const HTTP_PERMISSION_DENIED = 'Permission denied';

// main
export const HTTP_OUT_OF_DATE_API = 'Out of date api';
export const HTTP_UNUSABLE_VERSION_APPLICATION = 'Unusable version application';
export const HTTP_INVALID_APPLICATION_NAME = 'Invalid application name';
export const HTTP_CATEGORY_NAME_EXIST = 'Category name exist';
export const HTTP_NO_CHANGE = 'No change';
export const HTTP_TRANSACTION_EXIST = 'Transaction exist';
export const HTTP_NOT_DOWNLOAD = 'Not download';

export const HTTP_WRONG_FILE_TYPE = 'Wrong file type';

export const HTTP_UPDATE_NEWS_TOKEN = 'Update news token';

export const HTTP_CAN_NOT_DELETE_APPLICATION = 'Can not delete application';

export const HTTP_MISSION_LOCKED = 'Mission locked';
export const HTTP_MISSION_NOT_COMPLETE = 'Mission not complete';

export const HTTP_PUSH_SUCCESS = 'Push success';
export const HTTP_PUSH_ERROR = 'Push error';

export const HTTP_OUT_OF_LIMIT_WARNING = 'Out of limit warning';

export const HTTP_POINT_CAMPAIGN_INVALID = 'Point campaign invalid';

export const HTTP_GET_TUTORIAL_POINT_BEFORE = 'Get tutorial point before';

export const USER_NAME_IS_INVALID = '英語または日本語のみを入力してください。';
export const WOMEN_CAN_NOT_LOGIN = '女性はログイン不可です。';

export const TITLE_CONFIRM_CALL_VOICE = '音声通謂を発倍します';
export const TITLE_CONFIRM_CALL_VIDEO = 'ビデオ通話を発倍 します';

export const PLEASE_CHOOSE_VIDEO_FILE = 'ビデオを選択しください';
export const PLEASE_CHOOSE_IMAGE_FILE = 'ビデオ通話を発倍 画像を選択しください';
