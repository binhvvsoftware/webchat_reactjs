const data = [
    { id: '1', name: '北海道', alias: 'Hokkaido,北海道' },
    { id: '2', name: '青森', alias: 'Aomori,青森県' },
    { id: '3', name: '岩手', alias: 'Iwate,岩手県' },
    { id: '4', name: '宮城', alias: 'Miyagi,宮城県' },
    { id: '5', name: '秋田', alias: 'Akita,秋田県' },
    { id: '6', name: '山形', alias: 'Yamagata,山形県' },
    { id: '7', name: '福島', alias: 'Fukushima,福島県' },
    { id: '8', name: '茨城', alias: 'Ibaraki,￼茨城県' },
    { id: '9', name: '栃木', alias: 'Tochigi,￼栃木県' },
    { id: '10', name: '群馬', alias: 'Gunma,￼群馬県' },
    { id: '11', name: '埼玉', alias: 'Saitama,￼埼玉県' },
    { id: '12', name: '千葉', alias: 'Chiba,千葉￼県' },
    { id: '13', name: '東京', alias: 'Tokyo,東京都' },
    { id: '14', name: '神奈川', alias: 'Kanagawa,神奈川県' },
    { id: '15', name: '新潟', alias: 'Niigata,新潟県' },
    { id: '16', name: '富山', alias: 'Toyama,富山県' },
    { id: '17', name: '石川', alias: 'Ishikawa,石川県' },
    { id: '18', name: '福井', alias: 'Fukui,福井県' },
    { id: '19', name: '長野', alias: 'Nagano,長野県' },
    { id: '20', name: '山梨', alias: 'Yamanashi,山梨県' },
    { id: '21', name: '岐阜', alias: 'Gifu,岐阜県' },
    { id: '22', name: '静岡', alias: 'Shizuoka,静岡県' },
    { id: '23', name: '愛知', alias: 'Aichi,愛知県' },
    { id: '24', name: '三重', alias: 'Mie,三重県' },
    { id: '25', name: '滋賀', alias: 'Shiga,滋賀県' },
    { id: '26', name: '京都', alias: 'Kyoto,京都府' },
    { id: '27', name: '大阪', alias: 'Osaka,大阪府' },
    { id: '28', name: '兵庫', alias: 'Hyogo,兵庫県' },
    { id: '29', name: '奈良', alias: 'Nara,奈良県' },
    { id: '30', name: '和歌山', alias: 'Wakayama,和歌山県' },
    { id: '32', name: '鳥取', alias: 'Tottori,鳥取県' },
    { id: '31', name: '島根', alias: 'Shimane,島根県' },
    { id: '33', name: '岡山', alias: 'Okayama,岡山県' },
    { id: '34', name: '広島', alias: 'Hiroshima,広島県' },
    { id: '35', name: '山口', alias: 'Yamaguchi,山口県' },
    { id: '36', name: '徳島', alias: 'Tokushima,徳島県' },
    { id: '37', name: '香川', alias: 'Kagawa,香川県' },
    { id: '38', name: '愛媛', alias: 'Ehime,愛媛県' },
    { id: '39', name: '高知', alias: 'Kochi,高知県' },
    { id: '40', name: '福岡', alias: 'Fukuoka,福岡県' },
    { id: '41', name: '佐賀', alias: 'Saga,佐賀県' },
    { id: '42', name: '長崎', alias: 'Nagasaki,長崎県' },
    { id: '43', name: '熊本', alias: 'Kumamoto,熊本県' },
    { id: '44', name: '大分', alias: 'Oita,大分県' },
    { id: '45', name: '宮崎', alias: 'Miyazaki,宮崎県' },
    { id: '46', name: '鹿児島', alias: 'Kagoshima,鹿児島県' },
    { id: '47', name: '沖縄', alias: 'Okinawa,沖縄県' },
    { id: '48', name: '海外', alias: 'Overseas' },
];

const region = {};
data.forEach(item => {
    region[item.id] = item.name;

    return region;
});

export const AREA_REGION = region;

export const AREA_REGION_GROUPS = [
    {
        'name': '北海道',
        'region': {
            1: AREA_REGION[1],
        },
    },
    {
        'name': '東北',
        'region': {
            2: AREA_REGION[2],
            3: AREA_REGION[3],
            4: AREA_REGION[4],
            5: AREA_REGION[5],
            6: AREA_REGION[6],
            7: AREA_REGION[7],
        },
    },
    {
        'name': '関東',
        'region': {
            8: AREA_REGION[8],
            9: AREA_REGION[9],
            10: AREA_REGION[10],
            11: AREA_REGION[11],
            12: AREA_REGION[12],
            13: AREA_REGION[13],
            14: AREA_REGION[14],
        },
    },
    {
        'name': '中部',
        'region': {
            15: AREA_REGION[15],
            16: AREA_REGION[16],
            17: AREA_REGION[17],
            18: AREA_REGION[18],
            19: AREA_REGION[19],
            20: AREA_REGION[20],
            21: AREA_REGION[21],
            22: AREA_REGION[22],
            23: AREA_REGION[23],
        },
    },
    {
        'name': '近畿',
        'region': {
            24: AREA_REGION[24],
            25: AREA_REGION[25],
            26: AREA_REGION[26],
            27: AREA_REGION[27],
            28: AREA_REGION[28],
            29: AREA_REGION[29],
            30: AREA_REGION[30],
        },
    },
    {
        'name': '中国',
        'region': {
            31: AREA_REGION[31],
            32: AREA_REGION[32],
            33: AREA_REGION[33],
            34: AREA_REGION[34],
            35: AREA_REGION[35],
        },
    },
    {
        'name': '四国',
        'region': {
            36: AREA_REGION[36],
            37: AREA_REGION[37],
            38: AREA_REGION[38],
            39: AREA_REGION[39],
        },
    },
    {
        'name': '九州・沖縄',
        'region': {
            40: AREA_REGION[40],
            41: AREA_REGION[41],
            42: AREA_REGION[42],
            43: AREA_REGION[43],
            44: AREA_REGION[44],
            45: AREA_REGION[45],
            46: AREA_REGION[46],
            47: AREA_REGION[47],
            48: AREA_REGION[48],
        },
    },
];
