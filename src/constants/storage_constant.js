import sha1 from 'sha1';
import { ENV } from './env';

const getStorageKey = key => ['staging', 'production'].indexOf(String(ENV)) !== -1 ? sha1(key) : key;
/**
 * Storage key
 */

export const TOKEN = getStorageKey('_token');
export const TOKEN_REGISTER = getStorageKey('_token_register');
export const USER_INFO = getStorageKey('_user_info');
export const SEARCH_INFO = getStorageKey('_search_info');
export const EMAIL = getStorageKey('_email');
export const HIDE_NEWS = getStorageKey('hide_news');
export const USER_CLOSE_NEWS = getStorageKey('user_close_news');
export const USER_SECURITY = getStorageKey('_info');
export const DISPLAY_LIST_GRID_USER = getStorageKey('_display_list_grid_user');
export const DISPLAY_DIALOG_SEARCH_USER = getStorageKey('_display_dialog_search_user_');
export const NAME_SEARCH = getStorageKey('_name_search_');
export const CHAT_MENU_BUTTON = getStorageKey('chat_menu_button');
export const GENDER = getStorageKey('gender');
export const BIRTHDAY = getStorageKey('birthday');
export const TUTORIAL = getStorageKey('tutorial');
export const REGISTER_TIME = getStorageKey('register_time');


