export const SocketEvents = {
    CONNECT: "connect",
    AUTHENTICATION: "authen",
    NEW_MESSAGE: "newMessage",
    TYPING: 'typing',
    STOP_TYPING: 'stopTyping',
};
