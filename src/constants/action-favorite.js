import keyMirror from 'keymirror';

export default keyMirror({
    OPEN_FAVORITE_LOADING: null,
    CLOSE_FAVORITE_LOADING: null,
});
