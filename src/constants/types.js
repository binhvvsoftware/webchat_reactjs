import keyMirror from 'keymirror';

export const ChatFooterProperButtonTypes = keyMirror({
    SEND: null,
    CALL: null,
});

export const PrimaryUtilityTypes = keyMirror({
    CHAT: null,
    VOICE: null,
    VIDEO: null,
});

export const MessageTypes = keyMirror({
    STK: null,
    PP: null,
    AUTH: null,
    MDS: null,
    GIFT: null,
    FILE: null,
    CALLREQ: null,
    EVOICE: null,
    EVIDEO: null,
    PRC: null,
    WINK: null,
    RT: null,
    LCT: null,
    PHOTO: null,
    SVOICE: null,
    SVIDEO: null,
    TEMPLATE: null,
    SEND_GIFT: null,
    SEND_GIFT_STATUS: null,
    POP_UP_REVIEW: null,
});

export const MessageFileTypes = {
    PHOTO: 'p',
    VIDEO: 'v',
    AUDIO: 'a',
};

export const MessageFileStatusTypes = {
    DENIED: 0,
    APPROVED: 1,
    PENDING: 2,
};
