import keyMirror from 'keymirror';

export default keyMirror({
    OPEN_DOWNLOAD: null,
    CLOSE_DOWNLOAD: null,
});
