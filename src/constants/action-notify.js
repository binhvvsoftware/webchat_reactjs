import keyMirror from 'keymirror';

export default keyMirror({
    UPDATE_NOTIFY: null,
});
