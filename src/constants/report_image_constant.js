export const REPORT_IMAGE = [
    {
        value: -1,
        label: 'この写真を報告しない',
    },
    {
        value: 1,
        label: '暴力的なコンテンツ',
    },
    {
        value: 2,
        label: '不愉快なコンテンツ',
    },
    {
        value: 3,
        label: '危険なコンテンツ',
    },
    {
        value: 4,
        label: '著作権保護コンテンツ',
    },
];
