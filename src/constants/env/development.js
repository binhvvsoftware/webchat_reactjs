// Development
export const ENV = 'development';

export const WEB = 'https://stg.api.charincharin.net';
export const MEDIA = 'https://stg.stf.charincharin.net';
export const SOCKETIO = 'https://stg.chat.socketio.charincharin.net';
export const WEBVIEW = 'https://stg.webview.kyuun-kyuun.com';
export const APP_CALL_URL = 'https://4mk4.adj.st/?adjust_t=ps36vf3';
export const APP_STORE_ID = '1452741492';
