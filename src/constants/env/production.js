// Production
export const ENV = 'production';

export const WEB = 'https://stg.api.charincharin.net';
export const MEDIA = 'https://stg.stf.charincharin.net';
export const SOCKETIO = 'https://stg.chat.socketio.charincharin.net';
export const WEBVIEW = 'https://stg.webview.kyuun-kyuun.com';
export const APP_CALL_URL = 'https://7v3p.adj.st/?adjust_t=792wh3i&act=';
export const APP_STORE_ID = '1452741492';
