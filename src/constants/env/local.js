// Development
export const ENV = 'development';

export const WEB = 'https://api-ky.ntq.solutions';
export const MEDIA = 'https://media-ky.ntq.solutions';
// export const SOCKETIO = 'http://socket-ky.ntq.solutions';
export const SOCKETIO = 'http://192.168.2.153:9111';
export const WEBVIEW = 'https://stg.webview.kyuun-kyuun.com';
export const APP_CALL_URL = 'https://4mk4.adj.st/?adjust_t=ps36vf3';
export const APP_STORE_ID = '1452741492';
