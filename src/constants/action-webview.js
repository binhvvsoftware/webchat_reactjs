import keyMirror from 'keymirror';

export default keyMirror({
    OPEN_WEB_VIEW: null,
    CLOSE_WEB_VIEW: null,
});
