import { WEBVIEW } from './env';

export const ABOUT_PRICE_LINK = `${WEBVIEW}/point/explain/?sid=`;

export const BUY_POINT = `${WEBVIEW}/point/?app_use=webchat&enter=1&sid=`;
export const FREE_POINT = `${WEBVIEW}/bonus/?enter=1&sid=`;
export const HOW_TO_USE_MALE = `${WEBVIEW}/tutorial/?sid=`;
export const HOW_TO_USE_FEMALE = `${WEBVIEW}/tutorial/?sid=`;
export const SUPPORT_PAGE = `${WEBVIEW}/faq/?sid=`;
export const TERM_OF_USER = `${WEBVIEW}/tutorial/hyouki/?sid=`;
export const ABOUT_PAYMENT = `${WEBVIEW}/point/explain/?sid=`;
export const VERIFY_FEMALE = `${WEBVIEW}/auth/?sid=`;
export const VERIFY_AGE = `${WEBVIEW}/auth/?sid=`;
export const LOGIN_FROM_OTHER_SYSTEM = 'http://www2.kyuun-app.com/';
export const WARNING_VIDEO_CALL = `${WEBVIEW}/tutorial/inhibit/?sid=`;
