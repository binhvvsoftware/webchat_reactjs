import * as env from './env';

export const ROOT_URL = env.WEB;
export const MEDIA_URL = env.MEDIA;


// export const LOAD_IMG = 'load_img_for_web';
export const LOAD_IMG = 'load_img';
export const LOAD_FILE = 'load_file';
export const GET_VIDEO_URL = 'get_video_url';

export const APP_NAME = 'kyuun';
export const APP_VERSION = '1.0';
export const APP_TYPE = 1;
export const DEVICE_TYPE = 0;

export const GET_EXT_PAGE = 'get_ext_page';
export const CHECK_TOKEN = 'check_token';

export const LOGIN = 'login_version_2';
export const FORGOT_PASSWORD = 'fgt_pwd_version_2';
export const RESET_PASSWORD = 'chg_pwd_fgt_version_2';

export const CHANGE_PASSWORD = 'chg_pwd';
export const CHANGE_EMAIL = 'change_email';
export const REGISTER_EMAIL_PASSWORD = 'change_email';

export const GET_ALL_SYSTEM_ACC = 'get_all_system_acc';
export const LIST_NEWS_CLIENT = 'list_news_client';
export const GET_NOTICE = 'lst_noti';
export const GET_ATT_NUM = 'get_att_num';
export const LIST_CONVERSATION = 'list_conversation_version_3';
export const DELETE_MESSAGE = 'del_conversations';

export const MEET_PEOPLE = 'meet_people';
export const SEARCH_BY_NAME = 'search_by_name';

export const REGISTER = 'reg';
export const UPDATE_USER_INFO = 'upd_user_inf';
export const UPDATE_APPEAL_COMMENT = 'update_appeal_comment';
export const GET_USER_INFO = 'get_user_inf_version_2';
export const GET_FB_AVATAR = 'get_fb_avatar';
export const SETTING_NOTICE = 'add_onl_alt';
export const GET_SETTING_NOTICE = 'get_onl_alt';
export const UPDATE_MEMO = 'update_memo';

export const GET_ALL_GIFT = 'get_all_gift';
export const SEND_GIFT = 'send_gift';

export const LST_SENT_IMG_WITH_USER = 'lst_sent_img_with_user';

export const PROFILE_BLOCK = 'add_blk';
export const PROFILE_REPORT = 'rpt';
export const LIST_ONLINE_ALERT = 'list_online_alert';
export const LIST_BANNER_CLIENT = 'list_banner_client';

export const RMV_FAV = 'rmv_fav';
export const ADD_FAV = 'add_fav';
export const DEL_BUZZ = 'del_buzz';
export const DEL_CMT = 'del_cmt';
export const LIKE_BUZZ = 'like_buzz';
export const GET_BUZZ_DETAIL = 'get_buzz_detail';
export const DEL_SUB_CMT = 'delete_sub_comment';

export const LIST_ACTION_BUY_POINT = 'lst_action_point_pck_version_2';

export const SAVE_IMAGE = 'save_img_version_2';
export const GET_POINT_SAVE_IMAGE = 'get_connection_point_action';

export const CLOSE_TUTORIAL = 'log_user_accessed_page';
export const GET_RANKING_LIST = 'get_ranking_list';

export const GET_WARNING_POPUP = 'get_warning_popups';
export const UPDATE_WARNING_POP_UP = 'update_warning_popup_status';

export const GET_BANNED_WORD_V2 = 'get_banned_word_v2';
export const UPDATE_PARTICIPATION_RANKING = 'update_participation_ranking';

export const LIST_TEMPLATE = 'list_template';
export const UPDATE_TEMPLATE = 'update_template';
export const ADD_TEMPLATE = 'add_template';
export const DELETE_TEMPLATE = 'delete_template';
export const UPDATE_DEFAULT_TEMPLATE = 'update_default_template';

export const LIST_DEFAULT_STICKER_CATEGORY_FOR_WEB = 'list_default_sticker_category_for_web';

export const GET_CHAT_HISTORY = 'get_chat_history_version_2';
export const GET_CONNECTION_POINT = 'get_connection_point_action';
export const CHECK_IMAGE_EXISTED = 'check_image_existed';
export const CHECK_VIDEO_EXISTED = 'check_video_existed';
export const CHECK_FILE_UNLOCK = 'chk_unlck_version_3';
export const UNLOCK_FILE = 'unlck_version_3';
export const UPLOAD_FILE = 'upl_file_for_web';

export const GET_BUZZ = 'get_buzz';
export const GET_NOTI_SET = 'get_noti_set';

export const LIST_BACKSTAGE = 'lst_bckstg';
export const REMOVE_BACKSTAGE = 'rmv_bckstg';
export const UNLOCK_BACKSTAGE = 'unlck_version_3';

export const DEACTIVATE_ACCOUNT = 'de_act';

export const STATIC_PAGE = 'static_page';

export const UPLOAD_BANNER = 'upl_news_banner';

// Chat
export const UPLOAD_IMAGE = 'upl_img_for_web';
export const LOAD_THUMBNAIL_VIDEO = 'load_img_with_size';
export const DOWNLOAD_IMAGE = 'get_file';

export const IMAGE_CHAT = 0;

export const IMAGE_PUBLIC = 1;

export const IMAGE_BACKSTAGE = 2;

export const IMAGE_AVATAR = 3;

export const IMAGE_THUMBNAIL_VIDEO = 4;

export const PRO_TYPE_APPLE = 0;
export const PRO_TYPE_GOOGLE = 1;
export const PRO_TYPE_AMAZON = 2;

export const FILE_UNLOCK_TYPE = {
    BACKSTAGE: 1,
    VIEW_IMAGE: 2,
    WATCH_VIDEO: 3,
    LISTEN_AUDIO: 3,
}
