export const NOTICE = [
    {
        value: 0,
        label: 'いつも',
    },
    {
        value: 10,
        label: '1日10回まで',
    },
    {
        value: 5,
        label: '1日5回まで',
    },
    {
        value: 1,
        label: '1日1回まで',
    },
];

export const DATA_NOTICE = {
    0: 'いつも',
    10: '1日10回まで',
    5: '1日5回まで',
    1: '1日1回まで',
};

export const REPORT = [
    {
        value: -1,
        label: 'このユーザーを報告しない',
    },
    {
        value: 1,
        label: '暴力的なコンテンツ',
    },
    {
        value: 2,
        label: '不愉快なコンテンツ',
    },
    {
        value: 3,
        label: '著作権保護コンテンツ',
    },
    {
        value: 4,
        label: '著作権保護コンテンツ',
    },
];
