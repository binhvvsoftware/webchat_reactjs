import storage from './storage';
import { APP_CALL_URL, APP_STORE_ID } from '../constants/env';
import { USER_SECURITY } from '../constants/storage_constant';

export const decryption = (code) => {
    try {
        const value = atob(atob(code));
        try {
            return JSON.parse(value);
        } catch (e) {
            return value;
        }
    } catch (e) {
        throw e;
    }
};

export const getUserInfo = (friend = {}) => {
    const security = storage.get(USER_SECURITY);
    try {
        const password = security.substring(0, 40);
        const me = storage.getUserInfo();
        let email = security.substring(40);
        email = atob(email);

        return {
            email,
            password,
            ava_id_friend: friend.ava_id || '',
            user_id_friend: friend.user_id || '',
            user_id_self: me.user_id,
        };
    } catch (e) {
        return {
            email: '',
            password: '',
            username_friend: '',
        };
    }
};

export const encryption = (value) => btoa(btoa(typeof value === 'string' ? value : JSON.stringify(value)));

export const buildLinkConnect = (friend = {}) => {
    const params = {
        act: 'connect',
    };

    return `${APP_CALL_URL}&username_friend=${(friend.user_name || '')}&act=${encryption(Object.assign(getUserInfo(friend), params))}`;
};

export const buildLinkVoiceCall = (friend) => {
    const params = {
        act: 'voice',
    };

    return `${APP_CALL_URL}&username_friend=${(friend.user_name || '')}&act=${encryption(Object.assign(getUserInfo(friend), params))}`;
};

export const buildLinkVideoCall = (friend) => {
    const params = {
        act: 'video',
    };

    return `${APP_CALL_URL}&username_friend=${(friend.user_name || '')}&act=${encryption(Object.assign(getUserInfo(friend), params))}`;
};

export const buildLinkAppStore = () => `https://itunes.apple.com/app/id${APP_STORE_ID}`;
