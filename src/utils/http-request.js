import axios from 'axios';
import moment from 'moment';
import { ENV } from '../constants/env';
import history from './history';
import {
    getMessageFromCode,
    SUCCESS,
    INVALID_TOKEN,
    SERVER_MAINTAIN,
} from '../constants/response_status_constant';
import {
    ROOT_URL,
    MEDIA_URL,
    LOGIN,
    APP_NAME,
    APP_TYPE,
    APP_VERSION,
    REGISTER,
    FORGOT_PASSWORD,
    STATIC_PAGE,
    UPLOAD_IMAGE,
    DOWNLOAD_IMAGE,
    UPLOAD_BANNER,
    GET_FB_AVATAR,
    DEVICE_TYPE,
    LOAD_IMG,
    GET_BANNED_WORD_V2,
    CHECK_TOKEN,
} from '../constants/endpoint_constant';
import { USER_SECURITY } from '../constants/storage_constant';
import { checkEmail, getParameterByName } from './helpers';
import storage from './storage';


/* global localStorage */
const http = axios.create({
    method: 'post', // default
    baseURL: ROOT_URL,
    json: true,
});

const getApiName = (config) => {
    let apiName;
    if (config.data) {
        try {
            const data = typeof config.data === 'object' ? config.data : JSON.parse(config.data);
            apiName = data.api;
        } catch (e) {
            //
        }
    }

    return apiName || getParameterByName('api', config.url);

};

const reAuthentication = () => {
    removeMessageMaintain();
    let key;
    let params;
    try {
        const security = storage.get(USER_SECURITY);
        const token = storage.getToken();
        key = `${USER_SECURITY}_${security}_${token || ''}`;
        if (window[key]) {
            return window[key];
        }

        const pwd = security.substring(0, 40);
        const email = atob(security.substring(40));
        if (checkEmail(email) || /^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/.test(email)) {
            params = { email, pwd };
        } else {
            params = { fb_id: email };
        }
    } catch (e) {
        storage.destroy();
        history.push('/login');
        return false;
    }



    const request = Object.assign({
        device_id: '',
        os_version: '',
        device_type: DEVICE_TYPE,
        use_fcm: true,
        gps_adid: '',
        device_name: '',
        adid: '',
        application: APP_NAME,
        applicaton_type: APP_TYPE,
        login_time: moment().utc().format('YYYYMMDDHHmmss'),
        notify_token: '',
        api: LOGIN,
        application_version: APP_VERSION,
    }, params);

    document.body.classList.add('loading__full');
    window[key] = http.post(ROOT_URL, request).then((response) => {
        if (response.code === SUCCESS) {
            window.token = response.data.token;
            storage.setToken(response.data.token);
            storage.setUserInfo(response.data);
            history.push('/login', { time: moment().format('X') });
        } else {
            storage.destroy();
            history.push('/login');
        }
    }).catch(() => {
        storage.destroy();
        history.push('/login');
    }).then(() => {
        document.body.classList.remove('loading__full');
        window[key] = null;
    });

    return window[key];
};

const removeMessageMaintain = () => {
    const elem = document.getElementById('elm__maintain');

    if (elem) {
        elem.parentNode.removeChild(elem);
    }
}

const executeResponseError = (response) => {
    const element = document.createElement('div');
    const maintainReload = 30000;
    switch (response.data.code) {
        case INVALID_TOKEN:
            reAuthentication();
            break;
        case SERVER_MAINTAIN:
            if (window.maintainReloadTime || !document.getElementById('elm__maintain')) {
                element.id = 'elm__maintain';
                document.body.appendChild(element);
                window.maintainReloadTime = window.maintainReloadTime ||  maintainReload / 1000;
                if (!window.maintainInterval) {
                    window.maintainInterval = setInterval(() => {
                        window.maintainReloadTime -= 1;
                        const timeElement = document.getElementById('elm__maintain-timeout');
                        if (window.maintainReloadTime >= 0 && timeElement) {
                            timeElement.innerText = String(window.maintainReloadTime);
                        } else {
                            clearInterval(window.maintainInterval);
                            window.location.reload();
                        }
                    }, 1000)
                }

                document.getElementById('elm__maintain').innerHTML = `<div>サーバーがメインテナンス中です<br><span id="elm__maintain-timeout">${window.maintainReloadTime}</span>分後、再接続します</div>`;
                document.body.classList.add('server_maintain');
            }
            break;
        default:
            break;
    }

    if (GET_FB_AVATAR === getApiName(response.config)) {
        return response.data;
    }

    return Promise.reject(response.data);
};

http.interceptors.request.use((config) => {
    const newConfig = config;
    // Do something before request is sent
    const notToken = [
        REGISTER,
        LOGIN,
        FORGOT_PASSWORD,
        STATIC_PAGE,
        UPLOAD_IMAGE,
        DOWNLOAD_IMAGE,
        UPLOAD_BANNER,
        GET_BANNED_WORD_V2,
    ];
    const apiName = getApiName(config);

    if (notToken.indexOf(apiName) === -1) {
        const token = window.token || storage.getToken();
        if (token && token !== 'undefined' && token !== 'null' && !getParameterByName('token', config.url) && (!config.data || !config.data.token)) {
            newConfig.data = newConfig.data || {};
            newConfig.data.token = String(token);
        }
    }

    return newConfig;
}, (error) => Promise.reject(error));

http.interceptors.response.use((response) => {
    removeMessageMaintain();
    const requestMedia = response.config.url.indexOf(`${MEDIA_URL}/api=${LOAD_IMG}`) === 0;
    if (requestMedia) {
        return response.data;
    }

    if (String(ENV) !== 'production') {
        /* eslint-disable */
        const data = { ...response.data };
        data._api = getApiName(response.config) || response.config.url.replace(response.config.baseURL, '');
        data._config = response.config;
        try {
            data._parameters = JSON.parse(response.config.data);
        } catch (e) {
            //
        }
        data._time = new Date().toLocaleString();
        if (data._api !== CHECK_TOKEN) {
            console.info(data);
        }
        /* eslint-enable */
    }

    if (typeof response.data === 'object') {
        response.data.message = getMessageFromCode(response.data.code);

        if (response.data.code === SUCCESS) {
            return Promise.resolve(response.data);
        }

        return executeResponseError(response);
    }

    if (GET_FB_AVATAR === getApiName(response.config)) {
        return {
            data: response.data || '',
        };
    }

    if (String(ENV) !== 'production') {
        let params;
        try {
            params = JSON.parse(response.config.data);
        } catch (e) {
            //
        }

        console.error({ url: response.config.url, params, response: response.data });// eslint-disable-line
    }

    if (requestMedia) {
        return Promise.resolve(response.data);
    }

    return Promise.reject(response.data);
}, (errors) => {
    const headerStatus = (errors.response && errors.response.status) ? errors.response.status : 503;
    if (String(ENV) !== 'production') {
        console.error(errors);// eslint-disable-line
    }

    return Promise.reject({ message: 'Server errors!', headerStatus }); // eslint-disable-line
});

export default http;
