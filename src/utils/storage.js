import isObject from 'lodash/isObject';
import User from '../entities/User';
import * as storageConstant from '../constants/storage_constant';

export const keys = storageConstant;

/* eslint-disable no-underscore-dangle */

/**
 * Get storage
 *
 * @param {String} key
 * @returns {*}
 */
const _get = (key) => {
    const value = localStorage.getItem(key);

    try {
        return JSON.parse(value);
    } catch (e) {
        return value;
    }
};
/**
 * Set storage
 * @param {String} key
 * @param {*} value
 */
const _set = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};

/**
 * Remove storage
 *
 * @param {String} key
 */
const _remove = (key) => {
    localStorage.removeItem(key);
};


/**
 * Remove storage
 *
 * @param {Array} list
 */
const _removeMultiple = (list) => {
    list.forEach(key => {
        _remove(key);
    });
};


export default {
    set(key, value) {
        _set(key, value);
    },

    get(key) {
        return _get(key);
    },

    /**
     * Get storage
     *
     * @returns {User}
     */
    getUserInfo() {
        let user = _get(keys.USER_INFO);

        if (!isObject(user)) {
            user = {};
        }

        return new User(user);
    },

    /**
     * Set user to storage
     *
     * @returns {User}
     */
    setUserInfo(user) {
        _set(keys.USER_INFO, user);
    },

    /**
     * Get token
     * @returns {String}
     */
    getToken() {
        return _get(keys.TOKEN) || '';
    },

    /**
     * Set token to storage
     * @param {*} value
     */
    setToken(value) {
        window.token = value;
        _set(keys.TOKEN, value);
    },

    remove(key) {
        _remove(key);
    },

    /**
     * Remove all
     */
    destroy() {
        console.info('============= Clear all storage ============='); // eslint-disable-line
        _removeMultiple([
            keys.TOKEN,
            keys.TOKEN_REGISTER,
            keys.USER_INFO,
            keys.SEARCH_INFO,
            keys.EMAIL,
            keys.USER_CLOSE_NEWS,
            keys.USER_SECURITY,
            keys.DISPLAY_LIST_GRID_USER,
            keys.DISPLAY_DIALOG_SEARCH_USER,
            keys.NAME_SEARCH,
            keys.CHAT_MENU_BUTTON,
            keys.GENDER,
            keys.BIRTHDAY,
            keys.TUTORIAL,
            keys.REGISTER_TIME,
            // keys.HIDE_NEWS
        ]);

        // localStorage.clear();
    },
};
