import isMobile from 'ismobilejs';

if (isMobile.phone) {
    document.body.classList.add('is_phone');
}
if (isMobile.any) {
    document.body.classList.add('is_mobile');
}

if (isMobile.tablet) {
    document.body.classList.add('is_tablet');
}

if (!isMobile.any) {
    document.body.classList.add('is_desktop');
}

if (/^((?!chrome|android).)*safari/i.test(window.navigator.userAgent)) {
    document.body.classList.add('is_safari');
}
