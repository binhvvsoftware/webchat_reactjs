/**
 * @property {Boolean} isSupported
 */
export default class Visibility {
    constructor() {
        let hidden;
        let visibilityChange;
        if (typeof document.hidden !== 'undefined') { // Opera 12.10 and Firefox 18 and later support
            hidden = 'hidden';
            visibilityChange = 'visibilitychange';
        } else if (typeof document.msHidden !== 'undefined') {
            hidden = 'msHidden';
            visibilityChange = 'msvisibilitychange';
        } else if (typeof document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
            visibilityChange = 'webkitvisibilitychange';
        }

        // Warn if the browser doesn't support addEventListener or the Page Visibility API
        this.isSupported = typeof document.addEventListener !== 'undefined' && hidden !== undefined;
        this.hidden = hidden;
        this.visibilityChange = visibilityChange;
    }

    /**
     * @param {Function} callback
     */
    onChange = (callback) => {
        if (this.isSupported) {
            document.addEventListener(this.visibilityChange, () => {
                callback(!document[this.hidden]);
            }, false);
        }
    };

    /**
     * @return {Boolean}
     */
    isHidden = () => document[this.hidden];
}


