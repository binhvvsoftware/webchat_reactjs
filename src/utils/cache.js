const USER_CACHE_KEY = '__cache_avatar__';

export const cacheUser = (data) => {
    window[USER_CACHE_KEY] = window[USER_CACHE_KEY] || {};

    if (Array.isArray(data)) {
        data.forEach(user => {
            if (user) {
                window[USER_CACHE_KEY][user.user_id] = user;
            }
        });
    }
};

export const getUserFromCache = (userId) => {
    window[USER_CACHE_KEY] = window[USER_CACHE_KEY] || {};
    return window[USER_CACHE_KEY][userId];
};
