import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const locationHistory = {
    current: history.location,
    previous: {
        pathname: '',
    },
};

// Listen for changes to the current location.
history.listen((location) => {
    locationHistory.previous = locationHistory.current;
    locationHistory.current = { ...location };

});

export function getLocationHistory() {
    return locationHistory;
}

history.checkAndBack = () => {
    const location = getLocationHistory();
    if (!location.previous.pathname.length || location.previous.pathname.indexOf('/app-notification/') !== -1) {
        return history.push('/home');
    }
    
    return history.push(location.previous.pathname);
}

export default history;
