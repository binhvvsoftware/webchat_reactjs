import io from 'socket.io-client';
import * as ENV from '../constants/env';

export const connectSocket = () => {
    if (window.socket && !window.socket.disconnected) {
        return window.socket;
    }
    const secure = /^https/.test(ENV.SOCKETIO);
    window.socket = io.connect(ENV.SOCKETIO, { secure });

    return window.socket;
};

export const updateAuthenticate = (userId) => {
    connectSocket();
    window.socket.isAuthenticate = true;
    window.socket.userId = userId;
}

export const disconnectSocket = () => {
    const socket = connectSocket();
    window.socket = undefined;
    socket.disconnect();
};
