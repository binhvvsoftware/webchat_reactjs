/* global isNaN */
/* eslint-disable no-restricted-globals */
import numeral from 'numeral';
import moment from 'moment/moment';
import platform from './platform';
import storage from './storage';

import {
    WEEK_TEXT,
    DAY_TEXT,
    HOUR_TEXT,
    MINUTE_TEXT,
    NOW_TEXT,
    NOW_TEXT_TIMELINE,
    YEAR_TEXT,
    YEAR_TEXT_TIMELINE,
    MONTH_TEXT,
    MONTH_TEXT_TIMELINE,
    WEEK_TEXT_TIMELINE,
    DAY_TEXT_TIMELINE,
    HOUR_TEXT_TIMELINE,
    MINUTE_TEXT_TIMELINE,
    SECOND_TEXT,
    SECOND_TEXT_TIMELINE,
} from '../constants/messages';

/**
 * @param {Array} data
 * @param {Array}  array
 * @param {string}  primaryKey
 *
 * @return {Array}
 */
export const addArrayAndRemoveDuplicate = (data, array, primaryKey) => {
    let copyData = data;
    const keys = {};
    copyData.forEach((item, index) => {
        keys[item[primaryKey]] = index;
    });

    array.forEach((item) => {
        const tmp = item;
        if (keys[tmp[primaryKey]] !== undefined) {
            tmp.key = `${copyData[keys[tmp[primaryKey]]].key}_`;
            copyData[keys[tmp[primaryKey]]] = tmp;
        } else {
            copyData = copyData.concat(tmp);
            tmp.key = item[primaryKey];
        }
    });

    return copyData;
};

export const convertTimeUTCToLocal = (time) => {
    const yearReg = '[1-2][0-9][0-9][0-9]';           // < Allows a number between 1000 and 2999
    const monthReg = '(0[1-9]|1[0-2])';               // < Allows a number between 00 and 12
    const dayReg = '(0[1-9]|1[0-9]|2[0-9]|3[0-1])';   // < Allows a number between 00 and 31
    const hourReg = '([0-1][0-9]|2[0-3])';            // < Allows a number between 00 and 24
    const minReg = '([0-5][0-9])';                    // < Allows a number between 00 and 59
    const secondReg = '([0-5][0-9])';                    // < Allows a number between 00 and 59
    const reg = new RegExp('^' + yearReg + monthReg + dayReg + hourReg + minReg + secondReg, 'g'); // eslint-disable-line
    let date;

    if (reg.test(String(time))) {
        date = moment(String(time).substr(0, 14), 'YYYYMMDDHHmmss');
    } else {
        date = moment.unix(parseInt(String(time).substr(0, 10), 10)).utc();
    }
    const timezone = new Date().getTimezoneOffset();

    return date.add(-timezone, 'minutes');
};

export const checkIsFemale = genderCode => parseInt(genderCode, 10) === 1;

export const checkEmail = (email) => {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    return regex.test(String(email).toLowerCase());
};

export const addTargetLink = (str, target = '_blank') => {
    const content = String(str).replace(/<a (.*)>/gim, `<a target="${target}" $1>`);

    return content.replace('%%token%%', storage.getToken());
};

/**
 * Convert time UTC to time ago
 *
 * @param {Number} time
 * @param {Boolean} [isTimeLine=false]
 *
 * @returns {string}
 */
export const timeAgo = (time, isTimeLine = false) => {
    try {
        const currentTimeUTC = moment().utc().format('X');
        const convertTimeToUTC = convertTimeUTCToLocal(time).format('X');

        const agoSeconds = currentTimeUTC - convertTimeToUTC;

        if (!isNaN(agoSeconds) && agoSeconds > 0) {
            const minuteDefine = 60;
            const hourDefine = 60 * minuteDefine;
            const dayDefine = 24 * hourDefine;
            const weekDefine = 7 * dayDefine;
            const monthDefine = 30 * dayDefine;
            const yearDefine = 365 * dayDefine;

            const year = Math.floor(agoSeconds / yearDefine);
            if (year > 0) {
                return `${year}${(!isTimeLine ? YEAR_TEXT : YEAR_TEXT_TIMELINE)}`;
            }

            const month = Math.floor(agoSeconds / monthDefine);
            if (month > 0) {
                return `${month}${(!isTimeLine ? MONTH_TEXT : MONTH_TEXT_TIMELINE)}`;
            }

            const week = Math.floor(agoSeconds / weekDefine);
            if (week > 0) {
                return `${week}${(!isTimeLine ? WEEK_TEXT : WEEK_TEXT_TIMELINE)}`;
            }

            const day = Math.floor(agoSeconds / dayDefine);
            if (day > 0) {
                return `${day}${(!isTimeLine ? DAY_TEXT : DAY_TEXT_TIMELINE)}`;
            }

            const hour = Math.floor(agoSeconds / hourDefine);
            if (hour > 0) {
                return `${hour}${(!isTimeLine ? HOUR_TEXT : HOUR_TEXT_TIMELINE)}`;
            }

            const minute = Math.floor(agoSeconds / minuteDefine);
            if (minute > 30) {
                return `1${(!isTimeLine ? HOUR_TEXT : HOUR_TEXT_TIMELINE)}`;
            }
            if (minute > 0) {
                return `${minute}${(!isTimeLine ? MINUTE_TEXT : MINUTE_TEXT_TIMELINE)}`;
            }

            return (!isTimeLine ? SECOND_TEXT : SECOND_TEXT_TIMELINE);
        }

        return !isTimeLine ? NOW_TEXT : NOW_TEXT_TIMELINE;
    } catch (e) {
        console.error(e); // eslint-disable-line
        return '';
    }
};

export const getDeviceName = () => `${platform.os} - ${platform.name}`;
export const getBrowserVersion = () => platform.version;

export const getParameterByName = (name, url) => {
    let urlString = url;
    if (!urlString) {
        urlString = window.location.href;
    }
    const parameterName = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + parameterName + '(=([^&#]*)|&|#|$)'); // eslint-disable-line
    const results = regex.exec(urlString);

    if (!results) {
        return '';
    }

    if (!results[2]) {
        return '';
    }

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

export const convertToDateTime = (time, format = 'MM月DD日 HH時mm') => {
    if (time) {
        return convertTimeUTCToLocal(time).format(format);
    }
    return '';
};

export const convertToJapaneseDatetime = (sendTime) => {
    if (sendTime) {
        const dayOfWeeks = {
            '0': '日',
            '1': '月',
            '2': '火',
            '3': '水',
            '4': '木',
            '5': '金',
            '6': '土',
        };

        return `${sendTime.format('MM月DD日')} (${dayOfWeeks[sendTime.format('e')]})`;
    }
    return '';
};

export const convertDurationTimeToHHMMSS = (durationTime) => {
    let secNum;
    let hours;
    let minutes;
    let seconds;
    if (durationTime) {
        secNum = parseInt(durationTime, 10);
        hours = Math.floor(secNum / 3600);
        minutes = Math.floor((secNum - (hours * 3600)) / 60);
        seconds = secNum - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = `0${hours}`;
        }
        if (minutes < 10) {
            minutes = `0${minutes}`;
        }
        if (seconds < 10) {
            seconds = `0${seconds}`;
        }
        if (hours === '00') {
            return `${minutes}:${seconds}`;
        }
        return `${hours}:${minutes}:${seconds}`;
    }
    return '';
};

export function numberFormat(number) {
    try {
        let val = number;
        if (typeof val !== 'number') {
            val = parseInt(number, 10);
        }

        return numeral(val).format('0,0');
    } catch (e) {
        return number;
    }
}


export const checkOrientationImage = (file, callback) => {
    const callbackOrientation = (srcOrientation) => {
        const reader = new FileReader();
        reader.onload = (e) => {
            let orientation = 0;

            // transform context before drawing image
            switch (srcOrientation) {
                case 2:
                    orientation = 1;
                    // ctx.transform(-1, 0, 0, 1, width, 0);
                    break;
                case 3:
                    orientation = 180;
                    // ctx.transform(-1, 0, 0, -1, width, height);
                    break;
                case 4:
                    // ctx.transform(1, 0, 0, -1, 0, height);
                    break;
                case 5:
                    // ctx.transform(0, 1, 1, 0, 0, 0);
                    break;
                case 6:
                    orientation = 90;
                    // ctx.transform(0, 1, -1, 0, height, 0);
                    break;
                case 7:
                    // ctx.transform(0, -1, -1, 0, height, width);
                    break;
                case 8:
                    orientation = 240;
                    // ctx.transform(0, -1, 1, 0, 0, width);
                    break;
                default:
                    break;
            }

            callback(e.target.result, orientation);
        };

        reader.readAsDataURL(file);
    };

    const reader = new FileReader();
    reader.onload = (e) => {
        const view = new DataView(e.target.result);
        if (view.getUint16(0, false) !== 0xFFD8) {
            return callbackOrientation(-2);
        }
        const length = view.byteLength;
        let offset = 2;
        while (offset < length) {
            const marker = view.getUint16(offset, false);
            offset += 2;
            if (marker === 0xFFE1) {
                if (view.getUint32(offset += 2, false) !== 0x45786966) { // eslint-disable-line
                    return callbackOrientation(-1);
                }

                const little = view.getUint16(offset += 6, false) === 0x4949;
                offset += view.getUint32(offset + 4, little);
                const tags = view.getUint16(offset, little);
                offset += 2;
                for (let i = 0; i < tags; i += 1)
                    if (view.getUint16(offset + (i * 12), little) === 0x0112)
                        return callbackOrientation(view.getUint16(offset + (i * 12) + 8, little));
            }
            else if ((marker & 0xFF00) !== 0xFF00) { // eslint-disable-line
                break;
            }
            else {
                offset += view.getUint16(offset, false);
            }
        }

        return callbackOrientation(-1);
    };
    reader.readAsArrayBuffer(file);
};

export function checkLoadMore(event, isModal = false) {
    if (!isModal) {
        const classList = Array.from(document.body.classList);

        if (classList.indexOf('modal_open') !== -1 || !!document.querySelector('.sidebar_open')) {
            return false;
        }
    }

    let element = event.target;
    if (element === document) {
        element = document.body;
    }

    return element.scrollHeight - element.scrollTop - 100 <= element.clientHeight;
}
