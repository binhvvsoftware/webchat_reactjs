import ActionTypes from '../constants/action-main';

export function updateFreePage(payload) {
    return { type: ActionTypes.UPDATE_FREE_PAGE, payload };
};

export function openChatConversation(payload) {
    return { type: ActionTypes.OPEN_CHAT_CONVERSATION, payload }
};

export function closeChatConversation(payload) {
    return { type: ActionTypes.CLOSE_CHAT_CONVERSATION, payload }
};

export function openSidebarMenu(payload) {
    return { type: ActionTypes.OPEN_SIDEBAR_MENU, payload }
};

export function closeSidebarMenu(payload) {
    return { type: ActionTypes.CLOSE_SIDEBAR_MENU, payload }
};

export function updateNotification(payload) {
    return { type: ActionTypes.UPDATE_NOTIFICATION, payload }
};

export function pushNotification(payload) {
    return { type: ActionTypes.PUSH_NOTIFICATION, payload }
};

export function updateListAccountSystem(payload) {
    return { type: ActionTypes.UPDATE_LIST_ACCOUNT_SYSTEM, payload }
};

export function openBuyPoint(payload) {
    return { type: ActionTypes.OPEN_BUYPOINT, payload }
};

export function closeBuyPoint(payload) {
    return { type: ActionTypes.CLOSE_BUYPOINT, payload }
};
