import {
    FORGET_PASWORD_REQUEST,
    FORGET_PASWORD_SUCCESS,
    FORGET_PASWORD_FAILURE,
    FORGET_PASWORD_KEY_CHANGE,
} from '../constants/action_constant';

export function forgetPasswordKeyChange(email) {
    return {
        type : FORGET_PASWORD_KEY_CHANGE,
        email,
    }
}

export function submitForgetPassword(email) {
    return {
        type : FORGET_PASWORD_REQUEST,
        email,
        loading: true,
    }
}

export function forgetPasswordSuccess(message){
    return {
        type : FORGET_PASWORD_SUCCESS,
        message,
    }
}

export function forgetPasswordFailure(message){
    return {
        type : FORGET_PASWORD_FAILURE,
        message,
    }
}
