/* Action typo constants, use keyMirror helper */
import ActionTypes from "../constants/action-favorite";

export function openFavoriteLoading(payload) {
    return { type: ActionTypes.OPEN_FAVORITE_LOADING, payload }
};

export function closeFavoriteLoading(payload) {
    return { type: ActionTypes.CLOSE_FAVORITE_LOADING, payload }
};
