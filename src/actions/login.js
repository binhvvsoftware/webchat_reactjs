import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
} from '../constants/action_constant';

export function submitLogin(email, password) {
    return {
        type : LOGIN_REQUEST,
        email,
        password,
    }
}

export function loginSuccess(data){
    return {
        type : LOGIN_SUCCESS,
        data,
    }
}

export function loginFailure(errors){
    return {
        type : LOGIN_FAILURE,
        errors,
    }
}