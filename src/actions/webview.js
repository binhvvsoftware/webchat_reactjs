/* Action typo constants, use keyMirror helper */
import ActionTypes from "../constants/action-webview";

export function openWebView(payload) {
    return { type: ActionTypes.OPEN_WEB_VIEW, payload }
};

export function closeWebView(payload) {
    return { type: ActionTypes.CLOSE_WEB_VIEW, payload }
};
