/* Action typo constants, use keyMirror helper */
import ActionTypes from "../../constants/action-types";

export function clickChatBackButton(payload) {
    return { type: ActionTypes.CLICK_CHAT_BACK_BUTTON, payload }
};
