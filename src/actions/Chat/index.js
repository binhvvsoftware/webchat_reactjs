/* Action typo constants, use keyMirror helper */
import ActionTypes from "../../constants/action-types";

export function initChat(payload) {
    return { type: ActionTypes.INIT_CHAT, payload }
};

export function updateShowHideButtonBack(payload) {
    return { type: ActionTypes.UPDATE_SHOW_HIDE_BUTTON_BACK, payload }
};

export function changeWhoIAm(payload) {
    return { type: ActionTypes.CHANGE_WHO_I_AM, payload }
};

export function changeWhoYouAre(payload) {
    return { type: ActionTypes.CHANGE_WHO_YOU_ARE, payload }
};

export function updateChatTyping(payload) {
    return { type: ActionTypes.UPDATE_CHAT_TYPING, payload }
};

export function updateAttentionNumbers(payload) {
    return { type: ActionTypes.UPDATE_ATTENTION_NUMBERS, payload }
};

export function clickChatBackButton(payload) {
    return { type: ActionTypes.CLICK_CHAT_BACK_BUTTON, payload }
};

export function clickChatHeaderMenuMask(payload) {
    return { type: ActionTypes.TOGGLE_CHAT_HEADER_SUB_MENU, payload }
};

export function clickChatHeaderMenuButton(payload) {
    return { type: ActionTypes.TOGGLE_CHAT_HEADER_SUB_MENU, payload }
};

export function closeChatHeaderMenuButton(payload) {
    return { type: ActionTypes.CLOSE_CHAT_HEADER_SUB_MENU, payload }
};

export function fireNewMessageRawData(payload) {
    return { type: ActionTypes.FIRE_NEW_MESSAGE_RAW_DATA, payload }
};

export function changeChatMessageInput(payload) {
    return { type: ActionTypes.CHANGE_CHAT_MESSAGE_INPUT, payload }
};

export function commitFileSending(payload) {
    return { type: ActionTypes.COMMIT_FILE_SENDING, payload }
};

export function commitRawMessageHandling(payload) {
    return { type: ActionTypes.COMMIT_RAW_MESSAGE_HANDLING, payload }
};

export function submitChatMessage(payload) {
    return { type: ActionTypes.SUBMIT_CHAT_MESSAGE, payload }
};

export function loadChatPool(payload) {
    return { type: ActionTypes.LOAD_CHAT_POOL, payload }
};

export function concatChatPool(payload) {
    return { type: ActionTypes.CONCAT_CHAT_POOL, payload }
};

export function updateChatPool(payload) {
    return { type: ActionTypes.UPDATE_CHAT_POOL, payload }
};

export function pushChatPool(payload) {
    return { type: ActionTypes.PUSH_CHAT_POOL, payload }
};

export function closeGiftPool(payload) {
    return { type: ActionTypes.CLOSE_GIFT_POOL, payload }
};

export function openGiftPool(payload) {
    return { type: ActionTypes.OPEN_GIFT_POOL, payload }
};

export function scrollToTopChatPool(payload) {
    return { type: ActionTypes.SCROLL_TO_TOP_CHAT_POOL, payload }
};

export function commitSocketIONewMessageListeners(payload) {
    return { type: ActionTypes.COMPLETE_SOCKET_IO_NEW_MESSAGE_LISTENERS, payload }
};

export function clearSocketIONewMessageListeners(payload) {
    return { type: ActionTypes.DISCONNECT_SOCKET, payload }
};

export function scrollToNewestChatPool(payload) {
    return { type: ActionTypes.SCROLL_TO_NEWEST_CHAT_POOL, payload }
};

export function closeImageBox(payload) {
    return { type: ActionTypes.CLOSE_CHAT_IMAGE_BOX, payload }
};

export function openImageBox(payload) {
    return { type: ActionTypes.OPEN_CHAT_IMAGE_BOX, payload }
};

export function openBuyingPointPromotion(payload) {
    return { type: ActionTypes.OPEN_BUYING_POINT_PROMOTION, payload }
};

export function closeBuyingPointPromotion(payload) {
    return { type: ActionTypes.CLOSE_BUYING_POINT_PROMOTION, payload }
};

export function openFooterPlusMenu(payload) {
    return { type: ActionTypes.OPEN_FOOTER_PLUS_MENU, payload }
};

export function closeFooterPlusMenu(payload) {
    return { type: ActionTypes.CLOSE_FOOTER_PLUS_MENU, payload }
};

export function openFooterKeyboard(payload) {
    return { type: ActionTypes.OPEN_FOOTER_KEYBOARD, payload }
};

export function openFooterEmoji(payload) {
    return { type: ActionTypes.OPEN_FOOTER_EMOJI, payload }
};

export function openGalleryHistory(payload) {
    return { type: ActionTypes.OPEN_GALLERY_HISTORY, payload }
};

export function closeGalleryHistory(payload) {
    return { type: ActionTypes.CLOSE_GALLERY_HISTORY, payload }
};

export function openTemplate(payload) {
    return { type: ActionTypes.OPEN_TEMPLATE, payload }
};

export function closeTemplate(payload) {
    return { type: ActionTypes.CLOSE_TEMPLATE, payload }
};

export function selectedTemplate(payload) {
    return { type: ActionTypes.SELECTED_TEMPLATE, payload }
};

export function selectedEmoji(payload) {
    return { type: ActionTypes.SELECTED_EMOJI, payload }
};

export function openSelectedSticker(payload) {
    return { type: ActionTypes.OPEN_SELECTED_STICKER, payload }
};

export function closeSelectedSticker(payload) {
    return { type: ActionTypes.CLOSE_SELECTED_STICKER, payload }
};

export function updateConversation(payload) {
    return { type: ActionTypes.UPDATE_CONVERSATION, payload }
};
