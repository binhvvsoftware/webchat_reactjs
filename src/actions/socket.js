import {
    SEND_MESSAGE,
} from '../constants/action_constant';

export function sendMessage(message) {
    return {
        type: SEND_MESSAGE,
        message,
    };
}
