/* Action typo constants, use keyMirror helper */
import ActionTypes from "../../constants/action-types";

export function changeWhoYouAre(payload) {
    return { type: ActionTypes.CHANGE_WHO_YOU_ARE, payload }
};
