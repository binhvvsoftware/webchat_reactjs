/* Action typo constants, use keyMirror helper */
import ActionTypes from "../constants/action-download";

export function openDownload(payload) {
    return { type: ActionTypes.OPEN_DOWNLOAD, payload }
};

export function closeDownload(payload) {
    return { type: ActionTypes.CLOSE_DOWNLOAD, payload }
};
