/* Action typo constants, use keyMirror helper */
import ActionTypes from "../../constants/action-types";

export function clickUtilityButton(payload) {
    return { type: ActionTypes.CLICK_UTILITY_BUTTON, payload }
};
