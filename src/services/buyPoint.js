import HttpRequest from '../utils/http-request';
import {
    LIST_ACTION_BUY_POINT,
    PRO_TYPE_APPLE,
} from '../constants/endpoint_constant';

export default {

    getlistBuyPoint(actionType) {
        const data = {
            api : LIST_ACTION_BUY_POINT,
            pro_type : PRO_TYPE_APPLE,
            action_type : actionType,
        }
        return HttpRequest({ data });
    },

};