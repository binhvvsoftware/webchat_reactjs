import HttpRequest from '../utils/http-request';

export default {

    getCallWaiting(data){
        return HttpRequest({ data });
    },

    getAppealComment(data){
        return HttpRequest({ data });
    },

    setCallWaiting(data) {
        return HttpRequest({ data });
    },
};
