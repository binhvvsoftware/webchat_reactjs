import isObject from 'lodash/isObject';
import HttpRequest from '../utils/http-request';
import {
    LIST_DEFAULT_STICKER_CATEGORY_FOR_WEB,
} from '../constants/endpoint_constant';
import storage from '../utils/storage';
import CategorySticker from '../components/Emoji/entities/CategorySticker';
import Sticker from '../components/Emoji/entities/Sticker';

export default {
    getAllSticker() {
        const key = LIST_DEFAULT_STICKER_CATEGORY_FOR_WEB + storage.getToken();
        if (isObject(window[key])) {
            return Promise.resolve(window[key]);
        }

        const promise = `promise${key}`;

        if (window[promise]) {
            return window[promise];
        }

        const data = {
            'take': 1000,
            'language': 'jp',
            'skip': 0,
            'api': LIST_DEFAULT_STICKER_CATEGORY_FOR_WEB,
        };

        window[promise] = HttpRequest({ data }).then(response => {
            response.data = response.data.map(item => {
                const newItem = new CategorySticker(item);
                newItem.stickers = Array.isArray(newItem.stickers) ? newItem.stickers : [];
                newItem.stickers = newItem.stickers.map(sticker => new Sticker(sticker, item.cat_id));

                return newItem;
            });

            window[key] = response;

            return response;
        });

        return window[promise];
    },

    getStickerByStickerCateCode(code) {
        const key = `${LIST_DEFAULT_STICKER_CATEGORY_FOR_WEB + storage.getToken()}_${code}`;
        if (isObject(window[key])) {
            return Promise.resolve(window[key]);
        }

        const promise = `promise${key}`;

        if (window[promise]) {
            return window[promise];
        }

        window[promise] = this.getAllSticker().then(response => {
            let sticker = null;
            response.data.forEach(item => {
                item.stickers.forEach(itemSticker => {
                    if (code === `${itemSticker.sticker_id}`) {
                        sticker = itemSticker;
                    }
                })
            })

            if (sticker) {
                window[key] = sticker;

                return sticker;
            }

            return Promise.reject(code);
        });


        return window[promise];
    },
};
