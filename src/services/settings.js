import HttpRequest from '../utils/http-request';
import {
    UPDATE_PARTICIPATION_RANKING,
    GET_EXT_PAGE,
    GET_NOTI_SET,
} from '../constants/endpoint_constant';

export default {
    getNotiSetting() {
        const data = {
            api : GET_NOTI_SET,
        }
        return HttpRequest({ data });
    },

    setNotiSetting(data) {
        return HttpRequest({ data });
    },

    getBlockList(data) {
        return HttpRequest({ data });
    },

    unBlockUser(data) {
        return HttpRequest({ data });
    },

    blockUser(data) {
        return HttpRequest({ data });
    },

    updateJoinRanking(joinRanking = true) {
        const data = {
            api: UPDATE_PARTICIPATION_RANKING,
            is_participation: joinRanking,
        };

        return HttpRequest({ data });
    },

    getFreePage() {
        return HttpRequest({
            data: {
                api: GET_EXT_PAGE,
            },
        });
    },
};
