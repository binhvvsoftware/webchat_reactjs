import isObject from 'lodash/isObject';
import { GET_BANNED_WORD_V2 } from '../constants/endpoint_constant';
import HttpRequest from '../utils/http-request';
import storage from '../utils/storage';

export default {
    getBannedWord() {
        const key = GET_BANNED_WORD_V2 + storage.getToken();
        if (isObject(window[key])) {
            return Promise.resolve(window[key]);
        }

        const promise = `promise${key}`;

        if (window[promise]) {
            return window[promise];
        }

        window[promise] = HttpRequest({
            data: {
                api: GET_BANNED_WORD_V2,
                version: -1,
            },
        }).then(response => {
            window[key] = response;

            return response;
        });

        return window[promise];
    },
};
