import HttpRequest from '../utils/http-request';
import {  SAVE_IMAGE, GET_POINT_SAVE_IMAGE } from '../constants/endpoint_constant';

export default {

    saveImage(imgId){
        return HttpRequest({
            data: {
                api: SAVE_IMAGE,
                img_id : imgId,
            },
        })
    },

    getPointSaveImage(imgId){
        return HttpRequest({
            data: {
                api: GET_POINT_SAVE_IMAGE,
                img_id : imgId,
            },
        })
    },

};