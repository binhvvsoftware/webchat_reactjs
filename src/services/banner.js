import isObject from 'lodash/isObject';
import isMobile from 'ismobilejs'
import { DEVICE_TYPE, LIST_BANNER_CLIENT } from '../constants/endpoint_constant';
import storage from '../utils/storage';
import HttpRequest from '../utils/http-request';

export default {
    getList() {
        if (isObject(window[LIST_BANNER_CLIENT])) {
            return Promise.resolve(window[LIST_BANNER_CLIENT]);
        }

        const user = storage.getUserInfo();

        return HttpRequest({
            data: {
                api: 'list_banner_client',
                device_type: DEVICE_TYPE,
                gender: user.gender,
                is_ipad: isMobile.tablet,
            },
        }).then(response => {
            window[LIST_BANNER_CLIENT] = response;

            return response;
        });
    },
};
