import User from '../entities/User';
import HttpRequest from '../utils/http-request';

export default {

    getListLike() {
        return HttpRequest({
            data: {
                api : "get_like_notification",
                time_stamp : null,
                take : 100,
            },
        }).then(response => {
            response.data = response.data.map(user => new User(user));
            
            return response;
        });
    },
};