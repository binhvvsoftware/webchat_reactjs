import * as WEBVIEW from '../constants/link_webview';
import storage from '../utils/storage';

export function buyPoint(pckId) {
    if (pckId) {
        return `${WEBVIEW.BUY_POINT}${storage.getToken()}${`&package_id=${pckId}`}`;
    }
    return `${WEBVIEW.BUY_POINT}${storage.getToken()}`;
}

export function freePoint() {
    return `${WEBVIEW.FREE_POINT}${storage.getToken()}`;
}

export function howToUseMale() {
    return `${WEBVIEW.HOW_TO_USE_MALE}${storage.getToken()}`;
}

export function howToUseFemale() {
    return `${WEBVIEW.HOW_TO_USE_FEMALE}${storage.getToken()}`;
}

export function supportPage() {
    return `${WEBVIEW.SUPPORT_PAGE}${storage.getToken()}`;
}

export function termOfUser() {
    return `${WEBVIEW.TERM_OF_USER}${storage.getToken()}`;
}

export function aboutPayment() {
    return `${WEBVIEW.ABOUT_PAYMENT}${storage.getToken()}`;
}

export function verifyFemale() {
    return `${WEBVIEW.VERIFY_FEMALE}${storage.getToken()}`;
}

export function verifyAge() {
    return `${WEBVIEW.VERIFY_AGE}${storage.getToken()}`;
}

export function warningVideoCall() {
    return `${WEBVIEW.WARNING_VIDEO_CALL}${storage.getToken()}`;
}

export function loginFromOtherSystem() {
    return WEBVIEW.LOGIN_FROM_OTHER_SYSTEM;
}
