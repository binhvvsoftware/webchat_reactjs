import HttpRequest from '../utils/http-request';
import {
    GET_WARNING_POPUP,
    UPDATE_WARNING_POP_UP,
} from '../constants/endpoint_constant';

export default {
    getWarningPopup() {
        const data = {
            api : GET_WARNING_POPUP,
        }
        return HttpRequest({ data });
    },

    updateWarningPopup(id) {
        const data = {
            api : UPDATE_WARNING_POP_UP,
            popup_id : id,
        }
        return HttpRequest({ data });
    },
};