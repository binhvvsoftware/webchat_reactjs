import HttpRequest from '../utils/http-request';
import { LIST_BACKSTAGE, REMOVE_BACKSTAGE } from '../constants/endpoint_constant';

export default {
    getMyListBackStage(skip = 0, take = 48) {
        return HttpRequest({
            data: {
                api: LIST_BACKSTAGE,
                skip,
                take,
            },
        }).then(response => {
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },

    deleteBackStage(imgId) {
        const data = {
            api: REMOVE_BACKSTAGE,
            img_id: imgId,
        };

        return HttpRequest({ data });
    },
};
