import HttpRequest from '../utils/http-request';
import { ROOT_URL, FORGOT_PASSWORD } from '../constants/endpoint_constant';

export const API = {
    forgetPassword(email) {
        return HttpRequest.post(ROOT_URL, {
            api: FORGOT_PASSWORD,
            email,
        });
    },
}
