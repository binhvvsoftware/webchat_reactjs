import moment from 'moment/moment';
import {
    ROOT_URL,
    MEDIA_URL,
    LST_SENT_IMG_WITH_USER,
    GET_POINT_SAVE_IMAGE,
    UNLOCK_BACKSTAGE,
    LIST_CONVERSATION,
    DELETE_MESSAGE,
    CHECK_IMAGE_EXISTED,
    CHECK_FILE_UNLOCK,
    GET_CHAT_HISTORY,
    CHECK_VIDEO_EXISTED,
    FILE_UNLOCK_TYPE,
} from '../constants/endpoint_constant';
import HttpRequest from '../utils/http-request';
import Message from '../entities/Message';
import storage from '../utils/storage';
import MessageSocket from '../entities/MessageSocket';

export default {
    getConversations(filter) {
        return HttpRequest({
            data: {
                api: LIST_CONVERSATION,
                filter,
                take: 99999,
                time_stamp: moment().utc().format('YYYYMMDD'),
            },
        }).then(response => {
            response.data = response.data.map(message => new Message(message));

            return response;
        });
    },

    deleteConversation(conversationId) {
        return HttpRequest({
            data: {
                api: DELETE_MESSAGE,
                frd_id: conversationId,
            },
        });
    },

    getListImageSent(toUserId, skip = 0, take = 999) {
        return HttpRequest.post(ROOT_URL, {
            api: LST_SENT_IMG_WITH_USER,
            req_user_id: toUserId,
            skip,
            take,
        });
    },

    getPointAction(imageId) {
        return HttpRequest.post(ROOT_URL, {
            api: GET_POINT_SAVE_IMAGE,
            img_id: imageId,
        });
    },

    unlockImageChat(userId, imageId) {
        return HttpRequest.post(ROOT_URL, {
            api: UNLOCK_BACKSTAGE,
            req_user_id: userId,
            type: 2,
            image_id: imageId,
        });
    },

    checkImageUnlock(userId, imgId) {
        const data = {
            api: CHECK_FILE_UNLOCK,
            req_user_id: userId,
            type: FILE_UNLOCK_TYPE.VIEW_IMAGE,
            image_id: imgId,
        };
        return HttpRequest({ data });
    },

    checkVideoUnlock(userId, fileId) {
        const data = {
            api: CHECK_FILE_UNLOCK,
            req_user_id: userId,
            type: FILE_UNLOCK_TYPE.WATCH_VIDEO,
            file_id: fileId,
        };
        return HttpRequest({ data });
    },

    videoUnlock(userId, fileId) {
        const data = {
            api: UNLOCK_BACKSTAGE,
            req_user_id: userId,
            type: FILE_UNLOCK_TYPE.WATCH_VIDEO,
            file_id: fileId,
        };
        return HttpRequest({ data });
    },

    getChatHistory(frdId, timestamp, take = 24) {
        return HttpRequest({
            data: {
                api: GET_CHAT_HISTORY,
                frd_id: frdId,
                time_stamp: timestamp,
                take,
            },
        }).then(response => {
            response.data = response.data.map(message => new MessageSocket(message, 'history'));

            return response;
        });
    },

    checkImageExist(time, size) {
        const url = `${MEDIA_URL}/api=${CHECK_IMAGE_EXISTED}&token=${storage.getToken()}&time=${time}&size=${size}`;

        return HttpRequest.post(url).then(response => {
            console.info('checkImageExist', response); // eslint-disable-line

            return response;
        });
    },

    checkVideoExist(time, size) {
        const url = `${MEDIA_URL}/api=${CHECK_VIDEO_EXISTED}&token=${storage.getToken()}&time=${time}&size=${size}`;

        return HttpRequest.post(url).then(response => {
            console.info('checkVideoExist', response); // eslint-disable-line

            return response;
        });
    },
};
