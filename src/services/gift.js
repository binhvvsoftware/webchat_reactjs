import HttpRequest from '../utils/http-request';
import storage from '../utils/storage';
import {
    ROOT_URL,
    GET_ALL_GIFT,
    SEND_GIFT,
} from '../constants/endpoint_constant';

export default {
    getList() {
        const user = storage.getUserInfo();
        const data = {
            api: GET_ALL_GIFT,
            req_user_id: user.user_id,
            language: "en",
        };

        return HttpRequest.post(ROOT_URL, data);
    },

    sendGiftUser(giftId,userId) {
        const data = {
            api: SEND_GIFT,
            gift_id: giftId,
            rcv_id: userId,
        }

        return HttpRequest.post(ROOT_URL, data);
    },
};
