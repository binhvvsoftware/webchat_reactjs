import HttpRequest from '../utils/http-request';
import { ROOT_URL, LIST_TEMPLATE, UPDATE_TEMPLATE, ADD_TEMPLATE, DELETE_TEMPLATE } from '../constants/endpoint_constant';

export default {
    getList() {
        return HttpRequest.post(ROOT_URL, {
            api: LIST_TEMPLATE,
        });
    },

    add(title, content) {
        return HttpRequest.post(ROOT_URL, {
            api: ADD_TEMPLATE,
            template_title: title,
            template_content: content,
        });
    },

    update(title, content, templateId) {
        return HttpRequest.post(ROOT_URL, {
            api: UPDATE_TEMPLATE,
            template_title: title,
            template_content: content,
            template_id: templateId,
        });
    },

    delete(templateId) {
        return HttpRequest.post(ROOT_URL, {
            api: DELETE_TEMPLATE,
            template_id: templateId,
        });
    },
};
