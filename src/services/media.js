import HttpRequest from '../utils/http-request';
import {
    MEDIA_URL,
    UPLOAD_IMAGE,
    DOWNLOAD_IMAGE,
    GET_FB_AVATAR,
    LOAD_IMG,
    UPLOAD_FILE,
    IMAGE_AVATAR,
    IMAGE_THUMBNAIL_VIDEO,
    LOAD_THUMBNAIL_VIDEO,
    GET_VIDEO_URL,
} from '../constants/endpoint_constant';
import storage from '../utils/storage';
import Upload from '../entities/Upload';
import * as ENV from '../constants/env';

const getURL = (imageId, imgKind, api = LOAD_IMG) => {
    const token = storage.getToken();

    return `${MEDIA_URL}/api=${api}&token=${token}&img_id=${imageId}&img_kind=${imgKind}`;
};

export default {
    getPosterVideo(fileId) {
        const token = storage.getToken();

        return `${MEDIA_URL}/api=${LOAD_THUMBNAIL_VIDEO}&token=${token}&img_id=${fileId}&width_size=500`;
    },

    getURLVideo(fileId) {
        const url = `${MEDIA_URL}/api=${GET_VIDEO_URL}&token=${storage.getToken()}&file_id=${fileId}`;
        return HttpRequest.post(url)
    },

    uploadImageBase64(base64, category = IMAGE_AVATAR, orientation = 0, time = 0, size = 0) {
        let url = `${MEDIA_URL}/api=${UPLOAD_IMAGE}&img_cat=${category}&token=${storage.getToken()}`;
        if (orientation !== 0) {
            url = `${url}&angle=${orientation}`;
        }

        if (time) {
            url = `${url}&time=${time}`;
        }

        if (size) {
            url = `${url}&size=${size}`;
        }


        return HttpRequest.post(url, {
            data: String(base64).replace(/^data:image\/[a-z]+;base64,/, ''),
        }).then(response => {
            response.data = new Upload(response.data);

            return response;
        });
    },

    updateThumbnailVideo(base64, fileId, category = IMAGE_THUMBNAIL_VIDEO) {
        const url = `${MEDIA_URL}/api=${UPLOAD_IMAGE}&img_cat=${category}&file_id=${fileId}&token=${storage.getToken()}`;

        return HttpRequest.post(url, {
            data: String(base64).replace(/^data:image\/[a-z]+;base64,/, ''),
        }).then(response => {
            response.data = new Upload(response.data);

            return response;
        });
    },

    updateVideo(meta) {
        const url = `${ENV.MEDIA}/api=${UPLOAD_FILE}&token=${storage.getToken()}&time=${meta.time}&size=${meta.size}`;
        const videoFormData = new FormData();
        videoFormData.append('data', meta.data);

        return HttpRequest.post(url, videoFormData);
    },

    getBase64FromFbId(fbId) {
        return HttpRequest.get(`${MEDIA_URL}/api=${GET_FB_AVATAR}&fb_id=${fbId}&token=${storage.getToken()}`);
    },

    getImage(imageId, imgKind = 1) {
        const url = getURL(imageId, imgKind);
        window.cache_images = window.cache_images || [];

        if (window.cache_images.indexOf(url) !== -1) {
            return Promise.resolve(url);
        }

        if (!imageId) {
            return Promise.reject(url);
        }

        return HttpRequest.get(url).then((response) => {

            if (typeof response === 'object') {
                return Promise.reject(url);
            }

            window.cache_images.push(url);
            return url;
        });
    },

    downloadImageURL(imageId, fileName) {
        return `${MEDIA_URL}/api=${DOWNLOAD_IMAGE}&token=${storage.getToken()}&file_id=${imageId}&fileName=${fileName}`;
    },

    getURL,
};
