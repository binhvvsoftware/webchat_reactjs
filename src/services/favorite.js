import User from '../entities/User';
import HttpRequest from '../utils/http-request';

export default {

    getListFavorite(api, skip = 0, take = 30) {
        return HttpRequest({
            data: {
                api,
                skip,
                take,
            },
        }).then(response => {
            response.data = response.data.map(user => new User(user));
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },

    getNumberFavorite (data) {
        return HttpRequest({ data });
    },
};
