import {
    GET_BUZZ,
    LIST_BACKSTAGE,
    UNLOCK_BACKSTAGE,
    PROFILE_REPORT,
    PROFILE_BLOCK,
    SETTING_NOTICE,
    LIST_ONLINE_ALERT,
    GET_SETTING_NOTICE,
    UPDATE_MEMO,
    CHECK_FILE_UNLOCK,
} from '../constants/endpoint_constant';
import HttpRequest from '../utils/http-request';
import User from '../entities/User';
import Buzz from '../entities/Buzz';
import { cacheUser } from '../utils/cache';

export default {
    getBuzzUser(userId, skip = 0, take = 20) {
        return HttpRequest({
            data: {
                api: GET_BUZZ,
                buzz_kind: 0,
                take,
                skip,
                req_user_id: userId,
            },
        }).then(response => {
            response.data = response.data.map(buzz => new Buzz(buzz));
            response.hasLoadMore = response.data.length > 0;
            setTimeout(() => {
                cacheUser(response.data);
            }, 1);

            return response;
        });
    },

    setSettingNotice(userId, statusInstall, noticeSelect) {
        return HttpRequest({
            data: {
                api: SETTING_NOTICE,
                req_user_id: userId,
                is_alt: statusInstall,
                alt_fre: noticeSelect,
            },
        });
    },

    getSettingNotice(userId) {
        return HttpRequest({
            data: {
                api: GET_SETTING_NOTICE,
                req_user_id: userId,
            },
        });
    },

    updateMemo(userID, memotxt) {
        return HttpRequest({
            data: {
                api: UPDATE_MEMO,
                frd_id: userID,
                memo : memotxt,
            },
        });
    },

    checkUnLock(userId) {
        const data = {
            api: CHECK_FILE_UNLOCK,
            type: 1,
            req_user_id: userId,
        };

        return HttpRequest({ data });
    },

    getListBackStage(userId, skip = 0, take = 50) {
        return HttpRequest({
            data: {
                api: LIST_BACKSTAGE,
                take,
                skip,
                req_user_id: userId,
            },
        }).then(response => {
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },

    unLockBackStage(userId, type = 1) {
        const data = {
            api: UNLOCK_BACKSTAGE,
            type,
            req_user_id: userId,
        }

        return HttpRequest({ data });
    },

    likeBuzz(data) {
        return HttpRequest({ data });
    },

    profileBlock(userId) {
        const data = {
            api: PROFILE_BLOCK,
            req_user_id: userId,
        };
        return HttpRequest({ data });
    },

    profileReport(userId, typeReport) {
        const data = {
            api: PROFILE_REPORT,
            subject_id: userId,
            rpt_type: typeReport,
            subject_type: 2,
        };
        return HttpRequest({ data });
    },

    reportImage(imageId, typeReport) {
        const data = {
            api: PROFILE_REPORT,
            subject_id: imageId,
            rpt_type: typeReport,
            subject_type: 1,
        };
        return HttpRequest({ data });
    },

    listOnlineAlert() {
        const data = {
            api: LIST_ONLINE_ALERT,
        };
        return HttpRequest({ data }).then(response => {
            response.data = response.data.map(user => new User(user));

            return response;
        });
    },
};
