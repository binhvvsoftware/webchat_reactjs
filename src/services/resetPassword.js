import moment from 'moment';
import sha1 from 'sha1';
import HttpRequest from '../utils/http-request';import {
    DEVICE_TYPE,
    APP_NAME,
    APP_TYPE,
    RESET_PASSWORD,
    APP_VERSION,
} from '../constants/endpoint_constant';

export default {

    resetPassword(token, password, email) {

        const data = {
            vrf_code: token,
            original_pwd: password,
            email,
            device_type: DEVICE_TYPE,
            application: APP_NAME,
            applicaton_type: APP_TYPE,
            login_time: moment().utc().format('YYYYMMDDHHmmss'),
            api: RESET_PASSWORD,
            new_pwd: sha1(password),
            application_version: APP_VERSION,
        };

        return HttpRequest({ data });
    },
};
