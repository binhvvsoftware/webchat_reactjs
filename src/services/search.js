import isMobile from 'ismobilejs';
import User from '../entities/User';
import Notice from '../entities/Notice';
import News from '../entities/News';
import HttpRequest from '../utils/http-request';
import storage from '../utils/storage';
import { cacheUser } from '../utils/cache';
import { SEARCH_BY_NAME, MEET_PEOPLE, LIST_NEWS_CLIENT, GET_NOTICE, DEVICE_TYPE } from '../constants/endpoint_constant';

const takeUser = isMobile.tablet || !isMobile.any ? 50 : 24;

export default {
    searchInfo(data) {
        const param = { ...data };
        param.take = data.take || takeUser;
        param.api = MEET_PEOPLE;

        return HttpRequest({ data: param }).then(response => {
            response.data = response.data.map(user => new User(user));
            response.hasLoadMore = response.data.length > 0;

            setTimeout(() => {
                cacheUser(response.data);
            }, 1);
            return response;
        });
    },

    searchName(userName, skip = 0, take = 30) {
        return HttpRequest({
            data: {
                api: SEARCH_BY_NAME,
                skip,
                take,
                user_name: userName,
            },
        }).then(response => {
            response.data = response.data.map(user => new User(user));
            response.hasLoadMore = response.data.length > 0;

            setTimeout(() => {
                cacheUser(response.data);
            }, 1);
            return response;
        });
    },

    getNews(skip = 0, take = 9999) {
        const user = storage.getUserInfo();

        return HttpRequest({
            data: {
                api: LIST_NEWS_CLIENT,
                skip,
                device_type: DEVICE_TYPE,
                take,
                gender: parseInt(user.gender, 10),
            },
        }).then(response => {
            response.data = response.data.map(news => new News(news));
            return response;
        });
    },

    getNoticeData(timestamp = null, take = 30) {
        return HttpRequest({
            data: {
                api: GET_NOTICE,
                time_stamp: timestamp,
                take,
            },
        }).then(response => {
            response.data = response.data.map(notice => new Notice(notice));
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },
};
