import CallLog from '../entities/CallLog';
import HttpRequest from '../utils/http-request';

export default {
    getCallLog(type, skip = 0, take = 30) {
        return HttpRequest({
            data: {
                api: "get_call_log",
                skip,
                take,
                type,
            },
        }).then(response => {
            response.data = response.data.map(user => new CallLog(user));
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },
};
