import moment from 'moment/moment';
import sha1 from 'sha1';
import HttpRequest from '../utils/http-request';
import storage from '../utils/storage';
import { getDeviceName, getBrowserVersion } from '../utils/helpers';
import {
    ROOT_URL, LOGIN, APP_NAME, APP_TYPE, APP_VERSION, REGISTER, DEACTIVATE_ACCOUNT,
    REGISTER_EMAIL_PASSWORD, CHANGE_EMAIL, CHANGE_PASSWORD,
    DEVICE_TYPE, GET_USER_INFO, CHECK_TOKEN,
} from '../constants/endpoint_constant';
import { HTTP_WOMEN_CAN_NOT_LOGIN } from '../constants/response_status_constant';
import User from '../entities/User';
import { USER_SECURITY } from '../constants/storage_constant';

const rememberEmailPassword = (password, email) => {
    storage.set(USER_SECURITY, `${password}${btoa(email)}`);
};

const login = (data) => {
    const params = Object.assign({
        device_id: '',
        os_version: getBrowserVersion(),
        device_type: DEVICE_TYPE,
        use_fcm: true,
        gps_adid: '',
        device_name: getDeviceName(),
        adid: '',
        application: APP_NAME,
        applicaton_type: APP_TYPE,
        login_time: moment().utc().format('YYYYMMDDHHmmss'),
        notify_token: '',
        api: LOGIN,
        application_version: APP_VERSION,
    }, data);

    return HttpRequest.post(ROOT_URL, params).then(response => {
        const user = new User(response.data);

        if (user.isFemale) {
            return Promise.reject({ // eslint-disable-line
                code: HTTP_WOMEN_CAN_NOT_LOGIN,
            });
        }

        params.pwd = params.pwd || sha1(params.login_time);
        window.token = response.data.token;
        storage.setToken(response.data.token);
        storage.setUserInfo(response.data);
        rememberEmailPassword(params.pwd, data.email || params.fb_id);

        response.data = user;

        return response;
    });
};

const updateEmailPassword = (email, password) => {
    const user = storage.getUserInfo();
    if (email) {
        user.email = email;
        user.hasEmail = true;
        storage.setUserInfo(user);
    }
    rememberEmailPassword(password, user.email);
};

const changeMail = (email, password) => {
    const params = {
        api: CHANGE_EMAIL,
        email,
        old_pwd: sha1(password),
    };

    return HttpRequest.post(ROOT_URL, params).then((response) => {
        updateEmailPassword(email, params.old_pwd);

        return response;
    });
};

const changePassword = (email, newPassword, password) => {
    const params = {
        api: CHANGE_PASSWORD,
        new_pwd: sha1(newPassword),
        original_pwd: newPassword,
        old_pwd: sha1(password),
    };

    return HttpRequest.post(ROOT_URL, params).then((response) => {
        updateEmailPassword(null, params.new_pwd);

        return response;
    });
};

export default {
    register(data) {
        const params = {
            ...{
                device_id: '',
                os_version: getBrowserVersion(),
                device_type: DEVICE_TYPE,
                use_fcm: true,
                gps_adid: '',
                device_name: getDeviceName(),
                adid: '',
                application: APP_NAME,
                applicaton_type: APP_TYPE,
                login_time: moment().utc().format('YYYYMMDDHHmmss'),
                notify_token: '',
                api: REGISTER,
                application_version: APP_VERSION,
            },
            ...data,
        };

        return HttpRequest.post(ROOT_URL, params)
            .then((response) => {
                response.data = new User(response.data);
                rememberEmailPassword(response.data.pwd, response.data.email);

                return response;
            });
    },

    loginEmail(email, password, isDecryption = false) {
        return login({ email, pwd: isDecryption ? password : sha1(password), fb_id: '' });
    },

    loginFb(fbId) {
        return login({ fb_id: fbId });
    },

    deactivate(content) {
        storage.destroy();

        const params = {
            api: DEACTIVATE_ACCOUNT,
            cmt: content,
        };

        return HttpRequest.post(ROOT_URL, params);
    },

    registerEmailAndPassword(email, password) {
        const params = {
            api: REGISTER_EMAIL_PASSWORD,
            email,
            new_pwd: sha1(password),
            original_pwd: password,
        };

        return HttpRequest.post(ROOT_URL, params).then((response) => {
            updateEmailPassword(email, params.new_pwd);

            return response;
        });
    },

    changeMail,

    changePassword,

    changeMailPassword: async (email, newPassword, password) => {
        try {
            const response = await Promise.all([
                changeMail(email, password),
                changePassword(email, newPassword, password),
            ]);

            return response
        } catch (errors) {
            return errors;
        }
    },

    getInfo() {
        const user = storage.getUserInfo();

        return HttpRequest.post(ROOT_URL, {
            api: GET_USER_INFO,
            req_user_id: user.user_id,
        }).then(response => {
            response.data = new User(response.data);

            const newInfo = { ...user, ...response.data };
            newInfo.avatar_url = newInfo.avatar_url !== null ? newInfo.avatar_url : user.avatar_url;
            if (!response.data.ava_id) {
                newInfo.avatar_url = null;
                newInfo.ava_id = null;
            }
            storage.setUserInfo(newInfo);
            response.data = newInfo;

            return response;
        });
    },

    checkToken: () => {
        const params = {
            api: CHECK_TOKEN,
        };

        return HttpRequest.post(ROOT_URL, params);
    },
};
