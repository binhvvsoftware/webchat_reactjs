import Ranking from '../entities/Ranking';
import HttpRequest from '../utils/http-request';

import { ROOT_URL, GET_RANKING_LIST } from '../constants/endpoint_constant';

export default {
    getList(target) {
        let type;
        switch (target) {
            case 'weekly':
                type = 2;
                break;
            case 'monthly':
                type = 3;
                break;
            default:
                type = 1;
                break;
        }

        return HttpRequest.post(ROOT_URL, {
            api: GET_RANKING_LIST,
            type,
        }).then(response => {
            response.data = response.data.map(ranking => new Ranking(ranking));

            return response;
        });

    },
};
