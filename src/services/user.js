import User from '../entities/User';
import AppealComment from '../entities/AppealComment';
import HttpRequest from '../utils/http-request';
import {
    ROOT_URL,
    GET_USER_INFO,
    UPDATE_USER_INFO,
    UPDATE_APPEAL_COMMENT,
    GET_ATT_NUM,
    GET_ALL_SYSTEM_ACC,
} from '../constants/endpoint_constant';
import storage from '../utils/storage';
import Attention from '../entities/Attention';
import { cacheUser } from '../utils/cache';

export default {
    getUserInfo(userId = null) {
        const user = storage.getUserInfo();
        const reqUserId = userId || user.user_id;

        return HttpRequest.post(ROOT_URL, {
            api: GET_USER_INFO,
            req_user_id: reqUserId,
        }).then(response => {
            response.data = new User(response.data);
            cacheUser(response.data);

            return response;
        });
    },

    updateInfo(data) {
        const params = { ...data };
        params.api = UPDATE_USER_INFO;

        return HttpRequest.post(ROOT_URL, params).then(response => {
            response.data = new User(response.data);

            return response;
        });
    },

    updateAppealComment(comment) {
        return HttpRequest.post(ROOT_URL, {
            appeal_comment: comment,
            api: UPDATE_APPEAL_COMMENT,
        }).then(response => {
            response.data = new AppealComment(response.data);

            return response;
        });
    },

    getAttentionNumber() {
        return HttpRequest.post(ROOT_URL, {
            api: GET_ATT_NUM,
        }).then(response => {
            response.data = new Attention(response.data);

            return response;
        });
    },

    getAllAccountSystem() {
        return HttpRequest.post(ROOT_URL, {
            api: GET_ALL_SYSTEM_ACC,
        });
    },
};
