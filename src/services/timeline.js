import Buzz from '../entities/Buzz';
import Comment from '../entities/Comment';
import SubComment from '../entities/SubComment';
import HttpRequest from '../utils/http-request';
import {
    RMV_FAV,
    ADD_FAV,
    DEL_BUZZ,
    DEL_CMT,
    LIKE_BUZZ,
    GET_BUZZ_DETAIL,
    DEL_SUB_CMT,
} from '../constants/endpoint_constant';

export default {

    getBuzzTimeline(buzzKind, skip = 0, take = 50) {
        return HttpRequest({
            data: {
                api: 'get_buzz',
                buzz_kind: buzzKind,
                take,
                skip,
                long: 0,
                lat: 0,
            },
        }).then(response => {
            response.data = response.data.map(buzz => new Buzz(buzz));
            response.hasLoadMore = response.data.length > 0;

            return response;
        });
    },

    addBuzzTimeline(data) {
        return HttpRequest({ data });
    },

    likeBuzz(data) {
        return HttpRequest({ data });
    },

    like(buzzId, likeType) {
        return HttpRequest({
            data: {
                api: LIKE_BUZZ,
                buzz_id: buzzId,
                like_type: likeType,
            },
        });
    },

    favoriteTimeline(data) {
        return HttpRequest({ data });
    },

    favorite(buzzId, remove = false) {
        const data = { api: remove ? RMV_FAV : ADD_FAV, fav_id: buzzId, req_user_id: buzzId };

        return HttpRequest({ data });
    },

    deleteBuzz(buzzId) {
        const data = {
            api: DEL_BUZZ,
            buzz_id: buzzId,
        };
        return HttpRequest({ data });
    },

    addComment(comment,buzzId) {
        const data = {
            api : "add_cmt_version_2",
            buzz_id : buzzId,
            cmt_val : comment,
        }
        return HttpRequest({ data }).then(response => response);
    },

    getListComment(buzzId,skip = 0, take = 30){
        const data = {
            api : "lst_cmt",
            buzz_id : buzzId,
            skip,
            take,
        }

        return HttpRequest({ data }).then(response => {
            response.data[0] = new Comment(response.data[0]);

            return response;
        });
    },

    getListSubComment(buzzId,cmtId,skip = 0,take = 30){
        const data = {
            api : 'list_sub_comment',
            buzz_id : buzzId,
            cmt_id : cmtId,
            skip,
            take,
        }

        return HttpRequest({ data }).then(response => {
            response.data = response.data.map(subCmt => new SubComment(subCmt));

            return response;
        });
    },

    deleteComment(commentId, buzzId) {
        return HttpRequest({
            data: {
                api: DEL_CMT,
                buzz_id: buzzId,
                cmt_id: commentId,
            },
        });
    },

    getBuzzDetail(take, buzzId) {
        return HttpRequest({
            data : {
                api: GET_BUZZ_DETAIL,
                buzz_id: buzzId,
                cmt_take : take,
            },
        }).then(response => {
            response.data =  new Buzz(response.data);
            response.hasLoadMore = response.data.length > 0;

            return response;
        })
    },

    addSubComment(buzzId, cmtId, value) {
        const data = {
            api : 'add_sub_comment',
            cmt_id : cmtId,
            value,
            buzz_id : buzzId,
        }
        return HttpRequest({ data });
    },

    deleteSubComment(buzzId,cmtId,subId) {
        const data = {
            api : DEL_SUB_CMT,
            buzz_id : buzzId,
            cmt_id : cmtId,
            sub_comment_id : subId,
        }

        return HttpRequest({ data });
    },
};
