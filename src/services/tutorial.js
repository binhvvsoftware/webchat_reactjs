import HttpRequest from '../utils/http-request';
import {
    CLOSE_TUTORIAL,
} from '../constants/endpoint_constant';

export default {
    closeTutorial(page) {
        const data = {
            page,
            api : CLOSE_TUTORIAL,
        }
        return HttpRequest({ data });
    },
};
