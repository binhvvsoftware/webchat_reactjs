import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Popup from 'react-popup';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import PublicRoutes from './public';
import Home from '../pages/Home';
import Notification from '../pages/Notification';
import { LoginOption, SignIn } from '../pages/Login';
import Deactivate from '../pages/Deactivate';
import { ForgetPassword, ResetPassword } from '../pages/ForgetPassword';
import Register from '../pages/Register';
import ProfileRegister from '../pages/ProfileRegister';
import OnlineAlert from '../pages/OnlineAlert';
import Profile from '../pages/Profile';
import Favorite from '../pages/Favorite';
import SettingCall from '../pages/SettingCall';
import FootPrint from '../pages/FootPrint';
import MyPage from '../pages/MyPage';
import Settings from '../pages/Settings';
import Ranking from '../pages/Ranking';
import MyGallery from '../pages/MyGallery';
import NotFound from '../pages/NotFound';
import Static from '../pages/Static';
import Template from '../pages/Template';
import ListLike from '../pages/ListLike';
import Timeline from '../pages/Timeline';
import Chat from '../pages/Chat';
import BuzzDetail from '../pages/BuzzDetail';
import SettingPushNoti from '../pages/SettingPushNoti';
import SettingBlockList from '../pages/SettingBlockList';
import Splash from '../pages/Splash';
import history, { getLocationHistory } from '../utils/history';
import storage from '../utils/storage';
import CallLog from '../pages/CallLog';
import SearchByName from '../pages/SearchByName'
import Common from '../pages/Common';
import AppNotification from '../pages/AppNotification';
import Call from '../pages/Call';
import Auth from '../services/authentication';

class Router extends Component {
    componentWillUpdate() {
        const token = storage.getToken();
        if (!this.checkPublicRoute()) {
            if (token) {
                Auth.checkToken();
            } else {
                this.handleRedirectLogin();
            }
        }
    }

    componentWillMount() {
        setInterval(() => {
            const token = storage.getToken();
            if (!this.checkPublicRoute()) {
                if (token) {
                    Auth.checkToken();
                } else {
                    this.handleRedirectLogin();
                }
            }
        }, 60000);
    }

    checkPublicRoute = () => {
        const location = getLocationHistory();
        let isPublic = false;
        PublicRoutes.forEach(route => {
            if (location.current.pathname.indexOf(route) === 0) {
                isPublic = true;
            }
        });

        return isPublic;
    };

    handleRedirectLogin = () => {
        history.push('/login');
    };

    render() {
        return (
            <>
                <Switch>
                    <Route exact path='/' component={ Splash }/>

                    <PrivateRoute exact path='/home' component={ Home }/>
                    <PrivateRoute exact path='/notification' component={ Notification }/>
                    <PrivateRoute exact path='/my-page' component={MyPage}/>
                    <PrivateRoute path='/profile/:id' component={ Profile }/>
                    <PrivateRoute exact path = '/setting-call' component={ SettingCall } />
                    <PrivateRoute exact path='/template' component={Template }/>
                    <PrivateRoute exact path='/favorite' component={Favorite}/>
                    <PrivateRoute exact path='/foot-print' component={FootPrint}/>
                    <PrivateRoute exact path='/settings' component={ Settings }/>
                    <PrivateRoute exact path='/ranking' component={ Ranking }/>
                    <PrivateRoute exact path='/timeline' component={ Timeline }/>
                    <PrivateRoute exact path='/list-like' component={ ListLike }/>
                    <PrivateRoute exact path='/edit-profile' component={ ProfileRegister }/>
                    <PrivateRoute exact path='/my-gallery' component={ MyGallery }/>
                    <PrivateRoute path='/buzz-detail/:id' component={ BuzzDetail }/>
                    <PrivateRoute exact path='/setting-push-noti' component={ SettingPushNoti }/>
                    <PrivateRoute exact path='/setting-block-list' component={ SettingBlockList }/>
                    <PrivateRoute exact path='/online-alert' component={ OnlineAlert }/>
                    <PrivateRoute path='/chat/:id' component={ Chat }/>
                    <PrivateRoute path='/call-log' component={ CallLog }/>
                    <PrivateRoute path='/call' component={ Call }/>
                    <PrivateRoute exact path='/deactivate' component={ Deactivate }/>
                    <PrivateRoute exact path='/search-by-name' component={ SearchByName }/>

                    <PublicRoute exact path='/login' component={ LoginOption }/>
                    <PublicRoute exact path='/sign-in' component={ SignIn }/>
                    <PublicRoute exact path='/reset-password' component={ ResetPassword }/>
                    <PublicRoute exact path='/register' component={Register} />
                    <PublicRoute exact path='/forget-password' component={ ForgetPassword }/>

                    <Route path='/pages/:page' component={ Static }/>
                    <Route exact path='/callback-login' component={ LoginOption }/>
                    <Route exact path='/register-profile' component={ ProfileRegister } />
                    <Route path='/app-notification/:data' component={ AppNotification }/>
                    <Route component={NotFound} />
                </Switch>
                <Popup defaultOk="はい" defaultCancel="キャンセル" closeBtn={false}/>
                <Common />
            </>
        );
    }
}

export default Router;
