/* eslint-disable */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import storage from '../utils/storage';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        { ...rest }
        render={ (props) => {
            const isAuthenticated = !!storage.getToken();

            if (!isAuthenticated) {
                return <Redirect to={ { pathname: '/login' } }/>;
            }

            const user = storage.getUserInfo();

            if (user.user_name.length === 0) {
                return <Redirect to={ { pathname: '/register-profile' } }/>;
            }

            return <Component { ...props } />;
        } }/>
);

export default PrivateRoute;
