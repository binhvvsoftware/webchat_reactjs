export default [
    '/login',
    '/callback-login',
    '/sign-in',
    '/reset-password',
    '/register',
    '/register-profile',
    '/app-notification',
    '/forget-password',
    '/pages',
];
