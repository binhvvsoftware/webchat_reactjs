import { AREA_REGION } from '../constants/region_constant';
import { timeAgo } from '../utils/helpers';
import storage from '../utils/storage';
import Comment from './Comment';

/**
 * @property {String} buzz_id
 * @property {String} user_id
 * @property {String} buzzTime
 * @property {String} buzz_time
 * @property {String} buzz_val
 * @property {String} caption
 * @property {String} user_name
 * @property {Boolean} is_online
 * @property {Boolean} isOnline
 * @property {Boolean} isApprove
 * @property {Boolean} isCaptionApprove
 * @property {Boolean} isApproveAll
 * @property {Boolean} isAuthor
 * @property {Boolean} isFavorite
 * @property {Boolean} isLike
 * @property {String} region_name
 * @property {String} buzzType
 * @property {Object[]} comment
 * @property {Number} region
 * @property {Number} like_num
 * @property {Number} is_app
 * @property {Number} gender
 * @property {Number} is_fav
 * @property {Number} cmt_num
 * @property {Number} dist
 * @property {Number} lst_act
 * @property {Number} comment_buzz_point
 * @property {Number} caption_approve_flag
 * @property {Number} long
 * @property {Number} buzz_type
 * @property {Number} is_like
 * @property {Number} lat
 */
export default class Buzz {
    /**
     *
     * @param {Object|Buzz} attributes
     */
    constructor(attributes) {
        const buzz = Object.assign({}, attributes || {});
        const user = storage.getUserInfo();

        buzz.isAuthor = user.user_id === buzz.user_id;
        buzz.region_name = AREA_REGION[buzz.region] || '';
        buzz.profile_url = `/profile/${buzz.user_id}`;
        buzz.isApprove = buzz.is_app === 1;
        buzz.isCaptionApprove = buzz.caption_approve_flag !== 0;
        buzz.isApproveAll = buzz.isCaptionApprove && buzz.isApprove;
        buzz.isOnline = buzz.is_online;
        buzz.isFemale = buzz.gender !== 0;
        buzz.buzzTime = timeAgo(buzz.buzz_time);
        buzz.caption = buzz.caption || '';
        buzz.isFavorite = buzz.is_fav === 1;
        let buzzType = buzz.buzz_type === 0 ? 'text' : 'image';
        buzzType  = (buzzType === 'image' && buzz.caption.length) ? 'both' : buzzType;
        buzz.buzzType = buzzType;
        buzz.isLike = buzz.is_like === 1;
        const comment = Array.isArray(buzz.comment) ? buzz.comment : [];
        buzz.comment = comment.map(item => new Comment(item));

        Object.keys(buzz).forEach((key) => {
            this[key] = buzz[key];
        });
    }
}
