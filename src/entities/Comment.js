import { timeAgo } from '../utils/helpers';
import storage from '../utils/storage';

class SubComment {
    /**
     * @param {Object|Comment} attributes
     */
    constructor(attributes) {
        const comment = Object.assign({}, attributes || {});
        const user = storage.getUserInfo();

        comment.isAuthor = user.user_id === comment.user_id;
        comment.profile_url = `/profile/${comment.user_id}`;
        comment.isApprove = comment.is_app === 1;
        comment.isOnline = comment.is_online;
        comment.isFemale = comment.gender !== 0;
        comment.isFavorite = comment.is_fav === 1;
        comment.commentTime = timeAgo(comment.comment_time);
        comment.canDelete = comment.can_delete;
        comment.showReply = false;

        Object.keys(comment).forEach((key) => {
            this[key] = comment[key];
        });
    }
}

/**
 * @property {String} user_name
 * @property {String} cmt_val
 * @property {String} cmt_id
 * @property {String} user_id
 * @property {String} cmt_time
 * @property {String} ava_id
 * @property {String} commentTime
 * @property {Boolean} is_online
 * @property {Boolean} isOnline
 * @property {Boolean} isApprove
 * @property {Boolean} isAuthor
 * @property {Boolean} isFavorite
 * @property {Boolean} canDelete
 * @property {Boolean} showReply
 * @property {Object[]} sub_comment
 * @property {Number} is_app
 * @property {Number} gender
 * @property {Number} is_fav
 * @property {Number} type
 * @property {Number} sub_comment_number
 * @property {Number} subComment
 * @property {Number} can_delete
 * @property {Number} cmtTime
 */
export default class Comment {
    /**
     * @param {Object|Comment} attributes
     */
    constructor(attributes) {
        const comment = Object.assign({}, attributes || {});
        const user = storage.getUserInfo();

        comment.isAuthor = user.user_id === comment.user_id;
        comment.profile_url = `/profile/${comment.user_id}`;
        comment.isApprove = comment.is_app === 1;
        comment.isOnline = comment.is_online;
        comment.isFemale = comment.gender !== 0;
        comment.isFavorite = comment.is_fav === 1;
        comment.commentTime = timeAgo(comment.comment_time);

        const subComment = Array.isArray(comment.sub_comment) ? comment.sub_comment : [];
        comment.subComment = subComment.map(item => new SubComment(item));
        comment.canDelete = comment.can_delete === 1 && comment.isApprove;
        comment.showReply = true;

        Object.keys(comment).forEach((key) => {
            this[key] = comment[key];
        });
    }
}
