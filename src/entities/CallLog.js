import { MEDIA_URL, LOAD_IMG } from '../constants/endpoint_constant';
import { checkIsFemale, timeAgo } from '../utils/helpers';
import storage from '../utils/storage';

/**
 * @property {String} ava_id
 * @property {String} avatar_url
 * @property {Boolean} isFemale
 * @property {String} ava_id
 * @property {String} last_login
 * @property {String} login_ago
 * @property {String} user_id
 * @property {String} user_name
 * @property {String} ava_id
 * @property {Boolean} video_call_waiting
 * @property {Boolean} voice_call_waiting
 */
export default class CallLog {
    /**
     *
     * @param {Object|User} attributes
     */
    constructor(attributes) {
        const user = Object.assign({}, attributes || {});
        const token = storage.getToken();
        user.avatar_url = user.ava_id ? `${MEDIA_URL}/api=${LOAD_IMG}&token=${token}&img_id=${user.ava_id}&img_kind=1` : (user.avatar_url || null);
        user.isFemale = checkIsFemale(user.gender);
        user.login_ago = timeAgo(user.last_login);
        if (!user.video_call_waiting && user.voice_call_waiting) {
            user.icon_status = 'icon icon-phone-call-sm';
        } else if (user.video_call_waiting) {
            user.icon_status = 'icon icon-video-call-sm';
        } else {
            user.icon_status = 'icon icon-chats';
        }
        user.user_id = user.partner_id;
        user.user_name = user.partner_name;

        Object.keys(user).forEach((key) => {
            this[key] = user[key];
        });
    }
}
