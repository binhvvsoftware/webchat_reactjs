/**
 * @property {Boolean} reviewed
 * @property {String} appeal_comment
 * @property {String} appeal_comment_time
 * @property {Number} is_noti
 * @property {Boolean} is_reviewed
 * @property {Number} removed_review_appeal_comments
 * @property {Boolean} isApprove
 */
export default class AppealComment {
    constructor(attributes) {
        const appealComment = { ...attributes };
        appealComment.isApprove = !appealComment.is_reviewed;

        Object.keys(appealComment).forEach((key) => {
            this[key] = appealComment[key];
        });
    }
}
