import moment from 'moment';
import { MessageTypes } from '../constants/types';
import { convertTimeUTCToLocal } from '../utils/helpers';
import storage from '../utils/storage';
/* eslint-disable */

const DEFAULT = {
    TYPE: MessageTypes.PP,
    CONTENT: '',
    SOCKET_IO: 1,
    SEND_TIME: 0,
    DESTINATION_TIME: 0,
    USING_NEW_APP: 0,
    DATETIME_FORMAT: 'YYYYMMDDHHmmss',
};

const NEW_MESSAGE = 'newMessage';

const STATUS_TYPES = {
    rd: { code: 'rd', text: '既読' },
    rd_all: { code: 'rd_all', text: 'Seen all' },
    us: { code: 'us', text: 'Unsent' },
    st: { code: 'st', text: 'sent' },
    dlv: { code: 'dlv', text: 'Delivered' },
};


/**
 * @property {String} content
 * @property {String} sticker
 * @property {String} gift
 * @property {Object} file
 * @property {String} fileStatus
 * @property {Boolean} isFileDeleted
 * @property {String} buddyPresentation
 * @property {Boolean} isOwn
 * @property {Boolean} isUnlock
 * @property {String} id
 * @property {String} type
 * @property {String} readTime
 * @property {String} sendTime
 * @property {String} sender
 * @property {String} receiver
 * @property {String} statusCode
 * @property {Object} socketIO
 * @property {String} destinationTime
 * @property {String} originalTime
 * @property {Boolean} isUsingNewApp
 * @property {Object} statusData
 */
class MessageSocket {
    constructor(data, from) {
        this['_from'] = from;
        this.data = data;

        const me = storage.getUserInfo();
        const userId = me.user_id;
        let message = {
            gift: data.gift ? data.gift : null,
            file: data.file ? data.file : null,
            sticker: data.sticker ? data.sticker : null,
            isFileDeleted: data.deleted_image || data.deleted_file,
            content: data.content || data.value,
            isOwn: data.isOwn || data.is_own || (data.sender === userId),
            type: data.type || data.msg_type,
            id: data.id || data.msg_id,
            readTime: moment.isMoment(data.readTime ) && data.readTime.isValid() ? data.readTime : convertTimeUTCToLocal(data.read_time),
            sendTime: moment.isMoment(data.sendTime ) && data.sendTime.isValid() ? data.sendTime : convertTimeUTCToLocal(data.time_stamp || data.original_time),
            statusCode: data.statusCode,
            fileStatus: data.fileStatus,
        };

        message.mds = data.mds;
        switch (from) {
            case 'history':
                message.readTime = convertTimeUTCToLocal(data.read_time);
                message.sendTime = convertTimeUTCToLocal(data.time_stamp);
                message.isOwn = data.own;
                message.fileStatus = data.status; // 1: Approve | 0: Pending | -1: Deny or Deleted
                message.isFileDeleted = data.deleted_file || data.deleted_image;
                message.isUnlock = data.is_unlock || false;
                message.content = data.content;
                message.serverTime = data.time_stamp;
                break;

            case 'socket':
                if (data.mds) {
                    message.id = data.mds.msg_id;

                    if (data.mds.status_file !== undefined) {
                        message.fileStatus = data.mds.status_file;
                    }
                }
                message.content = data.value;
                message.mds = data.mds;
                message.type = data.msg_type;
                message.isOwn = data.sender === userId;
                message.sendTime = convertTimeUTCToLocal(data.original_time);
                message.receiver = data.receiver;
                message.sender = data.sender;
                message.isUnlock = data.isUnlock || false;
                message.serverTime = message.sendTime.format('YYYYMMDDHHmmss');

                if (message.type === MessageTypes.PRC && data.user_status && data.user_status.buddy_presentation) {
                    message.isTyping = data.user_status.buddy_presentation === 'wt';
                }
                break;
            default:
                Object.keys(Object.assign({}, data, message)).forEach((key) => {
                    this[key] = data[key];
                });
                break;
        }

        // message.readTime1 = message.readTime.format('YYYY/MM/DD HH:mm:ss');
        // message.sendTime1 = message.sendTime.format('YYYY/MM/DD HH:mm:ss');

        Object.keys(message).forEach((key) => {
            this[key] = message[key];
        });

        this.statusData = moment.isMoment(this.readTime) ? STATUS_TYPES.rd : STATUS_TYPES[this.statusCode];
    }
}

export default MessageSocket;

export const StatusSocket = {
    READ: { code: 'rd', text: '既読' },
    READ_ALL: { code: 'rd_all', text: 'Seen all' },
    UNSENT: { code: 'us', text: 'Unsent' },
    SENT: { code: 'st', text: 'sent' },
    DELIVERED: { code: 'dlv', text: 'Delivered' },
};

export const Defaults = DEFAULT;
