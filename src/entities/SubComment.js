import { timeAgo } from '../utils/helpers';
import storage from '../utils/storage';

export default class SubComment {
    /**
     * @param {Object|Comment} attributes
     */
    constructor(attributes) {
        const comment = Object.assign({}, attributes || {});
        const user = storage.getUserInfo();

        comment.isAuthor = user.user_id === comment.user_id;
        comment.profile_url = `/profile/${comment.user_id}`;
        comment.isApprove = comment.is_app === 1;
        comment.isOnline = comment.is_online;
        comment.isFemale = comment.gender !== 0;
        comment.isFavorite = comment.is_fav === 1;
        comment.commentTime = timeAgo(comment.comment_time);
        comment.canDelete = comment.can_delete;
        comment.showReply = false;

        Object.keys(comment).forEach((key) => {
            this[key] = comment[key];
        });
    }
}