/**
 * @property {Number} noit_read_time
 * @property {Number} checkout_num
 * @property {Number} noti_num
 * @property {Number} bckstg_num
 * @property {Number} buzz_number
 * @property {Number} fav_num
 * @property {Number} fvt_num
 * @property {Boolean} on_call_now
 * @property {Number} backstage
 * @property {Number} buzz
 * @property {Number} checkout
 * @property {Number} favorite
 * @property {Number} follow
 * @property {Number} myFootprint
 * @property {Number} newCheckout
 * @property {Number} newFavorite
 * @property {Number} like
 * @property {Number} notifyNumber
 * @property {Number} onCallNow
 * @property {Number} unread
 */
export default class Attention {
    /**
     *
     * @param {Object|User} attributes
     */
    constructor(attributes) {
        const attention = Object.assign({}, attributes || {});
        attention.backstage = attention.bckstg_num;
        attention.buzz = attention.buzz_number;
        attention.checkout = attention.checkout_num;
        attention.favorite = attention.fav_num;
        attention.follow = attention.fvt_num;
        attention.myFootprint = attention.my_footprint_num;
        attention.newCheckout = attention.new_checkout_num;
        attention.newFavorite = attention.new_fvt_num;
        attention.like = attention.noti_like_num;
        attention.notifyNumber = attention.noti_num;
        attention.onCallNow = attention.on_call_now;
        attention.unread = attention.unread_num;

        Object.keys(attention).forEach((key) => {
            this[key] = attention[key];
        });
    }
}
