import { timeAgo } from '../utils/helpers';

/**
 * @property {String} icon_status
 * @property {Number} flag
 * @property {Number} last_rank
 * @property {Number} current_rank
 * @property {String} is_first_time
 * @property {String} position
 * @property {String} last_login_time
 * @property {String} lastOnline
 */
export default class Ranking {
    constructor(attributes) {
        const ranking = { ...attributes };
        ranking.profile_url = `/profile/${ranking.user_id}`;
        ranking.lastOnline = timeAgo(ranking.last_login_time);

        if (ranking.is_first_time) {
            ranking.position = '<span class="icon icon-ranking-new"></span> NEW';
        } else if (ranking.last_rank > ranking.current_rank) {
            ranking.position = `<span class="icon icon-ranking-up"></span> 前回${ranking.last_rank}位`;
        } else if (ranking.last_rank < ranking.current_rank) {
            ranking.position = `<span class="icon icon-ranking-down"></span> 前回${ranking.last_rank}位`;
        } else {
            ranking.position = `<span class="icon icon-ranking-keep"></span>前回${ranking.last_rank}位`;
        }

        if (!ranking.video_call_waiting && ranking.voice_call_waiting) {
            ranking.icon_status = 'icon icon-phone-call-sm';
        } else if (ranking.video_call_waiting) {
            ranking.icon_status = 'icon icon-video-call-sm';
        } else {
            ranking.icon_status = 'icon icon-chats';
        }

        Object.keys(ranking).forEach((key) => {
            this[key] = ranking[key];
        });
    }
}
