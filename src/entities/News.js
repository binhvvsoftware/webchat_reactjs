import { addTargetLink } from '../utils/helpers';
/**
 * @property {String} banner_id
 * @property {String} news_id
 * @property {String} title
 * @property {String} content
 */
export default class News {
    constructor(attributes) {
        const news = { ...attributes };
        news.news_id = news.banner_id || news.news_id;
        news.content = addTargetLink(news.content);

        Object.keys(news).forEach((key) => {
            this[key] = news[key];
        });
    }
}
