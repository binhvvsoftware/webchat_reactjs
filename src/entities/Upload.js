/**
 * @property {String} image_id
 * @property {Number} is_app
 * @property {Boolean} isApprove
 */
export default class Upload {
    constructor(attributes) {
        const upload = { ...attributes };
        upload.isApprove = attributes.is_app !== 0;

        Object.keys(upload).forEach((key) => {
            this[key] = upload[key];
        });
    }
}
