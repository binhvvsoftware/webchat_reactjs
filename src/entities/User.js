import { AREA_REGION } from '../constants/region_constant';
import { MEDIA_URL, LOAD_IMG } from '../constants/endpoint_constant';
import { checkIsFemale, timeAgo } from '../utils/helpers';
import storage from '../utils/storage';

/**
 * @property {String} ava_id
 * @property {String} email
 * @property {String} fb_id
 * @property {String} user_name
 * @property {Boolean} isApprove
 * @property {Boolean} isFemale
 * @property {Boolean} hasEmail
 * @property {Number} unread_num
 * @property {String} login_bonus_img_id
 * @property {Boolean} turn_off_show_ranking_ios
 * @property {Boolean} joinRanking
 * @property {Boolean} isAlertNotice
 * @property {Boolean} isSystem
 * @property {Boolean} flag_warning_popup
 * @property {Number} update_email_flag
 * @property {Number} noit_read_time
 * @property {Number} checkout_num
 * @property {Number} noti_num
 * @property {Number} bckstg_num
 * @property {Number} buzz_number
 * @property {Number} checkout_num
 * @property {Number} fav_num
 * @property {Number} fvt_num
 * @property {Boolean} on_call_now
 * @property {Number} backStage
 * @property {Number} buzz
 * @property {Number} checkout
 * @property {Number} favorite
 * @property {Number} follow
 * @property {Number} myFootprint
 * @property {Number} newCheckout
 * @property {Number} newFavorite
 * @property {Number} like
 * @property {Number} notifyNumber
 * @property {Number} onCallNow
 * @property {Number} unread
 * @property {Boolean} video_call_waiting
 * @property {Boolean} voice_call_waiting
 */
export default class User {
    /**
     *
     * @param {Object|User} attributes
     */
    constructor(attributes) {
        const user = Object.assign({}, attributes || {});
        const token = storage.getToken();

        user.region_name = AREA_REGION[user.region] || '';
        user.profile_url = `/profile/${user.user_id}`;
        user.chat_url = `/chat/${user.user_id}`;
        user.avatar_url = user.ava_id ? `${MEDIA_URL}/api=${LOAD_IMG}&token=${token}&img_id=${user.ava_id}&img_kind=1` : (user.avatar_url || null);
        user.isApprove = !user.is_reviewed;
        user.isFemale = checkIsFemale(user.gender);
        user.hasEmail = user.update_email_flag !== 0;
        user.user_name = user.user_name || '';
        user.login_ago = timeAgo(user.last_login);
        user.time_like = timeAgo(user.time);
        user.isAlertNotice = !!user.is_alt;
        user.joinRanking = user.joinRanking !== undefined ? user.turn_off_show_ranking_ios : user.joinRanking;
        user.login_bonus_img_id = user.login_bonus_img_id || '';
        user.appeal_comment = user.appeal_comment || '';
        user.isFavorite = user.is_fav === 1;
        user.isSystem = user.sys_acc !== 0;

        user.backstage = user.bckstg_num;
        user.buzz = user.buzz_number;
        user.checkout = user.checkout_num;
        user.favorite = user.fav_num;
        user.follow = user.fvt_num;
        user.myFootprint = user.my_footprint_num;
        user.newCheckout = user.new_checkout_num;
        user.newFavorite = user.new_fvt_num;
        user.like = user.noti_like_num;
        user.notifyNumber = user.noti_num;
        user.onCallNow = user.on_call_now;
        user.unread = user.unread_num;

        if (!user.video_call_waiting && user.voice_call_waiting) {
            user.icon_status = 'icon icon-phone-call-sm';
        } else if (user.video_call_waiting) {
            user.icon_status = 'icon icon-video-call-sm';
        } else {
            user.icon_status = 'icon icon-chats';
        }

        Object.keys(user).forEach((key) => {
            this[key] = user[key];
        });
    }
}
