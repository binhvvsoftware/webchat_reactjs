import { MEDIA_URL } from '../constants/endpoint_constant';
import  storage from '../utils/storage';
import { timeAgo, numberFormat } from '../utils/helpers';
import {
    STICKER_TITLE,
    PHOTO_MESSAGE_FORMAT,
    VIDEO_MESSAGE_FORMAT,
    AUDIO_MESSAGE_FORMAT,
    MESSAGE_NOTIFICATION_WHEN_USER_VOICE_CALL,
    MESSAGE_NOTIFICATION_WHEN_USER_VIDEO_CALL,
    GIFT_MESSAGE_TEXT,
    VOIP_CANCEL,
} from '../constants/messages';

import { MessageTypes } from '../constants/types';

/**
 * @property {Boolean} isOwner
 * @property {String} avatar_url
 * @property {String} sentTime
 * @property {String} frd_name
 * @property {String} last_msg
 * @property {String} frd_id
 * @property {String} last_msg
 * @property {String} textMessage
 */
export default class Message {
    constructor(attributes) {
        const message = Object.assign({}, attributes || {});
        message.isOwner = message.is_own;
        message.avatar_url = message.ava_id ? `${MEDIA_URL}/api=load_img_with_size&token=${storage.getToken()}&img_id=${message.ava_id}&width_size=252` : (message.avatar_url || null);
        message.last_login = timeAgo(message.last_login);
        message.sentTime = timeAgo(message.sent_time);
        message.textMessage = message.last_msg || '';

        switch (message.msg_type) {
            case MessageTypes.STK:
                message.textMessage = STICKER_TITLE;
                break;
            case MessageTypes.FILE:
                message.textMessage = PHOTO_MESSAGE_FORMAT;
                break;
            case MessageTypes.SVIDEO:
                message.textMessage = VIDEO_MESSAGE_FORMAT;
                break;
            case MessageTypes.EVIDEO:
                message.textMessage = VIDEO_MESSAGE_FORMAT;
                break;
            case MessageTypes.EVOICE:
                message.textMessage = VOIP_CANCEL;
                break;
            case MessageTypes.SVOICE:
                message.textMessage = AUDIO_MESSAGE_FORMAT;
                break;
            case MessageTypes.PHOTO:
                message.textMessage = PHOTO_MESSAGE_FORMAT;
                break;
            case MessageTypes.CALLREQ:
                message.textMessage = (message.textMessage === 'voip_video' ? MESSAGE_NOTIFICATION_WHEN_USER_VIDEO_CALL : MESSAGE_NOTIFICATION_WHEN_USER_VOICE_CALL).replace('%s', message.frd_name);
                break;
            case MessageTypes.GIFT:
                message.textMessage = GIFT_MESSAGE_TEXT.replace('%d', numberFormat(message.gift.gift_price));
                break;
            default:
                break;
        }

        Object.keys(message).forEach((key) => {
            this[key] = message[key];
        });
    }
}
