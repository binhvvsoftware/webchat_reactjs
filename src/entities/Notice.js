import { NOTICE_TYPE } from '../constants/notice_type';
import { timeAgo } from '../utils/helpers';
import {
    NOTI_LIKE_YOUR_BUZZ,
    NOTIFICATION_SOMEONE,
    NOTI_RESPONSED_YOUR_BUZZ,
    NOTI_REPLY_YOUR_COMMENT,
    NOTI_ONLINE_ALERT,
    NOTI_EARNED_FROM_DAILY_BONUS_BEFORE,
    NOTI_EARNED_FROM_DAILY_BONUS_AFTER,
    NOTI_APPROVED_BUZZ,
    NOTI_APPROVED_BACKSTAGE,
    NOTI_FAVORITED_USERS_CREATE_BUZZ,
    MESSAGE_REQUEST_CALL_UPDATE,
    NOTI_DENIED_IMAGE_BUZZ,
    NOTI_DENIED_BACKSTAGE,
    NOTI_APPROVED_TEXT_BUZZ,
    NOTI_DENIED_TEXT_BUZZ,
    NOTI_APPROVED_COMMENT,
    NOTI_DENIED_COMMENT,
    NOTI_APPROVED_SUB_COMMENT,
    NOTI_DENIED_SUB_COMMENT,
    APPROVE_USER_INFOR_NOTI,
    DENIED_USER_INFOR_NOTI,
    APPROVE_APPEAL_COMMENT_NOTI,
    DENIED_APPEAL_COMMENT_NOTI,
    DENIED_FILE_ATTACHMENT,
    SET_WARNING_CALL_RECORD,
    RECEIVED_REAL_GIFT,
    NOTI_CONFIRM_GIFT,
    NOTIFICATION_CANCELED_REAL_GIFT,
} from '../constants/messages';
import { buyPoint } from '../services/webview';
import storage from '../utils/storage';

const getIcon = (type) => {
    switch (type) {
        case NOTICE_TYPE.LIKE_BUZZ:
            return 'icon_notification_like';
        case NOTICE_TYPE.APPROVED_TEXT_BUZZ:
        case NOTICE_TYPE.DENIED_TEXT_BUZZ:
        case NOTICE_TYPE.APPROVED_COMMENT:
        case NOTICE_TYPE.DENIED_COMMENT:
        case NOTICE_TYPE.APPROVED_SUB_COMMENT:
        case NOTICE_TYPE.DENIED_SUB_COMMENT:
        case NOTICE_TYPE.LIKE_OTHER_BUZZ:
        case NOTICE_TYPE.COMMENT_BUZZ:
        case NOTICE_TYPE.COMMENT_OTHER_BUZZ:
        case NOTICE_TYPE.SUB_COMMENT:
        case NOTICE_TYPE.FAVORITED_USER_CREATE_BUZZ:
            return 'icon_chat_notification';
        case NOTICE_TYPE.UNLOCK_BACKSTAGE:
        case NOTICE_TYPE.DAILY_BONUS:
            return 'icon_notification_point';
        case NOTICE_TYPE.ONLINE_ALERT:
        case NOTICE_TYPE.CALL_REQUEST:
        case NOTICE_TYPE.APPROVE_USER_INFO:
        case NOTICE_TYPE.DENIED_APART_OF_USER_INFO:
        case NOTICE_TYPE.DENIED_USER_INFO:
        case NOTICE_TYPE.APPROVED_APPEAL_COMMENT:
        case NOTICE_TYPE.DENIED_APPEAL_COMMENT:
            return 'icon_online_alert_notification';
        case NOTICE_TYPE.DENIED_BUZZ_IMAGE:
        case NOTICE_TYPE.DENIED_BACKSTAGE:
        case NOTICE_TYPE.APPROVED_BUZZ:
        case NOTICE_TYPE.APPROVED_BACKSTAGE:
            return 'icon_notification_picture';
        case NOTICE_TYPE.FROM_BACKEND:
            return 'icon_left_menu_notification';
        case NOTICE_TYPE.FILE_DENIED:
        case NOTICE_TYPE.MISSION_COMPLETE:
        case NOTICE_TYPE.WARNING_VIDEO_CALL:
        case NOTICE_TYPE.RECEIVED_REAL_GIFT:
        case NOTICE_TYPE.CONFIRM_RECEIVING_REAL_GIFT:
        case NOTICE_TYPE.CANCELED_REAL_GIFT:
            return 'icon_notification_from_backend';
        default:
            break;
    }

    return '';
};

const getTitle = (notice) => {
    const username = notice.noti_user_name || NOTIFICATION_SOMEONE;

    switch (notice.type) {
        case NOTICE_TYPE.LIKE_BUZZ:
            return `${ username }${ NOTI_LIKE_YOUR_BUZZ }`;
        case NOTICE_TYPE.COMMENT_BUZZ:
            return NOTI_RESPONSED_YOUR_BUZZ;
        case NOTICE_TYPE.SUB_COMMENT:
            return NOTI_REPLY_YOUR_COMMENT;
        case NOTICE_TYPE.ONLINE_ALERT:
            return `${ username }${ NOTI_ONLINE_ALERT }`;
        case NOTICE_TYPE.DAILY_BONUS:
            return `${ NOTI_EARNED_FROM_DAILY_BONUS_BEFORE}${(notice.point ? notice.point : '0')}${NOTI_EARNED_FROM_DAILY_BONUS_AFTER }`;
        case NOTICE_TYPE.APPROVED_BUZZ:
            return NOTI_APPROVED_BUZZ;
        case NOTICE_TYPE.APPROVED_TEXT_BUZZ:
            return NOTI_APPROVED_TEXT_BUZZ;
        case NOTICE_TYPE.APPROVED_BACKSTAGE:
            return NOTI_APPROVED_BACKSTAGE;
        case NOTICE_TYPE.FAVORITED_USER_CREATE_BUZZ:
            return `${ username }${ NOTI_FAVORITED_USERS_CREATE_BUZZ }`;
        case NOTICE_TYPE.FROM_BACKEND:
            return notice.content || '';
        case NOTICE_TYPE.CALL_REQUEST:
            return `${ username }${ MESSAGE_REQUEST_CALL_UPDATE }`;
        case NOTICE_TYPE.DENIED_BUZZ_IMAGE:
            return NOTI_DENIED_IMAGE_BUZZ;
        case NOTICE_TYPE.DENIED_TEXT_BUZZ:
            return NOTI_DENIED_TEXT_BUZZ;
        case NOTICE_TYPE.DENIED_BACKSTAGE:
            return NOTI_DENIED_BACKSTAGE;
        case NOTICE_TYPE.APPROVED_COMMENT:
            return NOTI_APPROVED_COMMENT;
        case NOTICE_TYPE.DENIED_COMMENT:
            return NOTI_DENIED_COMMENT;
        case NOTICE_TYPE.APPROVED_SUB_COMMENT:
            return NOTI_APPROVED_SUB_COMMENT;
        case NOTICE_TYPE.DENIED_SUB_COMMENT:
            return NOTI_DENIED_SUB_COMMENT;
        case NOTICE_TYPE.APPROVE_USER_INFO:
            return APPROVE_USER_INFOR_NOTI;
        case NOTICE_TYPE.DENIED_APART_OF_USER_INFO:
            return APPROVE_USER_INFOR_NOTI;
        case NOTICE_TYPE.DENIED_USER_INFO:
            return DENIED_USER_INFOR_NOTI;
        case NOTICE_TYPE.APPROVED_APPEAL_COMMENT:
            return APPROVE_APPEAL_COMMENT_NOTI;
        case NOTICE_TYPE.DENIED_APPEAL_COMMENT:
            return DENIED_APPEAL_COMMENT_NOTI;
        case NOTICE_TYPE.FILE_DENIED:
            return DENIED_FILE_ATTACHMENT;
        case NOTICE_TYPE.WARNING_VIDEO_CALL:
            return SET_WARNING_CALL_RECORD;
        case NOTICE_TYPE.RECEIVED_REAL_GIFT:
            return `${username}${RECEIVED_REAL_GIFT}`;
        case NOTICE_TYPE.CONFIRM_RECEIVING_REAL_GIFT:
            return `${username}${NOTI_CONFIRM_GIFT}`;
        case NOTICE_TYPE.CANCELED_REAL_GIFT:
            return NOTIFICATION_CANCELED_REAL_GIFT;
        default:
            break;
    }

    return '';
};

const buildLinkWebview = (notice) => {
    const backButton = true;

    switch (notice.type) {
        case NOTICE_TYPE.DAILY_BONUS:
            return {
                url: buyPoint(),
                backButton,
            };

        case NOTICE_TYPE.FROM_BACKEND:
            return {
                url: String(notice.url).replace('%%token%%', storage.getToken()),
                backButton,
            };

        default:
            return {};
    }
};

const buildPageUrl = (notice) => {
    let pathname = '/notification';
    let state = null;
    switch (notice.type) {
        case 12:
        case 30:
        case 31:
        case 32:
            pathname = `/profile/${notice.userId}`;
            break;
        case 53:
        case 11:
            pathname = `/chat/${notice.userId}`;
            break;
        case NOTICE_TYPE.UNLOCK_BACKSTAGE:
        case NOTICE_TYPE.APPROVED_BACKSTAGE:
        case NOTICE_TYPE.DENIED_BACKSTAGE:
            pathname = '/my-gallery';
            state = {
                imageId: notice.noti_image_id,
            };
            break;
        case NOTICE_TYPE.DENIED_APPEAL_COMMENT:
        case NOTICE_TYPE.APPROVED_APPEAL_COMMENT:
            pathname = '/my-page';
            break;
        case NOTICE_TYPE.DENIED_TEXT_BUZZ:
        case NOTICE_TYPE.DENIED_BUZZ_IMAGE:
            pathname = 'timeline';
            state = {
                typeTimeline: 'owner',
            }
            break;
        default:
            if (notice.buzzId) {
                pathname = `/buzz-detail/${notice.buzzId}`;
            }
            break;
    }

    return {
        pathname,
        state,
    };
};

/**
 * @property {String} noti_user_name
 * @property {String} noti_type
 * @property {String} userId
 * @property {String} buzzId
 * @property {String} url
 * @property {String} type
 * @property {String} time
 * @property {String} iconClass
 * @property {String} timeAgo
 * @property {Boolean} isWebView
 * @property {Object} webView
 * @property {String} pathname
 * @property {String} state
 */
export default class Notice {
    constructor(attributes) {
        const notice = Object.assign({}, attributes || {});
        notice.type = notice.noti_type;
        notice.userId = notice.userId || notice.noti_user_id || notice.userid;
        notice.buzzId = notice.buzzId || notice.noti_buzz_id || notice.buzzid;
        notice.timeAgo = timeAgo(notice.time);
        notice.title = getTitle(notice);
        notice.iconClass = getIcon(notice.type);
        notice.webView = buildLinkWebview(notice);
        notice.isWebView = notice.webView.url !== undefined;

        const page = buildPageUrl(notice);
        notice.pathname = page.pathname;
        notice.state = page.state;

        Object.keys(notice).forEach((key) => {
            this[key] = notice[key];
        });
    }
}
