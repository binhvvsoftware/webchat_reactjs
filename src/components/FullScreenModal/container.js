import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import debounce from 'lodash/debounce';

import './index.scss';

class FullScreenModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: props.isOpen,
        };

        this.handleCheckModalOpen = debounce(this.checkModalOpen, 200);
        this.modalRef = React.createRef();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.isOpen,
        }, this.handleCheckModalOpen);
    }

    componentDidUpdate() {
        this.handleCheckModalOpen();
    }

    componentWillUnmount() {
        this.handleCheckModalOpen();
    }

    handleClose = () => {
        this.setState({ open: false });
        this.props.onClose();
    };

    checkModalOpen = () => {
        const className = String(this.props.className).split(' ').map(name => `modal_${name}`);

        if (document.querySelectorAll('.full-screen.is_open').length) {
            document.body.classList.add('modal_open');
        } else {
            document.body.classList.remove('modal_open');
        }

        if(!className.length) {
            return;
        }

        if (this.modalRef.current) {
            const classList = Array.from(this.modalRef.current.classList);
            if (classList.indexOf('is_open') !== -1) {
                document.body.classList.add(...className);

                return;
            }
        }

        document.body.classList.remove(...className);
    }

    render() {
        const header = this.props.title !== null ? (
            <header className="k-header">
                <div className="nav-bar">
                    {this.props.hasBackButton ? <span className="nav-bar__icon nav-bar__icon--left" onClick={this.handleClose}><span className={ `icon  ${this.props.typeMenu ? 'icon-menu-left' : 'icon-arrow-left'}` }>&nbsp;</span></span> : null}
                    <span className="nav-bar__title">{this.props.title}</span>
                    {this.props.titleChildren}
                </div>
            </header>
        ) : null;

        const data = {
            "full-screen": true,
            is_open: this.state.open,
            is_hidden: !this.state.open,
            no_header: this.props.title === null,
        };
        data[this.props.className || ''] = true;

        const className = classNames(data);

        return (
            <div className={ className } ref={this.modalRef}>
                {header}
                <main className="full-screen__content">
                    {this.props.children}
                </main>
            </div>
        );
    }
}

FullScreenModal.propTypes = {
    isOpen: PropTypes.bool,
    hasBackButton: PropTypes.bool,
    title: PropTypes.string,
    typeMenu: PropTypes.bool,
    className: PropTypes.string,
    onClose: PropTypes.func,
    titleChildren: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
    ]),
    children: PropTypes.any,
};

FullScreenModal.defaultProps = {
    isOpen: false,
    hasBackButton: true,
    title: null,
    className: '',
    titleChildren: [],
    onClose: () => {},
    typeMenu: false,
};

export default FullScreenModal;
