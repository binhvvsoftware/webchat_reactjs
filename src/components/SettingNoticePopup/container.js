import React from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import ProfileService from '../../services/profile';
import SelectBox from '../SelectBox';
import FullScreenModal from '../FullScreenModal';
import Image from '../Image';
import { NOTICE, DATA_NOTICE } from '../../constants/notice_constant';
import { TITE_MESSAGE_SETTING_NOTICE, MESSAGE_SETTING_NOTICE_NO_ONLINE } from '../../constants/messages';
import './index.scss';

class SettingNoticePopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: props.isOpen,
            isLoading: true,
            userInfo: this.props.user,
            alertValue: 0,
            settingAlert: 0,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.isOpen,
            userInfo: nextProps.user,
        }, () => {
            if (!this.state.userInfo.user_id) {
                return;
            }

            ProfileService.getSettingNotice(this.state.userInfo.user_id)
                .then(response => {
                    this.setState({
                        isLoading: false,
                        settingAlert: response.data.is_alt,
                        alertValue: response.data.alt_fre,
                    });
                });
        })
    }

    handleClose = () => {
        this.setState({ open: false });
        this.props.onClose(this.state.settingAlert === 1);
    };

    handleSelectAlertValue = (alertValue) => {
        this.setState({ alertValue });
    };

    handleSettingAlert = (alertValue) => {
        if (alertValue.target.checked) {
            this.setState({ settingAlert: 1 });
        } else {
            this.setState({ settingAlert: 0 });
        }
    };

    handleSubmit(event) {
        event.preventDefault();
        const { user } = this.props;
        let messenge = '';
        if (this.state.settingAlert === 0)
        {
            messenge = this.state.userInfo.user_name + MESSAGE_SETTING_NOTICE_NO_ONLINE;
        } else {
            messenge = `${this.state.userInfo.user_name}さんがオンラインになる度に${DATA_NOTICE[this.state.alertValue] ? DATA_NOTICE[this.state.alertValue] : 0}通知します。`;
        }

        ProfileService.setSettingNotice(user.user_id, this.state.settingAlert || 0, this.state.alertValue || 0)
            .then(() => {
                Popup.create({
                    title : TITE_MESSAGE_SETTING_NOTICE,
                    content: messenge,
                    buttons: {
                        left: [{
                            text: 'はい',
                            className: 'ok',
                            action: () => {
                                Popup.close();
                                this.props.onClose(this.state.settingAlert === 1);
                            },
                        }],
                    },
                });
            });
    }

    render() {
        const enabledSelect = this.state.settingAlert === 1;
        const titleChildren = <button type="button" className="nav-bar__button" onClick={ (e) => this.handleSubmit(e) }>完了</button>;

        return (
            <FullScreenModal className="setting-notice" isOpen={this.state.open} title="通知設定" onClose={this.handleClose} titleChildren={titleChildren}>
                { this.state.isLoading && <div className="setting-notice__loading" /> }
                <div className="setting-notice__header">
                    <div className='user-avatar'>
                        <Image isAvatar imageId={this.state.userInfo.ava_id}/>
                    </div>
                    <div>
                        {this.state.userInfo.user_name}さんがオンラインになる度に 
                        <br/>プッシュ通知を受け取ることができます。
                    </div>
                </div>

                <div className="form__control">
                    <label className="form__label">通知設定</label>
                    <div className="form__group-input">
                        <label className="switch">
                            <input type="checkbox" className="switch__input" onChange={(e) => this.handleSettingAlert(e)} checked={this.state.settingAlert ? "checked" : ""}/>
                            <div className="switch__toggle">
                                <div className="switch__handle" />
                            </div>
                        </label>
                    </div>
                </div>

                <div className={ `form__control ${enabledSelect ? '' : 'is_disable'}` }>
                    <div className={ `form__label ${this.state.settingAlert ? 'label-active' : 'label-not-active'}` }>アラート回数</div>
                    <div className="form__group-input is_select">
                        <SelectBox options={NOTICE} textEmpty="いつも" hasEmpty={ false } selected={this.state.alertValue} onSelected={this.handleSelectAlertValue} enabled={enabledSelect}/>
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="text-note">
                    アプリを強制終了すると、<br/> 通知が遅れたり受信できない場合があります。
                </div>
            </FullScreenModal>
        );
    }
}

SettingNoticePopup.propTypes = {
    isOpen: PropTypes.bool,
    user: PropTypes.object,
    onClose: PropTypes.func,
};

SettingNoticePopup.defaultProps = {
    isOpen: false,
    user: {},
    onClose: () => {},
};


export default SettingNoticePopup;
