import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FullScreenModal from '../FullScreenModal';
import { AREA_REGION_GROUPS } from '../../constants/region_constant';
import './index.scss';

class AreaRegion extends Component {
    constructor(props) {
        super(props);
        let { selected } = props;
        if (props.selectMultiple  && !Array.isArray(selected)) {
            selected = [];
        }

        const displayNameSelectedAll = [];
        if (props.selectMultiple) {
            AREA_REGION_GROUPS.map((group) => {
                displayNameSelectedAll[group.name] = '選択を解除';
                Object.keys(group.region).forEach((key) => {
                    if (selected.indexOf(key) === -1) {
                        displayNameSelectedAll[group.name] = 'まとめて選択';
                    }
                });
                return true;
            });
        }

        this.state = {
            selected,
            displayNameSelectedAll,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: nextProps.selected,
        });
    }

    handleClose = () => {
        this.props.onSelected(this.state.selected);
    };

    handleChecked = (value, group) => () => {
        let selectedValue = value;
        if (this.props.selectMultiple) {
            selectedValue = this.state.selected;
            const index = selectedValue.indexOf(value);
            if (index === -1) {
                selectedValue.push(value);
            } else {
                selectedValue.splice(index, 1);
            }
        }
        const { displayNameSelectedAll } = this.state;

        displayNameSelectedAll[group.name] = '選択を解除';
        Object.keys(group.region).forEach((key) => {
            if (selectedValue.indexOf(key) === -1) {
                displayNameSelectedAll[group.name] = 'まとめて選択';
            }
        });

        this.setState({ selected: selectedValue, displayNameSelectedAll});
    };

    handleCheckAllGroup = (group) => () => {
        const { selected, displayNameSelectedAll } = this.state;
        displayNameSelectedAll[group.name] = '選択を解除';
        const unCheck = [];
        Object.keys(group.region).forEach((key) => {
            if (selected.indexOf(key) === -1) {
                unCheck.push(key);
            }
        });

        if (unCheck.length) {
            unCheck.forEach((key) => {
                selected.push(key);
            });
        } else {
            Object.keys(group.region).forEach((key) => {
                selected.splice(selected.indexOf(key), 1);
            });
        }

        Object.keys(group.region).forEach((key) => {
            if (selected.indexOf(key) === -1) {
                displayNameSelectedAll[group.name] = 'まとめて選択';
            }
        });

        this.setState({ selected, displayNameSelectedAll});
    };

    render() {
        const groups = AREA_REGION_GROUPS.map((group, index) => {
            const list = [];
            Object.keys(group.region).forEach(key => {
                const id =  `region_${Math.random().toString(36).substring(2, 15)}${key}`;
                let checked = String(this.state.selected) === String(key);
                if (this.props.selectMultiple) {
                    checked = (this.state.selected || []).indexOf(key) !== -1;
                }

                list.push(
                    <label className="region-group__item" key={id}>
                        {group.region[key]}
                        <input className="radio-button" type={this.props.selectMultiple ? 'checkbox' : 'radio'} id={id} name="region" value={key} onChange={ this.handleChecked(key, group) } checked={checked}/>
                        <label htmlFor={id} className="region-group__checkbox">
                            <span className="radio-button__icon" />
                        </label>
                    </label>
                );
            });

            return (
                <div className="region-group" key={index}>
                    <div className="region-group__title">
                        {group.name}
                        {this.props.selectMultiple ? <button type="button" className="region-group__check-all" onClick={this.handleCheckAllGroup(group)}>{this.state.displayNameSelectedAll[group.name]}</button> : null}
                    </div>
                    <div className="region-group__content">
                        {list}
                    </div>
                </div>
            );
        });

        return (
            <FullScreenModal className="region-area" isOpen={this.props.isOpen} title="地域選択" onClose={this.handleClose}>
                <main className="region-area__content">
                    {groups}
                </main>
            </FullScreenModal>
        );
    }
}

AreaRegion.propTypes = {
    selected: PropTypes.any,
    selectMultiple: PropTypes.bool,
    isOpen: PropTypes.bool,
    onSelected: PropTypes.func,
};

AreaRegion.defaultProps = {
    isOpen: false,
    selectMultiple: false,
    onSelected: () => {},
};


export default AreaRegion;
