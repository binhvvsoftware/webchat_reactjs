import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import {
    ADD_TO_FAVORITES_ALERT_MESSAGE,
    ADD_TO_FAVORITES_ALERT_TITLE, ALERT_BUTTON_NO_TITLE,
    ALERT_BUTTON_YES_TITLE, REMOVE_FAVORITE_ALERT_MESSAGE, REMOVE_FAVORITE_ALERT_TITLE,
} from '../../constants/messages';
import BuzzService from '../../services/timeline';
import { openGiftPool } from '../../actions/Chat';
import { openFavoriteLoading, closeFavoriteLoading } from '../../actions/favorite';

class Favorite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
        };
    }

    handleFavorite = () => {
        const { user } = this.props;

        let title = ADD_TO_FAVORITES_ALERT_TITLE;
        let message = ADD_TO_FAVORITES_ALERT_MESSAGE.replace(/%s/gi, user.user_name);
        let buttons = {
            left: [{
                text: ALERT_BUTTON_NO_TITLE,
                className: 'ok',
                action: () => {
                    Popup.close();
                },
            }],
            right: [{
                text: ALERT_BUTTON_YES_TITLE,
                className: 'ok',
                action: () => {
                    Popup.close();
                    this.props.openGiftPool({
                        you: {
                            id: this.props.user.user_id,
                            data: this.props.user,
                        },
                    });
                },
            }],
        };

        if (user.isFavorite) {
            title = REMOVE_FAVORITE_ALERT_TITLE;
            message = REMOVE_FAVORITE_ALERT_MESSAGE.replace(/%s/gi, user.user_name).replace(/%s/gi, user.user_name);
            buttons = {
                left: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
            };
        }
        this.props.openFavoriteLoading();

        BuzzService.favorite(user.user_id, user.isFavorite)
            .then(() => {
                user.isFavorite = !user.isFavorite;
                this.setState({
                    user,
                });

                Popup.create({
                    title,
                    content: message,
                    buttons,
                });

                this.props.favoriteCallback(user);
            })
            .catch(e => e)
            .then(this.props.closeFavoriteLoading);
    }

    render() {
        const { className, classNameActive } = this.props;
        return (
            <div className={ this.state.user.isFavorite ? classNameActive : className } onClick={this.props.isMyUser ? null : this.handleFavorite}>
                { this.props.children }
            </div>
        )
    }
}

Favorite.propTypes = {
    user: PropTypes.object,
    className: PropTypes.string,
    classNameActive: PropTypes.string,
    children: PropTypes.any,
    favoriteCallback: PropTypes.func,
    openGiftPool: PropTypes.func.isRequired,
    openFavoriteLoading: PropTypes.func.isRequired,
    closeFavoriteLoading: PropTypes.func.isRequired,
    isMyUser : PropTypes.bool,
};

Favorite.defaultProps = {
    className: '',
    classNameActive: '',
    isMyUser : false,
    favoriteCallback: () => {},
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
    openGiftPool: (payload) => dispatch(openGiftPool(payload)),
    openFavoriteLoading: () => dispatch(openFavoriteLoading()),
    closeFavoriteLoading: () => dispatch(closeFavoriteLoading()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Favorite);


