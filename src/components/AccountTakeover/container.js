import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native-web';
import Popup from 'react-popup';
import FullScreenModal from '../FullScreenModal'
import Loading from '../Loading';
import AuthService from '../../services/authentication'
import {
    OLD_PASSWORD_IS_EMPTY_MESSAGE,
    CHANGE_EMAIL_SUCCESS_MESSAGE,
    CHANGE_PASSWORD_EMAIL_SUCCESS_MESSAGE,
    EMAIL_IS_EMPTY_MESSAGE,
    EMAIL_IS_WRONG_MESSAGE,
    EMAIL_IS_TOO_LONG_MESSAGE,
    RETYPE_PASSWORD_IS_NOT_THE_SAME,
    PASSWORD_OUT_OF_RANGE,
    REGISTER_EMAIL_PASS_SUCCESS_MSG,
    ALERT_TEXT_DONT_VIEW,
    CHANGE_PASSWORD_EMAIL_ERROR_MESSAGE,
    REGISTER_FAILD_EMAIL_HAS_BEEN_USED,
    PASSWORD_IS_EMPTY_MESSAGE,
    TABLE_CELL_PASSWORD_TITLE,
} from '../../constants/messages';
import storage from '../../utils/storage'
import { checkEmail } from '../../utils/helpers';
import './index.scss';

class AccountTakeover extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            openHandover: false,
            handoverData: {
                currentPwd: '',
                newPwd: '',
                confirmPwd: '',
                email: '',
            },
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            openHandover: nextProps.isOpen,
            handoverData: {
                currentPwd: '',
                newPwd: '',
                confirmPwd: '',
                email: '',
            },
        });
    }

    handleClose = () => {
        this.props.onClose(this.props.hasEmail);
        this.setState({
            handoverData: {
                currentPwd: '',
                newPwd: '',
                confirmPwd: '',
                email: '',
            },
        })
    }

    handleChangeHandoverContent = (key, text) => {
        const { handoverData } = this.state;
        handoverData[key] = key === 'email' ? text.trim() : text;

        this.setState({
            handoverData,
        });
    }

    handleSubmitHandover = () => {
        const { handoverData } = this.state;
        if (this.props.hasEmail) {
            if (handoverData.currentPwd.length === 0) {
                return Popup.alert(OLD_PASSWORD_IS_EMPTY_MESSAGE);
            }
        } else if (handoverData.email.length === 0 ) {
            return Popup.alert(EMAIL_IS_EMPTY_MESSAGE);
        } else if (handoverData.newPwd.length === 0){
            return Popup.alert(PASSWORD_IS_EMPTY_MESSAGE);
        } else if  (handoverData.confirmPwd.length === 0){
            return Popup.alert(RETYPE_PASSWORD_IS_NOT_THE_SAME);
        }


        if (handoverData.email.length && !checkEmail(handoverData.email)) {
            return Popup.alert(EMAIL_IS_WRONG_MESSAGE);
        }

        if (handoverData.email.length > 150) {
            return Popup.alert(EMAIL_IS_TOO_LONG_MESSAGE);
        }

        if (handoverData.newPwd !== handoverData.confirmPwd) {
            return Popup.alert(RETYPE_PASSWORD_IS_NOT_THE_SAME);
        }

        if (!this.props.hasEmail || handoverData.newPwd.length > 0) {
            if (handoverData.newPwd.length < 6 || handoverData.newPwd.length > 12) {
                return Popup.alert(PASSWORD_OUT_OF_RANGE);
            }
        }

        let apiFunc = 'registerEmailAndPassword';
        let message = REGISTER_EMAIL_PASS_SUCCESS_MSG;
        let params = [handoverData.email, handoverData.newPwd];

        if (this.props.hasEmail) {
            apiFunc = 'changePassword';
            message = CHANGE_PASSWORD_EMAIL_SUCCESS_MESSAGE;
            params = [handoverData.email, handoverData.newPwd, handoverData.currentPwd];
            if (handoverData.email.length && handoverData.newPwd.length) {
                apiFunc = 'changeMailPassword';
                message = CHANGE_PASSWORD_EMAIL_SUCCESS_MESSAGE;
            } else if (handoverData.email.length) {
                apiFunc = 'changeMail';
                message = CHANGE_EMAIL_SUCCESS_MESSAGE;
                params = [handoverData.email, handoverData.currentPwd];
            }
        }

        return this.setState({ loading: true }, () => {
            AuthService[apiFunc](...params)
                .then(() => {
                    Popup.create({
                        content: message,
                        buttons: {
                            left: [{
                                text: ALERT_TEXT_DONT_VIEW,
                                className: 'ok',
                                action: () => {
                                    const user = storage.getUserInfo();
                                    user.hasEmail = true;
                                    storage.setUserInfo(user);
                                    Popup.close();
                                    this.props.onClose(true);
                                },
                            }],
                        },
                    });

                    this.setState({ loading: false });
                })
                .catch((errors) => {
                    if (errors.code !== 3) {
                        Popup.alert(errors.code === 12 ? REGISTER_FAILD_EMAIL_HAS_BEEN_USED : CHANGE_PASSWORD_EMAIL_ERROR_MESSAGE);
                    }

                    this.setState({ loading: false });
                });
        });
    }

    render() {
        const user = storage.getUserInfo();
        const handoverButton = <button type="button" className="nav-bar__button" onClick={this.handleSubmitHandover}>完了</button>;

        return (
            <FullScreenModal className="account-takeover" title="アカウントデータ引継ぎ" titleChildren={handoverButton} isOpen={this.state.openHandover} onClose={this.handleClose}>
                <Loading isLoading={ this.state.loading }/>
                <div>
                    <div className="form__control">
                        <div className="form__label text-black">{user.email}</div>
                    </div>

                    <div className="form__control">
                        <div className="form__group-input no_icon-right">
                            <TextInput
                                value={this.state.handoverData.currentPwd}
                                placeholder="現在のパスワード"
                                secureTextEntry
                                onChangeText={ (text) => this.handleChangeHandoverContent('currentPwd', text) } />
                        </div>
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__group-input no_icon-right">
                        <TextInput
                            value={this.state.handoverData.email}
                            placeholder='メールアドレス'
                            onChangeText={ (text) => this.handleChangeHandoverContent('email', text) } />
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__group-input no_icon-right">
                        <TextInput
                            value={this.state.handoverData.newPwd}
                            placeholder={TABLE_CELL_PASSWORD_TITLE}
                            secureTextEntry
                            onChangeText={ (text) => this.handleChangeHandoverContent('newPwd', text) } />
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__group-input no_icon-right">
                        <TextInput
                            value={this.state.handoverData.confirmPwd}
                            placeholder="パスワードの確認"
                            secureTextEntry
                            onChangeText={ (text) => this.handleChangeHandoverContent('confirmPwd', text) } />
                    </div>
                </div>

                <div className="text-note">
                    <p>
                        端末変更やアプリをアンインストールしてしまった場合、新しいアカウントを登録しなおす必要がございます。
                    </p>
                    <p>
                        メールアドレスを登録しておくことで、現在ご利用のアカウントをそのままお使いいただく事が可能となります。また、複数の端末からも利用する事が可能です。
                    </p>
                    <p>
                        是非、ご登録くださいませ。
                    </p>
                </div>
            </FullScreenModal>
        );
    }
}

AccountTakeover.propTypes = {
    isOpen: PropTypes.bool,
    onClose: PropTypes.func,
    hasEmail: PropTypes.bool,
};

AccountTakeover.defaultProps = {
    isOpen: false,
    hasEmail: false,
    onClose: () => {},
};

export default AccountTakeover;
