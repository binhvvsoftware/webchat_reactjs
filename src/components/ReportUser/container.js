import React from 'react';
import { connect } from "react-redux";
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import { REPORT } from '../../constants/notice_constant';
import { toggleReportUserDialog } from '../../actions/Utility/index'
import ProfileService from '../../services/profile';
import {
    TITLE_REPORT_MESSAGE,
    SUCCESS_CONTENT_REPORT_MESSAGE,
} from '../../constants/messages';
import './index.scss';

class ReportUser extends React.Component {

    componentWillUnmount() {
        if (this.props.isOpenReportUserDialog) {
            this.props.toggleReportUserDialog()
        }
    }

    handleReportUser(typeReport) {
        ProfileService.profileReport(this.props.userId, typeReport)
            .then(() => {
                this.props.toggleReportUserDialog();
                if (typeReport !== -1) {
                    Popup.create({
                        title: TITLE_REPORT_MESSAGE,
                        content: SUCCESS_CONTENT_REPORT_MESSAGE,
                        buttons: {
                            right: [{
                                text: 'はい',
                                className: 'ok',
                                action: () => {
                                    Popup.close();
                                },
                            }],
                        },
                    });
                }
            }).catch(err => err)
    }

    render() {
        if (this.props.isMyUser) {
            return '';
        }
        return (
            <Dialog
                open={this.props.isOpenReportUserDialog}
                onClose={() => this.props.toggleReportUserDialog()}
            >
                <div className="profile-report">
                    {REPORT.map((ireport, i) =>
                        <p className='profile-report-list' key={i} onClick={() => this.handleReportUser(ireport.value)}>{ireport.label}</p>
                    )}
                </div>
            </Dialog>
        );
    }
}

ReportUser.propTypes = {
    isOpenReportUserDialog: PropTypes.bool.isRequired,
    toggleReportUserDialog: PropTypes.func.isRequired,
    userId: PropTypes.string,
    isMyUser: PropTypes.bool,
};

ReportUser.defaultProps = {
    isMyUser: false,
};

const mapStateToProps = (state) => ({
    isOpenReportUserDialog: state.utilityControl.isOpenReportUserDialog,
})
const mapDispatchToProps = (dispatch) => ({
    toggleReportUserDialog: () => dispatch(toggleReportUserDialog()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ReportUser);
