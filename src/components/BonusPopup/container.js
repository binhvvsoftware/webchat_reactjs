import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Slider from "react-slick";
import moment from 'moment/moment';
import { getLocationHistory } from '../../utils/history';
import FullScreenModal  from '../FullScreenModal';
import News from '../News';
import Image from '../Image';
import storage from '../../utils/storage';
import tutorialView1 from '../../statics/images/tutorial.png';
import tutorialView2 from '../../statics/images/Firstview2.png';
import tutorialView3 from '../../statics/images/Firstview3.png';
import tutorialViewCancel from '../../statics/images/Firstview4.png';
import TutorialService from '../../services/tutorial';
import tutorialStep1 from '../../statics/images/tutorial-step1-content.png';
import tutorialStep1Item from '../../statics/images/thumbnail.png';
import tutorialStep2 from '../../statics/images/tutorial-step2-content.png';
import tutorialStep3 from '../../statics/images/tutorial-step3-content.png';
import tutorialGetPoint from '../../statics/images/get-point-tutorial.png';
import tutorialFinish from '../../statics/images/tutorial-finish.png';
import bgProfile from '../../statics/images/tutorial-step2-bg.jpg';
import bgChat from '../../statics/images/tutorial-step3-bg.jpg';
import btnProfile from '../../statics/images/Group10.png';
import './index.scss';
import SearchService from '../../services/search';
import { freePoint } from '../../services/webview';
import { HIDE_NEWS, USER_CLOSE_NEWS } from '../../constants/storage_constant';
import { openWebView } from '../../actions/webview';

const SHOW_TUTORIAL_HOME = 'tutorial_home';

class BonusPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPopupBonus: false,
            bonusImgId: '',
            showCancelTutorial: false,
            showTutorialStep: [false, false, false, false],
            textChat: '',
            showTutorialGetPoint: false,
            finishTutorial: false,
            isShowPopupNews: false,
            news: [],
        };
    }

    componentDidMount() {
        const user = storage.getUserInfo();
        if (!user || !user.user_id) {
            return;
        }

        const newsData = storage.get(HIDE_NEWS) || {};
        let registerDate = '';
        try {
            registerDate = moment(user.reg_date, "YYYYMMDDHHmmss").format("YYYY-MM-DD").toString();
        } catch (e) {
            //
        }

        const currentDate = moment().format('YYYY-MM-DD');
        const userCloseNews = storage.get(USER_CLOSE_NEWS) || false;
        if (currentDate !== registerDate && (!newsData || user.user_id !== newsData.user_id || currentDate !== newsData.date) && !userCloseNews) {
            SearchService.getNews()
                .then(response => {
                    this.setState({
                        news: response.data,
                    });
                    this.checkNews();
                });
        }

        const location = getLocationHistory();
        const showBonus = ['/login', '/sign-in', '/register-profile', '/reset-password'].indexOf(location.previous.pathname) !== -1;
        let showTutorial = Array.isArray(user.accessed_pages) && user.accessed_pages.indexOf(1) === -1 && storage.get(SHOW_TUTORIAL_HOME) !== false;
        if (location.previous.pathname === '/register-profile') {
            showTutorial = true;
        }

        this.setState({
            bonusImgId: showBonus ? user.login_bonus_img_id : '',
            showTutorialStep: [showTutorial, false, false, false],
        }, this.checkBonus);
    }

    checkBonus = () => {
        const { bonusImgId, showTutorialStep, showCancelTutorial, showTutorialGetPoint, finishTutorial } = this.state;
        let show = bonusImgId.length !== 0 && !showCancelTutorial && !showTutorialGetPoint && !finishTutorial;

        showTutorialStep.forEach((item) => {
            show = !item && show;
        });

        if (show) {
            this.setState({ showPopupBonus: show }, this.checkNews);
            return;
        }

        this.checkNews();
    }

    checkNews = () => {
        const { showPopupBonus, showTutorialStep, showCancelTutorial, showTutorialGetPoint, finishTutorial, news } = this.state;
        let show = !showPopupBonus && !showCancelTutorial && !showTutorialGetPoint && !finishTutorial;
        showTutorialStep.forEach((item) => {
            show = !item && show;
        });

        if (show && news.length) {
            this.setState({ isShowPopupNews: true });
        }
    }

    handleCloseModal = () => {
        this.setState({ showPopupBonus: false }, this.checkNews);
    }

    handleCancelTutorial() {
        this.setState({
            showTutorialStep: [false, false, false, false],
            showCancelTutorial: true,
        });
    }

    closeTutorial() {
        TutorialService.closeTutorial(1)
            .then(() => {
                storage.set(SHOW_TUTORIAL_HOME, false)
            });

        this.setState({
            showCancelTutorial: false,
        }, this.checkBonus);
    }

    handleNextTutorial(step) {
        const { showTutorialStep, showTutorialGetPoint } = this.state;
        showTutorialStep[step] = true;
        let show = showTutorialGetPoint;
        if (showTutorialStep[step - 1]) {
            showTutorialStep[step - 1] = false;
        }

        if (step === 4) {
            show = true;
            showTutorialStep[step] = false;
        }

        this.setState({
            showTutorialStep,
            showTutorialGetPoint: show,
        });
    }

    handleTutorialChat(e) {
        this.setState({
            textChat: e.target.value.replace(/\s/g, ''),
        });
    }

    getPointTutorial() {
        this.setState({
            showTutorialGetPoint: false,
            finishTutorial: true,
        });
    }

    finishTutorial() {
        this.setState({
            finishTutorial: false,
        });
        this.closeTutorial();
    }

    handleCloseNews = () => {
        this.setState({
            isShowPopupNews: false,
        });

        storage.set(USER_CLOSE_NEWS, true);
    };

    handleNotShowNews(e) {
        if (e.target.checked) {
            const infoUser = storage.getUserInfo();
            storage.set(HIDE_NEWS, { user_id: infoUser.user_id, date: moment().format('YYYY-MM-DD')});
        } else {
            storage.remove(HIDE_NEWS);
        }
    }

    handleOpenWebView = (url, title) => {
        this.setState({
            showTutorialGetPoint : false,
        })
        this.props.openWebView({
            url,
            title,
            backButton: true,
        });
        this.finishTutorial();
    }

    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
        };

        return (
            <div className="bonus-popup-wrapper">
                {
                    !!this.state.bonusImgId.length  && <Image className="hidden" imgKind={6} imageId={this.state.bonusImgId} alt="Load image bonus" />
                }

                <FullScreenModal className="bonus-popup" isOpen={this.state.showPopupBonus}>
                    <div className="bonus-popup__content login-bonus-popup">
                        <div style={{position:"relative"}}>
                            {this.state.bonusImgId.length && <Image className="bonus-popup__img" imgKind={6} imageId={this.state.bonusImgId} />}
                            <button onClick={this.handleCloseModal} type="button" className="bonus-popup__btn">
                                OK
                            </button>
                        </div>
                    </div>
                </FullScreenModal>

                <FullScreenModal className='bonus-popup tutorial' isOpen={this.state.showTutorialStep[0]}>
                    <div className="bonus-popup__content">
                        <Slider {...settings}>
                            <div >
                                <img className="bonus-popup__img" src={ tutorialView1 } alt=""/>
                            </div>
                            <div >
                                <img className="bonus-popup__img" src={ tutorialView2 } alt=""/>
                            </div>
                            <div>
                                <img className="bonus-popup__img " src={ tutorialView3 } alt=""/>
                                <button type="button" className="button-next-tutorial" onClick={() => this.handleNextTutorial(1)}>はじめる</button>
                                <button type="button" className="button-cancel-tutorial" onClick={()=> this.handleCancelTutorial()}>X</button>
                            </div>
                        </Slider>
                    </div>
                </FullScreenModal>

                <FullScreenModal className="bonus-popup" isOpen={this.state.showCancelTutorial}>
                    <div className="bonus-popup__content">
                        <div style={{position:"relative"}}>
                            <img className="bonus-popup__img" src={ tutorialViewCancel } alt=""/>
                            <Link to="/"><button type="button" className="btn-get-point">はじめる</button></Link>
                        </div>
                        <button type="button" className="button-cancel-tutorial" onClick={()=> this.closeTutorial()}>X</button>
                    </div>
                </FullScreenModal>

                <FullScreenModal className="bonus-popup" isOpen={this.state.showTutorialStep[1]}>
                    <div className="bonus-popup__content">
                        <div className="tutorial-step-1">
                            <img className="img-step-1_2" src = {tutorialStep1Item} alt="" onClick={() => this.handleNextTutorial(2)}/>
                            <img className="bonus-popup__img img-step-1_1" src={ tutorialStep1 } alt="" />
                        </div>
                    </div>
                </FullScreenModal>
                { this.state.showTutorialStep[2] &&
                    (<div className='background-tutorial-step2'>
                        <img className="bg-step2" src={ bgProfile } alt="" />
                        <img className="btn-step2" src={ btnProfile } alt="" />
                    </div>)
                }

                <FullScreenModal className="bonus-popup tutorial-step-2" isOpen={ this.state.showTutorialStep[2] }>
                    <div className="bonus-popup__content">
                        <img className="bonus-popup__img img-step-2_1" src={ tutorialStep2 } alt=""/>
                        <div className="tutorial-step2-btn">
                            <div className="step2-btn step2-pink-btn" onClick={ () => this.handleNextTutorial(3) }>
                                <span className="icon icon-chat-lg"></span>
                            </div>
                            <div className="step2-btn step2-blue-btn">
                                <span className="icon icon-call"></span>
                            </div>
                            <div className="step2-btn step2-blue-btn">
                                <span className="icon icon-video-call-lg"></span>
                            </div>
                        </div>
                    </div>
                </FullScreenModal>

                { (this.state.showTutorialStep[3] || this.state.showTutorialGetPoint) &&
                    (<div className='background-tutorial-step2'>
                        <img className="bg-step2" src={ bgChat } alt="" />
                    </div>)
                }

                <FullScreenModal className="bonus-popup tutorial-step-3" isOpen={this.state.showTutorialStep[3]}>
                    <div className="bonus-popup__content">
                        <img className="bonus-popup__img img-step-3_1" src={ tutorialStep3 } alt="" />
                        <div className="bottom-box">
                            <div className="bottom-box__item iconPlus">
                                <span className="icon icon-plus"></span>
                            </div>
                            <div className="bottom-box__item iconSmile">
                                <span className="icon icon-smile"></span>
                            </div>
                            <input type='text' onChange={(e) => this.handleTutorialChat(e)} />
                            { this.state.textChat.length !== 0 ? <span className="cursor-pointer icon icon-send iconDefault" onClick={() => this.handleNextTutorial(4) }/> : <span className="icon icon-tutorialStep3 iconActive"></span>}
                        </div>
                    </div>
                </FullScreenModal>

                <FullScreenModal className="bonus-popup" isOpen={this.state.showTutorialGetPoint}>
                    <div className="bonus-popup__content tutorial-get-point">
                        <div style={{position:"relative"}}>
                            <img className="bonus-popup__img" src={ tutorialGetPoint } alt="" />
                            <button type="button" className="btn-get-point" onClick={() => this.getPointTutorial()}>GETする</button>
                        </div>
                    </div>
                </FullScreenModal>

                <FullScreenModal className="bonus-popup" isOpen={this.state.finishTutorial}>
                    <div className="bonus-popup__content tutorial-get-point">
                        <div style={{position:"relative"}}>
                            <img className="bonus-popup__img" src={ tutorialFinish } alt="" />
                            <button type="button" className="btn-finish-tutorial" onClick={ () => this.handleOpenWebView(freePoint()) }>別の手段でGETする</button>
                        </div>
                        <button type="button" className="button-cancel-tutorial" onClick={()=> this.finishTutorial()}>X</button>
                    </div>
                </FullScreenModal>

                <FullScreenModal className="news-popup" isOpen={this.state.isShowPopupNews}>
                    <div className="news-popup__wrapper">
                        <div className="news-popup__header">お知らせ</div>
                        <div className="news-popup__content">
                            <News news={this.state.news} />
                        </div>
                        <div className="news-popup__footer">
                            <label className="news-popup__checkbox">
                                <input type="checkbox" value="1" onChange={(e) => this.handleNotShowNews(e)} />
                                今日は表示しない
                            </label>

                            <button type="button" className="btn-custom btn-active news-popup-ok"  onClick={this.handleCloseNews}>OK</button>
                        </div>
                    </div>
                </FullScreenModal>
            </div>
        );
    }
}

BonusPopup.propTypes = {
    openWebView: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(null, mapDispatchToProps)(BonusPopup);
