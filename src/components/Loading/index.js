import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native-web';

class Loading extends Component {
    render() {
        return (
            this.props.isLoading ?
                (
                    <div className="loading-wrapper">
                        <ActivityIndicator animating color="white" size="large" className="loading-icon" />
                    </div>
                )
                :
                null
        );
    }
}

Loading.propTypes = {
    isLoading: PropTypes.bool,
};


Loading.defaultProps = {
    isLoading: false,
};

export default Loading;
