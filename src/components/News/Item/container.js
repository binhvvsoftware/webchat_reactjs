import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Image from '../../Image';
import { openWebView } from '../../../actions/webview';

class NewsItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showContent: false,
        };
    }

    handleShowItem = () => {
        const { showContent } = this.state;
        this.setState({ showContent: !showContent });
    };

    handleClickLink = (event) => {
        event.preventDefault();
        try {
            this.props.onOpenWebView({
                url: event.target.getAttribute('href'),
                backButton: true,
            });
        } catch (error) {
            //
        }
    }

    componentDidMount = () => {
        const tags = document.querySelectorAll('.news__content a');

        tags.forEach((item) => {
            if (!item.getAttribute('data-add-event')) {
                item.addEventListener('click', this.handleClickLink, true);
                item.setAttribute('data-add-event', 'true');
            }
        })
    }

    render() {
        const { news } = this.props;
        const banner = news.banner_id ? (
            <div className="article-image">
                <Image imageId={ news.banner_id } imgKind={ 6 } alt=''/>
            </div>
        ) : (<div className="article-image"/>);

        return (
            <div className="article-content">
                { banner }
                <div className="article-status">
                    <h3 className="text-center">{ news.title }</h3>
                    <div className={ `news__content ${this.state.showContent ? '' : 'hide'}` } dangerouslySetInnerHTML={ { __html: news.content } }/>
                    <span className="news__show-hide" onClick={ () => this.handleShowItem() }>
                        <i className={ `icon ${this.state.showContent ? 'icon-close' : 'icon-view'}` }/>
                        { this.state.showContent ? '閉じる' : '詳畑を見る' }
                    </span>
                </div>
            </div>
        );
    }
}

NewsItem.propTypes = {
    news: PropTypes.object.isRequired,
    onOpenWebView: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
    onOpenWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsItem);
