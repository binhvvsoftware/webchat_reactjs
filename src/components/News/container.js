import React from 'react';
import PropTypes from 'prop-types';
import NewsItem from './Item';
import './index.scss';

class News extends React.Component {
    render() {
        const news = this.props.news.map(item => <NewsItem news={item} key={item.news_id} />);

        return (
            <div className="news__list">
                {news}
            </div>
        )
    }
}

News.propTypes = {
    news: PropTypes.array,
};

News.defaultProps = {
    news: [],
};

export default News;
