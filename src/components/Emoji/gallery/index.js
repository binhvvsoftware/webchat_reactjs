import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FullScreenModal from '../../FullScreenModal';
import ButtonDownload from '../../ButtonDownload';
import Image from '../../Image';
import './index.scss';

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: props.isOpen,
            isOpenPreview: false,
            previewIndex: 0,
            images: props.images,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isOpen: nextProps.isOpen,
            images: nextProps.images,
            isOpenPreview: false,
        });
    }

    handleOpenPreview = (index) => {
        this.setState({ isOpenPreview : true, previewIndex: index });
    }

    handleClosePreview = () => {
        this.setState({ isOpenPreview : false, previewIndex: 0 });
        this.props.onClose();
    }

    render() {
        const { isOpen, isOpenPreview, previewIndex } = this.state;
        let { images } = this.state;
        images = Array.isArray(images) ? images : [];
        const gallery = images.map((item, index) => (
            <div className={ `chat-gallery__item ${item.is_unlock ? '' : 'is_lock'}` } key={item.img_id} onClick={() => this.handleOpenPreview(index)}>
                <Image className="chat-gallery__img" imageId={item.img_id} imgKind="1" isBackground />
            </div>
        ));

        const image = images[previewIndex] || {};
        const title = `${previewIndex + 1}/${images.length}`;

        return (
            <FullScreenModal title="ギャラリー" className="chat-gallery" isOpen={isOpen} onClose={this.handleClosePreview}>
                <div className="chat-gallery__wrapper">
                    { gallery }
                </div>

                <FullScreenModal title={title} className="chat-gallery-preview" isOpen={isOpenPreview}>
                    <div className="chat-gallery__preview">
                        <div className="chat-gallery__preview-wrapper">
                            <Image className="chat-gallery__preview-img" imageId={image.img_id} imgKind="1" />
                        </div>
                    </div>
                    <div className="chat-gallery__download">
                        <ButtonDownload imageId={image.img_id} />
                    </div>
                </FullScreenModal>
            </FullScreenModal>
        )
    }
}

Gallery.propTypes = {
    isOpen: PropTypes.bool,
    images: PropTypes.array,
    onClose: PropTypes.func,
};

Gallery.defaultProps = {
    isOpen: false,
    images: [],
    onClose: () => {},
};

export default Gallery;
