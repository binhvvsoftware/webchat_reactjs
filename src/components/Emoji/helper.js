import EmojiIcon from './icon';

const characterMaps = {
    '&nbsp;': ' ',
    '&quot;': '"',
    '&lt;': '<',
    '&gt;': '>',
};
const emojiMaps = {}

EmojiIcon.forEach((icon) => {
    if (icon.code) {
        emojiMaps[icon.code] = icon.tag;
    }
});

const addReverseSolidus = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');

export default {
    encodedString(str) {
        let newStr = str.trim();
        Object.keys(characterMaps).forEach(key => {
            const regex = new RegExp(`(${addReverseSolidus(characterMaps[key])})`, 'gim');
            newStr = newStr.replace(regex, key);
        });

        return newStr;
    },
    decodedString(str) {
        let newStr = str.trim();
        Object.keys(characterMaps).forEach(key => {
            const regex = new RegExp(`(${addReverseSolidus(key)})`, 'gim');
            newStr = newStr.replace(regex, characterMaps[key]);
        });

        return newStr;
    },

    convertToView(text) {
        let content = text.trim();
        content = this.encodedString(content);
        content = content.replace(/\n/gim, '<br/>').replace(/\s/gim, '&nbsp;');
        Object.keys(emojiMaps).forEach((key) => {
            const regex = new RegExp(`(${this.encodedString(addReverseSolidus(key))})`, 'gim');
            content = content.replace(regex, emojiMaps[key]);
        });

        return content;
    },

    convertToCode(html) {
        let content = html.trim();
        content = this.decodedString(content);
        content = content.replace(/(<br\/>)/gim, '/n').replace(/(&nbsp;)/gim, '\s');

        Object.keys(emojiMaps).forEach((key) => {
            const regex = new RegExp(`(${addReverseSolidus(emojiMaps[key])})`, 'gim');
            content = content.replace(regex, key);
        });

        return content;
    },

    blackspace(str) {
        let newStr = str;
        Object.keys(emojiMaps).forEach(key => { // eslint-disable-line
            const regex = new RegExp(`(${addReverseSolidus(key)})\$`, 'gim');
            if (regex.test(str)) {
                newStr = newStr.replace(regex, '');
                return false;
            }
        });

        if (str === newStr) {
            newStr = str.substring(0, str.length - 1);
        }

        return newStr;
    },

    /**
     * Check container node has current cursor
     *
     * @param ancestor
     * @param descendant
     */
    isOrContainsNode(ancestor, descendant) {
        let node = descendant;
        while (node) {
            if (node === ancestor) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    },

    /**
     * Insert node over selection (of cursor position)
     *
     * @param node
     * @param containerNode
     */
    insertNodeOverSelection(node, containerNode) {
        let sel;
        let range;
        let html;

        if (window.getSelection) {
            sel = window.getSelection();

            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);

                if (this.isOrContainsNode(containerNode, range.commonAncestorContainer)) {
                    range.deleteContents();
                    range.insertNode(node);
                } else {
                    containerNode.appendChild(node);
                }

                range.setStartAfter(node);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();

            if (this.isOrContainsNode(containerNode, range.parentElement())) {
                html = (node.nodeType === 3) ? node.data : node.outerHTML;
                range.pasteHTML(html);
            } else {
                containerNode.appendChild(node);
            }

            range.setStartAfter(node);
        }

        containerNode.blur();
    },
};
