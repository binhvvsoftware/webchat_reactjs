import loadable from '@loadable/component';

export const EmojiView = loadable(() => import('./view'));
export const EmojiSticker = loadable(() => import('./sticker'));

export default {
    View: EmojiView,
    Sticker: EmojiSticker,
};
