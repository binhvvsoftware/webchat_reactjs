import EmojiIcon from './entities/EmojiIcon';

const emoji = [
    { 'code': 'o:)', 'img': '1' },
    { 'code': ':(', 'img': '2' },
    { 'code': 'xo', 'img': '3' },
    { 'code': '~x(', 'img': '4' },
    { 'code': '0-+', 'img': '5' },
    { 'code': '=d>', 'img': '6' },
    { 'code': '=))', 'img': '7' },
    { 'code': 'o=>', 'img': '8' },
    { 'code': ':z', 'img': '9' },
    { 'code': 'b-(', 'img': '10' },
    { 'code': ':bz', 'img': '11' },
    { 'code': ':p', 'img': '12' },
    { 'code': '<3', 'img': '13' },
    { 'code': ':-c', 'img': '14' },
    { 'code': ':-))', 'img': '15' },
    { 'code': '~:>', 'img': '16' },
    { 'code': ':o)', 'img': '17' },
    { 'code': '>:/', 'img': '18' },
    { 'code': '=((', 'img': '19' },
    { 'code': '=:)', 'img': '20' },
];

export default emoji.map(item => new EmojiIcon(item));
