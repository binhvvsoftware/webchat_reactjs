import storage from '../../../utils/storage';
import { LOAD_IMG, MEDIA_URL } from '../../../constants/endpoint_constant';

/**
 * @property {String} id
 * @property {String} sticker_id
 * @property {Number} code
 * @property {String} imgUrl
 */
export default class Sticker {
    constructor(attributes, cateId) {
        const sticker = Object.assign({}, attributes);
        const token = storage.getToken();
        sticker.imgUrl = `${MEDIA_URL}/api=${LOAD_IMG}&token=${token}&img_id=${sticker.code}&img_kind=3`;
        sticker.sticker_id = `${cateId}_${sticker.code}`;

        Object.keys(sticker).forEach((key) => {
            this[key] = sticker[key];
        });
    }
}
