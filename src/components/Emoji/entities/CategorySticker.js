import { MEDIA_URL, LOAD_IMG } from '../../../constants/endpoint_constant';
import storage from '../../../utils/storage';

/**
 * @property {String} cat_id
 * @property {String} cat_name
 * @property {String} imgUrl
 * @property {Number} version
 * @property {Number} stk_num
 * @property {Array|Sticker[]} stickers
 */
export default class CategorySticker {
    constructor(attributes) {
        const category = Object.assign({}, attributes);
        const token = storage.getToken();
        category.imgUrl = `${MEDIA_URL}/api=${LOAD_IMG}&token=${token}&img_id=${category.cat_id}&img_kind=5`;

        Object.keys(category).forEach((key) => {
            this[key] = category[key];
        });
    }
}
