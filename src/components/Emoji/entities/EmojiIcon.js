/**
 * @property {String} code
 * @property {String} img
 * @property {String} imgUrl
 */

export default class EmojiIcon {
    constructor(attributes) {
        const emoji = Object.assign({}, attributes);
        emoji.imgUrl = require(`../../../statics/emoji/${emoji.img}@2x.png`); // eslint-disable-line
        emoji.className = 'emoji-view__image';
        emoji.tag = `<img class="${emoji.className}" src="${emoji.imgUrl}">`;

        Object.keys(emoji).forEach((key) => {
            this[key] = emoji[key];
        });
    }
}
