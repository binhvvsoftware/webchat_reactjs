import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EmojiHelper from './helper';
import './index.scss';

class EmojiView extends Component {
    render() {
        let content = String(this.props.content).trim();
        if (content.length === 0 && typeof this.props.children === 'string') {
            content = this.props.children;
        }

        content = EmojiHelper.convertToView(content);

        return (
            <div className={`emoji-view ${this.props.className}`} title={this.props.title} dangerouslySetInnerHTML={{ __html: content }} />
        );
    }
}

EmojiView.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.string,
    children: PropTypes.any,
};

EmojiView.defaultProps = {
    className: '',
    title: '',
    content: '',
    children: '',
};

export default EmojiView;
