import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './index.scss';
import StickerService from '../../services/sticker';
const loading = require('../../statics/images/loading_spinner.gif');

class EmojiSticker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sticker: null,
        }
    }

    componentDidMount() {
        if (this.props.sticker && this.props.sticker.sticker_id) {
            StickerService.getStickerByStickerCateCode(this.props.sticker.sticker_id).then(sticker => {
                this.setState({ sticker })
            });
        }
    }


    render() {
        const { sticker } = this.state;

        return (
            <div className={`emoji-sticker-view ${this.props.className}`}>
                <img className="emoji-sticker-view__img" src={sticker ? sticker.imgUrl: loading } alt=""/>
            </div>
        );
    }
}

EmojiSticker.propTypes = {
    className: PropTypes.string,
    sticker: PropTypes.any,
};

EmojiSticker.defaultProps = {
    className: '',
    sticker: null,
};

export default EmojiSticker;
