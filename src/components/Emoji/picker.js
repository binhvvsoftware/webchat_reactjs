import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isMobile from 'ismobilejs';
import Slider from 'react-slick';
import EmojiEntity from './entities/EmojiIcon';
import icons from './icon';
import './index.scss';
import StickerService from '../../services/sticker';

const loading = require('../../statics/images/loading_spinner.gif');

class EmojiPicker extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        const numberOfEmojiInLine = 7;
        const numberOfEmojiInSlider = 20;
        const numberOfStickerInLine = isMobile.phone ? 4 : 8;
        const numberOfStickerSlider = numberOfStickerInLine * 2;

        const data = this.chunkArray(icons.slice(), numberOfEmojiInSlider);
        let emojis = [];
        data.forEach(item => {
            item.push(new EmojiEntity({
                code: '',
                img: 'chat_icon_btn_x_01',
            }));

            emojis.push(item);
        });

        emojis = emojis.map((emoji) => {
            const rows = emoji.map((item, key) => (
                <div className="editor-picker-emoji__icon" onClick={ (e) => this.handleClickEmoji(item, e) } title={item.code} key={ key }>
                    <img src={ item.imgUrl } alt={item.code} title={item.code} />
                </div>
            ));

            return this.chunkArray(rows, numberOfEmojiInLine).map((item, key) => (
                <div className="editor-picker-emoji__row" key={ key }>
                    { item }
                </div>
            ));
        });

        emojis = emojis.map((item, index) => (
            <div className="editor-picker-emoji__slider" key={ index }>
                { item }
            </div>
        ));

        this.state = {
            emojis,
            numberOfStickerInLine,
            numberOfStickerSlider,
            stickerActive: '',
            tabActive: 'emoji',
            isMobile: isMobile.phone,
            stickers: [],
        };
    }

    componentWillMount() {
        StickerService.getAllSticker().then(response => {
            if (!this.isUnmounted) {
                this.setState({ stickers: response.data });
            }
        });
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    chunkArray = (data, chunkSize) => {
        const results = [];

        while (data.length) {
            results.push(data.splice(0, chunkSize));
        }

        return results;
    };

    handleClose = () => {
        this.props.onSelectSticker(null);
    }

    handleClickEmoji = (emoji, event) => {
        event.stopPropagation();
        this.props.onSelectEmoji(emoji);
    };

    handleClickSticker = (sticker, event) => {
        event.stopPropagation();
        this.props.onSelectSticker(sticker);
    };

    handleClickStickerCategory = (item, event) => {
        event.stopPropagation();
        this.stickerSlide.slickGoTo(0);
        this.setState({ stickerActive: item.cat_id });
    };

    handleClickTab = (tab, event) => {
        event.stopPropagation();
        const { tabActive } = this.state;
        this.emojiSlide.slickGoTo(0);
        if (tabActive !== tab) {
            this.setState({ tabActive: tab });
        }
    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            lazyLoad: 'ondemand',
            customPaging: i => <button type="button" className="editor-picker__dot">{i + 1}</button>,
        };

        let content = [];
        const tabs = [];
        const { stickers } = this.state;
        let { stickerActive } = this.state;
        const hasSticker = Array.isArray(stickers) && !!stickers.length;

        if (hasSticker) {
            stickerActive = stickerActive.length ? stickerActive : stickers[0].cat_id;
        }

        stickers.forEach((category) => {
            const tmp = category.stickers.slice();

            let categoryContent = this.chunkArray(tmp, this.state.numberOfStickerSlider).map((cate) => {
                const rows = cate.map((item, key) => (
                    <div className={`editor-picker-sticker__icon ${this.state.isMobile ? 'is_mobile' : ''}`} onMouseDown={ (e) => this.handleClickSticker(item, e) } key={ key }>
                        <img src={ item.imgUrl }  alt="" onError={(e)=>{e.target.onerror = null; e.target.src=loading }} />
                    </div>
                ));


                return this.chunkArray(rows, this.state.numberOfStickerInLine).map((item, key) => (
                    <div className="editor-picker-sticker__row" key={ key }>
                        { item }
                    </div>
                ));
            });

            categoryContent = categoryContent.map((item, key) => (
                <div className="editor-picker-sticker__slider" key={ key }>
                    { item }
                </div>
            ));

            let isActive = false;
            if (stickerActive === category.cat_id) {
                content = categoryContent;
                isActive = true;
            }

            tabs.push(
                <div className={`editor-picker-sticker__tab ${isActive ? 'is_active' : ''}`} key={ category.cat_id } title={ category.cat_name } onMouseDown={ (e) => this.handleClickStickerCategory(category, e) }>
                    <img src={ category.imgUrl } alt={ category.cat_name } onError={(e)=>{e.target.onerror = null; e.target.src=loading }} />
                </div>,
            );
        });

        let footer = (
            <div className="editor-picker__footer">
                <button className="editor-picker__footer-btn is_emoji" type="button" onClick={(e) => this.handleClickTab('emoji', e)}>絵文字</button>
                {
                    hasSticker &&
                    <button className="editor-picker__footer-btn" type="button" onClick={(e) => this.handleClickTab('sticker', e)}>スタンプ</button>
                }
            </div>
        );

        footer = this.state.tabActive === 'emoji' ? footer :  (
            <div className="editor-picker-sticker__tabs">
                <div className="editor-picker-sticker__tab is_close" onClick={(e) => this.handleClickTab('emoji', e)}>
                    <span className="icon icon-arrow-left" />
                </div>
                { tabs }
            </div>
        );

        /* eslint-disable no-return-assign */
        return (
            <div className="editor-picker">
                <div className={`editor-picker__wrapper ${this.state.tabActive === 'emoji' ? 'is_active': ''}`}>
                    <div className="editor-picker-emoji">
                        <Slider { ...settings } ref={slider => (this.emojiSlide = slider)}>
                            { this.state.emojis }
                        </Slider>
                    </div>
                </div>

                <div className={`editor-picker__wrapper ${this.state.tabActive === 'sticker' ? 'is_active': ''}`}>
                    <div className="editor-picker-sticker">
                        <div className="editor-picker-sticker__wrapper">
                            <Slider { ...settings } ref={slider => this.stickerSlide = slider}>
                                { content }
                            </Slider>
                        </div>
                    </div>
                </div>

                { footer }
            </div>
        );
    }
}

EmojiPicker.propTypes = {
    stickers: PropTypes.array,
    onSelectEmoji: PropTypes.func,
    onSelectSticker: PropTypes.func,
};

EmojiPicker.defaultProps = {
    stickers: [],
    onSelectEmoji: () => {},
    onSelectSticker: () => {},
};

export default EmojiPicker;
