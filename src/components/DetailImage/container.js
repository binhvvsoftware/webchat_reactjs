import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Image from '../Image';
import FullScreenModal from '../FullScreenModal';
import ReportImage from '../ReportImage';
import ProfileService from '../../services/profile';
import ButtonDownload from '../ButtonDownload';
import history from '../../utils/history';
import TimelineService from '../../services/timeline';
import {
    DELETE_PHOTO_MESSAGE,
    BUTTON_ALERT_DONT_DELETE_TITLE,
} from '../../constants/messages';
import './index.scss';

class DetailImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buzz: this.props.buzz,
            openDialog: true,
            isLikeBuzz: this.props.buzz.isLike,
            statusPopupReportImage: false,
            loadDeleteBuzz: false,
        };

        this.onCloseAfterReportImage = this.onCloseAfterReportImage.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClose() {
        this.setState({
            openDialog: false,
        });

        this.props.handleCloseDialogDetailImage(this.state.buzz.buzz_id);
    }

    handleRedirectGoBack() {
        this.setState({
            openDialog: false,
        });

        history.checkAndBack();
    }

    openDialogReportImage() {
        this.setState({
            statusPopupReportImage: true,
        });
    }

    onCloseAfterReportImage(statusReport) {
        this.setState({
            statusPopupReportImage: false,
        });

        if (statusReport)
            this.handleRedirectGoBack();
    }

    handleLikeBuzz(buzzId) {
        const data = {
            api: "like_buzz",
            buzz_id: buzzId,
            like_type: this.state.isLikeBuzz ? 0 : 1,
        }
        this.setState({
            isLikeBuzz: !this.state.isLikeBuzz, // eslint-disable-line
        }, () => {
            ProfileService.likeBuzz(data);
        })
    }

    confirmDelete = (buzzId) => {
        Popup.create({
            content: DELETE_PHOTO_MESSAGE,
            buttons: {
                left: [{
                    text: BUTTON_ALERT_DONT_DELETE_TITLE,
                    className: 'ok custom-popup-profile',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: 'はい',
                    className: 'ok',
                    action: () => {
                        this.deleteBuzzImage(buzzId);
                        Popup.close();
                    },
                }],
            },
        });
    };

    deleteBuzzImage = (buzzId) => {
        this.setState({
            loadDeleteBuzz: true,
        }, () => {
            TimelineService.deleteBuzz(buzzId)
                .then(() => {
                    this.setState({
                        loadDeleteBuzz: false,
                    });
                    history.checkAndBack();
                });
        });
    };

    render() {
        const myImage = this.state.buzz.buzz_val;
        const buzzIdPublic = this.state.buzz.buzz_id;
        const avaId = this.state.buzz.ava_id;
        const { isLikeBuzz, loadDeleteBuzz} = this.state;
        const numComent = this.state.buzz.cmt_num;

        return (
            <FullScreenModal
                className="profile-page-detail-image"
                isOpen={this.state.openDialog}
                onClose={() => this.handleClose()}
            >

                <div className="image-detail">
                    <header className="k-header">
                        <div className="nav-bar">
                            <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.handleClose()}>
                                <i className="icon icon-arrow-left"></i>
                            </span>
                        </div>
                    </header>
                    <main className="k-main">
                        <div className="my-photo">
                            <Image imageId={myImage} isBackground imgKind="2" />
                        </div>
                    </main>
                    <footer className="k-footer">
                        { !this.props.isImageMyUser ?
                            (<div className="bottom-menu-option">
                                <div className="bottom-menu-item"  onClick={() => this.handleClose()}>
                                    <div className="gallery-user-btn" >
                                        <i className="icon icon-comment-ol"></i>
                                        <span className="d-title">コメント{numComent}</span>
                                    </div>
                                </div>
                                <button type="button" className="bottom-menu-item" onClick={() => this.handleLikeBuzz(buzzIdPublic)}>
                                    <i className="icon icon-fav-black"></i>
                                    <span className="d-title">{!isLikeBuzz ? "いいね" : "取り消す"}</span>
                                </button>
                                <div className="bottom-menu-item" onClick={() => this.handleRedirectGoBack()}>
                                    <Image isAvatar imageId={avaId} alt="img" />
                                </div>
                                <ButtonDownload className="bottom-menu-item" imageId={myImage} />
                                <button type="button" className="bottom-menu-item" onClick={() => this.openDialogReportImage()} >
                                    <div className="gallery-user-btn ">
                                        <i className="icon icon-report"></i>
                                        {this.state.statusPopupReportImage ? <ReportImage myImage={myImage} onCloseReportImage={this.onCloseAfterReportImage} onCloseDialogImage={this.handleClose} /> : ''}
                                        <span className="d-title">写稟の鞭告</span>
                                    </div>
                                </button>
                            </div>) : (
                                <button className="btn-delete-photo" type="button" onClick={ loadDeleteBuzz ? null : () => this.confirmDelete(buzzIdPublic)}>
                                    <i className="icon icon-recycle-grey" />
                                    <span className="d-block">写真を削除</span>
                                </button>
                            )
                        }
                    </footer>
                </div>
            </FullScreenModal>
        );
    }
}

DetailImage.propTypes = {
    buzz: PropTypes.object,
    handleCloseDialogDetailImage: PropTypes.func,
    isImageMyUser: PropTypes.bool,
};

DetailImage.defaultProps = {
    isImageMyUser: false,
}

export default DetailImage;
