import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Badge extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.number !== this.props.number;
    }

    render() {
        let { className, number } = this.props;
        try {
            number = parseInt(number, 10);
            number = number > 0 ? number : '';
        } catch (e) {
            number = '';
        }

        className = `badge ${className}`;
        if (number > 20) {
            number = '20+';
            className = `${className} badge__plus`;
        }

        return String(number).length ? <span className={ className }>{ number }</span> : null;
    }
}

Badge.propTypes = {
    className: PropTypes.string,
    number: PropTypes.any,
};

Badge.defaultProps = {
    className: '',
    number: '',
};

export default Badge;

