import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { ActivityIndicator, TextInput } from 'react-native-web';
import Image from '../../Image';
import { UserDisplayList } from '../../UserList';
import FullScreenModal from '../../FullScreenModal';
import SearchService from '../../../services/search';
import { EMPTY_CONTENT_SEARCH, EMPTY_CONTENT_TITLE } from '../../../constants/messages';
import { checkLoadMore } from '../../../utils/helpers';
import './index.scss';

class SearchName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            userName: '',
            dataUser: [],
            open: props.isOpen,
            hasLoadMore: true,
            loading: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.isOpen,
        })
    }

    componentDidMount() {
        this.handleCallApi();
        window.addEventListener('scroll', this.handleLoadMore, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleLoadMore, true);
    }

    handleLoadMore = (event) => {
        if (this.state.isOpenMessage || this.state.isOpenMenu || this.state.isOpenSearch) {
            return;
        }

        if (checkLoadMore(event, false)) {
            this.handleCallApi();
        }
    }

    handleClose = () => {
        this.setState({ open: false, dataUser: [], keyword: '', userName: '' });
    };

    handleSubmit(event) {
        event.preventDefault();
        const { keyword, userName } = this.state;
        if (!keyword.length) {
            Popup.alert(EMPTY_CONTENT_SEARCH, EMPTY_CONTENT_TITLE);
            return;
        }

        if (keyword === userName) {
            return;
        }

        this.setState({
            userName: keyword,
            dataUser: [],
            hasLoadMore: true,
        }, this.handleCallApi);
    }

    handleCallApi = () => {
        const { dataUser } = { ...this.state };

        if (this.state.loading || !this.state.hasLoadMore || this.state.userName.length === 0) {
            return;
        }

        this.setState({
            loading: true,
        }, () => {
            SearchService.searchName(this.state.userName, dataUser.length).then(response => {
                this.setState({
                    dataUser: dataUser.concat(response.data),
                    hasLoadMore: response.hasLoadMore,
                });
            }).then(() => {
                this.setState({
                    loading: false,
                });
            });
        });
    }

    handleChangeKeyword = (text) => {
        this.setState({
            keyword: text.trim(),
        });
    }

    render() {
        let displayList;

        if (this.state.dataUser.length || this.state.loading) {
            displayList = (
                <div className="user-result-wrap">
                    <UserDisplayList dataUser={this.state.dataUser} />
                    <div className={this.state.loading && this.state.hasLoadMore ? 'loading-box' : 'hidden'} >
                        <ActivityIndicator animating color="black" size="large" />
                    </div>
                </div>
            );
        } else {
            displayList = <Image isAvatar className="background-empty" />
        }

        return (
            <div>
                <FullScreenModal title="ニックネームで検索" isOpen={this.state.open} onClose={this.handleClose}>
                    <form className="form-search-name" onSubmit={(e) => this.handleSubmit(e)}>
                        <div className="form__control search-input">
                            <div className="form__group-input no_icon-right has_icon-left">
                                <span className="icon icon-search">&nbsp;</span>
                                <TextInput
                                    defaultValue={this.state.keyword}
                                    placeholder="こここに検索結果が表示されます"
                                    keyboardType="search"
                                    onChangeText={(text) => this.handleChangeKeyword(text)} />
                            </div>
                        </div>

                        {displayList}
                    </form>
                </FullScreenModal>
            </div>
        );
    }
}

SearchName.propTypes = {
    isOpen: PropTypes.bool,
};

SearchName.defaultProps = {
    isOpen: false,
};

export default SearchName;
