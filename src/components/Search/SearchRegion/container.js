import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import './index.scss';
import FullScreenModal from '../../FullScreenModal';
import AreaRegion from '../../AreaRegion/container';
import { PLEASE_SELECT_AT_LEAST_ONE_REGION, ERROR_TITLE } from '../../../constants/messages';
import { AREA_REGION } from '../../../constants/region_constant';

class SearchRegion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: props.region,
            regionOption: props.region.length ? 'multiple' : 'all',
            open: props.isOpen,
            openListRegion: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            region: nextProps.region,
            open: nextProps.isOpen,
        });
    }

    handleClose = () => {
        this.setState({ open: false });
        this.props.onSelected(this.props.region);
    };

    handleSelected = () => {
        if (this.state.region.length === 0 && this.state.regionOption === 'multiple') {
            Popup.alert(PLEASE_SELECT_AT_LEAST_ONE_REGION, ERROR_TITLE);
            return;
        }

        this.setState({ open: false });
        this.props.onSelected(this.state.region);
    };

    handleClickItem = (value) => {
        let openListRegion = false;
        let { region } = this.state;
        if (value === 'multiple') {
            openListRegion = true;
        } else {
            region = [];
        }

        this.setState({ region, regionOption: value, openListRegion });
    }

    handleSelectMultipleRegion = (region) => {
        this.setState({ region, openListRegion: false });
    }

    render() {
        const label = this.state.region.map(item => AREA_REGION[item] || '').join(', ');

        return (
            this.state.open ?
                <div>
                    <FullScreenModal isOpen>
                        <header className="k-header">
                            <div className="nav-bar">
                                <span className="nav-bar__icon nav-bar__icon--left" onClick={this.handleClose}><span className="icon icon-arrow-left">&nbsp;</span></span>
                                <span className="nav-bar__title">地域選択</span>
                                <button className="nav-bar__button" type="button" onClick={this.handleSelected}>完了</button>
                            </div>
                        </header>
                        <div className="region-area__content">
                            <label className={`region-group__item ${label.length ? 'has_label' : ''}`} onClick={ () => this.handleClickItem('multiple') } >
                                都道府県から検索
                                <p className={`region-group__label ${label.length ? '' : 'is_hidden'}`}>{ label }</p>
                                <input className="radio-button" type="radio" name="region" id="multiple" value="multiple" readOnly checked={this.state.regionOption !== 'all'}/>
                                <label className="region-group__checkbox" htmlFor="multiple">
                                    <span className="radio-button__icon">&nbsp;</span>
                                </label>
                            </label>

                            <label className="region-group__item" onClick={ () => this.handleClickItem('all') } >
                                全てのユーザー
                                <input className="radio-button" type="radio" id="all" value="all" readOnly checked={this.state.regionOption === 'all'}/>
                                <label className="region-group__checkbox" htmlFor="all">
                                    <span className="radio-button__icon">&nbsp;</span>
                                </label>
                            </label>
                        </div>
                    </FullScreenModal>

                    <AreaRegion isOpen={this.state.openListRegion} selected={this.state.region} selectMultiple onSelected={this.handleSelectMultipleRegion}/>
                </div>
                :
                null
        );
    }
}


SearchRegion.propTypes = {
    isOpen: PropTypes.bool,
    region:PropTypes.array,
    onSelected: PropTypes.func,
};

SearchRegion.defaultProps = {
    isOpen: false,
    region: [],
    onSelected: () => {},
};


export default SearchRegion;
