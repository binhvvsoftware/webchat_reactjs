import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import SearchRegion from './SearchRegion';
import SelectBox from '../SelectBox';
import FullScreenModal from '../FullScreenModal';
import { AREA_REGION } from '../../constants/region_constant';
import './index.scss';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortBy: props.sortBy,
            isTimeLogin: props.isTimeLogin,
            fromAge: props.fromAge,
            toAge: props.toAge,
            region: props.region,
            openModalSearchRegion: false,
            openModalSearchName: false,
            openModalAge: false,
            age: {
                from: props.fromAge,
                to: props.toAge,
            },
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            sortBy: nextProps.sortBy,
            isTimeLogin: nextProps.isTimeLogin,
            fromAge: parseInt(nextProps.fromAge, 10),
            toAge: parseInt(nextProps.toAge, 10),
            region: nextProps.region,
        });
    }

    handleTimeLogin() {
        const { isTimeLogin } = { ...this.state };
        this.setState({
            isTimeLogin: !isTimeLogin,
        });
    }

    handleOpenModalAge = () => {
        const { fromAge, toAge } = { ...this.state };

        this.setState({
            openModalAge: true,
            age: { from: fromAge, to: toAge },
        });
    };

    closeModalAge = () => {
        this.setState({
            openModalAge: false,
        });
    }

    handleSubmitAge = () => {
        const { from, to } = { ...this.state.age };

        this.setState({
            fromAge: from,
            toAge: to,
            openModalAge: false,
        });
    }

    handleSortBy = (sortBy) => {
        this.setState({
            sortBy,
        });
    };

    handleOpenModalSearchRegion = () => {
        this.setState({
            openModalSearchRegion: true,
            openModalSearchName: false,
        });
    };

    handleFromAge = (from) => {
        const { age } = this.state;
        age.from = from;
        this.setState({ age });
    }

    handleToAge = (to) => {
        const { age } = this.state;
        age.to = to;
        this.setState({ age });
    }

    handleSearchRegion = (region) => {
        this.setState({ region, openModalSearchRegion: false });
    }

    handleOpenModalSearchName = () => {
        this.setState({
            openModalSearchName: true,
            openModalSearchRegion: false,
        });
    }

    handleSearchInfo = () => {
        const { sortBy, isTimeLogin, fromAge, toAge, region } = { ...this.state };

        const mySearchInfo = {
            sortBy,
            isTimeLogin,
            fromAge: parseInt(fromAge, 10),
            toAge: parseInt(toAge, 10),
            region,
        };

        this.props.onSearch(mySearchInfo);
    };

    render() {
        const { isTimeLogin, region } = this.state;
        const fromAge = parseInt(this.state.age.from, 10);
        const toAge = parseInt(this.state.age.to, 10);

        const selectRegion = region.length === 0 ? '全てのユーザー' : `都道府県から検索 (${region.map(item => AREA_REGION[item]).join(', ')})`;
        let listItemFromAge = Array.from({ length: toAge - 17 }, (value, key) => ({ // eslint-disable-line
            value: 18 + key,
            label: 18 + key,
        }));

        let listItemToAge = Array.from({ length: 121 - fromAge }, (value, key) => ({ // eslint-disable-line
            value: fromAge + key,
            label: fromAge + key,
        }));

        const displaySelect = {
            login: 'オンライン順',
            register: '登録順',
        };

        return (
            <div>
                <div className="form__control">
                    <div className="form__label">職業</div>
                    <div className="form__group-input pink-text" onClick={this.handleOpenModalAge}>
                        {this.state.fromAge} ~ {this.state.toAge}
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__label">地域</div>
                    <div className="form__group-input pink-text one_line" onClick={this.handleOpenModalSearchRegion}>
                        {selectRegion}
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__label">24時間以内にログイン</div>
                    <div className="form__group-input no_icon-right">
                        <label className="switch">
                            <input type="checkbox" className="switch__input" checked={this.state.isTimeLogin} onChange={(e) => this.handleTimeLogin(e)} value={isTimeLogin} />
                            <div className="switch__toggle">
                                <div className="switch__handle">&nbsp;</div>
                            </div>
                        </label>
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__label">並び順</div>
                    <div className="form__group-input is_select pink-text">
                        <SelectBox options={displaySelect} selected={this.state.sortBy} onSelected={this.handleSortBy}>
                        </SelectBox>
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </div>

                <div className="form__control">
                    <div className="form__group-input text-center">
                        <button type="button" className="btn-custom  btn-active" onClick={this.handleSearchInfo}>
                            <span className="icon icon-search-w"></span>
                            検索する
                        </button>
                    </div>
                </div>
                <Link to="/search-by-name" className="form__control" style={{ color: "#000" }}>
                    <div className="form__label">ニックネームで検索</div>
                    <div className="form__group-input is_select pink-text" onClick={this.handleOpenModalSearchName}>
                        <span className="form__icon-right">&nbsp;</span>
                    </div>
                </Link>

                <SearchRegion isOpen={this.state.openModalSearchRegion} region={this.state.region} onSelected={this.handleSearchRegion} />

                <FullScreenModal className="is_popup" isOpen={this.state.openModalAge}>
                    <div className="search-age">
                        <div className="form__control">
                            <div className="form__group-input text-center">
                                <SelectBox options={listItemFromAge} selected={this.state.age.from} onSelected={this.handleFromAge}>
                                </SelectBox>
                            </div>
                            <div className="form__group-input text-center">
                                <SelectBox options={listItemToAge} selected={this.state.age.to} onSelected={this.handleToAge}>
                                </SelectBox>
                            </div>
                        </div>
                        <div className="form__control">
                            <div className="form__group-input text-right">
                                <button className="button_popup" type="button" onClick={() => this.closeModalAge()}>キャンセル</button>

                                <button className="button_popup" type="button" onClick={this.handleSubmitAge}>はい</button>
                            </div>
                        </div>
                    </div>
                </FullScreenModal>
            </div>
        );
    }
}

Search.propTypes = {
    isTimeLogin: PropTypes.bool,
    fromAge: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    toAge: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    onSearch: PropTypes.func,
    sortBy: PropTypes.string,
    region: PropTypes.array,
};

Search.defaultProps = {
    isTimeLogin: false,
    fromAge: 18,
    toAge: 80,
    region: [],
    sortBy: 'login',
    onSearch: () => { },
};


export default Search;

