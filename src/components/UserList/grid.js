import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { UserDisplayGridItem } from './UserItem';
import './index.scss';

class UserDisplayGrid extends Component {
    render() {
        const { dataUser } = this.props;
        const displayGrid = this.props.dataUser.map((user, indexUser) => (
            <UserDisplayGridItem
                user={ user }
                indexUser={ indexUser }
                dataUser={ dataUser }
                key={ indexUser }/>
        ));

        return (
            <div className={ `list-user is_grid ${this.props.className}` }>
                { displayGrid }
            </div>
        );
    }
}

UserDisplayGrid.propTypes = {
    dataUser: PropTypes.array,
    className: PropTypes.string,
};

UserDisplayGrid.defaultProps = {
    dataUser: [],
    className: '',
};

export default UserDisplayGrid;
