import loadable from '@loadable/component';

export const UserDisplayList = loadable(() => import('./list'));

export const UserDisplayGrid = loadable(() => import('./grid'));
