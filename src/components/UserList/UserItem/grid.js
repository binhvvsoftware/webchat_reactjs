import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Badge from '../../Badge';
import Image from '../../Image';
import '../index.scss';

class UserDisplayGridItem extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.user.user_id !== this.props.user.user_id;
    }

    render() {
        const { dataUser, user, indexUser } = this.props;

        return (
            <div className="list-user__item">
                <Link className="list-user__box" to={{ pathname: user.profile_url, state: { indexUser, dataUser, user } }}>
                    <LazyLoad throttle={100} offset={200} once> 
                        <Image isAvatar imageId={ user.ava_id }/>
                        <span className="list-user__thumbnail-icon">
                            <span className={user.icon_status}>&nbsp;</span>
                        </span>
                        <Badge className="nav-bar__badge" number={user.unread_num} />
                        <div className="user-name">
                            <span className = 'grid-name'>{user.user_name }</span>
                            <span className="name">{ user.region_name }</span>
                        </div>
                    </LazyLoad>
                </Link>
            </div>
        );
    }
}

UserDisplayGridItem.propTypes = {
    user: PropTypes.object,
    dataUser: PropTypes.array,
    indexUser: PropTypes.number,
};

UserDisplayGridItem.defaultProps = {
    user: {},
    dataUser: [],
    indexUser: 0,
};

export default UserDisplayGridItem;
