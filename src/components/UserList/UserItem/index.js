import loadable from '@loadable/component';

export const UserDisplayListItem = loadable(() => import('./list'));

export const UserDisplayGridItem = loadable(() => import('./grid'));
