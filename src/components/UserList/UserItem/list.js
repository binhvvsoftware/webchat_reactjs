import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';
import PropTypes from 'prop-types';
import Badge from '../../Badge';
import Image from '../../Image';
import '../index.scss';

class UserDisplayListItem extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.user.user_id !== this.props.user.user_id;
    }

    render() {
        const { dataUser, user, indexUser } = this.props;

        return (
            <Link className="list-user__item" to={{ pathname: user.profile_url, state: { indexUser, dataUser, user } }}>
                <LazyLoad throttle={100} offset={200} once> 
                    <div className="list-user__thumbnail">
                        <div className="list-user__thumbnail-img">
                            <Image isAvatar imageId={user.ava_id} />
                        </div>
                        <span className="list-user__thumbnail-icon">
                            <span className={user.icon_status}>&nbsp;</span>
                        </span>
                        <Badge className="nav-bar__badge" number={user.unread_num} />
                    </div>

                    <div className="list-user__info">
                        <p>
                            <span className="list-user__name">{ user.user_name }</span>
                            <span className="list-user__times">
                                <span className="icon icon-position">&nbsp;</span> { user.region_name }
                                <span className="icon icon-time">&nbsp;</span> { user.login_ago }
                            </span>
                        </p>

                        <p className="list-user__message">
                            { user.abt }
                            { user.is_contacted ? <span className="icon icon-received">&nbsp;</span> : '' }
                        </p>
                    </div>
                </LazyLoad>
            </Link>
        );
    }
}

UserDisplayListItem.propTypes = {
    user: PropTypes.object,
    dataUser: PropTypes.array,
    indexUser: PropTypes.number,
};

UserDisplayListItem.defaultProps = {
    user: {},
    dataUser: [],
    indexUser: 0,
};

export default UserDisplayListItem;
