import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { UserDisplayListItem } from './UserItem';
import './index.scss';

class UserDisplayList extends Component {
    render() {
        const { dataUser } = this.props;
        const displayList = dataUser.map((user, indexUser) => <UserDisplayListItem
            user={ user }
            indexUser={ indexUser }
            dataUser={ dataUser }
            key={ user.key }/>);

        return (
            <div className={ `list-user is_list ${this.props.className}` }>{ displayList }</div>
        );
    }
}

UserDisplayList.propTypes = {
    dataUser: PropTypes.array,
    className: PropTypes.string,
};

UserDisplayList.defaultProps = {
    dataUser: [],
    className: '',
};

export default UserDisplayList;
