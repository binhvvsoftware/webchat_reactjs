import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from 'react-motion-drawer';
import { connect } from 'react-redux';
import Badge from '../Badge';
import FullScreenModal from '../FullScreenModal';
import SidebarLeft from '../MainLayout/SidebarLeft';
import { getParameterByName } from '../../utils/helpers';
import history from '../../utils/history';
import './index.scss';
import { openWebView, closeWebView } from '../../actions/webview';
import { closeSidebarMenu, openSidebarMenu } from '../../actions';

class WebView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            title: this.props.webViewTitle,
        };
        this.iframe = React.createRef();
    }

    componentDidMount() {
        if (!window.addEventOnMessageIFrame) {
            window.addEventOnMessageIFrame = true;

            if (window.addEventListener) {
                window.addEventListener('message', this.onChangeTitle, false);
            } else {
                window.attachEvent('onmessage', this.onChangeTitle);
            }
        }
    }

    onChangeTitle = (message) => {
        if (message && message.data) {
            try {
                const data = JSON.parse(message.data);
                this.setState({
                    title: data.title || '',
                    loading: false,
                });

                if (String(data.status) === '401') {
                    this.handleClose();
                    history.push('/');
                    return;
                }

                this.handleChangeUrl(data.url);
            } catch (e) {
                //
            }
        }
    };

    handleChangeUrl = (url) => {
        const action = getParameterByName('act', url);
        switch (action) {
            case 'top_page':
                this.handleClose();
                history.push('/home');
                return;

            case 'close':
                this.handleClose();
                return;

            case 'myprofile':
                this.handleClose();
                history.push('/edit-profile');
                return;

            default:

                break;
        }

        if (action.indexOf('otherprofile-') === 0) {
            history.push(`/profile/${action.replace('otherprofile-', '')}`);
            this.handleClose();
        }
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            title: nextProps.webViewTitle,
        });
        this.handleChangeUrl(nextProps.webViewUrl);
    }

    handleToggleSidebarLeft = (open) => {
        if (open) {
            this.props.openSidebarMenu();
        } else {
            this.props.closeSidebarMenu();
        }
    };

    handleClose = () => {
        this.props.closeWebView();
    };

    handleRedirectLink = (path, state = {}) => {
        this.handleClose();
        this.props.closeSidebarMenu();

        history.push(path, state);
    };


    handleOpenWebView = (url, title) => {
        this.props.closeSidebarMenu();
        this.props.openWebView({ url, title });
    };

    render() {
        const { isOpenWebView, webViewUrl, webBackButton } = this.props;
        const { title } = this.state;
        let isValidUrl;

        const drawerPropsLeft = {
            handleWidth: 15,
            className: 'sidebar is_left',
            overlayClassName: 'sidebar__overlay',
            open: this.props.isOpenSidebarMenu,
            noTouchClose: false,
            onChange: open => this.handleToggleSidebarLeft(open),
        };

        try {
            new URL(webViewUrl); // eslint-disable-line
            isValidUrl = true;

            if (!/^http(s)?:\/\//.test(webViewUrl)) {
                isValidUrl = false;
            }
        } catch (_) {
            isValidUrl = false;
        }

        if (isOpenWebView && !isValidUrl) {
            console.log(webViewUrl, 'URL invalid'); // eslint-disable-line
        }

        const backButton = (
            <span className="nav-bar__icon nav-bar__icon--left" onClick={ this.handleClose }>
                <span className="icon icon-arrow-left"/>
            </span>
        );

        const menuButton = (
            <span className="nav-bar__icon nav-bar__icon--left" onClick={ this.handleToggleSidebarLeft }>
                <span className="icon icon-menu-left"/>
                <Badge className="nav-bar__badge" number={ this.props.attentionNumbers.notifyNumber }/>
            </span>
        );

        const header = (
            <header className="k-header">
                <div className="nav-bar">
                    { webBackButton ? backButton : menuButton }
                    <span className="nav-bar__title">{ title }</span>
                </div>
            </header>
        );

        const sidebar = (
            <Drawer { ...drawerPropsLeft }>
                <SidebarLeft callbackRedirect={ this.handleRedirectLink } callbackWebView={ this.handleOpenWebView }/>
            </Drawer>
        );

        const iFrame = (
            <iframe
                ref={ this.iframe }
                className={ `${this.state.loading ? 'web-view__iframe' : 'web-view__iframe_loaded'}` }
                src={ webViewUrl }
                title={ title }
                sandbox="allow-same-origin allow-scripts allow-forms allow-pointer-lock allow-popups allow-top-navigation"
            />
        );

        return (
            <FullScreenModal
                className="web-view sidebar_web-view__parent"
                isOpen={ isOpenWebView }
                onClose={ this.handleClose }>
                { isOpenWebView && header }
                { isOpenWebView && sidebar }
                { isOpenWebView && isValidUrl ? iFrame : null }
            </FullScreenModal>
        );
    }
}

WebView.propTypes = {
    openWebView: PropTypes.func.isRequired,
    closeWebView: PropTypes.func.isRequired,
    attentionNumbers: PropTypes.object.isRequired,
    isOpenSidebarMenu: PropTypes.bool.isRequired,
    isOpenWebView: PropTypes.bool.isRequired,
    webViewTitle: PropTypes.string.isRequired,
    webViewUrl: PropTypes.string.isRequired,
    webBackButton: PropTypes.bool.isRequired,
    openSidebarMenu: PropTypes.func.isRequired,
    closeSidebarMenu: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
    isOpenSidebarMenu: state.main.isOpenSidebarMenu,
    isOpenWebView: state.webView.isOpenWebView,
    webViewTitle: state.webView.webViewTitle,
    webViewUrl: state.webView.webViewUrl,
    webBackButton: state.webView.webBackButton,
});

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
    closeWebView: () => dispatch(closeWebView()),
    openSidebarMenu: (payload) => dispatch(openSidebarMenu(payload)),
    closeSidebarMenu: (payload) => dispatch(closeSidebarMenu(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WebView);

