import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import FullScreenModal from '../FullScreenModal';
import ProfileService from '../../services/profile';
import {
    toggleMenuSettingTop,
    toggleDialogMemo,
} from '../../reducers/control/profile';
import './index.scss';

class Memo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: props.content,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            content: nextProps.content,
        });
    }

    onchangeMemo = (text) => {
        this.setState({ content: text.trim() });
    };

    updateMemo = () => {
        const { content } = this.state;
        ProfileService.updateMemo(this.props.user.user_id, content)
            .then(() => {
                this.props.toggleDialogMemo();
                this.props.callBackUpdate(content);
            });
    };

    render() {
        const {
            isOpenDialogMemo,
        } = this.props;

        const titleChildren = (
            <button
                className="nav-bar__button"
                type="button"
                onClick={ () => this.updateMemo() }>
                完了
            </button>
        );

        return (
            <FullScreenModal
                className="memo"
                isOpen={ isOpenDialogMemo }
                onClose={ () => this.props.toggleDialogMemo() }
                title="メモ"
                titleChildren={ titleChildren }
            >
                <textarea
                    className="memo__textarea"
                    placeholder="大事なことはメモしておきましょう。※このメモ機能は個人用です。相手とは共有されません"
                    value={ this.state.content }
                    onChange={ (e) => this.onchangeMemo(e.target.value) }/>
            </FullScreenModal>
        );
    }
}

Memo.propTypes = {
    isOpenDialogMemo: PropTypes.bool.isRequired,
    toggleDialogMemo: PropTypes.func.isRequired,
    callBackUpdate: PropTypes.func.isRequired,
    content: PropTypes.string,
    user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    isOpenMenuSettingTop: state.profile.isOpenMenuSettingTop,
    isOpenDialogMemo: state.profile.isOpenDialogMemo,
});

const mapDispatchToProps = (dispatch) => ({
    toggleMenuSettingTop: (value) => dispatch(toggleMenuSettingTop(value)),
    toggleDialogMemo: () => dispatch(toggleDialogMemo()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Memo);

