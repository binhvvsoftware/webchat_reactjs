import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './index.scss';
import { openWebView } from '../../actions/webview';
import history from '../../utils/history';

class Notice extends React.Component {
    handleClick = (event, notify) => {
        event.preventDefault();

        if (notify.isWebView) {
            return this.props.openWebView(notify.webView);
        }

        return history.push(notify.pathname, notify.state);
    };

    render() {
        const listNotification = this.props.notice.map((item, i) => (
            <div className="system-noti-item" key={ i } onClick={ (e) => this.handleClick(e, item) }>
                <div className="left">
                    <i className={ `icon ${item.iconClass}` }/>
                </div>
                <div className="right">
                    <span className="text-noti">{ item.title }</span>
                    <span className="time">{ item.timeAgo }</span>
                </div>
            </div>
        ));

        return (
            <div className="list-notice">
                { listNotification }
            </div>
        );
    }
}

Notice.propTypes = {
    notice: PropTypes.array,
    openWebView: PropTypes.func.isRequired,
};

Notice.defaultProps = {
    notice: [],
};

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(null, mapDispatchToProps)(Notice);
