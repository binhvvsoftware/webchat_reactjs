import React, { Component } from 'react';
import PropTypes from 'prop-types';
import history from '../../utils/history';

class Header extends Component {
    constructor(props) {
        super(props);

        this.header = React.createRef();
    }

    componentDidMount() {
        if (this.props.title) {
            this.handleUpdateDocumentTitle(this.props.title);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.title) {
            this.handleUpdateDocumentTitle(nextProps.title);
        }
    }

    handleUpdateDocumentTitle = (title) => {
        let isPopup =  false;

        let parentElem = this.header.current.parentNode;
        const popupClass = [
            'full-screen',
        ];

        while (!isPopup && parentElem) {
            popupClass.forEach(className => { // eslint-disable-line
                if (parentElem.classList && Array.from(parentElem.classList).indexOf(className) !== -1) {
                    isPopup = true;
                }
            });

            parentElem = parentElem.parentNode;
        }

        if (!isPopup) {
            document.title = title;
        }
    }

    handleBack = () => {
        switch (typeof this.props.backUrl) {
            case 'string':
                return history.push(this.props.backUrl);
            case 'object':
                return history.push(this.props.backUrl.pathname, this.props.backUrl.state);
            case 'function':
                return this.props.backUrl();
            default:
                return this.props.backUrl;
        }
    }

    render() {
        const backUrl = this.props.backUrl ? (
            <span className="nav-bar__icon nav-bar__icon--left" onClick={() => this.handleBack()}><span className="icon icon-arrow-left" /></span>
        ) : null;

        return (
            <header className="k-header" ref={this.header}>
                <div className="nav-bar">
                    {backUrl}
                    <span className="nav-bar__title nav-bar__name">{this.props.title}</span>
                    {this.props.children}
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    title: PropTypes.string,
    backUrl: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.object,
        PropTypes.string,
    ]),
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
    ]),
};

Header.defaultProps = {
    title: null,
    backUrl: null,
};

export default Header;
