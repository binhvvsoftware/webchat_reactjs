import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Image from '../Image';
import UtilityButtonCall from '../presentational/Utility/UtilityButtonCall';
import { PrimaryUtilityTypes } from '../../constants/types';
import { convertToDateTime, convertDurationTimeToHHMMSS, timeAgo } from '../../utils/helpers';
import {
    MINUTE_TEXT,
} from '../../constants/messages';
import './index.scss';

class CallLogItem extends Component {
    render() {
        const { user, type } = this.props;
        let info = '';
        switch (type) {
            case 'CallLog':
                info = (
                    <>
                        <span className="call-log__info-date">{convertToDateTime(user.start_time)}</span>
                        <span className="call-log__info-user-name">{user.user_name}</span>
                        <span className="call-log__info-time">
                            <i className="icon icon-time" />
                            { user.duration ? convertDurationTimeToHHMMSS(user.duration) : "通話失敗"}{user.duration ? MINUTE_TEXT : null}
                        </span>
                    </>
                );
                break;

            case 'Favorite':
                info = (
                    <>
                        <span className="call-log__info-user-name">{user.user_name}</span>
                        <span className="call-log__info-time">
                            <i className="icon icon-time" />
                            { timeAgo(user.last_login) }
                        </span>
                    </>
                );
                break;

            case 'FootPrint':
            default:
                info = (
                    <>
                        <span className="call-log__info-date">{convertToDateTime(user.check_out_time)}</span>
                        <span className="call-log__info-user-name">{user.user_name}</span>
                        <span className="call-log__info-time">
                            <i className="icon icon-time" />
                            { timeAgo(user.last_login) }
                        </span>
                    </>
                );
                break;
        }

        return (
            <div className="call-log__item">
                <Link className="call-log__link" to={{ pathname: `/profile/${user.user_id}`, state: { user } }}>
                    <div className="call-log__avatar">
                        <Image isAvatar imageId={user.ava_id ? user.ava_id : null} />
                    </div>
                    <div className="call-log__info">
                        { info }
                    </div>
                </Link>
                <div className="call-log__group-button">
                    <UtilityButtonCall className="call-log__button" type={PrimaryUtilityTypes.VOICE} friend={user}>
                        <span className= {user.voice_call_waiting ? 'icon icon-onCall' : 'icon icon-offCall'}></span>
                    </UtilityButtonCall>

                    <UtilityButtonCall className="call-log__button" type={PrimaryUtilityTypes.VIDEO} friend={user}>
                        <span className= {user.voice_call_waiting ? 'icon icon-onVideoCall' : 'icon icon-offVideoCall'}></span>
                    </UtilityButtonCall>
                </div>
            </div>

        )
    }
}


CallLogItem.propTypes = {
    user: PropTypes.object,
    type: PropTypes.string,
};


export default CallLogItem;
