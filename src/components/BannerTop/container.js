import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from "react-slick";
import { connect } from 'react-redux';
import BannerService from '../../services/banner';
import Image from '../Image'
import storage from '../../utils/storage'
import './index.scss';
import { openWebView } from '../../actions/webview';

class BannerTop extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props)
        this.state = {
            listBanner: [],
        }
    }

    componentDidMount() {
        BannerService.getList().then(response => {
            if (!this.isUnmounted) {
                this.setState({ listBanner: response.data })
            }
        });
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };


    handleOpenWebView = (url) => {
        this.props.openWebView({
            url: String(url).replace('%%token%%', storage.getToken()),
            backButton: true,
            title: '',
        })
    };

    render() {
        const settings = {
            dots: false,
            arrows: false,
            infinite: true,
            autoplaySpeed: 3000,
            autoplay: true,
            slidesToShow: 1,
        };

        const banners = this.state.listBanner.map((entry, index) =>
            <div className="banner-top-wrap" key={index} onClick={() => this.handleOpenWebView(entry.url)} >
                <Image
                    imageId={entry.image_id}
                    imgKind={6}
                    className="banner-top-img"
                />
            </div>
        );

        if (banners.length > 1) {
            return (
                <div className="banner-top-wrap">
                    <Slider {...settings}>
                        {banners}
                    </Slider>
                </div>
            )
        }

        return banners;
    }
}

BannerTop.propTypes = {
    openWebView: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BannerTop)

