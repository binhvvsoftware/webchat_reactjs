import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Badge from '../Badge';

import './index.scss';

class Footer extends Component {
    render() {
        const { attentionNumbers } = this.props;

        return (
            <footer className="k-footer">
                <ul className="footer__tabs">
                    <li className="footer__tab">
                        <Link to="/foot-print">
                            <span className="position-relative">
                                <span className="icon icon-foot"></span>
                                <Badge className="nav-bar__badge" number={attentionNumbers.newCheckout} />
                            </span>
                        </Link>
                    </li>

                    <li className="footer__tab">
                        <Link to="/list-like">
                            <span className="position-relative">
                                <span className="icon icon-thumbs-up"></span>
                                <Badge className="nav-bar__badge" number={attentionNumbers.like} />
                            </span>
                        </Link>
                    </li>

                    <li className="footer__tab">
                        <Link to="/favorite">
                            <span className="position-relative">
                                <span className="icon icon-heart-sm"></span>
                                <Badge className="nav-bar__badge" number={attentionNumbers.newFavorite} />
                            </span>
                        </Link>
                    </li>

                    <li className="footer__tab">
                        <Link to="/setting-call">
                            <span className="position-relative">
                                <span className="icon icon-call-setting"></span>
                            </span>
                        </Link>
                    </li>
                </ul>
            </footer>
        );
    }
}

Footer.propTypes = {
    attentionNumbers: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
});

export default connect(mapStateToProps)(Footer);
