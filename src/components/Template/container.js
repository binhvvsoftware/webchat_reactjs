import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import MainLayout from '../MainLayout';
import TemplateItem from './item';
import TemplateForm from './form';
import TemplateService from '../../services/template';
import { toggleLoading } from '../../reducers/control/template'
import './index.scss';

class Template extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            templates: [],
            loading: false,
            activeTemplate: '',
            isOpenForm: false,
            templateForm: {},
        };
    }

    componentWillReceiveProps() {
        this.setState({
            activeTemplate: null,
        })
    }

    componentWillMount() {
        if (!this.isUnmounted) {
            this.handleCallApi();
        }
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    handleCallApi = () => {
        this.setState({
            loading: true,
        }, () => {
            TemplateService.getList()
                .then(response => {
                    this.setState({
                        templates: response.data,
                    });
                })
                .then(() => {
                    this.setState({
                        loading: false,
                    });
                });
        });
    }

    handleActiveTemplate = (template) => {
        if (template.template_id === this.state.activeTemplate) {
            this.setState({ activeTemplate: "" })
        } else {
            this.setState({ activeTemplate: template.template_id });
        }
    }

    handleOpenForm = () => {
        if (this.state.loading || this.state.templates.length >= 10) {
            return;
        }

        this.setState({ isOpenForm: true, templateForm: {} });
    }

    handleEdit = (templateForm) => {
        this.setState({ isOpenForm: true, templateForm });
    }

    handleDelete = (template) => {
        const { templates } = this.state;
        let key = 0;
        templates.forEach((item, i) => {
            if (item.template_id === template.template_id) {
                key = i;
            }
        });

        templates.splice(key, 1);
        this.setState({ templates });
    }

    handleFormSubmitted = () => {
        this.setState({ isOpenForm: false, loading: true, activeTemplate: '' }, () => {
            this.props.toggleLoading();
            this.handleCallApi();
        });
    }

    render() {
        const { activeTemplate, templates, templateForm, loading } = this.state;
        const { isChat, onUse, backFunction } = this.props;

        const templatesList = templates.map(item => (
            <TemplateItem
                template={item}
                isChat={isChat}
                isActive={activeTemplate === item.template_id}
                onActive={this.handleActiveTemplate}
                onDelete={this.handleDelete}
                onEdit={this.handleEdit}
                onUse={onUse}
                key={item.template_id}
            />
        ));

        return (
            <MainLayout
                className="template-page"
                title={`テンプレート(${templates.length}/10)`}
                hasFooter={false}
                hasSidebarLeft={false}
                hasBackButton
                backFunction={backFunction}
            >
                {loading && <div className="template__loading" />}
                {templates.length === 0 && !this.state.loading ?
                    <div className="no_data">最大10個まで登録できます。<br /> 右下の＋を押してよく使う内容を登録して下さい。</div> : ''
                }
                <div className="template__list">
                    { templatesList }
                </div>
                <div className={ templates.length >= 10 ? "template__btn-add is_disabled" : "template__btn-add"} onClick={this.handleOpenForm}>
                    <i className= "icon icon-plus-white"/>
                </div>

                <TemplateForm isOpen={this.state.isOpenForm} template={templateForm} onSubmitted={this.handleFormSubmitted}/>
            </MainLayout>
        );
    }
}

Template.propTypes = {
    backFunction: PropTypes.any,
    isChat: PropTypes.bool,
    onUse:PropTypes.func,
    toggleLoading: PropTypes.func.isRequired,
}

Template.defaultProps = {
    backFunction: '/my-page',
    isChat: false,
    onUse:() => {},
}

const mapStateToProps = (state) => ({
    isLoading: state.template.isLoading,
})
const mapDispatchToProps = (dispatch) => ({
    toggleLoading: () => dispatch(toggleLoading()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Template);
