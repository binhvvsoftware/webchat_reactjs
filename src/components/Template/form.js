import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Popup from 'react-popup';
// import Header from '../Header';
import TemplateService from '../../services/template';
import BannedWordService from '../../services/bannedWord';
import './index.scss';
import { BANNED_WORDS_MESSAGE_FORMAT, TEMPLATE_CONTEN_AND_TEMPLATE_TITLE_EMPTY } from '../../constants/messages';
import FullScreenModal from '../FullScreenModal/container';
import { toggleLoading } from '../../reducers/control/template'

class TemplateForm extends React.Component {
    isUnmounted = false;

    constructor(props) {
        super(props);

        const { template } = props;

        this.state = {
            template: {
                id: template.template_id,
                title: template.template_title || '',
                content: template.template_content || '',
            },
            loading: false,
            bannedWord: [],
            maxLength: 30,
            target: 'title',
        };
    }

    componentWillReceiveProps(nextProps) {
        const { template } = nextProps;
        this.setState({
            template: {
                id: template.template_id,
                title: template.template_title || '',
                content: template.template_content || '',
            },
        })
    }

    componentDidMount() {
        BannedWordService.getBannedWord()
            .then(response => {
                if (!this.isUnmounted) {
                    this.setState({
                        bannedWord: response.data.chat,
                    });
                }
            });
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    onFocus(target) {
        const maxLength = target === 'title' ? 30 : 100;

        this.setState({
            target,
            maxLength,
        });
    }

    handleChange = (e, target) => {
        const { template } = this.state;
        const maxLength = target === 'title' ? 30 : 100;
        template[target] = e.target.value.replace(/\s/g, '').substring(0, maxLength);

        this.setState({ template });
    }

    handleCheckBannedWord(value) {
        const checkBannedWord = [];
        const { bannedWord } = this.state;

        bannedWord.map((word) => {
            if (value.indexOf(word) !== -1) {
                checkBannedWord.push(word);
            }
            return '';
        });
        return checkBannedWord;
    }

    handleSubmit = () => {
        const { template } = this.state;

        const checkBannedWord = this.handleCheckBannedWord(template.title);
        checkBannedWord.concat(this.handleCheckBannedWord(template.content));

        if (checkBannedWord.length !== 0) {
            const message = BANNED_WORDS_MESSAGE_FORMAT.replace('%s', checkBannedWord[0]);
            Popup.alert(message);
            return;
        }

        if (template.title.length === 0 || template.content.length === 0) {
            Popup.alert(TEMPLATE_CONTEN_AND_TEMPLATE_TITLE_EMPTY);
            return;
        }

        let action = 'add';
        if (template.id) {
            action = 'update';
        }
        this.props.toggleLoading();
        TemplateService[action](template.title, template.content, template.id).then(this.props.onSubmitted);
    };

    render() {
        const { template, maxLength, target, loading } = this.state;
        const count = `${target === 'content' ? template.content.length : template.title.length}/${maxLength}`;

        return (
            <FullScreenModal className="template-form" title="テンプレートの作成" isOpen={this.props.isOpen}>
                <div className="template-page">
                    {loading && <div className="template__loading" />}
                    <main className="k-main">
                        <form className="template__form" onSubmit={(e) => { e.preventDefault(); this.handleSubmit() }}>
                            <div>
                                <input
                                    className="template__input-title"
                                    value={template.title}
                                    onChange={(e) => this.handleChange(e, 'title')}
                                    type="text"
                                    placeholder="テンプレートタイトル"
                                    onFocus={() => this.onFocus('title')}
                                />
                            </div>

                            <div>
                                <textarea
                                    className="template__input-content"
                                    onChange={(e) => this.handleChange(e, 'content')}
                                    placeholder="本文"
                                    value={template.content}
                                    onFocus={() => this.onFocus('content')}
                                />
                            </div>

                            <div>
                                <span className="template__count">{count}</span>
                            </div>
                            <div className="text-center">
                                <button className="template__btn-submit" disabled={this.props.isLoading} type='submit'>保存する</button>
                            </div>
                        </form>
                    </main>
                </div>
            </FullScreenModal>
        );
    }
}


TemplateForm.propTypes = {
    template: PropTypes.object,
    isOpen: PropTypes.bool,
    onSubmitted: PropTypes.func,
    toggleLoading:PropTypes.func.isRequired,
    isLoading:PropTypes.bool.isRequired,
};

TemplateForm.defaultProps = {
    template: {},
    isOpen: false,
    onSubmitted: () => { },
};

const mapStateToProps = (state) => ({
    isLoading: state.template.isLoading,
})
const mapDispatchToProps = (dispatch) => ({
    toggleLoading: () => dispatch(toggleLoading()),
})

export default connect(mapStateToProps, mapDispatchToProps)(TemplateForm);
