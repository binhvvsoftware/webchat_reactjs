import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import './index.scss';
import TemplateService from '../../services/template';
import { TEMPLATE_DELETE_BUTTON_TITLE, BUTTON_NO_TEMPLATE, BUTTON_YES_TEMPLATE } from '../../constants/messages';

class TemplateItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isDelete: false,
        };
    }

    handleConfirmDelete = () => {
        Popup.create({
            content: TEMPLATE_DELETE_BUTTON_TITLE,
            buttons: {
                left: [{
                    text: BUTTON_NO_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: BUTTON_YES_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        this.handleDismiss();
                        Popup.close();
                    },
                }],
            },
        });
    };

    handleDismiss = () => {
        this.setState({
            isLoading: true,
        }, () => {
            TemplateService.delete(this.props.template.template_id)
                .then(() => {
                    this.setState({ isDelete: true });
                    this.props.onDelete(this.props.template);
                })
                .then(() => this.setState({ isLoading: false }));
        });
    };

    handleToggle = () => {
        this.props.onActive(this.props.template);
    };

    handleEdit = () => {
        this.props.onEdit(this.props.template);
    }

    handleUse = () => {
        this.props.onUse(this.props.template);
    };

    render() {
        const { template, isChat, isActive } = this.props;
        const { isDelete, isLoading } = this.state;

        return !isDelete && (
            <div className={ `template__item ${isLoading ? 'is_loading' : ''} ${ isActive ? 'is_active' : ''}` }>
                <div className="template__item-header">
                    <div className="template__item-title" onClick={ this.handleToggle }>{ template.template_title }</div>

                    {
                        <div className="template__btn-delete" onClick={ this.handleConfirmDelete }>
                            <i className="icon icon-recycle-grey"/>
                        </div>
                    }
                </div>
                <div className="template__content">
                    <div className="template__text">{ template.template_content }</div>
                    <div className="template__buttons">
                        <button className="template__btn" type="button" onClick={this.handleEdit}>
                            <i className="icon icon-write-pink"/>編集する
                        </button>
                        {
                            isChat && <button className="template__btn is_use" type="button" onClick={this.handleUse}>
                                <i className="icon icon-check-pink"/>挿入する
                            </button>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

TemplateItem.propTypes = {
    template: PropTypes.object,
    isChat: PropTypes.bool,
    isActive: PropTypes.bool,
    onActive: PropTypes.func,
    onDelete: PropTypes.func,
    onEdit: PropTypes.func,
    onUse: PropTypes.func,
};

TemplateItem.defaultProps = {
    template: {},
    isChat: false,
    isActive: false,
    onActive: () => {},
    onDelete: () => {},
    onEdit:() => {},
    onUse: PropTypes.func,
};

export default TemplateItem;
