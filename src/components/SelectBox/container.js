import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Picker } from 'react-native-web';

class SelectBox extends Component {
    constructor(props) {
        super(props);
        const className = window['__select-box'] || 0;
        window['__select-box'] = className + 1;

        this.state = {
            enabled: props.enabled,
            selected: props.selected,
            className: `picker-hide-empty ${className}`,
        };
    }

    handleSelected = (value) => {
        const selected = String(value).trim();
        this.setState({ selected });
        this.props.onSelected(selected);
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: nextProps.selected,
            enabled: nextProps.enabled,
        })
    }

    componentDidMount() {
        try {
            document.querySelector(`.picker-hide-empty.${this.state.className} [data-testid="empty"]`).setAttribute('disabled', 'disabled');
        } catch (e) {
            //
        }
    }

    render() {
        let pickerItems = [];
        if (Array.isArray(this.props.options)) {
            pickerItems = this.props.options.map(item => <Picker.Item label={String(item.label)} value={item.value} key={item.value} />)
        } else {
            pickerItems = Object.keys(this.props.options).map(key => <Picker.Item label={String(this.props.options[key])} value={key} key={key} />)
        }
        if (!this.state.selected && this.props.hasEmpty) {
            pickerItems.unshift(<Picker.Item label={this.props.textEmpty} testID="empty" key=""/>);
        }


        return (
            <Picker
                className={this.state.selected ||  this.state.selected === 0 ? `picker-hide-empty pink-text ${this.state.className}`: `picker-hide-empty grey-text ${this.state.className}`}
                selectedValue={this.state.selected}
                enabled={this.state.enabled}
                onValueChange={this.handleSelected}>
                {pickerItems}
            </Picker>
        );
    }
}

SelectBox.propTypes = {
    options: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    selected: PropTypes.any,
    enabled: PropTypes.bool,
    hasEmpty: PropTypes.bool,
    textEmpty: PropTypes.string,
    onSelected: PropTypes.func,
};

SelectBox.defaultProps = {
    options: {},
    hasEmpty: true,
    enabled: true,
    textEmpty: '',
    onSelected: () => {},
};


export default SelectBox;
