import React, { Component } from 'react';
import { connect } from 'react-redux';
import './index.scss';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { buyPoint } from '../../services/webview';
import {
    TITLE_BUYPOINT,
    CONTENT_BUYPOINT,
    BUTTON_BUYPOINT_YES,
    BUTTON_BUYPOINT_NO,
} from '../../constants/messages';
import { openWebView } from '../../actions/webview';

class BuyPoint extends Component {
    replaceText(text){
        return text.split('\n').map((item, key) => <span key={key}>{item}<br/></span>)
    }

    render() {
        const contentBuyPoint = this.replaceText(CONTENT_BUYPOINT);
        this.props.closeBuyPoint();
        Popup.create({
            title: TITLE_BUYPOINT,
            content: contentBuyPoint,
            buttons: {
                left: [{
                    text: BUTTON_BUYPOINT_NO,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: BUTTON_BUYPOINT_YES,
                    className: 'ok',
                    action: () => {
                        this.props.openWebView({
                            url: buyPoint(),
                            backButton: true,
                        });
                        Popup.close();
                    },
                }],
            },
        });

        return (
            <></>
        )
    }
}

BuyPoint.propTypes = {
    openWebView: PropTypes.func.isRequired,
    closeBuyPoint : PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
});

export default connect(null, mapDispatchToProps)(BuyPoint);
