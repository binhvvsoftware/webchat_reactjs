import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import history from '../../utils/history';
import ProfileService from '../../services/profile';
import SettingNoticePopup from '../SettingNoticePopup';
import ReportUser from '../ReportUser'
import {
    BLOCK_USER_MSG_TITLE,
    CONTENT_BLOCK_MESSAGE,
    TITE_MESSAGE_SETTING_NOTICE,
    SET_ONLINE_ALERT,
    ALERT_BUTTON_NO_TITLE,
    ALERT_BUTTON_YES_TITLE,
} from '../../constants/messages';
import storage from '../../utils/storage';
import './index.scss';
import { openGiftPool } from '../../actions/Chat';
import { toggleReportUserDialog } from '../../actions/Utility/index'

class TopMenuProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: props.isOpen,
            isOpenSettingNotice: false,
            isAlertNotice: props.user.isAlertNotice,
        };
        this.handleCloseSettingNotice = this.handleCloseSettingNotice.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.isOpen,
        })
    }

    componentDidUpdate() {
        this.handleCheckModalOpen();
    }

    handleCheckModalOpen = () => {
        if (document.querySelectorAll('.menu-profile').length) {
            document.body.classList.add('menu-profile-open');
        } else {
            document.body.classList.remove('menu-profile-open');
        }
    }

    handleCloseModal = () => {
        this.handleCheckModalOpen();
        this.setState({
            open: false,
        });
        this.props.onClose();
    }

    handleOpenSettingNotice(isAlertNotice) {
        if (!this.isMyProfile()) {
            this.setState({
                isOpenSettingNotice: true,
                isAlertNotice,
            });
        }
    }

    handleCloseSettingNotice(statusSettingOnline) {
        this.setState({
            isOpenSettingNotice: false,
            isAlertNotice: statusSettingOnline,
        });
    }

    handleBlock() {
        if (!this.isMyProfile()) {
            Popup.create({
                title: BLOCK_USER_MSG_TITLE,
                content: this.props.user.user_name + CONTENT_BLOCK_MESSAGE,
                buttons: {
                    left: [{
                        text: ALERT_BUTTON_NO_TITLE,
                        className: 'block_cancel',
                        action: () => {
                            Popup.close();
                        },
                    }],

                    right: [{
                        text: ALERT_BUTTON_YES_TITLE,
                        className: 'block_ok',
                        action: () => {
                            ProfileService.profileBlock(this.props.user.user_id)
                                .then(() => {
                                    Popup.close();
                                    history.push("/home");
                                });
                        },
                    }],
                },
            });
        }
    }

    isMyProfile() {
        const myInfo = storage.getUserInfo()
        const userInfo = this.props.user
        if (myInfo.user_id === userInfo.user_id) {
            return true
        }
        return false
    }

    handleOpenGiff = () => {
        if (this.isMyProfile()) {
            return;
        }
        this.props.openGiftPool({
            you: {
                id: this.props.user.user_id,
                data: this.props.user,
            },
        });
        this.props.onClose();
    }

    render() {
        return (
            this.state.open &&
            <div className="menu-profile">
                <div className="menu-profile__overlay" onClick={this.handleCloseModal} />
                <div className="menu-profile__list">
                    <div className="menu-profile__item" onClick={this.handleOpenGiff}>
                        <span className="icon icon-gift" />
                        <span className="d-block">ギフト</span>
                    </div>
                    <div
                        className="menu-profile__item" onClick={() => { this.handleOpenSettingNotice() }}>
                        <span className="icon icon-notify-ol" />
                        <span className="d-block">{this.state.isAlertNotice ? SET_ONLINE_ALERT : TITE_MESSAGE_SETTING_NOTICE}</span>
                    </div>
                    <div className="menu-profile__item" onClick={() => this.handleBlock()}>
                        <span className="icon icon-block" />
                        <span className="d-block">ブロック</span>
                    </div>
                    <div className="menu-profile__item" onClick={() => this.props.toggleReportUserDialog()}>
                        <span className="icon icon-report" />
                        <span className="d-block">報告</span>
                    </div>
                </div>
                <SettingNoticePopup user={this.props.user} isOpen={this.state.isOpenSettingNotice} onClose={this.handleCloseSettingNotice} />
                <ReportUser isMyUser={this.isMyProfile()} userId={this.props.user.user_id} />
            </div>
        );
    }
}

TopMenuProfile.propTypes = {
    isOpen: PropTypes.bool,
    user: PropTypes.object,
    onClose: PropTypes.func,
    openGiftPool: PropTypes.func.isRequired,
    toggleReportUserDialog: PropTypes.func.isRequired,
};

TopMenuProfile.defaultProps = {
    isOpen: false,
    user: {},
    onClose: () => { },
};


const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
    openGiftPool: payload => dispatch(openGiftPool(payload)),
    toggleReportUserDialog: () => dispatch(toggleReportUserDialog()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TopMenuProfile);
