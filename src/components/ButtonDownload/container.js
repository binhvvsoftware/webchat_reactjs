import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import MediaService from '../../services/media';
import SaveImageService from '../../services/imageDowload';
import {
    TITLE_COMFIRM_SAVE_IMAGE,
    CONTENT_COMFIRM_SAVE_IMAGE,
    ALERT_BUTTON_YES_TITLE,
    ALERT_BUTTON_NO_TITLE,
    VIEWER_PICTURE_SAVE_PICTURE_LABLE_TITLE,
} from '../../constants/messages';
import './index.scss';
import { openDownload } from '../../actions/download';
import { openBuyPoint } from '../../actions/index';

class ButtonDownload extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            pointSaveImage: 0,
        };
    };

    componentDidMount() {
        if (this.props.imageId) {
            SaveImageService.getPointSaveImage(this.props.imageId)
                .then(response => {
                    if (!this.isUnmounted) {
                        this.setState({
                            pointSaveImage: response.data.save_image_point,
                        });
                    }
                });
        }
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    handleDownloadImage = () => {
        if (!this.props.imageId || this.state.isLoading) {
            return;
        }

        const contentMessage = CONTENT_COMFIRM_SAVE_IMAGE.replace(/%s/, this.state.pointSaveImage);

        Popup.create({
            title: TITLE_COMFIRM_SAVE_IMAGE,
            content: contentMessage,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_NO_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
                right: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'ok',
                    action: () => {
                        this.downLoadImage();
                        Popup.close();
                    },
                }],
            },
        });
    };

    downLoadImage() {
        this.setState({
            isLoading: true,
        }, () => {
            SaveImageService.saveImage(this.props.imageId)
                .then(() => {
                    if (this.props.allowDown && this.state.isLoading) {
                        this.setState({ isLoading: false }, () => {
                            this.createPageDownload();
                        });
                    }
                })
                .catch(err => {
                    if (err.code === 70) {
                        this.props.openBuyPoint();
                    }
                })
                .then(() => {
                    this.setState({ isLoading: false });
                });
        });
    }

    createPageDownload = () => {
        const { imageUrl, imageId, username } = this.props;
        let url = imageUrl;
        const name = `${username || imageId || 'avatar'}.jpg`;

        if (imageId) {
            url = MediaService.downloadImageURL(imageId, name);
        }

        this.props.openDownload({
            url,
            name,
        });
    };

    render() {
        return (
            <div className={ `page-dowload ${this.props.className}` }>
                { this.state.isLoading && <div className="buzz__loading"/> }
                <span className="download-btn" onClick={ this.handleDownloadImage }>
					 <div className="gallery-user-btn ">
                        <span className="icon icon-cloud-download"/>
                        <span className="download-label">{ VIEWER_PICTURE_SAVE_PICTURE_LABLE_TITLE }</span>
					 </div>
                </span>
            </div>
        );
    }
}

ButtonDownload.propTypes = {
    allowDown: PropTypes.bool,
    username: PropTypes.string,
    className: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    openDownload: PropTypes.func.isRequired,
    openBuyPoint: PropTypes.func.isRequired,
};

ButtonDownload.defaultProps = {
    allowDown: true,
    className: '',
    username: '',
    imageId: '',
    imageUrl: '',
};

const mapDispatchToProps = (dispatch) => ({
    openDownload: payload => dispatch(openDownload(payload)),
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
});

const mapStateToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ButtonDownload);
