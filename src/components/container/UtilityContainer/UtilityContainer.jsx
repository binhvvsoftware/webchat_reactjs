import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import "./UtilityContainer.scss";
import isMobile from 'ismobilejs';
import Utility from "../../presentational/Utility";

import { PrimaryUtilityTypes } from "../../../constants/types";

class UtilityContainer extends Component {
    render() {
        const rootClass = classNames({
            "natural": true,
            "natural-mobile": isMobile.any,
            "natural-desktop": !isMobile.any,
            "natural-utility-container-root": true,
            "natural-utility-container-active": true,
        });

        const { friend } = this.props;

        return (
            <div className={rootClass}>
                <div className="natural natural-wrapper">
                    <Utility friend={ friend } type={PrimaryUtilityTypes.CHAT} />
                    <Utility friend={ friend } type={PrimaryUtilityTypes.VOICE} />
                    <Utility friend={ friend } type={PrimaryUtilityTypes.VIDEO} />
                </div>
            </div>
        );
    }
}

UtilityContainer.propTypes = {
    friend: PropTypes.object.isRequired,
}

export default UtilityContainer;
