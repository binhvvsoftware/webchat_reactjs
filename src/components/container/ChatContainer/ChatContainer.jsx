import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classNames from "classnames";
import Drawer from 'react-motion-drawer';
import "./ChatContainer.scss";
import ChatImageBox from "../../presentational/Chat/ChatImageBox";
/* Import Presentationals */
import ChatHeader from "../../presentational/Chat/ChatHeader";
import ChatPool from "../../presentational/Chat/ChatPool";
import ChatFooter from "../../presentational/Chat/ChatFooter";
import SidebarLeft from '../../MainLayout/SidebarLeft'
import SidebarRight from '../../MainLayout/SidebarRight'
import Memo from '../../Memo';
import ReportUser from '../../ReportUser'
import { closeGiftPool, openFooterKeyboard } from '../../../actions/Chat';
import { openChatConversation, closeChatConversation, openSidebarMenu, closeSidebarMenu } from '../../../actions';
import { changeWhoYouAre } from '../../../actions/Tree'

const mapStateToProps = state => ({
    isShowButtonBack: state.chatControl.isShowButtonBack,
    clientChain: state.chatControl.clientChain,
    you: state.chatControl.you,
    isOpenSidebarMenu: state.main.isOpenSidebarMenu,
    isOpenChatConversation: state.main.isOpenChatConversation,
    imageBox: state.chatControl.imageBox,
});

const mapDispatchToProps = (dispatch) => ({
    closeGiftPool: payload => dispatch(closeGiftPool(payload)),
    changeWhoYouAre: payload => dispatch(changeWhoYouAre(payload)),
    openSidebarMenu: (payload) => dispatch(openSidebarMenu(payload)),
    closeSidebarMenu: (payload) => dispatch(closeSidebarMenu(payload)),
    openChatConversation: () => dispatch(openChatConversation()),
    closeChatConversation: () => dispatch(closeChatConversation()),
    openFooterKeyboard: payload => dispatch(openFooterKeyboard(payload)),
});

class ConnectedChatContainer extends Component {
    getImageBox = ()=>  {
        const box = this.props.imageBox;

        return box.isOpened ? (
            <ChatImageBox message={box.message} />
        ) : null;
    }

    handleToggleSidebarLeft = (open) => {
        this.props.closeChatConversation();
        if (open) {
            this.props.openSidebarMenu();
        } else {
            this.props.closeSidebarMenu();
        }
    };

    handleToggleSidebarRight = (open) => {
        this.props.closeSidebarMenu();
        if (open) {
            this.props.openChatConversation();
        } else {
            this.props.closeChatConversation();
        }
    }

    handleCallbackUpdateMemo(content) {
        const youData = this.props.you.data;
        youData.memo = content;

        this.props.changeWhoYouAre({
            id: this.props.you.id,
            data: youData,
        })
    }

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-chat-container-root': true,
            'natural-chat-container-active': true,
            'sidebar__parent': true,
        });

        const {
            isOpenSidebarMenu,
            isOpenChatConversation,
        } = this.props;

        const drawerPropsLeft = {
            handleWidth: 15,
            className: 'sidebar is_left',
            overlayClassName: 'sidebar__overlay',
            open: isOpenSidebarMenu,
            noTouchClose: false,
            onChange: open => this.handleToggleSidebarLeft(open),
        };

        const drawerPropsRight = {
            handleWidth: 15,
            className: 'sidebar is_right',
            overlayClassName: 'sidebar__overlay',
            open: isOpenChatConversation,
            noTouchClose: false,
            right: true,
            onChange: open => this.handleToggleSidebarRight(open),
        };

        return (
            <div id="chat-primary" className={rootClass}>
                {
                    !this.props.isShowButtonBack && <Drawer {...drawerPropsLeft}>
                        <SidebarLeft />
                    </Drawer>
                }

                {this.getImageBox()}
                <Drawer {...drawerPropsRight}>
                    <SidebarRight />
                </Drawer>
                <div className="natural natural-wrapper natural-wrapper-v">
                    <ChatHeader />
                    <ChatPool />
                    <ChatFooter />
                </div>
                <Memo user={this.props.you.data} content={this.props.you.data.memo} callBackUpdate={(content) => { this.handleCallbackUpdateMemo(content) }} />
                <ReportUser userId={this.props.you.id} />
            </div>
        );
    }
}

ConnectedChatContainer.propTypes = {
    isShowButtonBack: PropTypes.bool.isRequired,
    clientChain: PropTypes.array.isRequired,
    you: PropTypes.object.isRequired,
    isOpenSidebarMenu: PropTypes.bool.isRequired,
    openSidebarMenu: PropTypes.func.isRequired,
    closeSidebarMenu: PropTypes.func.isRequired,
    isOpenChatConversation: PropTypes.bool.isRequired,
    openChatConversation: PropTypes.func.isRequired,
    closeChatConversation: PropTypes.func.isRequired,
    changeWhoYouAre: PropTypes.func.isRequired,
    openFooterKeyboard: PropTypes.func.isRequired,
    imageBox: PropTypes.object.isRequired,
}

const ChatContainer = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatContainer);

export default ChatContainer;
