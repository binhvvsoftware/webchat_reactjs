import React, { Component } from 'react';
/* Prop */
import PropTypes from 'prop-types';
/* Styling */
import classNames from 'classnames';
import history from '../../../utils/history';
import UtilityButtonCall from './UtilityButtonCall';
import { PrimaryUtilityTypes } from '../../../constants/types';
import storage from '../../../utils/storage';

class Utility extends Component {
    handleClickCall = () => {
        const myUser = storage.getUserInfo();
        const { friend } = this.props;
        if (friend.user_id === myUser.user_id) {
            return;
        }

        if (this.props.type === PrimaryUtilityTypes.CHAT) {
            history.push(`/chat/${friend.user_id}`);
        }
    };

    getDecoration() {
        const statusVoiceCall = this.props.friend.voice_call_waiting;
        const statusVideoCall = this.props.friend.video_call_waiting;

        switch (this.props.type) {
            case PrimaryUtilityTypes.CHAT:
                return (
                    <div className="natural natural-deco">
                        <span className="icon icon-chat-white"></span>
                    </div>
                );
            case PrimaryUtilityTypes.VOICE:
                return (
                    <UtilityButtonCall
                        type={ PrimaryUtilityTypes.VOICE }
                        friend={this.props.friend}
                        className={ `natural natural-deco ${statusVoiceCall ? '' : 'natural-deco-offline'}` }>
                        <span className={ statusVoiceCall ? 'icon icon-call' : 'icon icon-call-blue'}></span>
                    </UtilityButtonCall>
                );
            case PrimaryUtilityTypes.VIDEO:
                return (
                    <UtilityButtonCall
                        type={ PrimaryUtilityTypes.VIDEO }
                        friend={this.props.friend}
                        className={ `natural natural-deco ${statusVideoCall ? '' : 'natural-deco-offline'}` }>
                        <span className={ statusVideoCall ? 'icon icon-video' : 'icon icon-video-blue'}></span>
                    </UtilityButtonCall>
                );
            default:
                return null;
        }

    }

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-btn': true,
            'natural-utility-btn': true,
            'natural-utility-chat-btn-root': true,
            'natural-utility-chat-btn-active': this.props.active,
            'natural-utility-pink-btn': this.props.type === PrimaryUtilityTypes.CHAT,
            'natural-utility-blue-btn': this.props.type === PrimaryUtilityTypes.VOICE || this.props.type === PrimaryUtilityTypes.VIDEO,
        });

        return (
            <div className={ rootClass } onClick={ this.handleClickCall }>
                <div className="natural-wrapper">
                    { this.getDecoration() }
                </div>
            </div>
        );
    }
}

Utility.propTypes = {
    friend: PropTypes.object,
    active: PropTypes.bool,
    type: PropTypes.string.isRequired,
};

Utility.defaultProps = {
    active: true,
};

export default Utility;
