import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import { PrimaryUtilityTypes, MessageTypes } from '../../../constants/types';
import storage from '../../../utils/storage';
import {
    ALERT_BUTTON_CANCEL_TITLE,
    BUTTON_YES_TEMPLATE, OK,
    TITLE_CONFIRM_CALL_VIDEO,
    TITLE_CONFIRM_CALL_VOICE,
} from '../../../constants/messages';

import {
    closeChatHeaderMenuButton,
    openBuyingPointPromotion,
} from '../../../actions/Chat';
import * as AppCall from '../../../utils/app-call';
import {
    openBuyPoint,
} from '../../../actions/index';
import { sendMessage } from '../../../actions/socket';

class UtilityButtonCall extends Component {
    handleSendMessage = (contentSend) => {
        if (contentSend) {
            this.props.sendMessage({
                type: MessageTypes.CALLREQ,
                to: this.props.friend.user_id,
                content: contentSend,
            });

            this.props.closeChatHeaderMenuButton();
        }
    };

    popupConfirm = (contentFirstConfirm, contentConfirm, contentSend) => {
        Popup.create({
            title: '',
            content: contentFirstConfirm,
            buttons: {
                left: [{
                    text: BUTTON_YES_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        Popup.create({
                            title: '',
                            content: contentConfirm,
                            buttons: {
                                left: [{
                                    text: OK,
                                    className: 'ok',
                                    action: () => {
                                        Popup.close();
                                        this.handleSendMessage(contentSend);
                                    },
                                }],

                                right: [{
                                    text: ALERT_BUTTON_CANCEL_TITLE,
                                    className: 'ok',
                                    action: () => {
                                        Popup.close();
                                    },
                                }],
                            },
                        });

                        Popup.close();
                    },
                }],

                right: [{
                    text: ALERT_BUTTON_CANCEL_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
            },
        });
    };

    handleCheckCall = () => {
        const myUser = storage.getUserInfo();
        const { friend } = this.props;
        if (friend.user_id === myUser.user_id) {
            return;
        }

        const username = friend.user_name || '';
        const contentConfirm = `${username}さんは現在通話ができません。リクエス卜してお誘いしましょう(無料)。`;

        if (this.props.type === PrimaryUtilityTypes.VOICE) {
            if (friend.voice_call_waiting) {
                this.handleOpenAllCall();
                return;
            }

            this.popupConfirm(TITLE_CONFIRM_CALL_VOICE, contentConfirm, `${username}さん、音声通話しませんか?`);
            return;
        }

        if (this.props.type === PrimaryUtilityTypes.VIDEO) {
            if (friend.video_call_waiting) {
                this.handleOpenAllCall();
                return;
            }

            this.popupConfirm(TITLE_CONFIRM_CALL_VIDEO, contentConfirm, `${username}さん、ビデオ通話しませんか?`);
        }
    };


    handleOpenAllCall = () => {
        const pointUser = this.props.me.data.point;
        if (pointUser <= 0 ) {
            this.props.openBuyPoint();
            this.props.closeChatHeaderMenuButton();
            return;
        }
        this.props.closeChatHeaderMenuButton();

        const url = this.props.type === PrimaryUtilityTypes.VOICE ?
            AppCall.buildLinkVoiceCall(this.props.friend) : AppCall.buildLinkVideoCall(this.props.friend);

        window.open(url, '_blank');
        // history.push(`/call/${ this.props.friend.user_id }`);
    };

    render() {
        return (
            <div className={ this.props.className } onClick={ this.handleCheckCall }>
                { this.props.children }
            </div>
        );
    }
}

UtilityButtonCall.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    closeChatHeaderMenuButton: PropTypes.func.isRequired,
    className: PropTypes.string,
    type: PropTypes.string,
    friend: PropTypes.object,
    me: PropTypes.object,
    openBuyPoint: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
    ]),
};

UtilityButtonCall.defaultProps = {
    className: '',
};

const mapStateToProps = (state) => ({
    me: state.chatControl.me,
});

const mapDispatchToProps = (dispatch) => ({
    closeChatHeaderMenuButton: payload => dispatch(closeChatHeaderMenuButton(payload)),
    openBuyingPointPromotion: payload => dispatch(openBuyingPointPromotion(payload)),
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UtilityButtonCall);
