import React, { Component } from 'react';
import Popup from 'react-popup';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import Favorite from '../../../Favorite';
import './ChatHeaderDock.scss';
import {
    clickChatHeaderMenuMask,
    openGiftPool,
    changeWhoYouAre,
} from '../../../../actions/Chat';
import history from '../../../../utils/history';
import { toggleReportUserDialog } from '../../../../actions/Utility';
import { PrimaryUtilityTypes } from '../../../../constants/types';
import {
    ADD_TO_FAVORITES,
    BTN_TITLE_FAVORITED,
    BLOCK_USER_MSG_TITLE,
    CONTENT_BLOCK_MESSAGE,
    ALERT_BUTTON_NO_TITLE,
    ALERT_BUTTON_YES_TITLE,
    GIFT,
} from '../../../../constants/messages';
import ProfileService from '../../../../services/profile';
import UtilityButtonCall from '../../Utility/UtilityButtonCall';

const mapStateToProps = (state) => ({
    active: state.chatControl.isHeaderMenuOpened,
    you: state.chatControl.you,
});

const mapDispatchToProps = (dispatch) => ({
    clickChatHeaderMenuMask: payload => dispatch(clickChatHeaderMenuMask(payload)),
    openGiftPool: payload => dispatch(openGiftPool(payload)),
    changeWhoYouAre: payload => dispatch(changeWhoYouAre(payload)),
    toggleReportUserDialog: () => dispatch(toggleReportUserDialog()),
});

class ConnectedChatHeaderDock extends Component {
    handleOnSendingGiftClick = () => {
        this.props.openGiftPool({
            you: this.props.you,
        });
    }

    handleOnChatHeaderMenuMaskClick = () => {
        this.props.clickChatHeaderMenuMask();
    }

    handleFavoriteCallback = () => {
        const youData = this.props.you.data;
        youData.is_fav = youData.is_fav === 1 ? 0 : 1;

        this.props.changeWhoYouAre({
            id: this.props.you.id,
            data: youData,
        });
    }

    handleOnClickUserBlockButton = () => {
        Popup.create({
            title: BLOCK_USER_MSG_TITLE,
            content: this.props.you.data.user_name + CONTENT_BLOCK_MESSAGE,
            buttons: {
                left: [{
                    text: ALERT_BUTTON_NO_TITLE,
                    className: 'block_cancel',
                    action: () => {
                        Popup.close();
                    },
                }],

                right: [{
                    text: ALERT_BUTTON_YES_TITLE,
                    className: 'block_ok',
                    action: () => {
                        ProfileService.profileBlock(this.props.you.id)
                            .then(() => {
                                Popup.close();
                                history.replace('/home');
                            });
                    },
                }],
            },
        });
    }

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-chat-header-dock-root': true,
            'natural-chat-header-dock-root-active': this.props.active,
        });
        const statusVoiceCall = this.props.you.data.voice_call_waiting;
        const statusVideoCall = this.props.you.data.video_call_waiting;

        return !this.props.active ? null : (
            <div className={ rootClass }>
                <div className="natural-wrapper natural-quick-middle">
                    <div className="natural-chat-header-dock-item natural-chat-header-dock-item-gift" onClick={ this.handleOnSendingGiftClick }>
                        <div className="natural-wrapper natural-wrapper-v">
                            <div className="natural-deco">
                                <span className="icon icon-gift"></span>
                            </div>
                            <div className="natural-literal">{ GIFT }</div>
                        </div>
                    </div>
                    <div className="natural-chat-header-dock-item">
                        <Favorite
                            user={ this.props.you.data }
                            favoriteCallback={ () => this.handleFavoriteCallback() }
                            className="natural-wrapper natural-wrapper-v"
                            classNameActive="natural-wrapper natural-wrapper-v">
                            <div className="natural-deco">
                                <span className={this.props.you.data.is_fav === 1 ? "icon icon-red-heart" : "icon icon-heart-ol-sm "}></span>
                            </div>
                            <div
                                className="natural-literal">{ this.props.you.data.is_fav === 1 ? BTN_TITLE_FAVORITED : ADD_TO_FAVORITES }</div>
                        </Favorite>
                    </div>
                    <div className="natural-chat-header-dock-item">
                        <div className="natural-wrapper natural-wrapper-v" onClick={ this.handleOnClickUserBlockButton }>
                            <div className="natural-deco">
                                <span className="icon icon-block"></span>
                            </div>
                            <div className="natural-literal">ブロック</div>
                        </div>
                    </div>
                    <div className="natural-chat-header-dock-item">
                        <div className="natural-wrapper natural-wrapper-v" onClick={ this.props.toggleReportUserDialog }>
                            <div className="natural-deco">
                                <span className="icon icon-report"></span>
                            </div>
                            <div className="natural-literal">報告</div>
                        </div>
                    </div>
                </div>
                <div className="menu-call">
                    <UtilityButtonCall
                        className="menu-call-btn"
                        type={PrimaryUtilityTypes.VOICE}
                        friend={this.props.you.data}
                    >
                        <i className={ `icon ${statusVoiceCall ? 'icon-phone-call-blue' : 'icon-phone-call-ol'}` }/>
                        <span>音声通話<br/>{ statusVoiceCall ? '' : 'リクエスト' }</span>
                    </UtilityButtonCall>

                    <UtilityButtonCall
                        className="menu-call-btn"
                        type={PrimaryUtilityTypes.VIDEO}
                        friend={this.props.you.data}
                    >
                        <i className={ `icon ${statusVideoCall ? 'icon-video-call-blue' : 'icon-video-call-ol'}` }/>
                        <span>ビデオ通話<br/>{ statusVideoCall ? '' : 'リクエスト' }</span>
                    </UtilityButtonCall>
                </div>
                <div className="natural-mask" onClick={ this.handleOnChatHeaderMenuMaskClick }/>
            </div>
        );
    }
}

ConnectedChatHeaderDock.propTypes = {
    active: PropTypes.bool.isRequired,
    you: PropTypes.object.isRequired,
    clickChatHeaderMenuMask: PropTypes.func.isRequired,
    openGiftPool: PropTypes.func.isRequired,
    changeWhoYouAre: PropTypes.func.isRequired,
    toggleReportUserDialog: PropTypes.func.isRequired,
};

const ChatHeaderDock = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatHeaderDock);

export default ChatHeaderDock;
