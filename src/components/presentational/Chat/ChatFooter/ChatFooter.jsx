import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classNames from "classnames";
import ChatFooterInput from "../ChatFooterInput";
import ChatFooterEmoji from "../ChatFooterEmoji";
import ChatFooterPlusMenu from "../ChatFooterPlusMenu";
import ChatFooterTyping from '../ChatFooterTyping';
import ChatGalleryHistory from "../ChatGalleryHistory";
import ChatTemplate from "../ChatTemplate";
import "./ChatFooter.scss";
import {
    openFooterPlusMenu,
    openFooterKeyboard,
    openFooterEmoji,
    closeFooterPlusMenu,
} from "../../../../actions/Chat";

const mapStateToProps = (state) => ({
    isPlusMenuOpened: state.chatControl.isFooterPlusMenuOpened,
    isOpenGalleryHistory: state.chatControl.isOpenGalleryHistory,
    isFooterEmoticonMenuOpened: state.chatControl.isFooterEmoticonMenuOpened,
    isFooterKeyBoardOpened: state.chatControl.isFooterKeyBoardOpened,
    isOpenTemplate: state.chatControl.isOpenTemplate,
});

const mapDispatchToProps = (dispatch) => ({
    openFooterPlusMenu: payload => dispatch(openFooterPlusMenu(payload)),
    closeFooterPlusMenu: payload => dispatch(closeFooterPlusMenu(payload)),
    openFooterKeyboard: payload => dispatch(openFooterKeyboard(payload)),
    openFooterEmoji: payload => dispatch(openFooterEmoji(payload)),
});

class ConnectedChatFooter extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showCall: false,
        }
        this.onClickCallFooter = this.onClickCallFooter.bind(this);
    }

    onClickCallFooter(statusShowCall) {
        this.setState({
            showCall: statusShowCall,
        });
        this.props.closeFooterPlusMenu();
    }

    handleOpenFooterPlusMenu = () => {
        this.props.openFooterPlusMenu();
    }

    handleOpenFooterKeyboard = () => {
        this.props.openFooterKeyboard();
        const input = document.getElementById('chat-editor__input');
        if (input) {
            input.focus();
        }
    }

    handleOpenFooterEmoji = () => {
        this.props.openFooterEmoji();
    }

    render() {
        const {
            isPlusMenuOpened,
            isFooterEmoticonMenuOpened,
            isFooterKeyBoardOpened,
            isOpenGalleryHistory,
            isOpenTemplate,
        } = this.props;

        const rootClass = classNames({
            "natural": true,
            "natural-chat-footer-root": true,
            "natural-chat-footer-plus-menu-active": isPlusMenuOpened,
        });

        return (
            <div className={rootClass}>
                <div className="natural-wrapper natural-wrapper-v">
                    <div className="natural-wrapper natural-row natural-row-primary">
                        <div className="natural-actions">
                            <div className="natural-wrapper">
                                {
                                    !isPlusMenuOpened && !this.state.showCall &&
                                    <div className="natural-quick-middle natural-action natural-action-plus" onClick={this.handleOpenFooterPlusMenu}>
                                        <div className="natural-deco deco">
                                            <span className="icon icon-plus"/>
                                        </div>
                                    </div>
                                }
                                {
                                    !isFooterKeyBoardOpened && !this.state.showCall &&
                                    <div className="natural-quick-middle natural-action" onClick={this.handleOpenFooterKeyboard}>
                                        <div className="natural-deco deco">
                                            <span className="icon icon-keyboard"/>
                                        </div>
                                    </div>
                                }
                                {
                                    !isFooterEmoticonMenuOpened && !this.state.showCall &&
                                    <div className="natural-quick-middle natural-action" onClick={this.handleOpenFooterEmoji}>
                                        <div className="natural-deco deco">
                                            <span className="icon icon-smile"/>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <ChatFooterInput onClickCallFooter={this.onClickCallFooter}/>
                    </div>
                    <ChatFooterPlusMenu active={isPlusMenuOpened} onClickCallFooter={this.onClickCallFooter}/>
                    <ChatFooterEmoji active={isFooterEmoticonMenuOpened} onClickCallFooter={this.onClickCallFooter}/>
                </div>

                <ChatFooterTyping />
                <ChatGalleryHistory isOpen={isOpenGalleryHistory} onClickCallFooter={this.onClickCallFooter}/>
                <ChatTemplate isOpen={isOpenTemplate} />
            </div>
        )
    }
}

ConnectedChatFooter.propTypes = {
    isPlusMenuOpened: PropTypes.bool.isRequired,
    isFooterEmoticonMenuOpened: PropTypes.bool.isRequired,
    isFooterKeyBoardOpened: PropTypes.bool.isRequired,
    openFooterPlusMenu: PropTypes.func.isRequired,
    closeFooterPlusMenu: PropTypes.func.isRequired,
    openFooterKeyboard: PropTypes.func.isRequired,
    openFooterEmoji: PropTypes.func.isRequired,
    isOpenGalleryHistory: PropTypes.bool.isRequired,
    isOpenTemplate: PropTypes.bool.isRequired,
}

const ChatFooter = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatFooter);

export default ChatFooter;
