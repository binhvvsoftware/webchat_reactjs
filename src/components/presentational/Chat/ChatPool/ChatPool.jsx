import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classNames from "classnames";
import "./ChatPool.scss";
import ChatPoolItem from "../ChatPoolItem";
import { scrollToTopChatPool, concatChatPool, scrollToNewestChatPool, openFooterKeyboard } from "../../../../actions/Chat";
import { convertToJapaneseDatetime } from "../../../../utils/helpers";
import ChatService  from "../../../../services/chat";
import MessageSocket from '../../../../entities/MessageSocket';
import { MessageTypes } from '../../../../constants/types';
import { sendMessage } from '../../../../actions/socket';

const DEFAULT = {
    LOAD_TRIGGER_OFFSET_TOP: -200,
    SCROLL_TIMEOUT: 100,
    SELF_ELEMENT_ID: "message-chain",
};

const mapStateToProps = (state) => ({
    serverChain: state.chatControl.serverChain,
    clientChain: state.chatControl.clientChain,
    currentLastMessage: state.chatControl.currentLastMessage,
    newestMessage: state.chatControl.newestMessage,
    loadingStatus: state.chatControl.loadingStatus,
    hasLoadMore: state.chatControl.hasLoadMore,
    you: state.chatControl.you,
    me: state.chatControl.me,
    hasNewMessageToScroll: state.chatControl.hasNewMessageToScroll,
    isGiftPoolOpened: state.chatControl.isGiftPoolOpened,
    isHeaderMenuOpened: state.chatControl.isHeaderMenuOpened,
});

const mapDispatchToProps = (dispatch) => ({
    scrollToTopChatPool: payload => dispatch(scrollToTopChatPool(payload)),
    scrollToNewestChatPool: payload => dispatch(scrollToNewestChatPool(payload)),
    concatChatPool: payload => dispatch(concatChatPool(payload)),
    openFooterKeyboard: payload => dispatch(openFooterKeyboard(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

class ConnectedChatPool extends Component {
    constructor(props) {
        super(props);
        this.handleOnScroll = this.handleOnScroll.bind(this);
        this.scrollToNewest = this.scrollToNewest.bind(this);
        this.getItems = this.getItems.bind(this);
    }

    scrollToNewest(timeout = DEFAULT.SCROLL_TIMEOUT, options = {behavior: "smooth", block: "end", inline: "end" }) {
        if (window.messageChainScrolling) clearTimeout(window.messageChainScrolling);
        window.messageChainScrolling = setTimeout(() => {
            const newestMessageElement = this.props.newestMessage ? document.getElementById(this.props.newestMessage.id) : null;
            if (newestMessageElement) {
                newestMessageElement.scrollIntoView(options);
                this.props.scrollToNewestChatPool();
            } else {
                this.scrollToNewest(timeout, options);
            }
        }, timeout);
    }

    componentDidUpdate() {
        if (!this.props.isHeaderMenuOpened && !this.props.isGiftPoolOpened && this.props.hasNewMessageToScroll)
            this.scrollToNewest(100);
    }

    componentDidMount() {
        if (!this.props.isHeaderMenuOpened && !this.props.isGiftPoolOpened) {
            this.scrollToNewest(100, {behavior: "auto"});
        }
    }

    handleOnScroll() {
        if (this.props.currentLastMessage) {
            if (window.messageChainScrolling) clearTimeout(window.messageChainScrolling);
            window.messageChainScrolling = setTimeout(() => {
                const messageChainElement = document.getElementById(DEFAULT.SELF_ELEMENT_ID);
                const lastMessage = this.props.currentLastMessage;
                const lastMessageElement = document.getElementById(lastMessage.id);
                if (lastMessageElement) {
                    const lastMessageRect = lastMessageElement.getBoundingClientRect();
                    if (lastMessageRect.top > DEFAULT.LOAD_TRIGGER_OFFSET_TOP && this.props.hasLoadMore) {
                        if (!this.props.loadingStatus.flag) {
                            this.props.scrollToTopChatPool({ message: lastMessage });

                            ChatService.getChatHistory(this.props.you.id, lastMessage.serverTime, 15).then((response) => {
                                if (response.data.length > 0) {
                                    this.props.concatChatPool({ data: response.data });
                                    setTimeout(() => {
                                        const elm = document.getElementById(lastMessage.id);
                                        if (elm) {
                                            elm.scrollIntoView();
                                        }
                                    }, 100);
                                }
                            });
                        }
                    }
                    messageChainElement.scrollTop = messageChainElement.scrollTop === 0 ? 1 : messageChainElement.scrollTop;
                }
            }, DEFAULT.SCROLL_TIMEOUT);
        }
    }

    handleReadMessage = (message) => {
        const newMessage = new MessageSocket({
            sender: message.receiver,
            msg_id: message.id,
            mds: {
                msg_status: 'rd',
                msg_id: message.id,
            },
            msg_type: MessageTypes.MDS,
            receiver: message.sender,
        });
        this.props.sendMessage(newMessage);
    }

    getItems() {
        const itemsByDatetime = [];
        let dateGroupKey = null;
        Object.entries(this.props.clientChain).forEach(([key, value]) => {
            const datetimeGroupString = value.sendTime.format('YYYYMMDD');
            let rotateImage = 0;
            if (value.file && value.file.meta && value.file.meta.orientation) {
                rotateImage = value.file.meta.orientation;
            }

            if (!value.isOwn && !value.readTime.isValid()) {
                this.handleReadMessage(value);
            }

            const item = <ChatPoolItem
                key={key.toString()}
                id={value.id}
                type={value.type}
                isOwn={value.isOwn}
                isUnlock={value.isUnlock}
                content={value.content}
                sticker={value.sticker}
                gift={value.gift}
                file={value.file}
                fileStatus={value.fileStatus}
                readTime={value.readTime}
                sendTime={value.sendTime}
                status={value.statusData}
                attachmentStatus={value.status}
                me={this.props.me}
                you={this.props.you}
                message={value}
                rotateImage={rotateImage}
                imageFileId={value.imageFileId}
                sendError={value.sendError}
                value={value}
            />;

            if (dateGroupKey !== datetimeGroupString) {
                dateGroupKey = datetimeGroupString;
                itemsByDatetime.push(
                    <div key={key.toString()}>
                        <div className="natural-date-separation">
                            <div className="natural-wrapper natural-quick-middle">
                                <span>{ convertToJapaneseDatetime(value.sendTime) }</span>
                            </div>
                        </div>
                        { item }
                    </div>
                );
            } else {
                itemsByDatetime.push(item);
            }
        });

        return itemsByDatetime;
    }

    render() {
        const rootClass = classNames({
            "natural": true,
            "natural-chat-pool-root": true,
        });

        return (
            <div className={rootClass} onClick={this.props.openFooterKeyboard}   >
                <div className="natural-wrapper natural-wrapper-v"   >
                    <div className="message-chain" id={ DEFAULT.SELF_ELEMENT_ID } onScroll={this.handleOnScroll}>
                        <div className="natural-wrapper natural-wrapper-v">
                            { this.getItems() }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ConnectedChatPool.propTypes = {
    scrollToTopChatPool: PropTypes.func.isRequired,
    concatChatPool: PropTypes.func.isRequired,
    scrollToNewestChatPool: PropTypes.func.isRequired,
    clientChain: PropTypes.array.isRequired,
    hasLoadMore: PropTypes.bool.isRequired,
    currentLastMessage: PropTypes.object,
    newestMessage: PropTypes.object,
    loadingStatus: PropTypes.object.isRequired,
    you: PropTypes.object.isRequired,
    me: PropTypes.object.isRequired,
    isHeaderMenuOpened: PropTypes.bool.isRequired,
    isGiftPoolOpened: PropTypes.bool.isRequired,
    hasNewMessageToScroll: PropTypes.bool.isRequired,
    openFooterKeyboard: PropTypes.func.isRequired,
    sendMessage: PropTypes.func,
};

const ChatPool = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatPool);

export default ChatPool;
