import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import ReactPlayer from 'react-player'
import classNames from 'classnames';
import history from '../../../../utils/history';
import Image from '../../../Image';
import MediaService from '../../../../services/media';
import './ChatPoolItem.scss';
import { MessageTypes, MessageFileTypes } from '../../../../constants/types';
import { EmojiView, EmojiSticker } from '../../../Emoji';
import ChatService from '../../../../services/chat';
import ActionImageService from '../../../../services/imageDowload';
import {
    VOIP_CANCEL,
    MEDIA_DENIED,
    BUTTON_YES_TEMPLATE,
    ALERT_BUTTON_CANCEL_TITLE,
    VIEW_IMAGE_POPUP_CONFIRM_TITLE,
    PLAY_VIDEO_POPUP_CONFIRM_TITLE,
    CHAT_CONFIRM_MSG_VIEW_PICTURE_FOR_JUS_POINT,
    CHAT_CONFIRM_MSG_VIEW_VIDEO_FOR_JUS_POINT,
    NOTI_DENIED_ATTACHMENT_FILE_IN_CHAT,
} from '../../../../constants/messages';
import { updateChatPool, openImageBox, openBuyingPointPromotion } from '../../../../actions/Chat';
import { convertDurationTimeToHHMMSS, addTargetLink } from '../../../../utils/helpers';
import { openWebView } from '../../../../actions/webview';

const mapDispatchToProps = (dispatch) => ({
    updateChatPool: payload => dispatch(updateChatPool(payload)),
    openImageBox: payload => dispatch(openImageBox(payload)),
    openBuyingPointPromotion: payload => dispatch(openBuyingPointPromotion(payload)),
    onOpenWebView: (payload) => dispatch(openWebView(payload)),
});

class ConnectedChatPoolItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isPlay: false,
        }
        this.getDecoration = this.getDecoration.bind(this);
        this.getTime = this.getTime.bind(this);
        this.getContent = this.getContent.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    componentDidMount() {
        const tags = document.querySelectorAll('.natural-chat-pool-item-root a');
        if (!this.playerAddEvent && this.player && this.player.subscribeToStateChange) {
            this.playerAddEvent = true;
            this.player.subscribeToStateChange(this.handleUpdatePlayer);
        }

        tags.forEach((item) => {
            if (!item.getAttribute('data-add-event')) {
                item.addEventListener('click', this.handleClickLink, true);
                item.setAttribute('data-add-event', 'true');
            }
        });

        if (!this.props.type === MessageTypes.FILE || !this.props.file) {
            return;
        }
        if (this.props.file.msg_content_type === MessageFileTypes.VIDEO && this.props.file.file_id) {
            MediaService.getURLVideo(this.props.file.file_id)
                .then((getVideoUrlResponse) => {
                    this.props.updateChatPool({ message: Object.assign({}, this.props.message, { videoUrl: getVideoUrlResponse.data.url }) });
                });
        }
    }

    handleClickLink = (event) => {
        event.preventDefault();
        try {
            this.props.onOpenWebView({
                url: event.target.getAttribute('href'),
                backButton: true,
            });
        } catch (error) {
            //
        }
    };

    unLockImageChat = () => {
        ChatService.unlockImageChat(this.props.you.id, this.props.file.file_id)
            .then(() => {
                const unlockedMessage = Object.assign({}, this.props.message, {
                    isUnlock: true,
                });
                this.props.updateChatPool({ message: unlockedMessage });
            })
            .catch(err => {
                if (err.code === 70) {
                    this.props.openBuyingPointPromotion();
                }
            });
    };

    confirmUnlockImage = (getConnectionPointAPIResponse) => {
        Popup.create({
            title: VIEW_IMAGE_POPUP_CONFIRM_TITLE,
            content: `${getConnectionPointAPIResponse.data.view_image_point} ${CHAT_CONFIRM_MSG_VIEW_PICTURE_FOR_JUS_POINT}`,
            buttons: {
                right: [{
                    text: BUTTON_YES_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        this.unLockImageChat();
                        Popup.close();
                    },
                }],
                left: [{
                    text: ALERT_BUTTON_CANCEL_TITLE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],
            },
        });
    };

    checkUnlockImage = () => {
        if (this.props.isOwn) {
            this.props.openImageBox({
                message: this.props.message,
            });
            return;
        }
        ChatService.checkImageUnlock(this.props.you.id, this.props.file.file_id || this.props.imageFileId)
            .then((checkFileUnlockAPIResponse) => {
                if (checkFileUnlockAPIResponse.data.is_unlck === 1) {
                    this.props.openImageBox({
                        message: this.props.message,
                    });
                    return;
                }

                ActionImageService.getPointSaveImage(this.props.file.file_id)
                    .then((getConnectionPointAPIResponse) => {
                        this.confirmUnlockImage(getConnectionPointAPIResponse);
                    }).catch(err => err);
            });
    };

    unLockVideoChat = () => {
        ChatService.videoUnlock(this.props.you.id, this.props.file.file_id)
            .then(() => {
                const unlockedMessage = Object.assign({}, this.props.message, {
                    isUnlock: true,
                });
                this.props.updateChatPool({ message: unlockedMessage });
            }).catch(err => {
                if (err.code === 70) {
                    this.props.openBuyingPointPromotion();
                }
            });
    };

    checkUnlockVideo = () => {
        ChatService.checkVideoUnlock(this.props.you.id, this.props.file.file_id)
            .then((checkFileUnlockAPIResponse) => {
                if (checkFileUnlockAPIResponse.data.is_unlck === 1) {
                    this.setState({
                        isPlay: true,
                    })
                    const unlockedMessage = Object.assign({}, this.props.message, {
                        isUnlock: true,
                    });
                    this.props.updateChatPool({ message: unlockedMessage });
                    return;
                }

                const watchVideoPrice = checkFileUnlockAPIResponse.data.price;

                Popup.create({
                    title: PLAY_VIDEO_POPUP_CONFIRM_TITLE,
                    content: `${watchVideoPrice} ${CHAT_CONFIRM_MSG_VIEW_VIDEO_FOR_JUS_POINT}`,
                    buttons: {
                        right: [{
                            text: BUTTON_YES_TEMPLATE,
                            className: 'ok',
                            action: () => {
                                this.unLockVideoChat();
                                this.setState({
                                    isPlay: true,
                                })
                                Popup.close();
                            },
                        }],
                        left: [{
                            text: ALERT_BUTTON_CANCEL_TITLE,
                            className: 'ok',
                            action: () => {
                                Popup.close();
                            },
                        }],
                    },
                });
            }).catch ((err) => err);
    };

    handleOnClick() {
        if (this.props.type !== MessageTypes.FILE || !this.props.file) {
            return;
        }

        if (this.props.fileStatus === -1 || this.props.message.isFileDeleted) {
            Popup.alert(NOTI_DENIED_ATTACHMENT_FILE_IN_CHAT);
            return;
        }

        if (this.props.isOwn && this.props.file.msg_content_type !== MessageFileTypes.PHOTO) {
            this.setState({
                isPlay: true,
            })
            return;
        }

        if (this.props.file.msg_content_type === MessageFileTypes.PHOTO) {
            this.checkUnlockImage();
            return;
        }

        if (this.props.file.msg_content_type === MessageFileTypes.VIDEO) {
            this.checkUnlockVideo();
        }
    }

    routerProfile = () => {
        history.push(`/profile/${this.props.you.data.user_id}`);
    };

    getDecoration() {
        if (this.props.isOwn) {
            return null;
        }

        return (
            <div className="natural-avatar-user" onClick={ this.routerProfile }>
                <Image isAvatar imageId={ this.props.you.data.ava_id }/>
            </div>
        );
    }

    getTime() {
        const readedLabel = this.props.isOwn && this.props.status ? (
            <div className="natural-label">{ this.props.status.text }</div>
        ) : null;

        const time = (this.props.isOwn && this.props.readTime) ? this.props.readTime : this.props.sendTime;

        if (!time || !time.format || !time.isValid()) {
            return '';
        }

        return (
            <div className="natural-quiet natural-time">
                <div className="natural-wrapper natural-wrapper-v natural-video">
                    { readedLabel }
                    <div className="natural-number">
                        { time.format('HH:mm') }
                    </div>
                </div>
            </div>
        );
    }

    getContentGift(contentClass) {
        let gift = {};
        if (this.props.gift) {
            gift = {
                id: this.props.gift.gift_id,
                name: this.props.gift.gift_name ? this.props.gift.gift_name : 'GIFT',
                sender: this.props.gift.sender_name,
                receiver: this.props.gift.receiver_name,
                price: this.props.gift.gift_price,
            };
        } else {
            const giftContent = this.props.content.split('|');
            gift = {
                id: giftContent[0],
                name: 'GIFT',
                sender: giftContent[1],
                receiver: giftContent[2],
                price: giftContent[3],
            };
        }

        const backgroundImage = MediaService.getURL(gift.id, 4);

        return (
            <div className={ contentClass }>
                <div className="natural-wrapper">
                    <div
                        className="natural-neutral-bg-color natural-gift-appearance"
                        style={ { 'backgroundImage': `url(${backgroundImage})` } }/>
                    <div className="natural-gift-literal">
                        <div className="natural-wrapper natural-wrapper-v">
                            <div className="natural-ellipsis"><span>{ gift.sender }力ヽら</span></div>
                            <div className="natural-ellipsis natural-gift-sender"><span>{ gift.receiver }へギフト</span>
                            </div>
                            <div className="natural-ellipsis natural-gift-price"><span>({ gift.price }pts)を送りました</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getContentFile(contentClass, classRotateImage) {
        if (this.props.fileStatus === 0) {
            return ((finalContentClass) => (
                <div className={ finalContentClass }>
                    <span>{ MEDIA_DENIED }</span>
                </div>
            ))(contentClass);
        }

        if (this.props.fileStatus === -1) {
            return ((finalContentClass) => (
                <div className={ finalContentClass }>
                    <span className="icon icon-x-white"/>
                </div>
            ))(contentClass);
        }

        if (!this.props.file) {
            return '';
        }

        if (
            [MessageFileTypes.PHOTO, MessageFileTypes.VIDEO].indexOf(this.props.file.msg_content_type) !== -1 &&
            (this.props.fileStatus === -1 || this.props.message.isFileDeleted)
        ) {
            return (
                <div className={ contentClass }>
                    <div className="natural-wrapper">
                        <div className="natural-mask-blur"/>
                        <div
                            className="natural-neutral-bg-color natural-file-appearance natural-file-appearance-deny">
                            <span className="icon icon-x-white"/>
                        </div>
                    </div>
                </div>
            )
        }

        switch (this.props.file.msg_content_type) {
            case MessageFileTypes.PHOTO:
                return ((imageSource, finalContentClass) => (
                    <div className={ finalContentClass }>
                        <div className="natural-wrapper">
                            <div className="natural-mask-blur"/>
                            <div
                                className={ `natural-neutral-bg-color natural-file-appearance ${classRotateImage}` }
                                style={ { backgroundImage: `url(${imageSource})` } }/>
                        </div>
                    </div>
                ))(this.props.file.meta ? this.props.file.meta.data : MediaService.getURL(this.props.file.file_id, 1), contentClass);
            case MessageFileTypes.VIDEO:
                return ((videoSource, finalContentClass, thumbnailVideo) => (
                    <div className={ finalContentClass }>
                        <div className="natural-wrapper">
                            <div id={`video${this.props.id}`} className="natural-video-appearance">
                                <ReactPlayer
                                    id='my-video'
                                    className='video-appearance'
                                    light={thumbnailVideo}
                                    ref={ player => this.player = player } //eslint-disable-line
                                    url={videoSource}
                                    width='100%'
                                    height='100%'
                                    playing={this.state.isPlay}
                                    controls
                                    onStart={this.onStart}
                                    onPause={this.onPause}
                                    onPlay={this.onStart}
                                />
                            </div>
                        </div>
                    </div>
                ))(this.props.message.videoUrl || '', contentClass, this.props.file.file_id ? MediaService.getPosterVideo(this.props.file.file_id) : this.props.file.meta.poster);
            default:
                return '';
        }
    }

    onStart = () => {
        const el = document.getElementById(`video${this.props.id}`);
        el.style.height = '250px'
        el.style.width = 'unset'
    }

    onPause = () => {
        const el = document.getElementById(`video${this.props.id}`);
        el.style.height = '150px'
        el.style.width = '135px'
    }

    getContent() {
        const contentClass = classNames({
            'natural-content': true,
            'natural-content-gift': this.props.type === MessageTypes.GIFT,
            'natural-content-file': this.props.type === MessageTypes.FILE,
            'natural-content-waiting': this.props.isOwn && this.props.type === MessageTypes.FILE && this.props.fileStatus !== 1 && this.props.fileStatus !== 0,
            'natural-content-lock': !this.props.isOwn && this.props.fileStatus !== -1 && this.props.type === MessageTypes.FILE && (this.props.isUnlock === false) && !this.props.message.isFileDeleted,
            'natural-primary-bg-color': this.props.isOwn,
            'natural-neutral-bg-color': !this.props.isOwn,
            'natural-video-deny': this.props.fileStatus === -1,
        });
        const classRotateImage = `rotate-${this.props.rotateImage || 0}`;
        if (this.props.type === MessageTypes.GIFT) {

            return this.getContentGift(contentClass);
        }

        if (this.props.type === MessageTypes.FILE) {
            return this.getContentFile(contentClass, classRotateImage);
        }

        if (this.props.type === MessageTypes.STK) {
            return <EmojiSticker sticker={ this.props.sticker }/>;
        }

        if (this.props.type === MessageTypes.CALLREQ) {
            return (
                <div className={ contentClass }>
                    {this.props.message.content}
                </div>
            );
        }

        if (this.props.type === MessageTypes.EVOICE || this.props.type === MessageTypes.EVIDEO) {
            return (
                <div className={ `${contentClass} natural-chat-custom` }>
                    <div className="text-center">
                        <span className="icon icon-call-wait-w"/>
                    </div>
                    { parseInt(this.props.content.split('|')[2], 10) === 0 ? VOIP_CANCEL : `${convertDurationTimeToHHMMSS(parseInt(this.props.content.split('|')[2], 10))}` }
                </div>
            );
        }

        if ((this.props.you.data.isSystem && !this.props.isOwn) || (this.props.me.data.isSystem && this.props.isOwn)) {
            return <div className='natural-neutral-bg-color natural-content' dangerouslySetInnerHTML={ { __html: addTargetLink(this.props.content) } }/>;
        }

        return (
            <EmojiView className={ contentClass } content={ this.props.content }/>
        );
    }

    checkSendMessage = () => {
        if (!this.props.sendError) {
            return '';
        }

        return (
            <div className="natural-send-error">
                <i className="icon icon-msg-failed" />
            </div>
        );
    };

    handleUpdatePlayer = (player) => {
        if (player && !player.paused && !player.isFullscreen) {
            //
        }
    };

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-chat-pool-item-root': true,
            'natural-chat-pool-item-send-error': this.props.sendError,
            'natural-chat-pool-item-own': this.props.isOwn,
            [`natural-chat-pool-item-${this.props.type.toString().toLowerCase()}`]: true,
        });

        return (
            <div className={rootClass} id={this.props.id}>
                <div className="natural-wrapper">
                    { this.getDecoration() }
                    <div
                        className={ `natural-primary ${ this.props.file && this.props.file.msg_content_type === MessageFileTypes.VIDEO ? 'is_video' : '' }` }
                        onClick={ this.handleOnClick }>
                        <div className="natural-wrapper">
                            { this.getContent() }
                            { this.checkSendMessage() }
                            { this.getTime() }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ConnectedChatPoolItem.propTypes = {
    type: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    isOwn: PropTypes.bool.isRequired,
    isUnlock: PropTypes.bool,
    content: PropTypes.string,
    sticker: PropTypes.any,
    gift: PropTypes.object,
    file: PropTypes.object,
    fileStatus: PropTypes.number,
    readTime: PropTypes.object,
    sendTime: PropTypes.object.isRequired,
    status: PropTypes.object,
    me: PropTypes.object.isRequired,
    you: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
    updateChatPool: PropTypes.func.isRequired,
    openImageBox: PropTypes.func.isRequired,
    openBuyingPointPromotion: PropTypes.func.isRequired,
    onOpenWebView: PropTypes.func.isRequired,
    rotateImage: PropTypes.number,
    imageFileId: PropTypes.string,
    sendError: PropTypes.bool,
};

const ChatPoolItem = connect(null, mapDispatchToProps)(ConnectedChatPoolItem);


export default ChatPoolItem;
