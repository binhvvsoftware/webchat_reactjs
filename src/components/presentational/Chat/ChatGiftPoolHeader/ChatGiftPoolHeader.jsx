import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classNames from "classnames";
import { closeGiftPool } from "../../../../actions/Chat";
import "./ChatGiftPoolHeader.scss"

const mapDispatchToProps = (dispatch) => ({
    closeGiftPool: payload => dispatch(closeGiftPool(payload)),
});

class ConnectedChatGiftPoolHeader extends Component {
    constructor(props) {
        super(props);

        this.handleOnBackButtonClick = this.handleOnBackButtonClick.bind(this);
    }

    handleOnBackButtonClick() {
        this.props.closeGiftPool();
    }

    render() {
        const rootClass = classNames({
            "natural": true,
            "natural-primary-header": true,
            "natural-gift-pool-header-root": true,
        });

        return (
            <header className={rootClass}>
                <div className="natural-wrapper">
                    <div className="natural natural-action-container natural-quick-middle">
                        <div className="natural natural-quick-middle natural-action-btn natural-action-back" onClick={this.handleOnBackButtonClick}>
                            <div className="natural natural-deco">
                                <span className="icon icon-arrow-left"></span>
                            </div>
                        </div>
                    </div>
                    <div className="natural natural-quick-middle natural-title">
                        <span className="natural-text-wrapper">ギフト</span>
                    </div>
                </div>
            </header>
        );
    }
}

ConnectedChatGiftPoolHeader.propTypes = {
    closeGiftPool: PropTypes.func.isRequired,
}

const ChatGiftPoolHeader = connect(null, mapDispatchToProps)(ConnectedChatGiftPoolHeader);

export default ChatGiftPoolHeader;
