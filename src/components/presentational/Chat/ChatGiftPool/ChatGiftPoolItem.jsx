import React, { Component } from "react";
import PropTypes from "prop-types";
import Image from "../../../Image";

class ChatGiftPoolItem extends Component {
    render() {
        const { gift } = this.props;
        return (
            <div className="natural-wrapper natural-wrapper-v">
                <Image className="natural-neutral-bg-color natural-deco" imageId={gift.gift_id} imgKind={4} isBackground />
                <div className="natural-literal natural-ellipsis"><span>{gift.gift_name}</span></div>
                <div className="natural-primary-color natural-ellipsis natural-price"><span>{gift.gift_pri} pts</span></div>
            </div>
        );
    }
}

ChatGiftPoolItem.propTypes = {
    gift: PropTypes.object.isRequired,
};


export default ChatGiftPoolItem;
