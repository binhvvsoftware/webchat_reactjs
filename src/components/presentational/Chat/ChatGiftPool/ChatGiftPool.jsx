import React, { Component } from "react";
import PropTypes from "prop-types";
import Popup from 'react-popup';
import { connect } from "react-redux";
import classNames from "classnames";
import "./ChatGiftPool.scss";
import ChatGiftPoolHeader from "../ChatGiftPoolHeader";
import ChatGiftPoolItem from './ChatGiftPoolItem';
import GiftService from '../../../../services/gift';
import history, { getLocationHistory } from '../../../../utils/history';
import { MessageTypes } from "../../../../constants/types";
import { closeGiftPool, changeWhoIAm } from '../../../../actions/Chat';
import { openBuyPoint } from '../../../../actions/index';
import { CONFIRM_GIFT_ALERT_TITLE, BUTTON_NO_TEMPLATE, BUTTON_YES_TEMPLATE } from '../../../../constants/messages';
import { sendMessage } from '../../../../actions/socket';

const mapStateToProps = (state) => ({
    me: state.chatControl.me,
    you: state.chatControl.you,
});

const mapDispatchToProps = (dispatch) => ({
    closeGiftPool: payload => dispatch(closeGiftPool(payload)),
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
    changeWhoIAm: payload => dispatch(changeWhoIAm(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

class ConnectedChatGiftPool extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            gifts: [],
        }
    }

    componentDidMount() {
        GiftService.getList().then(response => {
            if (!this.isUnmounted) {
                this.setState({
                    loading: false,
                    gifts: response.data,
                });
            }
        })
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    sendGiftSocket = (gift) => {

        this.setState({ loading: true }, () => {
            this.props.sendMessage({
                type: MessageTypes.GIFT,
                to: this.props.you.id,
                gift: {
                    gift_name: gift.gift_name,
                    gift_price: gift.gift_pri,
                    gift_id: gift.gift_id,
                    sender_name: this.props.me.data.user_name,
                    receiver_name: this.props.you.data.user_name,
                },
                content: gift.gift_id,
            });
            this.props.closeGiftPool();
            this.setState({ loading: false });

            const location = getLocationHistory();

            if (location.current.pathname !== 'chat') {
                history.push(`/chat/${this.props.you.id}`);
            }
        })
    }

    handleOnGiftItemClick = (gift) => {
        const messConfirm = `'${gift.gift_name}'ギフ卜を${this.props.you.data.user_name}さんに贈りますか?`;

        Popup.create({
            title: CONFIRM_GIFT_ALERT_TITLE,
            content: messConfirm,
            buttons: {
                left: [{
                    text: BUTTON_NO_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                    },
                }],

                right: [{
                    text: BUTTON_YES_TEMPLATE,
                    className: 'ok',
                    action: () => {
                        Popup.close();
                        GiftService.sendGiftUser(gift.gift_id,this.props.you.id)
                            .then((response) => {
                                this.sendGiftSocket(gift);

                                const user = this.props.me;
                                user.data.point = response.data.point;
                                this.props.changeWhoIAm(user);
                            })
                            .catch(() => this.props.openBuyPoint())
                    },
                }],
            },
        });
    }

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-chat-gift-pool-root': true,
            'natural-chat-gift-pool-active': this.props.active,
        });

        const gifts = this.state.gifts.map((gift, index) => (
            <li className="natural-gift-item" onClick={ () => this.handleOnGiftItemClick(gift) }>
                <ChatGiftPoolItem key={ index } gift={ gift }/>
            </li>
        ));

        return (
            <div className = {rootClass}>
                { this.props.active && this.state.loading && <div className="gift__loading" />}
                <div className="natural-wrapper natural-wrapper-v">
                    <ChatGiftPoolHeader />
                    <p className="text-center text-gift-des">
                        お気に入りの人やお世話になった人に<br/>感謝のポイントギフトを贈ってみよう！
                    </p>
                    <div className="natural-wrapper">
                        <ul className="natural-gift-list">
                            { gifts }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

ConnectedChatGiftPool.propTypes = {
    me: PropTypes.object.isRequired,
    you: PropTypes.object.isRequired,
    active: PropTypes.bool.isRequired,
    closeGiftPool: PropTypes.func.isRequired,
    openBuyPoint: PropTypes.func.isRequired,
    changeWhoIAm: PropTypes.func.isRequired,
    sendMessage: PropTypes.func.isRequired,
};

const ChatGiftPool = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatGiftPool);

export default ChatGiftPool;
