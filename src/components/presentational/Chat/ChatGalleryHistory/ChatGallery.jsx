import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import Popup from 'react-popup';
import FullScreenModal from '../../../FullScreenModal';
import ButtonDownload from '../../../ButtonDownload';
import Image from '../../../Image';
import ChatService from '../../../../services/chat';
import { closeGalleryHistory } from '../../../../actions/Chat';
import { openBuyPoint } from '../../../../actions/index';
import {
    ALERT_BUTTON_YES_TITLE,
    ALERT_BUTTON_NO_TITLE,
    VIEW_IMAGE_POPUP_CONFIRM_TITLE,
    CHAT_CONFIRM_MSG_VIEW_PICTURE_FOR_JUS_POINT,
} from '../../../../constants/messages';
import './index.scss';

const mapStateToProps = (state) => ({
    you: state.chatControl.you,
});

const mapDispatchToProps = (dispatch) => ({
    closeGalleryHistory: payload => dispatch(closeGalleryHistory(payload)),
    openBuyPoint: payload => dispatch(openBuyPoint(payload)),
});

class ChatGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: props.isOpen,
            isOpenPreview: false,
            loading: true,
            previewIndex: 0,
            images: [],
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isOpen: nextProps.isOpen,
            isOpenPreview: false,
        });

        if (nextProps.you && nextProps.you.id && nextProps.isOpen) {
            this.setState({
                loading: true,
            }, () => {
                ChatService.getListImageSent(nextProps.you.id)
                    .then((response) => {
                        this.setState({
                            images: response.data.list,
                        });
                    })
                    .then(() => {
                        this.setState({
                            loading: false,
                        });
                    });
            });
        }
    }

    unlockImage(imgId,index){
        const {images} = this.state;
        ChatService.unlockImageChat(this.props.you.id,imgId)
            .then(() => {
                images[index].is_unlock = true;
                this.setState({
                    images,
                })
            }).catch((err) => {
                if(err.code === 70){
                    this.props.openBuyPoint();
                }
            })
    }

    comfirmUnlockImage(imgId,index){
        ChatService.getPointAction(imgId)
            .then((response) => {
                Popup.create({
                    title: VIEW_IMAGE_POPUP_CONFIRM_TITLE,
                    content: `${response.data.view_image_point}${ CHAT_CONFIRM_MSG_VIEW_PICTURE_FOR_JUS_POINT}`,
                    buttons: {
                        left: [{
                            text: ALERT_BUTTON_NO_TITLE,
                            className: 'ok',
                            action: () => {
                                Popup.close();
                            },
                        }],
                        right: [{
                            text: ALERT_BUTTON_YES_TITLE,
                            className: 'ok',
                            action: () => {
                                Popup.close();
                                this.unlockImage(imgId,index);
                            },
                        }],
                    },
                });
            })
    }

    handleOpenPreview = (index,isUnlock,imgId) => {
        if(!isUnlock){
            this.comfirmUnlockImage(imgId,index);
            return;
        }
        this.setState({ isOpenPreview: true, previewIndex: index });
        this.slider.slickGoTo(index);
    };

    handleChangeIndex = (newIndex) => {
        const {images} = this.state;

        this.setState({ previewIndex: newIndex });
        if(images.length && images[newIndex] && !images[newIndex].is_unlock){
            this.comfirmUnlockImage(images[newIndex].img_id,newIndex);
        }
    }

    closePreview = () => {
        this.setState({
            isOpenPreview: false,
        })
    }

    closeGallery = () => {
        this.props.closeGalleryHistory();
        this.props.onClickCallFooter();
    }

    render() {
        const { isOpen, isOpenPreview, previewIndex, loading } = this.state;
        let { images } = this.state;
        images = Array.isArray(images) ? images : [];

        const gallery = images.map((item, index) => (
            <div className={ `chat-gallery__item ${item.is_unlock ? '' : 'is_lock'}` } key={ item.img_id } onClick={ () => this.handleOpenPreview(index,item.is_unlock,item.img_id)}>
                <Image className="chat-gallery__img" imageId={ item.img_id } imgKind="1" isBackground/>
            </div>
        ));
        const sliderContent = images.map((item, index) => (
            <div className={`chat-gallery__preview-wrapper ${item.is_unlock ? '' : 'is_lock'}`} key={index + item}>
                <Image className="chat-gallery__preview-img" imageId={ item.img_id } imgKind={2}/>
            </div>
        ));

        const image = images[previewIndex] || {};
        const title = `${previewIndex + 1}/${images.length}`;
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.handleChangeIndex,
            initialSlide: previewIndex,
        };

        /* eslint-disable no-return-assign */
        return (
            <FullScreenModal title="ギャラリー" className="chat-gallery" isOpen={ isOpen } onClose={ this.closeGallery }>
                <div className="chat-gallery__wrapper">
                    { loading && <div className="chat-gallery__loading" /> }
                    { gallery }
                </div>

                <FullScreenModal title={ title } className="chat-gallery-preview" isOpen={ isOpenPreview } onClose={this.closePreview}>
                    <div className="chat-gallery__preview">
                        <Slider { ...settings } ref={slider => this.slider = slider}>
                            { sliderContent }
                        </Slider>
                    </div>

                    <div className="chat-gallery__download">
                        { image.img_id && <ButtonDownload imageId={ image.img_id }/> }
                    </div>
                </FullScreenModal>
            </FullScreenModal>
        );
    }
}

ChatGallery.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    you: PropTypes.object.isRequired,
    closeGalleryHistory: PropTypes.func.isRequired,
    openBuyPoint: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatGallery);
