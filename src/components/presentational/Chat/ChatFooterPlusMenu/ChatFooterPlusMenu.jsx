import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import { connect } from 'react-redux';
import classNames from 'classnames';
import './ChatFooterPlusMenu.scss';
import { MessageTypes, MessageFileTypes } from '../../../../constants/types';
import { checkOrientationImage } from '../../../../utils/helpers';
import {
    openGalleryHistory,
    openTemplate,
} from '../../../../actions/Chat';
import {
    PLEASE_CHOOSE_VIDEO_FILE,
    PLEASE_CHOOSE_IMAGE_FILE,
} from '../../../../constants/messages';
import Auth from '../../../../services/authentication';
import { sendMessage } from '../../../../actions/socket';

const mapStateToProps = (state) => ({
    me: state.chatControl.me,
    you: state.chatControl.you,
    pendingFileMessage: state.chatControl.pendingFileMessage,
});

/* eslint-disable */
const mapDispatchToProps = (dispatch) => ({
    openGalleryHistory: payload => dispatch(openGalleryHistory(payload)),
    openTemplate: payload => dispatch(openTemplate(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

class ConnectedChatFooterPlusMenu extends Component {
    constructor(props) {
        super(props);

        this.handleOnSendPictureButtonChange = this.handleOnSendPictureButtonChange.bind(this);
        this.handleOnSendVideoButtonChange = this.handleOnSendVideoButtonChange.bind(this);
        this.handleOnSendFileButtonClick = this.handleOnSendFileButtonClick.bind(this);
    }

    handleOnSendVideoButtonChange(event) {
        const targetFile = event.target.files[0];
        const dataFormID = event.target.getAttribute('data-form-id');

        Auth.checkToken().then(() => {
            if (targetFile && (/^video\/[a-z0-9]/).test(targetFile.type)) {
                const video = document.createElement('video');
                const uploadVideo = (dataBase64, poster = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7') => {
                    video.pause();
                    const message = {
                        type: MessageTypes.FILE,
                        to: this.props.you.id,
                        file: {
                            msg_content_type: MessageFileTypes.VIDEO,
                            meta: {
                                time: targetFile.lastModified,
                                size: targetFile.size,
                                data: targetFile,
                                dataBase64,
                                poster,
                            },
                        },
                    }
                    this.props.sendMessage(message);
                    this.props.onClickCallFooter();
                }

                const reader = new FileReader();

                reader.onload = () => {
                    const blob = new Blob([reader.result], { type: targetFile.type });
                    const url = URL.createObjectURL(blob);
                    const snapImage = () => {
                        const canvas = document.createElement('canvas');
                        canvas.width = video.videoWidth;
                        canvas.height = video.videoHeight;
                        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                        const thumbnail = canvas.toDataURL();

                        if (thumbnail && thumbnail.length > 100000) {
                            video.removeEventListener('timeupdate', timeUpdate);
                            uploadVideo(reader.result, thumbnail)
                        }
                    };

                    const timeUpdate = ()  => {
                        if (snapImage()) {
                            video.removeEventListener('timeupdate', timeUpdate);
                            video.pause();
                        }
                    };
                    video.addEventListener('loadeddata', ()  => {
                        if (snapImage()) {
                            video.removeEventListener('timeupdate', timeUpdate);
                        }
                    });

                    video.addEventListener('timeupdate', timeUpdate);
                    video.preload = 'metadata';
                    video.src = url;
                    // Load video in Safari / IE11
                    video.muted = true;
                    video.playsInline = true;
                    video.play();

                    video.onerror = () => {
                        uploadVideo(reader.result)
                    };
                };
                reader.readAsArrayBuffer(targetFile);
            } else {
                Popup.alert(PLEASE_CHOOSE_VIDEO_FILE);
            }

            document.getElementById(dataFormID).reset();
        });

    }

    handleOnSendPictureButtonChange(event) {
        const targetFile = event.target.files[0];
        const dataFormID = event.target.getAttribute('data-form-id');

        Auth.checkToken().then(() => {
            if (targetFile && (targetFile.type === 'image/gif' || targetFile.type === 'image/jpeg' || targetFile.type === 'image/png')) {
                const reader = new FileReader();

                reader.addEventListener('load', () => {
                    // Base64 result
                    if (reader.result) {
                        checkOrientationImage(targetFile, (imgBase64, orientation) => {
                            const message = {
                                type: MessageTypes.FILE,
                                to: this.props.you.id,
                                file: {
                                    msg_content_type: MessageFileTypes.PHOTO,
                                    meta: {
                                        time: targetFile.lastModified,
                                        size: targetFile.size,
                                        data: imgBase64,
                                        file: targetFile,
                                        orientation: orientation,
                                    },
                                },
                            };
                            this.props.sendMessage(message);
                            this.props.onClickCallFooter();
                        });
                    }
                }, false);

                reader.readAsDataURL(targetFile);

            } else {
                Popup.alert(PLEASE_CHOOSE_IMAGE_FILE);
            }
            document.getElementById(dataFormID).reset();
        });
    }

    handleOnSendFileButtonClick(event) {
        if (this.props.pendingFileMessage) {
            event.preventDefault();
        }
    }

    render() {
        const rootId = "natural-chat-footer-plus-menu-form";
        const rootClass = classNames({
            'natural-wrapper': true,
            'natural-wrapper-v': true,
            'natural-row': true,
            'natural-chat-footer-plus-menu-root': true,
            'natural-chat-footer-plus-menu-active': this.props.active,
        });
        return (
            <form id={rootId} className={rootClass}>
                <div className="natural-wrapper natural-wrapper-v">
                    <div className="natural-wrapper natural-quick-middle natural-grid-row">
                        <label
                            className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-pic" htmlFor="natural-image-sending-input" onClick={this.handleOnSendFileButtonClick}>
                            <input className="natural-quick-hidden" type="file" accept=".jpg,.jpeg,.png" name="" id="natural-image-sending-input" onChange={this.handleOnSendPictureButtonChange} data-form-id={rootId} />
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper">
                                <i className="icon icon-picture" />
                            </div>
                            <div className="natural-label">画像を選択</div>
                        </label>
                        <label className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-pic" htmlFor="natural-image-sending-input" onClick={this.handleOnSendFileButtonClick}>
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper">
                                <i className="icon icon-camera" />
                            </div>
                            <div className="natural-label">写真を撮影</div>
                        </label>
                        <label className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-vid" htmlFor="natural-video-sending-input" onClick={this.handleOnSendFileButtonClick}>
                            <input className="natural-quick-hidden" type="file" accept="video/*" name="" id="natural-video-sending-input" onChange={this.handleOnSendVideoButtonChange} data-form-id={rootId} />
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper">
                                <i className="icon icon-movie" />
                            </div>
                            <div className="natural-label">動画を選択</div>
                        </label>
                    </div>
                    <div className="natural-wrapper natural-quick-middle natural-grid-row">
                        <label className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-vid" htmlFor="natural-video-sending-input" onClick={this.handleOnSendFileButtonClick}>
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper">
                                <i className="icon icon-video-grey" />
                            </div>
                            <div className="natural-label">動画を撮影</div>
                        </label>
                        <label className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-pic">
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper" onClick={this.props.openTemplate}>
                                <div className="icon icon-template-grey" />
                            </div>
                            <div className="natural-label">テンプレート</div>
                        </label>
                        <label className="natural-wrapper natural-wrapper-v natural-quick-middle natural-plus-grid-item natural-plus-grid-item-pic">
                            <div className="natural-wrapper natural-quick-end natural-icon-wrapper" onClick={this.props.openGalleryHistory}>
                                <div className="icon icon-gallery" />
                            </div>
                            <div className="natural-label">ギャラリー</div>
                        </label>
                    </div>
                </div>
            </form>
        );
    }
}

ConnectedChatFooterPlusMenu.propTypes = {
    pendingFileMessage: PropTypes.object,
    me: PropTypes.object.isRequired,
    you: PropTypes.object.isRequired,
    active: PropTypes.bool.isRequired,
    openGalleryHistory: PropTypes.func.isRequired,
    openTemplate: PropTypes.func.isRequired,
    onClickCallFooter: PropTypes.func,
    sendMessage: PropTypes.func,
}

const ChatFooterPlusMenu = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatFooterPlusMenu);

export default ChatFooterPlusMenu;
