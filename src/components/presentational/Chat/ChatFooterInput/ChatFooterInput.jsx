import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import './ChatFooterInput.scss';
import EmojiHelper from '../../../Emoji/helper';
import UtilityButtonCall from '../../Utility/UtilityButtonCall';
import { ChatFooterProperButtonTypes, PrimaryUtilityTypes, MessageTypes } from '../../../../constants/types';
/* Actions */
import {
    changeChatMessageInput,
    openFooterKeyboard,
} from '../../../../actions/Chat';
import {
    sendMessage,
} from '../../../../actions/socket';
import AuthService from '../../../../services/authentication';

const mapDispatchToProps = (dispatch) => ({
    changeChatMessageInput: payload => dispatch(changeChatMessageInput(payload)),
    openFooterKeyboard: payload => dispatch(openFooterKeyboard(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

const mapStateToProps = (state) => ({
    message: state.chatControl.standbyMessage,
    templateId: state.chatControl.templateId,
    you: state.chatControl.you,
    emoji: state.chatControl.emoji,
});

class ConnectedChatFooterInput extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.contentRef = React.createRef();
        this.state = {
            hasScroll: false,
            properAction: props.message ? ChatFooterProperButtonTypes.SEND : ChatFooterProperButtonTypes.CALL,
            showOptionCall: false,
            toggleFixHideText: false,
        };
    };

    componentWillReceiveProps(nextProps) {
        let message = this.getRawMessage();
        if (this.props.emoji.time !== nextProps.emoji.time && this.contentRef.current) {
            if (nextProps.emoji.code === '') {
                message = EmojiHelper.blackspace(message);
                this.props.changeChatMessageInput({ message });
                setTimeout(this.handleCheckButton, 100);
                return;
            }
            if (window.storeRange !== null && window.storeRange !== undefined) {
                // set the selection range (of cursor position)
                const sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(window.storeRange);
            } else {
                let range;
                let selection;
                if (document.createRange) // Firefox, Chrome, Opera, Safari, IE 9+
                {
                    range = document.createRange(); // Create a range (a range is a like the selection but invisible)
                    range.selectNodeContents(this.contentRef.current); // Select the entire contents of the element with the range
                    range.collapse(false); // Collapse the range to the end point. false means collapse to end rather than the start
                    selection = window.getSelection(); // Get the selection object (allows you to change selection)
                    selection.removeAllRanges(); // remove any selections already made
                    selection.addRange(range); // make the range you have just created the visible selection
                } else if (document.selection) // IE 8 and lower
                {
                    range = document.body.createTextRange();// Create a range (a range is a like the selection but invisible)
                    range.moveToElementText(this.contentRef.current); // Select the entire contents of the element with the range
                    range.collapse(false); // collapse the range to the end point. false means collapse to end rather than the start
                    range.select(); // Select the range (make it the visible selection
                }
            }

            let emojiNode = document.createElement('span');
            emojiNode.innerHTML = nextProps.emoji.tag;
            emojiNode = emojiNode.firstChild;
            EmojiHelper.insertNodeOverSelection(emojiNode, this.contentRef.current);
            this.changeCursorPosition();
            this.contentRef.current.scrollTop = this.contentRef.current.scrollHeight;
        }

        this.setState({
            properAction: nextProps.message || message ? ChatFooterProperButtonTypes.SEND : ChatFooterProperButtonTypes.CALL,
        }, this.handleCheckHasScroll);

        if (this.props.you.id !== nextProps.you.id) {
            this.handleCloseOptionCall();
        }
        setTimeout(this.handleCheckButton, 100);
    }

    componentDidMount() {
        if (this.contentRef.current) {
            this.contentRef.current.addEventListener('paste', this.execCommandPaste);
            const scrollToKeyboard = () => {
                setTimeout(() => {
                    if (this.contentRef.current && this.contentRef.current.scrollIntoView) {
                        this.contentRef.current.scrollIntoView({ behavior: 'smooth' });
                    }
                }, 100);
            };
            this.contentRef.current.addEventListener('focus', scrollToKeyboard);
            this.contentRef.current.addEventListener('mouseUp', scrollToKeyboard);
        }
    }

    handleCheckButton = () => {
        const contentRaw = this.getRawMessage();
        const { properAction } = this.state;
        const newProperAction = contentRaw ? ChatFooterProperButtonTypes.SEND : ChatFooterProperButtonTypes.CALL;
        if (properAction !== newProperAction) {
            this.setState({
                properAction: newProperAction,
            });
        }
    };

    execCommandPaste = (e) => {
        e.preventDefault();
        const text = e.clipboardData.getData('text/plain');
        document.execCommand('insertHTML', false, text);
    };

    handleCheckHasScroll = () => {
        const { hasScroll } = this.state;
        const div = this.contentRef.current;
        const check = div && div.scrollHeight > div.clientHeight;
        if (hasScroll !== check) {
            this.setState({
                hasScroll: check,
            });
        }
    };

    getRawMessage = () => {
        if (!this.contentRef.current) {
            return '';
        }

        let html = this.contentRef.current.innerHTML;
        html = html.replace(/<br>/gim, '\n').replace(/<(\\)?div>/gim, '').replace(/<\/div>/gim, '\n');

        return EmojiHelper.convertToCode(html);
    };

    changeCursorPosition = () => {
        window.storeRange = (window.getSelection().rangeCount > 0) ? window.getSelection().getRangeAt(0) : null;
    };

    handleChangeContent = (action, e) => {
        if (!this.contentRef.current) {
            return;
        }

        if (e.keyCode === 13 && e.shiftKey) {
            this.handleSubmit(e);
            return;
        }

        this.handleCheckButton();

        if (action === 'focus') {
            e.persist();
        }

        if (action === 'mouseUp') {
            this.props.openFooterKeyboard();
        }


        if (action === 'mouseUp' || action === 'keyUp') {
            this.changeCursorPosition();
            this.setState({
                toggleFixHideText: !this.state.toggleFixHideText, // eslint-disable-line
            });
        }

        if (action === 'keyUp') {
            this.props.sendMessage({
                type: MessageTypes.PRC,
                to: this.props.you.id,
                content: this.getRawMessage(),
            });
        }

        this.handleCheckHasScroll();
    };

    handleCloseOptionCall = () => {
        this.setState({
            showOptionCall: false,
        });
    };

    handleOpenOptionCall = () => {
        this.setState({
            showOptionCall: true,
        });
        this.props.onClickCallFooter();
    };

    getProperButton() {
        const className = classNames({
            'action': true,
            'natural-quick-middle': true,
            'natural-contrast-bg-color': this.props.you.data.voice_call_waiting,
            'natural-contrast-bg-color-off': !this.props.you.data.voice_call_waiting,
        });

        switch (this.state.properAction) {
            case ChatFooterProperButtonTypes.SEND:
                return (
                    <div className="natural-primary-bg-color natural-quick-middle action">
                        <button type="submit" className="natural-button natural-deco">
                            <span className="icon icon-send-white"></span>
                        </button>
                    </div>
                );

            case ChatFooterProperButtonTypes.CALL:
            default:
                return (
                    <div className={ className } onClick={ this.handleOpenOptionCall }>
                        <div className="natural-deco">
                            <span className="icon icon-call-sm"></span>
                        </div>
                    </div>
                );
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        AuthService.checkToken().then(() => {
            const contentRaw = this.getRawMessage();
            if (contentRaw) {
                this.props.sendMessage({
                    type: MessageTypes.PP,
                    to: this.props.you.id,
                    content: contentRaw,
                    templateId: this.props.templateId,
                });
                this.setState({
                    properAction: ChatFooterProperButtonTypes.CALL,
                })
                this.props.onClickCallFooter();
                this.props.changeChatMessageInput({ message: '' });
                this.contentRef.current.innerHTML = '';
            }
        });
    }

    render() {
        const rootClass = classNames({
            'natural-chat-footer-input-root': true,
            'input-box': true,
        });

        const inputClass = classNames({
            'chat-editor__input': true,
            has_scroll: this.state.hasScroll,
            'fix-hide-text-safari': this.state.toggleFixHideText,
        });

        const voiceCallWaiting = this.props.you.data.voice_call_waiting;
        const videoCallWaiting = this.props.you.data.video_call_waiting;
        return (
            <form className={ rootClass } onSubmit={ this.handleSubmit }>
                { !this.state.showOptionCall ? (
                    <div className="natural-wrapper">
                        <div
                            ref={ this.contentRef }
                            id="chat-editor__input"
                            className={ inputClass }
                            contentEditable
                            onBlur={ (e) => this.handleChangeContent('blur', e) }
                            onKeyUp={ (e) => this.handleChangeContent('keyUp', e) }
                            onMouseUp={ (e) => this.handleChangeContent('mouseUp', e) }
                            onFocus={ (e) => this.handleChangeContent('focus', e) }
                            onInput={ (e) => this.handleChangeContent('change', e) }
                            dangerouslySetInnerHTML={ { __html: EmojiHelper.convertToView(this.props.message) } }
                            placeholder="メッセージを入力"
                        />
                        <div className="proper-action">
                            <div className="natural-wrapper natural-quick-middle">
                                { this.getProperButton() }
                            </div>
                        </div>
                    </div>
                ) : (
                    <ul className="chat-call-button">
                        <li>
                            <UtilityButtonCall
                                className="btn-voice-call"
                                type={PrimaryUtilityTypes.VOICE}
                                friend={this.props.you.data}
                            >
                                <i className={ `icon ${ voiceCallWaiting ? 'icon-phone-call-blue' : 'icon-phone-call-ol icon-off' }` }/>
                                <span>音声通話<br/>{ voiceCallWaiting ? '' : 'リクエスト' }</span>
                            </UtilityButtonCall>
                        </li>
                        <li>
                            <UtilityButtonCall
                                className="btn-video-call"
                                type={PrimaryUtilityTypes.VIDEO}
                                friend={this.props.you.data}
                            >
                                <i className={ `icon ${ videoCallWaiting ? 'icon-video-call-blue' : 'icon-video-call-ol icon-off' }` }/>
                                <span>ビデオ通話<br/>{ videoCallWaiting ? '' : 'リクエスト' }</span>
                            </UtilityButtonCall>
                        </li>
                        <li>
                            <button type="button" onClick={ this.handleCloseOptionCall } className="btn-close">
                                <i className="icon icon-x"/>
                            </button>
                        </li>
                    </ul>
                ) }
            </form>
        );
    }
}

ConnectedChatFooterInput.propTypes = {
    changeChatMessageInput: PropTypes.func.isRequired,
    openFooterKeyboard: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
    emoji: PropTypes.any,
    templateId: PropTypes.any,
    you: PropTypes.object.isRequired,
    onClickCallFooter: PropTypes.func.isRequired,
    sendMessage: PropTypes.func.isRequired,
};

const ChatFooterInput = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatFooterInput);

export default ChatFooterInput;
