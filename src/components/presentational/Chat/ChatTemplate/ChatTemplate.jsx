import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FullScreenModal from '../../../FullScreenModal';
import Template from '../../../Template';
import { closeTemplate, selectedTemplate } from '../../../../actions/Chat';

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
    closeTemplate: payload => dispatch(closeTemplate(payload)),
    selectedTemplate: payload => dispatch(selectedTemplate(payload)),
});

class ChatTemplate extends Component {

    handleSelectTemplate = (template) => {
        this.props.closeTemplate();
        this.props.selectedTemplate(template);
    }

    render() {
        return (
            <FullScreenModal className="chat-template" isOpen={this.props.isOpen} >
                { this.props.isOpen ? <Template
                    backFunction={this.props.closeTemplate}
                    onUse={this.handleSelectTemplate}
                    isChat
                /> : null }
            </FullScreenModal>
        );
    }
}

ChatTemplate.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    closeTemplate: PropTypes.func.isRequired,
    selectedTemplate: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatTemplate);
