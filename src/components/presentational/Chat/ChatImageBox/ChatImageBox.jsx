import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import FullScreenModal from '../../../FullScreenModal';
import Image from '../../../Image';
import {
    closeImageBox,
    openGalleryHistory,
} from "../../../../actions/Chat";
import './ChatImageBox.scss';
import ButtonDownload from '../../../ButtonDownload';

const mapDispatchToProps = (dispatch) => ({
    closeImageBox: (payload) => dispatch(closeImageBox(payload)),
    openGalleryHistory: payload => dispatch(openGalleryHistory(payload)),
});

const mapStateToProps = (state) => ({
    isOpenGalleryHistory: state.chatControl.isOpenGalleryHistory,
});

class ConnectedChatImageBox extends Component {


    handleOnCloseClick = ()=> {
        this.props.closeImageBox();
    }

    handleOpenAllImagePublic(){
        this.props.openGalleryHistory();
    }

    render() {
        return (
            <FullScreenModal
                className='chat-image-detail'
                title=''
                isOpen
                onClose={this.handleOnCloseClick}
                titleChildren={ <button className="btn-view-all" type="button" onClick={() => this.handleOpenAllImagePublic()}>全て見る</button>}
            >
                <div className="backstage-profile">
                    <Image imageId={this.props.message.file.file_id || this.props.message.imageFileId} alt={this.props.message.file.file_id} imgKind={2} isBackground/>
                </div>

                <footer className="k-footer">
                    <div className="natural-download-container btn">
                        <ButtonDownload imageId={this.props.message.file.file_id} />
                    </div>
                </footer>
            </FullScreenModal>
        );
    }
}

ConnectedChatImageBox.propTypes = {
    message: PropTypes.object.isRequired,
    closeImageBox: PropTypes.func.isRequired,
    openGalleryHistory: PropTypes.func.isRequired,
}

const ChatImageBox = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatImageBox);

export default ChatImageBox;
