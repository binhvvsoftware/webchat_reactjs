import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import './index.scss';
import EmojiPicker from '../../../Emoji/picker';
import {
    selectedEmoji,
    closeSelectedSticker,
    openSelectedSticker,
} from '../../../../actions/Chat';
import {
    sendMessage,
} from '../../../../actions/socket';
import { MessageTypes } from '../../../../constants/types';

const mapStateToProps = (state) => ({
    you: state.chatControl.you,
    sticker: state.chatControl.sticker,
});

const mapDispatchToProps = (dispatch) => ({
    selectedEmoji: payload => dispatch(selectedEmoji(payload)),
    closeSelectedSticker: payload => dispatch(closeSelectedSticker(payload)),
    openSelectedSticker: payload => dispatch(openSelectedSticker(payload)),
    sendMessage: payload => dispatch(sendMessage(payload)),
});

class ChatFooterEmoji extends Component {
    handleSelectedEmoji = (emoji) => {
        this.props.selectedEmoji({ emoji: Object.assign({ time: new Date().getTime() }, emoji) });
    };

    handleSelectedSticker = (sticker) => {
        this.props.openSelectedSticker({ sticker });
    };

    handleCloseSelectedSticker = () => {
        this.props.closeSelectedSticker({ sticker: null });
    };

    handleSendSticker = (event) => {
        event.preventDefault();
        if (this.props.sticker) {
            this.props.sendMessage({
                type: MessageTypes.STK,
                to: this.props.you.id,
                sticker: {
                    sticker_id: this.props.sticker.sticker_id,
                },
            })
            this.props.openSelectedSticker({ sticker: null });
            this.props.onClickCallFooter();
        }
    };

    render() {
        const { active, sticker } = this.props;
        const rootId = 'natural-chat-footer-emoji-menu';
        const rootClass = classNames({
            'natural-wrapper': true,
            'natural-wrapper-v': true,
            'natural-row': true,
            'natural-chat-footer-emoji-menu-root': true,
            'is_active': active,
        });
        return (
            <div id={ rootId } className={ rootClass }>
                <div className="natural-wrapper natural-wrapper-v">
                    {
                        !!sticker &&
                        <div className="editor-picker__sticker">
                            <span className="editor-picker__sticker-close" onClick={ this.handleCloseSelectedSticker }>
                                <span className="icon icon-x"/>
                            </span>
                            <img src={ sticker.imgUrl } alt="" onClick={ this.handleSendSticker }/>
                        </div>

                    }
                    <EmojiPicker
                        onSelectEmoji={ this.handleSelectedEmoji }
                        onSelectSticker={ this.handleSelectedSticker }
                    />
                </div>
            </div>
        );
    }
}

ChatFooterEmoji.propTypes = {
    sticker: PropTypes.any,
    you: PropTypes.object.isRequired,
    active: PropTypes.bool.isRequired,
    selectedEmoji: PropTypes.func.isRequired,
    openSelectedSticker: PropTypes.func.isRequired,
    closeSelectedSticker: PropTypes.func.isRequired,
    onClickCallFooter: PropTypes.func,
    sendMessage: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatFooterEmoji);
