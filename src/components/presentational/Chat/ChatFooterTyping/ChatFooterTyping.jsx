import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import classNames from 'classnames';
import { TYPING } from '../../../../constants/messages';
import './ChatFooterTyping.scss';

const mapStateToProps = (state) => ({
    isTyping: state.chatControl.isTyping,
});

class ConnectedChatFooterTyping extends Component {
    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-chat-footer-typing-root': true,
            'natural-chat-footer-typing-active': this.props.isTyping === true,
        });
        return (
            <div className={rootClass}>
                <div className="natural-wrapper">
                    <span>{ TYPING }</span>
                    <span className="typing-dot">.</span>
                    <span className="typing-dot">.</span>
                    <span className="typing-dot">.</span>
                </div>
            </div>
        );
    }
}

ConnectedChatFooterTyping.propTypes = {
    isTyping: PropTypes.bool.isRequired,
};

const ChatFooterTyping = connect(mapStateToProps)(ConnectedChatFooterTyping);

export default ChatFooterTyping;
