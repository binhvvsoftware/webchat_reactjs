import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { clickChatHeaderMenuButton, clickChatBackButton } from "../../../../actions/Chat";
import { closeChatConversation, closeSidebarMenu, openChatConversation, openSidebarMenu } from '../../../../actions';
import Badge from '../../../Badge';
import ChatHeaderDock from "../ChatHeaderDock";
import ChatGiftPool from "../ChatGiftPool";
import history  from '../../../../utils/history';
import {
    toggleDialogMemo,
} from '../../../../reducers/control/profile'
import './ChatHeader.scss';

const mapStateToProps = state => ({
    isShowButtonBack: state.chatControl.isShowButtonBack,
    title: state.chatControl.you.data.user_name ? state.chatControl.you.data.user_name : '',
    you: state.chatControl.you,
    isGiftPoolOpened: state.chatControl.isGiftPoolOpened,
    attentionNumbers: state.chatControl.clientAttentionNumbers,
});

const mapDispatchToProps = (dispatch) => ({
    clickChatHeaderMenuButton: payload => dispatch(clickChatHeaderMenuButton(payload)),
    clickChatBackButton: payload => dispatch(clickChatBackButton(payload)),
    toggleDialogMemo: () => dispatch(toggleDialogMemo()),
    openSidebarMenu: (payload) => dispatch(openSidebarMenu(payload)),
    closeSidebarMenu: (payload) => dispatch(closeSidebarMenu(payload)),
    openChatConversation: (payload) => dispatch(openChatConversation(payload)),
    closeChatConversation: (payload) => dispatch(closeChatConversation(payload)),
});

class ConnectedChatHeader extends Component {
    constructor(props) {
        super(props);

        this.handleOnBackButtonClick = this.handleOnBackButtonClick.bind(this);
        this.handleOnHeaderMenuButtonClick = this.handleOnHeaderMenuButtonClick.bind(this);
        this.getGiftPool = this.getGiftPool.bind(this);
    }

    handleOnBackButtonClick() {
        this.props.clickChatBackButton();
        this.props.closeSidebarMenu();
        history.push(`/profile/${this.props.you.id}`);
    }

    handleToggleSidebarLeft = (open) => {
        this.props.closeChatConversation();
        if (open) {
            this.props.openSidebarMenu();
        } else {
            this.props.closeSidebarMenu();
        }
    };

    handleOnHeaderMenuButtonClick() {
        this.props.clickChatHeaderMenuButton();
    }

    handleOnHeaderMemoButtonClick = () => {
        this.props.toggleDialogMemo();
    };

    handleOnHeaderChatConversationButtonClick = () => {
        this.props.openChatConversation();
    };

    getGiftPool() {
        return this.props.isGiftPoolOpened ? <ChatGiftPool active={ this.props.isGiftPoolOpened }/> : null;
    }

    render() {
        const rootClass = classNames({
            'natural': true,
            'natural-primary-header': true,
            'natural-chat-header-root': true,
        });

        return (
            <header className={ rootClass }>
                <div className="natural natural-wrapper">
                    <div className="natural natural-action-container natural-quick-middle">
                        {
                            this.props.isShowButtonBack  ? (
                                <div className="natural natural-quick-middle natural-action-btn natural-action-back" onClick={ this.handleOnBackButtonClick }>
                                    <div className="natural natural-deco">
                                        <span className="icon icon-arrow-left"></span>
                                    </div>
                                </div>
                            ) : (
                                <span className="natural natural-quick-middle natural-action-btn natural-action-menu" onClick={ () => this.handleToggleSidebarLeft(true) }>
                                    <div className="natural natural-deco">
                                        <span className="icon icon-menu-left"></span>
                                        <Badge className="nav-bar__badge" number={ this.props.attentionNumbers.notifyNumber } />
                                    </div>
                                </span>
                            )
                        }
                    </div>
                    <div className="natural natural-quick-middle natural-title" title={this.props.title}>
                        <Link to={`/profile/${this.props.you.id}`}>
                            <span className="natural-text-wrapper">
                                { this.props.title }
                            </span>
                        </Link>
                    </div>
                    <div className="natural-actions">
                        <div className="natural-wrapper natural-quick-middle">
                            <div className="natural-action-item" onClick={ this.handleOnHeaderMenuButtonClick }>
                                <div className="natural-deco">
                                    <span className="icon icon-etc-menu"></span>
                                </div>
                            </div>
                            <div className="natural-action-item" onClick={ this.handleOnHeaderMemoButtonClick }>
                                <div className="natural-deco">
                                    <span className="icon icon-memo"></span>
                                </div>
                            </div>
                            <div className="natural-action-item" onClick={ this.handleOnHeaderChatConversationButtonClick }>
                                <div className="natural-deco">
                                    <Badge className="icon-unread" number={this.props.attentionNumbers.unread} />
                                    <span className="icon icon-message-sm"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ChatHeaderDock/>
                { this.getGiftPool() }
            </header>
        );
    }
}

ConnectedChatHeader.propTypes = {
    isShowButtonBack: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    you: PropTypes.object.isRequired,
    clickChatHeaderMenuButton: PropTypes.func.isRequired,
    clickChatBackButton: PropTypes.func.isRequired,
    toggleDialogMemo: PropTypes.func.isRequired,
    isGiftPoolOpened: PropTypes.bool.isRequired,
    attentionNumbers: PropTypes.object.isRequired,
    openSidebarMenu: PropTypes.func.isRequired,
    closeSidebarMenu: PropTypes.func.isRequired,
    closeChatConversation: PropTypes.func.isRequired,
    openChatConversation: PropTypes.func.isRequired,
};

const ChatHeader = connect(mapStateToProps, mapDispatchToProps)(ConnectedChatHeader);

export default ChatHeader;
