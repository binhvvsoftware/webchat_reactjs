import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from 'react-motion-drawer';
import { connect } from 'react-redux';
import Footer from '../Footer';
import SidebarLeft from './SidebarLeft';
import SidebarRight from './SidebarRight';
import Badge from '../Badge';
import Header from '../Header';
import UserService from '../../services/user';
import './index.scss';
import { openWebView } from '../../actions/webview';
import { updateAttentionNumbers } from '../../actions/Chat';
import {
    openChatConversation,
    closeChatConversation,
    openSidebarMenu,
    closeSidebarMenu,
} from '../../actions';

class MainLayout extends Component {
    componentWillMount() {
        UserService.getAttentionNumber().then(response => {
            this.props.updateAttentionNumbers({ attentionNumbers: response.data });
        });
    }

    handleToggleSidebarLeft = (open) => {
        this.props.closeChatConversation();
        if (open) {
            this.props.openSidebarMenu();
        } else {
            this.props.closeSidebarMenu();
        }
    };

    handleToggleSidebarRight = (open) => {
        this.props.closeSidebarMenu();
        if (open) {
            this.props.openChatConversation();
        } else {
            this.props.closeChatConversation();
        }
    };

    render() {
        const {
            isOpenWebView,
            hasSidebarLeft,
            hasSidebarRight,
            isOpenSidebarMenu,
            isOpenChatConversation,
        } = this.props;

        const drawerPropsLeft = {
            handleWidth: 15,
            className: 'sidebar is_left',
            overlayClassName: 'sidebar__overlay',
            open: isOpenSidebarMenu,
            noTouchClose: false,
            onChange: open => this.handleToggleSidebarLeft(open),
        };

        const drawerPropsRight = {
            handleWidth: 15,
            className: 'sidebar is_right',
            overlayClassName: 'sidebar__overlay',
            open: isOpenChatConversation,
            noTouchClose: false,
            right: true,
            onChange: open => this.handleToggleSidebarRight(open),
        };

        return (
            <div className={`main-layout sidebar__parent ${this.props.className}`}>
                {
                    !isOpenWebView && hasSidebarLeft && <Drawer { ...drawerPropsLeft } >
                        <SidebarLeft />
                    </Drawer>
                }

                {
                    hasSidebarRight && <Drawer { ...drawerPropsRight } >
                        <SidebarRight />
                    </Drawer>
                }

                <Header title={this.props.title} backUrl={this.props.backFunction}>
                    {
                        hasSidebarLeft && <span className="nav-bar__icon nav-bar__icon--left" onClick={ () => this.handleToggleSidebarLeft(true) }>
                            <span className="icon icon-menu-left" />
                            <Badge className="nav-bar__badge" number={this.props.attentionNumbers.notifyNumber} />
                        </span>
                    }

                    {
                        hasSidebarRight && <span className="nav-bar__icon nav-bar__icon--right is_message" onClick={ () => this.handleToggleSidebarRight(true) }>
                            <span className="icon icon-message" />
                            <Badge className="nav-bar__badge" number={this.props.attentionNumbers.unread} />
                        </span>
                    }
                    {this.props.contentHeader}
                </Header>
                { this.props.subHeader }
                <main className="k-main">
                    { this.props.children }
                </main>
                { this.props.hasFooter ? <Footer/> : null }
            </div>
        );
    }
}

MainLayout.propTypes = {
    attentionNumbers: PropTypes.object,
    title: PropTypes.string,
    className: PropTypes.string,
    hasBackButton: PropTypes.bool,
    hasFooter: PropTypes.bool,
    backFunction: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.object,
        PropTypes.string,
    ]),
    hasSidebarLeft: PropTypes.bool,
    hasSidebarRight: PropTypes.bool,
    subHeader: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
    ]),
    contentHeader: PropTypes.any,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
    ]),
    openWebView: PropTypes.func.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    isOpenWebView: PropTypes.bool.isRequired,
    isOpenSidebarMenu: PropTypes.bool.isRequired,
    openSidebarMenu: PropTypes.func.isRequired,
    closeSidebarMenu: PropTypes.func.isRequired,
    isOpenChatConversation: PropTypes.bool.isRequired,
    closeChatConversation: PropTypes.func.isRequired,
    openChatConversation: PropTypes.func.isRequired,
};

MainLayout.defaultProps = {
    className: '',
    hasFooter: true,
    hasBackButton: false,
    backFunction: null,
    hasSidebarLeft: true,
    hasSidebarRight: true,
    contentHeader: null,
    subHeader: [],
    children: [],
    title: null,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
    isOpenWebView: state.webView.isOpenWebView,
    isOpenSidebarMenu: state.main.isOpenSidebarMenu,
    isOpenChatConversation: state.main.isOpenChatConversation,
});

const mapDispatchToProps = (dispatch) => ({
    openWebView: (payload) => dispatch(openWebView(payload)),
    updateAttentionNumbers: (payload) => dispatch(updateAttentionNumbers(payload)),
    openSidebarMenu: (payload) => dispatch(openSidebarMenu(payload)),
    closeSidebarMenu: (payload) => dispatch(closeSidebarMenu(payload)),
    openChatConversation: (payload) => dispatch(openChatConversation(payload)),
    closeChatConversation: (payload) => dispatch(closeChatConversation(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);
