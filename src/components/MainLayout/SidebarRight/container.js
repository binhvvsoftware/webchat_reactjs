import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native-web';
import ChatService from '../../../services/chat';
import UserService from '../../../services/user';

import Badge from '../../Badge';
import Message from './message';
import {
    updateConversation,
    updateAttentionNumbers,
} from '../../../actions/Chat';

import { updateListAccountSystem } from '../../../actions';

class SidebarRight extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            isDelete: false,
            isLoading: false,
        }
    }

    componentWillMount() {
        this.handleCallApi();

        if (this.props.attentionNumbers !== undefined) {
            UserService.getAttentionNumber().then(response => {
                this.props.updateAttentionNumbers({ attentionNumbers: response.data });
            });
        }

        if (this.props.listUserSystem.length === 0) {
            UserService.getAllAccountSystem().then(response => {
                this.props.updateListAccountSystem({ data: response.data.using_application });
            });
        }
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    handleCallApi = (filter = 'all') => {
        this.setState({
            isLoading: true,
        }, () => {
            ChatService.getConversations(filter)
                .then(response => {
                    this.props.updateConversation({
                        data: response.data,
                        filter,
                    });

                    if (!this.isUnmounted) {
                        this.setState({
                            isLoading: false,
                        });
                    }
                });
        });
    }

    handleDeleteMessage() {
        if (this.props.listConversation.length === 0) {
            return;
        }

        const { isDelete } = this.state;

        this.setState({
            isDelete : !isDelete,
        })
    }

    render() {
        const { isDelete } = this.state;
        const { listConversation } = this.props;
        const messages = listConversation.map((message, key) => <Message message={message} showButtonDelete={isDelete} key={message.frd_id + message.unread_num +  key} />);

        return (
            <div className="sidebar-right">
                { isDelete ?
                    <div className="sidebar-options">
                        <button className="sidebar-option__btn" type="button" onClick={ () => this.handleDeleteMessage()}>
                            <span className="icon icon-chat-check" />
                            <span className="d-block">完了</span>
                        </button>
                    </div>
                    :
                    <div className="sidebar-options">
                        <button className="sidebar-option__btn" type="button" onClick={ () => this.handleCallApi()}>
                            <span className="icon icon-chat-c" />
                            <span className="d-block">すべて表示</span>
                        </button>
                        <button className="sidebar-option__btn" type="button" onClick={ () => this.handleCallApi('unread')}>
                            <span className="icon icon-message-1">
                                <Badge className="sidebar-option__icon-unread" number={this.props.attentionNumbers.unread } />
                            </span>
                            <span className="d-block">未読</span>
                        </button>
                        <button className="sidebar-option__btn" type="button" onClick={ () => this.handleDeleteMessage()}>
                            <span className={ `icon icon-message-2 ${listConversation.length === 0 ? 'none-display' : ''}` } />
                            <span className="d-block">選択解除</span>
                        </button>
                    </div>
                }

                <div className="list-messages" >
                    {
                        this.state.isLoading ? (
                            <div className="loading-box mt_20">
                                <ActivityIndicator animating color="black" size="large"/>
                            </div>
                        )
                            :
                            (
                                <div>
                                    <div className="text-center mt_10">
                                        { listConversation.length > 0 ? '' : '表示出来る項目がありません。' }
                                    </div>
                                    { messages }
                                </div>
                            )
                    }
                </div>
            </div>
        );
    }
}


SidebarRight.propTypes = {
    attentionNumbers: PropTypes.object.isRequired,
    listConversation: PropTypes.array.isRequired,
    updateConversation: PropTypes.func.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    updateListAccountSystem: PropTypes.func.isRequired,
    listUserSystem: PropTypes.array.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
    updateConversation: payload => dispatch(updateConversation(payload)),
    updateAttentionNumbers: payload => dispatch(updateAttentionNumbers(payload)),
    updateListAccountSystem: payload => dispatch(updateListAccountSystem(payload)),
});


const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
    listConversation: state.chatControl.listConversation,
    listUserSystem: state.main.listUserSystem,
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarRight);

