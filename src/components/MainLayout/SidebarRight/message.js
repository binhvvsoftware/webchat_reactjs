import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ChatService from '../../../services/chat';
import Badge from '../../Badge';
import Image from '../../Image/container';
import { EmojiView } from '../../Emoji/index';
import { CANCEL } from '../../../constants/messages';
import { convertDurationTimeToHHMMSS, addTargetLink } from '../../../utils/helpers'
import history from '../../../utils/history'
import { closeChatConversation } from '../../../actions';

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDeleting: false,
            isDeleted: false,
            showButtonDelete: props.showButtonDelete,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            showButtonDelete: nextProps.showButtonDelete,
        })
    }

    componentDidUpdate() {
        const tags = document.querySelectorAll('.list-messages__info .list-messages__msg a');

        tags.forEach((item) => {
            if (!item.getAttribute('data-add-event')) {
                item.addEventListener('click', e => e.preventDefault(), true);
                item.setAttribute('data-add-event', 'true');
            }
        });
    }

    handleDeleteOneMes(event) {
        event.preventDefault();

        this.setState({
            isDeleting: true,
        }, () => {
            ChatService.deleteConversation([this.props.message.frd_id]).then(() => {
                this.setState({
                    isDeleted: true,
                });
            });
        })
    }

    handleClickLink = () => {
        this.props.closeChatConversation();
        history.push(`/chat/${this.props.message.frd_id}`)
    }

    render() {
        const { showButtonDelete, isDeleting, isDeleted } = this.state;
        const { message } = this.props;
        const rootClass = classNames({
            'list-messages__item': true,
            'is_sent': message.isOwner,
            'is_received': !message.isOwner,
            'is_deleting': isDeleting,
        });

        return !isDeleted && (
            <div onClick={this.handleClickLink} className={ rootClass }>
                <div className="list-messages__avatar">
                    <Badge className="list-messages__icon-unread" number={message.unread_num} />
                    <Image isAvatar className="list-messages__avatar-img" imageId={message.ava_id} />
                </div>
                <div className="list-messages__info">
                    <div className="list-messages__name">
                        {message.frd_name}
                        <span className="list-messages__time">{message.sentTime}</span>
                    </div>
                    {(() => {
                        if ((message.msg_type === "EVOICE") || (message.msg_type === "EVIDEO")) {
                            if (parseInt(message.last_msg.split("|")[2], 10) === 0) {
                                return <div className="list-messages__msg">{CANCEL}</div>
                            }
                            return <div className="list-messages__msg">{`通話時間${convertDurationTimeToHHMMSS(parseInt(message.last_msg.split("|")[2], 10))}`}</div>
                        }

                        if (this.props.listUserSystem.indexOf(message.frd_id) !== -1) {
                            return <div className="list-messages__msg" dangerouslySetInnerHTML={ { __html: addTargetLink(message.textMessage) } }/>
                        }

                        return <EmojiView className="list-messages__msg">{message.textMessage}</EmojiView>
                    })()}
                </div>
                {showButtonDelete ?
                    <div className="list-messages__delete" onClick={(e) => this.handleDeleteOneMes(e)}>
                        <span className="icon icon-trash-white" />
                    </div> : ''
                }
            </div>
        );
    }
}

Message.propTypes = {
    message: PropTypes.object,
    showButtonDelete: PropTypes.bool,
    listUserSystem: PropTypes.array.isRequired,
    closeChatConversation: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
    listUserSystem: state.main.listUserSystem,
});

const mapDispatchToProps = (dispatch) => ({
    closeChatConversation: () => dispatch(closeChatConversation()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Message);
