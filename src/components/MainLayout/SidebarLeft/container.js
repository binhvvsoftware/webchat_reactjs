import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'; 
import Badge from '../../Badge';
import SettingService from '../../../services/settings';
import history  from '../../../utils/history';
import { buyPoint, supportPage, howToUseFemale, freePoint } from '../../../services/webview';
import { numberFormat } from '../../../utils/helpers';
import { updateAttentionNumbers } from '../../../actions/Chat';
import {
    updateFreePage,
    closeChatConversation,
    closeSidebarMenu,
} from '../../../actions';
import { closeWebView, openWebView } from '../../../actions/webview';

class SidebarLeft extends Component {
    isUnmounted = false;

    componentDidMount() {
        const user = this.props.me.data;
        if (!this.isUnmounted && user.user_name) {
            this.props.updateAttentionNumbers({
                backstage: user.backstage,
                buzz: user.buzz,
                checkout: user.checkout,
                favorite: user.favorite,
                follow: user.follow,
                myFootprint: user.myFootprint,
                newCheckout: user.newCheckout,
                newFavorite: user.newFavorite,
                like: user.like,
                notifyNumber: user.notifyNumber,
                onCallNow: user.onCallNow,
                unread: user.unread,
            });
        }

        if (!Array.isArray(this.props.freePage)) {
            SettingService.getFreePage().then(response => {
                this.props.updateFreePage(response);
            });
        }
    }

    componentWillUnmount() {
        this.isUnmounted = false;
    }

    handleOpenWebView = (url, title) => {
        this.props.closeChatConversation();
        this.props.closeSidebarMenu();
        this.props.openWebView({ url, title });
    };

    handleRedirectLink = (path) => {
        this.props.closeChatConversation();
        this.props.closeSidebarMenu();
        this.props.closeWebView();
        history.push(path);
    };

    render() {
        const user = this.props.me.data;
        const freePage = this.props.freePage || [];
        let previewImage = {};
        if (user.avatar_url) {
            previewImage = { backgroundImage: `url(${user.avatar_url})` };
        }

        const extPage = freePage.map((item) => (
            <div className="sidebar-left__item cursor-pointer" key={ item.id } onClick={ () => this.handleOpenWebView(item.url) }>
                <span className="sidebar-left__icon">
                    <span className="icon icon-free-page" />
                </span>
                <span>{ item.title }</span>
            </div>
        ));

        return (
            <div className="sidebar-left">
                <div className="sidebar-left__header">
                    <div className="top-bar top-icons">
                        <div onClick={() => this.handleRedirectLink('/settings')}>
                            <span className="setting-icon icon icon-setting"></span>
                        </div>
                        <div onClick={() => this.handleRedirectLink('/notification')}>
                            <span className="noti-icon">
                                <span className="icon icon-notice-white"></span>
                                <Badge className="badge-left-menu" number={this.props.attentionNumbers.notifyNumber} />
                            </span>
                        </div>
                    </div>
                    <div className="user-account">
                        <div className={ previewImage.backgroundImage ? 'preview-avatar is-circle' : 'preview-avatar' } style={ previewImage }  onClick={() => this.handleRedirectLink('/my-page')} />

                        <p>{ user.user_name }</p>
                        <span className="points">
                            <span className="icon icon-point-gold"></span>
                            { numberFormat(user.point) }pts
                        </span>
                    </div>
                </div>
                <div className="category">
                    <div className="sidebar-left__row">
                        <div className="sidebar-left__item">
                            <div onClick={() => this.handleRedirectLink('/my-page')}>
                                <span className="sidebar-left__icon"><span className="icon icon-user"></span></span>
                            </div>
                            <span>マイベージ</span>
                        </div>
                        <div className="sidebar-left__item" onClick={ () => this.handleOpenWebView(buyPoint()) }>
                            <span className="sidebar-left__icon"><span className="icon icon-purchase-points"></span></span>
                            <span>ポイント購入</span>
                        </div>
                        <div className="sidebar-left__item">
                            <div onClick={() => this.handleRedirectLink('/ranking')}>
                                <span className="sidebar-left__icon"><span className="icon icon-level"></span></span>
                            </div>
                            <span>ランキング</span>
                        </div>
                    </div>
                    <div>
                        <div className="sidebar-left__row">
                            <div className="sidebar-left__item">
                                <div onClick={() => this.handleRedirectLink('/timeline')}>
                                    <span className="sidebar-left__icon"><span className="icon icon-timeline"></span></span>
                                </div>
                                <span>タイムライン</span>
                            </div>
                            <div className="sidebar-left__item" onClick={ () => this.handleOpenWebView(freePoint()) }>
                                <span className="sidebar-left__icon"><span className="icon icon-free"></span></span>
                                <span>無料ポイント</span>
                            </div>
                            <div className="sidebar-left__item" onClick={ () => this.handleOpenWebView(howToUseFemale()) }>
                                <span className="sidebar-left__icon"><span className="icon icon-profile"></span></span>
                                <span>使い万</span>
                            </div>
                            <div className="sidebar-left__item" onClick={ () => this.handleOpenWebView(supportPage()) }>
                                <span className="sidebar-left__icon"><span className="icon icon-info"></span></span>
                                <span>ヘルプ</span>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar-left__row">
                        { extPage }
                    </div>
                </div>
                <div onClick={() => this.handleRedirectLink('/home')}>
                    <button type="button" className="btn-custom btn-active btn-back-top ">TOP</button>
                </div>
            </div>
        );
    }
}

SidebarLeft.propTypes = {
    me: PropTypes.object.isRequired,
    updateAttentionNumbers: PropTypes.func.isRequired,
    updateFreePage: PropTypes.func.isRequired,
    attentionNumbers: PropTypes.object.isRequired,
    freePage: PropTypes.any,
    closeChatConversation: PropTypes.func.isRequired,
    closeSidebarMenu: PropTypes.func.isRequired,
    openWebView: PropTypes.func.isRequired,
    closeWebView: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    attentionNumbers: state.chatControl.clientAttentionNumbers,
    me: state.chatControl.me,
    freePage: state.main.freePage,
});

const mapDispatchToProps = (dispatch) => ({
    updateAttentionNumbers: (payload) => dispatch(updateAttentionNumbers(payload)),
    updateFreePage: (payload) => dispatch(updateFreePage(payload)),
    closeChatConversation: (payload) => dispatch(closeChatConversation(payload)),
    closeSidebarMenu: (payload) => dispatch(closeSidebarMenu(payload)),
    openWebView: (payload) => dispatch(openWebView(payload)),
    closeWebView: () => dispatch(closeWebView()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarLeft);
