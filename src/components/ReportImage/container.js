import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import Dialog from '@material-ui/core/Dialog';
import { REPORT_IMAGE } from '../../constants/report_image_constant';
import ProfileService from '../../services/profile';
import { REPORT_PICTURE_TITE, REPORT_PICTURE_MESSAGE} from '../../constants/messages';
import './index.scss';

class ReportImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statusPopupReportImage: true,
        };
    }

    closeDialogReportImage(){
        this.setState({
            statusPopupReportImage: false,
        });
        this.props.onCloseReportImage();
    }

    handleReportImage(event, typeReport) {
        event.preventDefault();
        const { myImage } = this.props;
        this.setState({
            statusPopupReportImage: false,
        });

        if (typeReport !== -1) {
            ProfileService.reportImage(myImage, typeReport)
                .then(() => {
                    Popup.create({
                        title : REPORT_PICTURE_TITE,
                        content: REPORT_PICTURE_MESSAGE,
                        buttons: {
                            right: [{
                                text: 'OK',
                                className: 'block_ok',
                                action: () => {
                                    Popup.close();
                                    this.props.onCloseReportImage(true);
                                    this.props.onCloseDialogImage();
                                },
                            }],
                        },
                    });
                });
        } else {
            this.props.onCloseReportImage(false);
        }
    }

    render() {
        return (
            <Dialog
                open={this.state.statusPopupReportImage}
                onClose={() => this.closeDialogReportImage()}
                className="report-wrapper">
                <div className="profile-report">
                    {REPORT_IMAGE.map((ireport, i) =>
                        <p className='profile-report-list' key={i} onClick = {(e) => this.handleReportImage(e, ireport.value)}>{ireport.label}</p>
                    )}
                </div>
            </Dialog>
        );
    }
}

ReportImage.propTypes = {
    onCloseReportImage: PropTypes.func,
    onCloseDialogImage: PropTypes.func,
    myImage: PropTypes.string,
};

export default ReportImage;
