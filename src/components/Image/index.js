import loadable from '@loadable/component';
import './index.scss';

export default loadable(() => import('./container'));
