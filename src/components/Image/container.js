import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MediaService from '../../services/media';
import avatarDefault from '../../statics/images/avatar-default.png';
import avatarSystem from '../../statics/images/avatar-system.png';

class Image extends Component {
    isUnmounted = false;

    constructor(props) {
        super(props);
        this.state = {
            url: MediaService.getURL(this.props.imageId, this.props.imgKind, 'load_img_for_web'),
            thumbnail: MediaService.getURL(this.props.imageId, 1, 'load_img_for_web'),
        };
        this.imgElm = React.createRef();
    }

    componentWillUnmount = () => {
        this.isUnmounted = true;
    };

    handleError = () => {
        if (this.isUnmounted) {
            return;
        }

        this.setState({
            url: avatarDefault,
        });
    };

    render() {
        const { alt, isAvatar, isSystem } = this.props;
        let { className } = this.props;
        const { url, thumbnail } = this.state;
        const backgroundUrl = url === thumbnail ? `url(${url})` : `url(${url}), url(${thumbnail})`;
        if (isAvatar) {
            const style = {};
            if (this.props.imageId) {
                style.backgroundImage = `${backgroundUrl}, url(${avatarDefault})`;

                if (isSystem) {
                    style.backgroundImage = `${backgroundUrl}, url(${avatarSystem})`;
                }
            } else if (isSystem) {
                style.backgroundImage = `url(${avatarSystem})`;
            } else {
                style.backgroundImage = `url(${avatarDefault})`;
            }

            className = `${className} img-avatar`;

            return (<div ref={ this.imgElm } className={ className } data-url={ this.state.url } style={ style }/>);
        }

        const tag = <img
            ref={ this.imgElm }
            src={ this.state.url }
            className={ className }
            alt={ alt }
            onError={ this.handleError }/>;

        const background = <div
            ref={ this.imgElm }
            className={ `img-bg ${className}` }
            style={ { backgroundImage: `${backgroundUrl}, url(${avatarDefault})` } }/>;

        return this.props.isBackground ? background : tag;
    }
}

Image.propTypes = {
    isBackground: PropTypes.bool,
    isAvatar: PropTypes.bool,
    imageId: PropTypes.string,
    className: PropTypes.string,
    imgKind: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    alt: PropTypes.string,
    isSystem: PropTypes.bool,
};

Image.defaultProps = {
    isAvatar: false,
    isBackground: false,
    className: '',
    imgKind: 1,
    alt: '',
    isSystem: PropTypes.false,
};

export default Image;
