import { put, takeEvery, call } from 'redux-saga/effects';
import Popup from 'react-popup';
import { API as forgetPasswordApi } from '../services/forgetPassword';
import { forgetPasswordSuccess, forgetPasswordFailure } from '../actions/forgetPassword';
import { EMAIL } from '../constants/storage_constant';
import history from '../utils/history';
import {
    EMAIL_IS_NOT_REGITTER_MESSAGE,
    ERROR_SYSTEM,
    FORGET_PASSWORD_SUCCESS_MESSAGE,
    EMAIL_NOT_FOUND_MESSAGE_CODE_LOGIN,
    VERIFY_CHANGE_PASSWORD_MESSAGE,
} from '../constants/messages';
import {
    FORGET_PASWORD_REQUEST,
} from '../constants/action_constant';
import storage from '../utils/storage';

export function* watchForgetPasswordAsync() {
    yield takeEvery(FORGET_PASWORD_REQUEST, forgetPasswordAsync);
}

export function* forgetPasswordAsync(action) {
    try {
        const data = yield call(forgetPasswordApi.forgetPassword, action.email);
        switch (data.code) {
            case 0:
                storage.set(EMAIL, action.email);
                yield put(forgetPasswordSuccess(FORGET_PASSWORD_SUCCESS_MESSAGE));
                Popup.create({
                    content: VERIFY_CHANGE_PASSWORD_MESSAGE,
                    buttons: {
                        left: [{
                            text: 'はい',
                            className: 'ok',
                            action: () => {
                                history.push('/reset-password', { email: action.email });
                                Popup.close();
                            },
                        }],
                    },
                });
                return;
            case 10:
                yield put(forgetPasswordFailure(EMAIL_IS_NOT_REGITTER_MESSAGE));
                break;
            default:
                yield put(forgetPasswordFailure(ERROR_SYSTEM));
        }
    } catch (err) {
        if (err.code === 10) {
            Popup.alert(EMAIL_NOT_FOUND_MESSAGE_CODE_LOGIN)
            yield put(forgetPasswordFailure(EMAIL_IS_NOT_REGITTER_MESSAGE));
        }
    }
}