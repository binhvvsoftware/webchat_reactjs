import { fork } from 'redux-saga/effects';
import {watchLoginAsync} from './login';
import {watchSocketAsync} from './socket';
import {watchForgetPasswordAsync} from './forgetPassword';

export default function* root() {
    yield fork(watchLoginAsync);
    yield fork(watchForgetPasswordAsync);
    yield fork(watchSocketAsync);
}
