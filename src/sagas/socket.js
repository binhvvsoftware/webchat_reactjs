/* eslint-disable  */
import { put, takeEvery, call, all } from 'redux-saga/effects';
import moment from 'moment';
import { submitChatMessage, pushChatPool } from '../actions/Chat';
import storage from '../utils/storage';
import { connectSocket } from '../utils/socket-io';
import { SEND_MESSAGE } from '../constants/action_constant';
import { MessageTypes } from '../constants/types';
import MessageSocket from '../entities/MessageSocket';


const createMessageSocket = (message) => {
    let me = storage.getUserInfo();
    const sender = me.user_id;
    const now = moment().utc().format('X');
    let serve = {
        sender,
        msg_id: message.msg_id || `${sender}&${message.to}&${now}`,
        msg_type: message.type || message.msg_type,
        mds: message.mds,
        value: message.content,
        receiver: message.to,
        original_time: message.original_time || now,
        destination_time: 0,
        using_new_app: 0,
        server_time: 0,
        socket_io: 1,
    };

    switch (serve.msg_type) {
        case MessageTypes.PP:
            if (message.templateId) {
                serve.msg_type = MessageTypes.TEMPLATE;
                serve.template = {
                    template_id: message.templateId,
                };
            }
            break;
        case MessageTypes.STK:
            serve.sticker = message.sticker;
            break;

        case MessageTypes.GIFT:
            serve = Object.assign({}, serve, {
                gift: message.gift,
            });
            break;

        case MessageTypes.FILE:
            serve = Object.assign({}, serve, {
                file: {
                    msg_content_type: message.file.msg_content_type,
                    file_id: message.file.file_id,
                    msg_id: message.file.msg_id,
                    meta: message.file.meta,
                },
            });

            break;

        case MessageTypes.PRC:
            serve = Object.assign({}, serve, {
                user_status: {
                    buddy_presentation: serve.value.length ? 'wt' : 'sw',
                },
            });
            delete serve.value;
            break;

        case MessageTypes.MDS:
            serve = Object.assign({}, serve, message);
            delete serve.value;
            break;
    }

    return new MessageSocket(serve);
};

const send = (message) => new Promise((resolve => {
    const socket = connectSocket();
    if (!socket.isAuthenticate && message.msg_type !== MessageTypes.AUTH) {
        window.queueSocet = window.queueSocet || [];
        window.queueSocet.push(message);
        console.log('Wait connect socket', message);
        resolve();
    }

    if (message.file && message.file.meta) {
        delete message.file.meta;
    }
    delete message.data;
    socket.emit('newMessage', message);
    console.log('%cEMIT Socket', 'font-weight: bold; color: green', message);
    resolve();
}));

export function* socketSendMessage(action) {
    let message = createMessageSocket(action.message);
    console.log(message);
    if (message.msg_type === MessageTypes.FILE && !message.file.file_id) {
        yield all([
            put(submitChatMessage({ message, isFile: true })),
        ]);
    }
    if (
        message.msg_type === MessageTypes.PP ||
        message.msg_type === MessageTypes.STK ||
        message.msg_type === MessageTypes.GIFT ||
        message.msg_type === MessageTypes.CALLREQ ||
        message.msg_type === MessageTypes.EVOICE ||
        message.msg_type === MessageTypes.EVIDEO ||
        message.msg_type === MessageTypes.PHOTO ||
        message.msg_type === MessageTypes.SVOICE ||
        message.msg_type === MessageTypes.SVIDEO ||
        message.msg_type === MessageTypes.TEMPLATE
    ) {
        yield put(pushChatPool({ message }));
    }

    message = createMessageSocket(action.message);
    yield call(send, message);

}

export function* watchSocketAsync() {
    yield takeEvery(SEND_MESSAGE, socketSendMessage);
}
