import { put, takeEvery, call } from 'redux-saga/effects';
import Popup from 'react-popup';
import AuthService from '../services/authentication';
import { loginSuccess, loginFailure } from '../actions/login';
import history from '../utils/history';
import { LOGIN_REQUEST } from '../constants/action_constant';
import {
    CHANGE_PASSWORD_EMAIL_ERROR_MESSAGE,
    ALERT_TITLE_VALIDATE_EMAIL_LOGIN,
    WOMEN_CAN_NOT_LOGIN,
    EMAIL_NOT_FOUND_MESSAGE_CODE_LOGIN,
} from '../constants/messages';
import { SUCCESS, HTTP_WOMEN_CAN_NOT_LOGIN, EMAIL_NOT_FOUND } from '../constants/response_status_constant';

const executeLoginFailure = (response) => {
    if (response && response.code === HTTP_WOMEN_CAN_NOT_LOGIN) {
        return Popup.alert(WOMEN_CAN_NOT_LOGIN);
    }

    if (response && response.code === EMAIL_NOT_FOUND) {
        return Popup.alert(EMAIL_NOT_FOUND_MESSAGE_CODE_LOGIN, ALERT_TITLE_VALIDATE_EMAIL_LOGIN);
    }

    return Popup.alert(CHANGE_PASSWORD_EMAIL_ERROR_MESSAGE, ALERT_TITLE_VALIDATE_EMAIL_LOGIN);
};

export function* loginAsync(action) {
    try {
        const response = yield call(AuthService.loginEmail, action.email, action.password);

        if (response && response.code === SUCCESS) {
            yield put(loginSuccess(response));
            history.push('/home');
        } else {
            executeLoginFailure(response);
            yield put(loginFailure(response));
        }
    } catch (errors) {
        executeLoginFailure(errors);
        yield put(loginFailure(errors));
    }
}

export function* watchLoginAsync() {
    yield takeEvery(LOGIN_REQUEST, loginAsync);
}
