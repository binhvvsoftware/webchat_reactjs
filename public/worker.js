// Flag for enabling cache in production
var doCache = true;
var CACHE_NAME = 'pwa-app-cache';

// Delete old caches
self.addEventListener('activate', event => {
    const currentCacheList = [CACHE_NAME];
    event.waitUntil(
        caches.keys()
            .then(keyList =>
                Promise.all(keyList.map(key => {
                    if (!currentCacheList.includes(key)) {
                        return caches.delete(key);
                    }
                })),
            ),
    );
});


// This triggers when user starts the app
self.addEventListener('install', function(event) {
    if (doCache) {
        event.waitUntil(
            caches.open(CACHE_NAME)
                .then(function(cache) {
                    fetch(self.location.origin + '/static-manifest.json')
                            .then(
                            function(response) {
                                if (response.status !== 200) {
                                    console.log('Looks like there was a problem. Status Code: ' +
                                        response.status);
                                    return;
                                }

                                // Examine the text in the response
                                response.json().then(function(data) {
                                    cache.addAll(data);
                                }).catch(error => {
                                    console.log(error);
                                });
                            },
                        )
                        .catch(function(err) {
                            console.log('Fetch Error :-S', err);
                        });

                    fetch(self.location.origin + '/asset-manifest.json')
                        .then(
                            function(response) {
                                if (response.status !== 200) {
                                    console.log('Looks like there was a problem. Status Code: ' +
                                        response.status);
                                    return;
                                }

                                // Examine the text in the response
                                response.json().then(function(data) {
                                    const urlsToCache = [
                                        '/',
                                        '/favicon.ico',
                                        '/logo.png',
                                    ].concat(Object.values(data));

                                    cache.addAll(urlsToCache);
                                });
                            },
                        )
                        .catch(function(err) {
                            console.log('Fetch Error :-S', err);
                        });
                }),
        );
    }
});

// Here we intercept request and serve up the matching files
self.addEventListener('fetch', function(event) {
    if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
        return;
    }

    if (doCache) {
        event.respondWith(
            caches.match(event.request).then(function(response) {
                return response || fetch(event.request);
            }),
        );
    }
});
