#!/usr/bin/env bash

# Get path
PROJECT_PATH="$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")"

cd $PROJECT_PATH

options=("staging" "production")

echo "Please choose an option build:"
select option in "${options[@]}"; do
        [ -n "${option}" ] && break
done

option="development"

echo "Build: ${option}"
echo "ENV:"
echo $'\n'
printf $'====================================================================\n'
cat "$PROJECT_PATH/src/constants/env/$option.js"
printf $'====================================================================\n'

echo -n "Do you want to build?(y/n)? "
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then
     git co .
    git pull origin develop
    yarn install
    eval "yarn $option"


    echo -n "Enter document root: Example: /var/www/kyuun"
    read DOCUMENT_ROOT

    now=DATE=`date '+%Y-%m-%d %H:%M:%S'`
    echo "<div style='display: none'>Deploy at $now</div>" >> "$PROJECT_PATH/build/index.html"
    echo "Build source success $now"
    rm -rf $DOCUMENT_ROOT
    cp -rf "$PROJECT_PATH/build" $DOCUMENT_ROOT
else
    echo "Exit build"
fi
